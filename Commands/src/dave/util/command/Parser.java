package dave.util.command;

public interface Parser<T>
{
	public abstract T parse(String content) throws ParseException;
}
