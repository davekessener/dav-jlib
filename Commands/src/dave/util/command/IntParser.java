package dave.util.command;

public class IntParser implements Parser<Long>
{
	@Override
	public Long parse(String content) throws ParseException
	{
		try
		{
			if(content.startsWith("0x") || content.startsWith("0X"))
			{
				return Long.parseLong(content.substring(2), 16);
			}
			else if(content.startsWith("$"))
			{
				return Long.parseLong(content.substring(1), 16);
			}
			else if(content.startsWith("0b") || content.startsWith("0B"))
			{
				return Long.parseLong(content.substring(2), 2);
			}
			else if(content.startsWith("0"))
			{
				return Long.parseLong(content, 8);
			}
			else
			{
				return Long.parseLong(content);
			}
		}
		catch(NumberFormatException e)
		{
			e.printStackTrace();
			throw new ParseException("Failed to parse " + content + " (" + e.getMessage() + ")");
		}
	}
}
