package dave.util.command;

public interface Terminal
{
	public abstract void printf(String s, Object ... o);
}
