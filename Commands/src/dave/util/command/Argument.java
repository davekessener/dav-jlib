package dave.util.command;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Stream;

import dave.util.Identifiable;

public class Argument implements Identifiable
{
	private final String mID;
	private final String mHelp;
	private final Type mType;
	private final Parser<?> mParser;
	private final List<Constraint> mConstraints;
	private final Object mDefault;
	
	public Argument(String id, String h, Type t, Object d, Parser<?> p)
	{
		mID = id;
		mHelp = h;
		mType = t;
		mParser = p;
		mConstraints = new ArrayList<>();
		mDefault = d;
		
		if(mID == null)
			throw new NullPointerException("ID missing!");
		if(mType == null)
			throw new NullPointerException("Type missing!");
		if(mParser == null)
			throw new NullPointerException("Parser missing!");
	}
	
	@Override
	public String getID( )
	{
		return mID;
	}
	
	public Type getType( ) { return mType; }
	public String getHelp( ) { return mHelp; }
	public boolean hasDefault( ) { return mDefault != null; }
	
	public Object getDefault( )
	{
		if(mDefault == null)
			throw new NullPointerException();
		
		return mDefault;
	}
	
	public Argument addConstraint(String name, Predicate<?> f)
	{
		mConstraints.add(new Constraint(name, f));
		
		return this;
	}
	
	public Object parse(String content) throws ParseException
	{
		Class<?> c = mType.getArgumentClass();
		Object o = mParser.parse(content);
		
		for(Constraint f : mConstraints)
		{
			if(!test(f.filter, c, o))
				throw new ParseException("Violated Constraint " + f.name + "!");
		}
		
		return o;
	}
	
	@SuppressWarnings("unchecked")
	private <T> boolean test(Predicate<?> f, Class<T> t, Object o)
	{
		return ((Predicate<T>) f).test((T) o);
	}
	
	public static Type findType(Class<?> c)
	{
		return Stream.of(Type.values()).filter(t -> t.getArgumentClass() == c).findFirst().get();
	}
	
	private static class Constraint
	{
		public final String name;
		public final Predicate<?> filter;
		
		public Constraint(String name, Predicate<?> filter)
		{
			this.name = name;
			this.filter = filter;
		}
	}
	
	public static enum Type
	{
		INT(int.class),
		LONG(long.class),
		DOUBLE(double.class),
		STRING(String.class);
		
		public Class<?> getArgumentClass( ) { return mClass; }
		
		private final Class<?> mClass;
		
		private Type(Class<?> c)
		{
			mClass = c;
		}
	}
	
	public static Argument build(String id, Type type) { return (new Builder()).setID(id).setType(type).build(); }
	public static Argument build(String id, Type type, Object d) { return (new Builder()).setID(id).setType(type).setDefault(d).build(); }

	public static class Builder
	{
		private String id, help;
		private Type type;
		private Parser<?> parser;
		private Object def;
		
		public Builder setID(String id) { this.id = id; return this; }
		public Builder setHelp(String help) { this.help = help; return this; }
		public Builder setType(Type type) { this.type = type; return this; }
		public Builder setParser(Parser<?> parser) { this.parser = parser; return this; }
		public Builder setDefault(Object def) { this.def = def; return this; }
		
		public Argument build()
		{
			if(parser == null)
				parser = Parsers.get(type.getArgumentClass());
			
			return new Argument(id, help, type, def, parser);
		}
	}
}
