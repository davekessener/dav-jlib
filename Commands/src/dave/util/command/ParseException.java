package dave.util.command;

public class ParseException extends Exception
{
	private static final long serialVersionUID = 475138439982832935L;
	
	public ParseException(String msg)
	{
		super(msg);
	}
	
	public ParseException(Exception e)
	{
		super(e);
	}
}
