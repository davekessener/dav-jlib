package dave.util.command;

import java.util.HashMap;
import java.util.Map;

public final class Parsers
{
	private static final Map<Class<?>, Parser<?>> PARSERS = new HashMap<>();
	
	static
	{
		PARSERS.put(int.class, v -> (new IntParser()).parse(v).intValue());
//		PARSERS.put(int.class, new TransformingParser<Long, Integer>(new IntParser(), v -> v.intValue()));
		PARSERS.put(long.class, new IntParser());
		PARSERS.put(double.class, new DoubleParser());
		PARSERS.put(String.class, new StringParser());
	}
	
	public static Parser<?> get(Class<?> c)
	{
		Parser<?> p = PARSERS.get(c);
		
		if(p == null)
			throw new IllegalArgumentException("Unknown parser for " + c.getName());
		
		return p;
	}
	
	private Parsers( ) { }
}
