package dave.util.command;

import java.util.function.Function;

public class TransformingParser<S, T> implements Parser<T>
{
	private final Parser<S> mSource;
	private final Function<S, T> mCallback;
	
	public TransformingParser(Parser<S> s, Function<S, T> f)
	{
		mSource = s;
		mCallback = f;
	}

	@Override
	public T parse(String content) throws ParseException
	{
		return mCallback.apply(mSource.parse(content));
	}
}
