package dave.util.command;

import java.util.ArrayList;
import java.util.List;

import dave.util.command.Argument.Type;

public class CommandBuilder<T>
{
	private final String mName, mHelp;
	private final T mRemote;
	private final List<Argument> mArguments;
	private boolean mNeedsDefault;
	
	public CommandBuilder(String name, String help, T o)
	{
		mName = name.toLowerCase();
		mHelp = help;
		mRemote = o;
		mArguments = new ArrayList<>();
		mNeedsDefault = false;
	}
	
	public ComplexCommand<T> build( )
	{
		return new ComplexCommand<>(mName, mHelp, mRemote, mArguments.toArray(new Argument[mArguments.size()]));
	}
	
	public CommandBuilder<T> add(Argument a)
	{
		if(mNeedsDefault)
		{
			if(!a.hasDefault())
				throw new IllegalArgumentException(a.getID() + " needs a default value!");
		}
		else
		{
			if(a.hasDefault())
				mNeedsDefault = true;
		}
		
		mArguments.add(a);
		
		return this;
	}
	
	public CommandBuilder<T> add(String id, Type t)
	{
		return add(Argument.build(id, t));
	}
	
	public CommandBuilder<T> add(String id, Type t, Object d)
	{
		return add(Argument.build(id, t, d));
	}
}
