package dave.util.command;

public class DoubleParser implements Parser<Double>
{
	@Override
	public Double parse(String content) throws ParseException
	{
		try
		{
			return Double.parseDouble(content);
		}
		catch(NumberFormatException e)
		{
			throw new ParseException(e);
		}
	}
}
