package dave.util.command;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.function.Consumer;

import dave.util.Streamable;

public class Tokenizer implements Streamable<String>
{
	private final List<String> mParts;
	
	public Tokenizer(String line) throws ParseException
	{
		mParts = new ArrayList<>();
		
		Processor p = new Processor(mParts::add);
		
		for(int i = 0 ; i < line.length() ; ++i)
		{
			p.process(line.charAt(i));
		}
		
		p.process(-1);
	}

	@Override
	public Iterator<String> iterator( )
	{
		return mParts.iterator();
	}
	
	private static enum State
	{
		INIT,
		ID,
		STR,
		ESC
	}

	private static class Processor
	{
		private final Consumer<String> mCallback;
		private State mState;
		private StringBuilder mBuf;
		
		public Processor(Consumer<String> f)
		{
			mCallback = f;
			mState = State.INIT;
			mBuf = new StringBuilder();
		}
		
		public void process(int c) throws ParseException
		{
			switch(mState)
			{
				case INIT:
					handleInit(c);
					break;
					
				case ID:
					handleID(c);
					break;
					
				case STR:
					handleStr(c);
					break;
					
				case ESC:
					handleEsc(c);
					break;
			}
		}
		
		private void handleInit(int c) throws ParseException
		{
			if(c == '"')
			{
				mState = State.STR;
			}
			else if(!isWS(c))
			{
				push(c);
				mState = State.ID;
			}
		}
		
		private void handleID(int c) throws ParseException
		{
			if(isWS(c))
			{
				next();
			}
			else
			{
				push(c);
			}
		}
		
		private void handleStr(int c) throws ParseException
		{
			if(c == '\\')
			{
				mState = State.ESC;
			}
			else if(c == -1)
			{
				throw new ParseException("Unterminated string \"" + mBuf.toString());
			}
			else
			{
				if(c == '"')
				{
					next();
				}
				else
				{
					push(c);
				}
			}
		}
		
		private void handleEsc(int c) throws ParseException
		{
			switch(c)
			{
			case '"':
				push('"');
				break;
				
			case 'n':
				push('\n');
				break;
				
			case 't':
				push('\t');
				break;
				
			case '\\':
				push('\\');
				break;
				
			default:
				throw new ParseException("Invalid escape sequence: '\\" + (char) c + "'!");
			}
			
			mState = State.STR;
		}
		
		private void next( )
		{
			mCallback.accept(mBuf.toString());
			mBuf = new StringBuilder();
			mState = State.INIT;
		}
		
		private void push(int c) { mBuf.append((char) c); }
		
		private static boolean isWS(int c) { return c == -1 || c == ' ' || c == '\t' || c == '\n'; }
	};
}
