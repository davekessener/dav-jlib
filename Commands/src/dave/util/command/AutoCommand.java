package dave.util.command;

import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.stream.Stream;

import dave.util.SevereException;

public class AutoCommand<T> extends ComplexCommand<T>
{
	public AutoCommand(T f) { this(f, analyse(f)); }
	
	private AutoCommand(T f, Result r)
	{
		super(r.name, r.help, f, r.method, r.arguments);
	}
	
	private static <T> Result analyse(T f)
	{
		String name = null;
		String help = null;
		Method m = null;
		Argument[] args = null;
		
		try
		{
			Class<?> c = f.getClass();
			
			m = Stream.of(c.getInterfaces())
				.flatMap(k -> Stream.of(k.getMethods()))
				.filter(e -> e.getAnnotation(Help.class) != null)
				.findFirst().get();
			
			m.setAccessible(true);
			
			name = m.getName();
			help = m.getAnnotation(Help.class).value();
			args = new Argument[m.getParameterCount()];
			
			for(int i = 0 ; i < args.length ; ++i)
			{
				Class<?> a = m.getParameterTypes()[i];
				
				String n = a.getName();
				String h = getHelp(a);
				Argument.Type t = Argument.findType(a);
				Parser<?> p = Parsers.get(t.getArgumentClass());
				Object d = getDefault(p, m.getParameters()[i]);
				
				args[i] = (new Argument.Builder())
					.setID(n)
					.setHelp(h)
					.setType(t)
					.setParser(p)
					.setDefault(d)
					.build();
			}
		}
		catch(ParseException e)
		{
			throw new SevereException(e);
		}
		
		return new Result(name, help, m, args);
	}

	private static String getHelp(Class<?> c)
	{
		Help h = c.getAnnotation(Help.class);
		
		return (h == null ? null : h.value());
	}

	private static Object getDefault(Parser<?> p, Parameter a) throws ParseException
	{
		Default h = a.getAnnotation(Default.class);
		
		return (h == null ? null : p.parse(h.value()));
	}
	
	private static class Result
	{
		public final String name;
		public final String help;
		public final Method method;
		public final Argument[] arguments;
		
		public Result(String name, String help, Method method, Argument[] arguments)
		{
			this.name = name;
			this.help = help;
			this.method = method;
			this.arguments = arguments;
		}
	}
}
