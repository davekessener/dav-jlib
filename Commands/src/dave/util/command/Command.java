package dave.util.command;

import dave.util.Identifiable;

public interface Command extends Identifiable
{
	public abstract String help( );
	public abstract void execute(String[] args) throws ParseException;
}
