package dave.util.command;

import dave.util.SevereException;
import dave.util.Utils.ThrowingRunnable;

public class SimpleCommand implements Command
{
	private final String mID, mHelp;
	private final ThrowingRunnable mCallback;

	public SimpleCommand(String name, String help, ThrowingRunnable f)
	{
		mID = name;
		mHelp = help;
		mCallback = f;
	}
	
	@Override
	public String getID()
	{
		return mID;
	}

	@Override
	public String help()
	{
		return mHelp;
	}

	@Override
	public void execute(String[] args) throws ParseException
	{
		try
		{
			mCallback.run();
		}
		catch(Exception e)
		{
			throw new SevereException(e);
		}
	}
}