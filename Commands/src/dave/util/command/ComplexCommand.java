package dave.util.command;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import dave.util.SevereException;

public class ComplexCommand<T> implements Command
{
	private final String mName, mHelp;
	private final T mRemote;
	private final Method mCallback;
	private final Argument[] mArguments;
	
	public ComplexCommand(String n, String h, T o, Argument ... a) { this(n, h, o, extractMethod("execute", o, a), a); }
	public ComplexCommand(String name, String help, T o, Method m, Argument ... a)
	{
		mName = name;
		mHelp = help;
		mRemote = o;
		mArguments = a;
		mCallback = m;
	}
	
	public static <T> Method extractMethod(String name, T o, Argument ... a)
	{

		Class<?>[] c = new Class[a.length];
		for(int i = 0 ; i < c.length ; ++i)
		{
			c[i] = a[i].getType().getArgumentClass();
		}
		
		try
		{
			Method m = o.getClass().getMethod(name, c);
			
			m.setAccessible(true);
			
			return m;
		}
		catch(NoSuchMethodException | SecurityException e)
		{
			throw new SevereException(e);
		}
	}
	
	@Override
	public String getID( )
	{
		return mName;
	}
	
	@Override
	public String help( )
	{
		return mHelp;
	}

	@Override
	public void execute(String[] args) throws ParseException
	{
		Object[] o = new Object[mArguments.length];
		
		for(int i = 0 ; i < o.length ; ++i) try
		{
			Argument a = mArguments[i];
			
			if(i >= args.length)
			{
				if(!a.hasDefault())
					throw new ParseException("Missing argument " + a.getID() + "!");
				
				o[i] = a.getDefault();
			}
			else
			{
				o[i] = a.parse(args[i]);
			}
		}
		catch(ParseException e)
		{
			throw new ParseException("In argument " + i + " (" + (i >= args.length ? "n/a" : args[i]) + "): " + e.getMessage());
		}
		
		try
		{
			mCallback.invoke(mRemote, o);
		}
		catch(IllegalAccessException | IllegalArgumentException | InvocationTargetException e)
		{
			throw new ParseException(e.getMessage());
		}
	}
}
