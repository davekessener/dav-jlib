package dave.util.command;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import dave.util.Producer;
import dave.util.SevereException;
import dave.util.Streamable;
import dave.util.Utils;
import dave.util.log.Logger;
import dave.util.log.Severity;

public class CmdEngine implements Streamable<Command>, Terminal
{
	private final List<Command> mCommands;
	private final OutputStream mOut;
	private String mLast;
	private boolean mPrompt;
	
	public CmdEngine( ) { this(System.out); }
	public CmdEngine(OutputStream os)
	{
		mCommands = new ArrayList<>();
		mLast = null;
		mOut = os;
		mPrompt = true;
	}
	
	public CmdEngine add(Command cmd)
	{
		mCommands.add(cmd);
		
		return this;
	}
	
	@Override
	public void printf(String s, Object ... o)
	{
		String msg = String.format(s, o);
		
		if(!mPrompt)
		{
			msg = "\n" + msg;
		}
		
		byte[] buf = msg.getBytes();
		
		try
		{
			mOut.write(buf, 0, buf.length);
			mOut.flush();
		}
		catch(IOException e)
		{
			throw new SevereException(e);
		}
		
		mPrompt = true;
	}

	@Override
	public Iterator<Command> iterator( )
	{
		return mCommands.iterator();
	}
	
	public void run(String line) throws ParseException
	{
		List<String> parts = (new Tokenizer(line)).stream().collect(Collectors.toList());
		
		if(parts.isEmpty())
		{
			if(mLast != null)
			{
				run(mLast);
			}
		}
		else
		{
			mLast = line;
			
			String id = parts.remove(0).toLowerCase();
			
			for(Command cmd : mCommands)
			{
				if(cmd.getID().equals(id))
				{
					cmd.execute(parts.toArray(new String[parts.size()]));
					
					return;
				}
			}
			
			throw new ParseException("Unknown command '" + id + "'!");
		}
	}
	
	public void run(String prompt, InputStream in, Producer<Boolean> f)
	{
		PrintStream out = new PrintStream(mOut);
		
		for(BufferedReader input = new BufferedReader(new InputStreamReader(in)) ; f.produce() ;)
		{
			try
			{
				while(!input.ready() && f.produce())
				{
					if(mPrompt)
					{
						out.print(prompt);
						
						mPrompt = false;
					}
					
					Utils.sleep(100);
				}
				
				if(!f.produce())
					break;
				
				mPrompt = true;
				run(input.readLine());
			}
			catch(ParseException e)
			{
				out.println(e.getMessage());
				
				LOG.log(Severity.INFO, e.getMessage());
			}
			catch(Throwable e)
			{
				e.printStackTrace(out);
				
				LOG.log(Severity.WARNING, "Error while", e.getMessage());
			}
		}
	}
	
	private static final Logger LOG = Logger.get("cmd-engine");
}
