package dave.arguments;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Parser
{
	private final Set<Option> mOptions;
	
	public Parser(Option ... o) { this(Arrays.stream(o).collect(Collectors.toCollection(() -> new LinkedHashSet<>()))); }
	public Parser(Set<Option> opts)
	{
		mOptions = opts;
	}
	
	public Arguments parse(String[] args) throws ParseException
	{
		List<String> m = new ArrayList<>();
		Map<Option, String> a = new HashMap<>();
		List<Token> tokens = new ArrayList<>();
		String total = Arrays.stream(args).collect(Collectors.joining(" "));
		
		Arrays.stream(args).forEach(v -> {
			if(v.startsWith("--"))
			{
				Matcher mm = PTRN_FORMAL.matcher(v);
				
				if(mm.matches())
				{
					tokens.add(new Token(Type.LONG_A, mm.group(1)));
					tokens.add(new Token(Type.ARG, mm.group(2)));
				}
				else
				{
					tokens.add(new Token(Type.LONG_A, v.substring(2)));
				}
			}
			else if(v.length() > 1 && v.startsWith("-"))
			{
				v = v.substring(1);
				
				if(v.length() == 1)
				{
					tokens.add(new Token(Type.SHORT_A, v));
				}
				else for(int i = 0 ; i < v.length() ; ++i)
				{
					tokens.add(new Token(Type.SHORT, "" + v.charAt(i)));
				}
			}
			else
			{
				tokens.add(new Token(Type.ARG, v));
			}
		});
		
		Function<String, Option> findShort = id -> mOptions.stream().filter(o -> (o.hasShortcut() && o.getShortcut().equals(id))).findFirst().get();
		Function<String, Option> findLong = id -> mOptions.stream().filter(o -> o.getName().equals(id)).findFirst().get();
		
		Option expect = null;
		int idx = 0;
		Token t = null;
		for(Iterator<Token> i = tokens.iterator() ; i.hasNext() ; ++idx) try
		{
			t = i.next();
			
			if(expect != null)
			{
				if(t.type == Type.ARG)
				{
					a.put(expect, t.value);
					expect = null;
				}
				else
				{
					throw new ParseException("unexpected " + t.type, t.value, idx, total);
				}
			}
			else if(t.type == Type.ARG)
			{
				m.add(t.value);
			}
			else if(t.type == Type.SHORT)
			{
				a.put(findShort.apply(t.value), null);
			}
			else
			{
				expect = (t.type == Type.SHORT_A ? findShort : findLong).apply(t.value);
				
				if(!expect.hasValue())
				{
					a.put(expect, null);
					expect = null;
				}
			}
		}
		catch(NoSuchElementException e)
		{
			throw new ParseException("unknown option", t.value, idx, total);
		}
		
		mOptions.stream().filter(o -> o.hasDefault() && !a.containsKey(o)).forEach(o -> a.put(o, o.getDefault()));
		
		Option missing = mOptions.stream().filter(o -> o.isRequired() && !a.containsKey(o)).findAny().orElse(null);
		if(missing != null)
			throw new ParseException("argument '" + missing.getName() + "' is missing!", missing.getName(), -1, total);
		
		String main = m.isEmpty() ? "" : m.stream().collect(Collectors.joining(" ")).trim();
		
		return new Arguments(main.isEmpty() ? null : main, a);
	}
	
	public String help( ) { return toString(); }
	
	@Override
	public String toString()
	{
		StringBuilder main = new StringBuilder();
		StringBuilder help = new StringBuilder();
		
		int hlen = mOptions.stream()
			.filter(o -> o.hasHelp())
			.mapToInt(o -> o.getName().length() + 2 + (o.hasShortcut() ? 3 : 0))
			.max().orElse(-1);
		
		String hindent = (hlen == -1 ? null : repeat(" ", hlen + 4));
		
		for(Option o : mOptions)
		{
			StringBuilder part = new StringBuilder();
			
			if(o.hasShortcut())
			{
				part.append('-').append(o.getShortcut()).append(',');
			}
			
			part.append("--").append(o.getName());
			
			if(o.hasValue())
			{
				part.append("=");
				
				if(o.hasDefault())
				{
					part.append('"').append(o.getDefault()).append('"');
				}
				else
				{
					part.append('<').append(o.getName()).append('>');
				}
			}
			
			if(!o.isRequired() && !o.hasDefault())
			{
				part.insert(0, '[').append(']');
			}
			
			if(o.hasHelp())
			{
				StringBuilder sb = new StringBuilder();
				
				if(o.hasShortcut())
				{
					sb.append('-').append(o.getName()).append(',');
				}
				
				sb.append("--").append(o.getName());
				
				String h = sb.toString();
				
				help
					.append(h)
					.append(hindent.substring(h.length()))
					.append(o.getHelp())
					.append('\n');
			}
			
			main.append(part.toString()).append(' ');
		}
		
		main.append('\n').append(help);
		
		return main.toString();
	}
	
	private static String repeat(String p, int l)
	{
		if(l < 0)
		{
			return null;
		}
		else
		{
			StringBuilder sb = new StringBuilder();
			
			for(int i = 0 ; i < l ; ++i)
			{
				sb.append(p);
			}
			
			return sb.toString();
		}
	}
	
	private static final Pattern PTRN_FORMAL = Pattern.compile("--([a-z0-9_-]+)=(.+)");
	
	private static enum Type
	{
		SHORT,
		SHORT_A,
		LONG_A,
		ARG
	}
	
	private static class Token
	{
		public final Type type;
		public final String value;
		
		public Token(Type type, String value)
		{
			this.type = type;
			this.value = value;
		}
	}
}
