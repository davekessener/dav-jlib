package dave.arguments;

public class Option
{
	private final String mName;
	private final String mShort;
	private final boolean mForced, mHasValue;
	private final String mDefault;
	private final String mHelp;
	
	public Option(String s, String n, String h, boolean f, boolean v, String d)
	{
		mName = n;
		mShort = s;
		mForced = f;
		mHasValue = v;
		mDefault = d;
		mHelp = h;
		
		if(mName == null || mName.isEmpty())
			throw new IllegalArgumentException(mName);
		
		if(mShort != null && mShort.length() != 1)
			throw new IllegalArgumentException(mShort);
		
		if(mForced && mDefault != null)
			throw new IllegalArgumentException(mDefault);
		
		if(!mHasValue && mDefault != null)
			throw new IllegalArgumentException(mDefault);
	}
	
	public String getName( ) { return mName; }
	public String getShortcut( ) { return mShort; }
	public boolean hasShortcut( ) { return mShort != null; }
	public boolean isRequired( ) { return mForced; }
	public boolean hasValue( ) { return mHasValue; }
	public String getDefault( ) { return mDefault; }
	public boolean hasDefault( ) { return mDefault != null; }
	public boolean hasHelp( ) { return mHelp != null; }
	public String getHelp( ) { return mHelp; }
	
	public static class OptionBuilder
	{
		private final String mName, mHelp;
		private String mShort;
		private boolean mForced, mHasValue;
		private String mDefault;
		
		public OptionBuilder(String name) { this(name, null); }
		public OptionBuilder(String name, String help)
		{
			mName = name;
			mHelp = help;
			mShort = null;
			mForced = false;
			mHasValue = false;
			mDefault = null;
		}
		
		public Option build( )
		{
			return new Option(mShort, mName, mHelp, mForced, mHasValue, mDefault);
		}
		
		public OptionBuilder setShortcut(String s)
		{
			mShort = s;
			
			return this;
		}
		
		public OptionBuilder setForced(boolean f)
		{
			mForced = f;
			
			return this;
		}
		
		public OptionBuilder hasValue(boolean f)
		{
			mHasValue = f;
			
			return this;
		}
		
		public OptionBuilder setDefault(String d)
		{
			mHasValue = true;
			mDefault = d;
			
			return this;
		}
	}
}
