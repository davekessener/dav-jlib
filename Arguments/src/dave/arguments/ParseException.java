package dave.arguments;

public class ParseException extends Exception
{
	private static final long serialVersionUID = -8727058258332482034L;
	
	public ParseException(String e, String a, int i, String t)
	{
		super(String.format("Error: %s [in token '%s' @(%d) of '%s']", e, a, i, t));
	}
}
