package dave.arguments;

import java.util.Map;

public class Arguments
{
	private final String mMain;
	private final Map<Option, String> mArgs;
	
	public Arguments(String m, Map<Option, String> a)
	{
		mMain = m;
		mArgs = a;
	}
	
	public boolean hasMainArgument( )
	{
		return mMain != null;
	}
	
	public String getMainArgument( )
	{
		if(!hasMainArgument())
			throw new IllegalStateException();
		
		return mMain;
	}
	
	public boolean hasArgument(Option o)
	{
		return mArgs.containsKey(o);
	}
	
	public String getArgumentIfPresent(Option o)
	{
		String v = mArgs.get(o);
		
		if(v == null)
		{
			if(o.hasDefault())
			{
				v = o.getDefault();
			}
		}
		
		return v;
	}
	
	public String getArgument(Option o)
	{
		String v = getArgumentIfPresent(o);
		
		if(v == null)
		{
			throw new IllegalArgumentException(o.getName());
		}
		
		return v;
	}
}
