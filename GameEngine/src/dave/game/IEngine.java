package dave.game;

import dave.game.IScreen;

public interface IEngine
{
	public abstract IScreen getScreen( );
	public abstract void setKeyState(Key k, boolean pressed);
	public abstract boolean getKeyState(Key k);
	public abstract void tick( );
	public abstract void draw( );
	public abstract int score( );
	public abstract boolean isAlive( );
	public abstract boolean isIdle( );
}
