package dave.game;

public final class GameUtils
{
	public static float[] transform(IScreen s)
	{
		float[] raw = new float[s.getWidth() * s.getHeight()];
		
		for(int y = 0 ; y < s.getHeight() ; ++y)
		{
			for(int x = 0 ; x < s.getWidth() ; ++x)
			{
				raw[x + y * s.getWidth()] = s.getPixel(x, y) / 3.0f;
			}
		}
		
		return raw;
	}
	
	public static boolean decide(float v)
	{
		return v < 0;
	}
	
	private GameUtils( ) { }
}
