package dave.game;

public enum Key
{
	A,
	B,
	START,
	SELECT,
	UP,
	DOWN,
	LEFT,
	RIGHT
}
