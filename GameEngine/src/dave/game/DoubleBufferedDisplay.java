package dave.game;

public class DoubleBufferedDisplay implements IDisplay
{
	private final IDisplay[] mDisplays;
	private final int mWidth, mHeight;
	private int mIdx;
	
	public DoubleBufferedDisplay(IDisplay f, IDisplay s)
	{
		mWidth = f.getWidth();
		mHeight = f.getHeight();
		
		if(mWidth != s.getWidth() || mHeight != s.getHeight())
			throw new IllegalArgumentException();
		
		mDisplays = new IDisplay[] { f, s };
		mIdx = 0;
	}
	
	public void update( )
	{
		mIdx ^= 1;
		
		for(int y = 0 ; y < mHeight ; ++y)
		{
			for(int x = 0 ; x < mWidth ; ++x)
			{
				mDisplays[mIdx ^ 1].setPixel(x, y, mDisplays[mIdx].getPixel(x, y));
			}
		}
	}
	
	@Override
	public int getWidth()
	{
		return mWidth;
	}

	@Override
	public int getHeight()
	{
		return mHeight;
	}

	@Override
	public byte getPixel(int x, int y)
	{
		return mDisplays[mIdx].getPixel(x, y);
	}

	@Override
	public void setPixel(int x, int y, byte px)
	{
		mDisplays[mIdx ^ 1].setPixel(x, y, px);
	}
}
