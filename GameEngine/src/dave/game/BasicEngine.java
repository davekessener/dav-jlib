package dave.game;

import dave.game.BasicScreen;
import dave.game.DoubleBufferedDisplay;
import dave.game.IDisplay;
import dave.game.IScreen;

public abstract class BasicEngine implements IEngine
{
	private final DoubleBufferedDisplay mScreen;
	private final boolean[] mKeys;
	private int mScore;
	
	public BasicEngine(int w, int h)
	{
		mScreen = new DoubleBufferedDisplay(new BasicScreen(w, h), new BasicScreen(w, h));
		mKeys = new boolean[Key.values().length];
		
		for(int i = 0 ; i < mKeys.length ; ++i)
		{
			mKeys[i] = false;
		}
		
		for(int y = 0 ; y < mScreen.getHeight() ; ++y)
		{
			for(int x = 0 ; x < mScreen.getWidth() ; ++x)
			{
				mScreen.setPixel(x, y, (byte) 0);
			}
		}
	}
	
	protected void vsync( ) { mScreen.update(); }
	protected IDisplay display( ) { return mScreen; }
	protected void addScore(int i) { mScore += i; }

	@Override
	public IScreen getScreen()
	{
		return mScreen;
	}

	@Override
	public void setKeyState(Key k, boolean pressed)
	{
		mKeys[k.ordinal()] = pressed;
	}

	@Override
	public boolean getKeyState(Key k)
	{
		return mKeys[k.ordinal()];
	}

	@Override
	public int score()
	{
		return mScore;
	}
}
