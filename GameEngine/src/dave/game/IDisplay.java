package dave.game;

public interface IDisplay extends IScreen
{
	public abstract void setPixel(int x, int y, byte px);
}
