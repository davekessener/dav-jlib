package dave.game;

public class ObservableScreen implements IScreen
{
	private final IScreen mParent;
	private final byte[][] mData;
	
	public ObservableScreen(IScreen s)
	{
		int w = s.getWidth(), h = s.getHeight();
		
		mParent = s;
		mData = new byte[w][h];
		
		for(int y = 0 ; y < h ; ++y)
		{
			for(int x = 0; x < w ; ++x)
			{
				mData[x][y] = -1;
			}
		}
	}

	@Override
	public int getWidth()
	{
		return mData.length;
	}

	@Override
	public int getHeight()
	{
		return mData[0].length;
	}

	@Override
	public byte getPixel(int x, int y)
	{
		return mData[x][y];
	}
	
	public void update(PixelObserver cb)
	{
		for(int y = 0 ; y < getHeight() ; ++y)
		{
			for(int x = 0 ; x < getWidth() ; ++x)
			{
				byte px = mParent.getPixel(x, y);
				
				if(px != mData[x][y])
				{
					cb.onChange(x, y, mData[x][y] = px);
				}
			}
		}
	}
	
	public static interface PixelObserver
	{
		public abstract void onChange(int x, int y, byte px);
	}
}
