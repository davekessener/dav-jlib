package dave.game;

public interface IScreen
{
	public abstract int getWidth( );
	public abstract int getHeight( );
	public abstract byte getPixel(int x, int y);
}
