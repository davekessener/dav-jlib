package dave.game;

public class BasicScreen implements IDisplay
{
	private final byte[][] mData;
	
	public BasicScreen(int w, int h)
	{
		mData = new byte[w][h];
	}

	@Override
	public int getWidth()
	{
		return mData.length;
	}

	@Override
	public int getHeight()
	{
		return mData[0].length;
	}

	@Override
	public byte getPixel(int x, int y)
	{
		return mData[x][y];
	}

	@Override
	public void setPixel(int x, int y, byte px)
	{
		mData[x][y] = px;
	}
}
