package dave.game;

import java.util.Iterator;

public class Controller
{
	public static interface AI extends Iterable<boolean[]> { }
	
	private final Thread mThread;
	private final IEngine mEngine;
	private final int mPeriod;
	private final AI mAI;
	private boolean mRunning;
	
	public Controller(IEngine e) { this(e, 0, null); }
	public Controller(IEngine e, int p, AI ai)
	{
		mThread = new Thread(() -> update());
		mAI = ai;
		mEngine = e;
		mPeriod = p;
	}
	
	public void start( )
	{
		if(!mRunning)
		{
			mRunning = true;
			
			mThread.start();
		}
	}
	
	public void stop( )
	{
		if(mRunning)
		{
			mRunning = false;
			
			try
			{
				mThread.interrupt();
				mThread.join();
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
		}
	}
	
	public void step(float s)
	{
		if(mRunning)
			throw new IllegalStateException();
		
		mEngine.tick();
	}
	
	private void update( )
	{
		long lastTick = System.currentTimeMillis();
		long lastDraw = lastTick;
		Iterator<boolean[]> i = mAI == null ? null : mAI.iterator();
		
		while(mRunning)
		{
			long ts = System.currentTimeMillis();
			
			if(mPeriod == 0 || (ts - lastTick) >= mPeriod)
			{
				lastTick += mPeriod;
				mEngine.tick();
				
				if(mAI != null)
				{
					boolean[] btns = i.next();
					
					for(int j = 0 ; j < btns.length ; ++j)
					{
						mEngine.setKeyState(Key.values()[j], btns[j]);
					}
				}
			}
			
			if((ts - lastDraw) >= 16)
			{
				lastDraw = ts;
				mEngine.draw();
			}
			
			if(mPeriod > 0) try { Thread.sleep(2); } catch(InterruptedException e) { }
		}
	}
}
