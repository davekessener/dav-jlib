package dave.tcg.bus;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import dave.tcg.common.Entity;
import dave.tcg.common.Event;
import dave.tcg.common.Responder;
import dave.tcg.common.Response;

public class Bus
{
	private final List<Entity> mEntities;
	
	public Bus()
	{
		mEntities = new ArrayList<>();
	}
	
	public void register(Entity r)
	{
		mEntities.add(r);
	}
	
	public <T> T inquire(Entity src, T def, Event evt)
	{
		List<Responder<T>> responder = mEntities.stream()
			.filter(e -> e.alive())
			.map(e -> e.respond(src, def, evt))
			.filter(r -> r != null)
			.collect(Collectors.toList());
		
		List<Response<T>> responses = new ArrayList<>();
		
		for(int i = 0 ; i < responder.size() ;)
		{
			if(responder.get(i).process(evt, def, responses))
			{
				i = 0;
			}
			else
			{
				++i;
			}
		}
		
		T v = def;
		
		for(Response<T> r : responses)
		{
			if(r.enabled())
			{
				v = r.apply(v);
			}
		}
		
		return v;
	}
}
