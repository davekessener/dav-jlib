package dave.tcg.bus;

import java.util.ArrayList;
import java.util.List;

import dave.tcg.common.Entity;
import dave.tcg.common.Response;

public abstract class BaseResponse<T> implements Response<T>
{
	private final Entity mSource;
	private final List<Response<?>> mDisablers;
	
	protected BaseResponse(Entity src)
	{
		mSource = src;
		mDisablers = new ArrayList<>();
	}
	
	@Override
	public Entity source()
	{
		return mSource;
	}

	@Override
	public boolean enabled()
	{
		return !mDisablers.stream().anyMatch(r -> r.enabled());
	}
	
	@Override
	public void disable(Response<?> e)
	{
		mDisablers.add(e);
	}
}
