package dave.tcg.bus;

import java.util.List;

import dave.tcg.common.Entity;
import dave.tcg.common.Event;
import dave.tcg.common.Responder;
import dave.tcg.common.Response;

public abstract class BaseResponder<T> implements Responder<T>
{
	private final Entity mSource;
	
	protected BaseResponder(Entity src)
	{
		mSource = src;
	}
	
	protected boolean isResponseAlreadyPresent(List<Response<T>> l)
	{
		return l.stream().anyMatch(r -> r.source().getID().equals(mSource.getID()));
	}
	
	protected abstract Response<T> generateResponse(Event e, T df, List<Response<T>> r);
	
	@Override
	public Entity source()
	{
		return mSource;
	}
	
	@Override
	public boolean process(Event e, T df, List<Response<T>> r)
	{
		if(!isResponseAlreadyPresent(r))
		{
			r.add(generateResponse(e, df, r));
			
			return true;
		}
		
		return false;
	}
}
