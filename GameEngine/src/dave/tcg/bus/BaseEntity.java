package dave.tcg.bus;

import java.util.HashMap;
import java.util.Map;

import dave.tcg.common.Entity;
import dave.tcg.common.Event;
import dave.tcg.common.Responder;

public abstract class BaseEntity implements Entity
{
	private final String mID;
	private final Map<Event, ResponseGenerator<?>> mResponses;
	private boolean mAlive;
	
	protected BaseEntity(String id)
	{
		mID = id;
		mResponses = new HashMap<>();
		mAlive = true;
		
		registerResponder((Event) null, this::test);
	}
	
	private Responder<String> test(Entity src, String s, Event e) { return null; }
	
	public void kill() { mAlive = false; }
	
	protected <T> void registerResponder(Event e, ResponseGenerator<T> g) { mResponses.put(e, g); }

	@Override
	public String getID()
	{
		return mID;
	}

	@Override
	public boolean alive()
	{
		return mAlive;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> Responder<T> respond(Entity src, T def, Event e)
	{
		ResponseGenerator<T> cb = (ResponseGenerator<T>) mResponses.get(e);
		
		return (cb == null ? null : cb.create(src, def, e));
	}
	
	public static interface ResponseGenerator<T> { Responder<T> create(Entity src, T def, Event e); }
}
