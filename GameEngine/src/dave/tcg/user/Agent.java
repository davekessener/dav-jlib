package dave.tcg.user;

import dave.util.command.Command;
import dave.util.command.Engine;
import dave.util.command.SimpleCommand;

public abstract class Agent
{
	private final Engine mEngine;
	private boolean mRunning;
	
	public Agent()
	{
		mEngine = new Engine();
		mRunning = false;
		
		mEngine.add(new SimpleCommand("quit", "Ends the program", this::commandQuit));
	}
	
	protected void register(Command cmd) { mEngine.add(cmd); }
	
	public void run()
	{
		mRunning = true;
		
		mEngine.run("> ", System.in, () -> mRunning);
	}
	
	private void commandQuit()
	{
		mRunning = false;
	}
}
