package dave.tcg.user;

import dave.util.command.SimpleCommand;
import dave.util.log.Stdout;

public class TCGAgent extends Agent
{
	public TCGAgent()
	{
		register(new SimpleCommand("help", "Displays help", this::commandHelp));
	}
	
	@Override
	public void run()
	{
		Stdout.println("YATCG!");
		
		super.run();
	}
	
	private void commandHelp()
	{
		Stdout.println("TODO");
	}
}
