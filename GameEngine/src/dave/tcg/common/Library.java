package dave.tcg.common;

public final class Library
{
	public static final String TITLE = "YetAnotherTradingCardGame";
	public static final int VERSION = 1;
	
	private Library( ) { }
}
