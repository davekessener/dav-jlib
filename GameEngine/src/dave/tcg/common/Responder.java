package dave.tcg.common;

import java.util.List;

public interface Responder<T>
{
	public abstract Entity source( );
	public abstract boolean process(Event e, T df, List<Response<T>> r);
}
