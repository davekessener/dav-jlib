package dave.tcg.common;

public interface Response<T>
{
	public abstract Entity source( );
	public abstract boolean enabled( );
	public default void disable(Response<?> e) { throw new UnsupportedOperationException(); }
	
	public default T apply(T v) { return v; }
}
