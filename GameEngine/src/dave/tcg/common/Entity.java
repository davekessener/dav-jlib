package dave.tcg.common;

import dave.util.Identifiable;

public interface Entity extends Identifiable
{
	public abstract boolean alive( );
	public abstract <T> Responder<T> respond(Entity src, T def, Event e);
}
