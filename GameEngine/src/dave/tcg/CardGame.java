package dave.tcg;

import dave.tcg.common.Library;
import dave.tcg.user.Agent;
import dave.tcg.user.TCGAgent;
import dave.util.ShutdownService;
import dave.util.ShutdownService.Priority;
import dave.util.log.LogBase;
import dave.util.log.LogSink;
import dave.util.log.Logger;
import dave.util.log.Stdout;

public class CardGame
{
	public static void main(String[] args)
	{
		LogBase.INSTANCE.registerSink(e -> true, LogSink.build());
		
		LogBase.INSTANCE.start();
		
		ShutdownService.INSTANCE.register(Priority.LAST, LogBase.INSTANCE::stop);
		
		try
		{
			LOG.info("%s v%d", Library.TITLE, Library.VERSION);
			
			run(args);
		}
		finally
		{
			ShutdownService.INSTANCE.shutdown();
		}
		
		Stdout.println("Goodbye.");
	}
	
	private static void run(String[] args)
	{
		Agent app = new TCGAgent();
		
		app.run();
	}
	
	public static final Logger LOG = Logger.DEFAULT;
}
