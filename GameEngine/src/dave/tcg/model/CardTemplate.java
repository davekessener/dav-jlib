package dave.tcg.model;

import java.util.Arrays;
import java.util.stream.Stream;

import dave.json.Container;
import dave.json.JsonCollectors;
import dave.json.JsonObject;
import dave.json.JsonValue;
import dave.json.Loader;
import dave.json.Saveable;
import dave.json.Saver;

@Container
public class CardTemplate implements Saveable
{
	public final String name;
	public final EffectTemplate[] effects;
	public final int cost;
	
	public CardTemplate(String name, EffectTemplate[] effects, int cost)
	{
		this.name = name;
		this.effects = Arrays.copyOf(effects, effects.length);
		this.cost = cost;
	}
	
	@Saver
	@Override
	public JsonValue save()
	{
		JsonObject json = new JsonObject();
		
		json.putString("name", name);
		json.putInt("cost", cost);
		json.put("effects", Stream.of(effects)
			.map(EffectTemplate::save)
			.collect(JsonCollectors.ofArray()));
		
		return json;
	}
	
	@Loader
	public static CardTemplate load(JsonValue json)
	{
		JsonObject o = (JsonObject) json;
		
		String name = o.getString("name");
		int cost = o.getInt("cost");
		EffectTemplate[] effects = o.getArray("effects").stream()
			.map(EffectTemplate::load)
			.toArray(l -> new EffectTemplate[l]);
		
		return new CardTemplate(name, effects, cost);
	}
}
