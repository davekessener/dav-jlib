package dave.tcg.model;

import dave.json.Container;
import dave.json.JsonObject;
import dave.json.JsonValue;
import dave.json.Loader;
import dave.json.Saveable;
import dave.json.Saver;

@Container
public class HeroPowerTemplate implements Saveable
{
	public final String name;
	public final EffectTemplate effect;
	public final int cost;
	
	public HeroPowerTemplate(String name, EffectTemplate effect, int cost)
	{
		this.name = name;
		this.effect = effect;
		this.cost = cost;
	}
	
	@Saver
	@Override
	public JsonValue save()
	{
		JsonObject json = new JsonObject();
		
		json.putString("name", name);
		json.put("effect", effect.save());
		json.putInt("cost", cost);
		
		return json;
	}
	
	@Loader
	public static HeroPowerTemplate load(JsonValue json)
	{
		JsonObject o = (JsonObject) json;
		
		String name = o.getString("name");
		EffectTemplate effect = EffectTemplate.load(o.get("effect"));
		int cost = o.getInt("cost");
		
		return new HeroPowerTemplate(name, effect, cost);
	}
}
