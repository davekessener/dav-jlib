package dave.tcg.model;

import dave.json.Container;
import dave.json.JsonObject;
import dave.json.JsonValue;
import dave.json.Loader;
import dave.json.Saveable;
import dave.json.Saver;

@Container
public class HeroTemplate implements Saveable
{
	public final String name;
	public final HeroPowerTemplate power;
	public final int health;
	
	public HeroTemplate(String name, HeroPowerTemplate power, int health)
	{
		this.name = name;
		this.power = power;
		this.health = health;
	}
	
	@Saver
	@Override
	public JsonValue save()
	{
		JsonObject json = new JsonObject();
		
		json.putString("name", name);
		json.put("power", power.save());
		json.putInt("health", health);
		
		return json;
	}
	
	@Loader
	public static HeroTemplate load(JsonValue json)
	{
		JsonObject o = (JsonObject) json;
		
		String name = o.getString("name");
		HeroPowerTemplate power = HeroPowerTemplate.load(o.get("power"));
		int health = o.getInt("health");
		
		return new HeroTemplate(name, power, health);
	}
}
