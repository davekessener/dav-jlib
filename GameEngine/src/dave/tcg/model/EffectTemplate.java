package dave.tcg.model;

import dave.json.Container;
import dave.json.JsonObject;
import dave.json.JsonValue;
import dave.json.Loader;
import dave.json.Saveable;
import dave.json.Saver;

@Container
public class EffectTemplate implements Saveable
{
	public final String id;
	
	public EffectTemplate(String id)
	{
		this.id = id;
	}
	
	@Saver
	@Override
	public JsonValue save()
	{
		JsonObject json = new JsonObject();
		
		json.putString("id", id);
		
		return json;
	}
	
	@Loader
	public static EffectTemplate load(JsonValue json)
	{
		JsonObject o = (JsonObject) json;
		
		String id = o.getString("id");
		
		return new EffectTemplate(id);
	}
}
