package dave.tcg.game;

import dave.tcg.bus.Bus;
import dave.tcg.model.HeroTemplate;

public class Hero extends GameEntity
{
	public Hero(Bus bus, HeroTemplate hero)
	{
		super(bus, "hero:" + hero.name);
	}
}
