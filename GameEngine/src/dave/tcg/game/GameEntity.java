package dave.tcg.game;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import dave.tcg.bus.BaseEntity;
import dave.tcg.bus.Bus;
import dave.tcg.common.Entity;
import dave.tcg.common.Event;
import dave.tcg.common.Responder;
import dave.tcg.common.Response;

public class GameEntity extends BaseEntity
{
	private final Bus mBus;
	private final List<Effect> mEffects;
	
	protected GameEntity(Bus bus, String id)
	{
		super(id);
		
		mBus = bus;
		mEffects = new ArrayList<>();
	}
	
	public Bus bus() { return mBus; }
	
	public void attachEffect(Effect e) { mEffects.add(e); }
	public void detachEffect(Effect e) { mEffects.remove(e); }
	
	@Override
	public <T> Responder<T> respond(Entity src, T def, Event e)
	{
		List<Responder<T>> effects = mEffects.stream()
			.map(f -> f.respondTo(src, def, e, this))
			.collect(Collectors.toList());
		
		return (effects.isEmpty() ? null : new ResponderProxy<>(effects));
	}
	
	private class ResponderProxy<T> implements Responder<T>
	{
		private final List<Responder<T>> mResponders;
		
		private ResponderProxy(List<Responder<T>> r)
		{
			mResponders = r;
		}

		@Override
		public Entity source()
		{
			return GameEntity.this;
		}

		@Override
		public boolean process(Event e, T df, List<Response<T>> rl)
		{
			return mResponders.stream().anyMatch(r -> r.process(e, df, rl));
		}
	}
}
