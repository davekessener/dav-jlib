package dave.tcg.game;

import java.util.stream.Stream;

import dave.tcg.bus.Bus;
import dave.tcg.common.Event;
import dave.tcg.model.CardTemplate;

public class Card extends GameEntity
{
	private final CardTemplate mTemplate;
	
	public Card(Bus bus, CardTemplate t)
	{
		super(bus, "card:" + t.name);
		
		mTemplate = t;
		
		Stream.of(mTemplate.effects).forEach(e -> attachEffect(EffectRegistry.generate(e)));
	}
	
	public int cost()
	{
		return bus().inquire(this, mTemplate.cost, Event.GET_CARD_COST);
	}
}
