package dave.tcg.game;

import dave.tcg.common.Entity;
import dave.tcg.common.Event;
import dave.tcg.common.Responder;

public interface Effect
{
	public default <T> Responder<T> respondTo(Entity src, T def, Event e, Entity self) { return null; }
}
