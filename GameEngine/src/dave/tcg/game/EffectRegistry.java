package dave.tcg.game;

import java.util.Map;
import java.util.function.Function;

import dave.tcg.model.EffectTemplate;
import dave.util.ImmutableMapBuilder;

public class EffectRegistry
{
	public static Effect generate(EffectTemplate effect)
	{
		return sEffectGenerators.get(effect.id).apply(effect);
	}
	
	private static final Map<String, Function<EffectTemplate, Effect>> sEffectGenerators
		= (new ImmutableMapBuilder<String, Function<EffectTemplate, Effect>>())
			.build();
	
	private EffectRegistry( ) { }
}
