package dave.actor;

public interface Receiver
{
	public abstract void receive(Exchange x, Object[] args);
}
