package dave.actor;

import dave.util.Identifiable;

public interface ActorHandle extends Identifiable
{
	public abstract ActorAddress address( );
}
