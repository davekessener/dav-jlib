package dave.actor.error;

public class UnsupportedOperationError extends Error
{
	private static final long serialVersionUID = -4123881691231609485L;

	public UnsupportedOperationError( ) { super(""); }
	public UnsupportedOperationError(String n) { super("%s", n); }
}
