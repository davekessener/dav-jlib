package dave.actor.error;

public class Error extends RuntimeException
{
	private static final long serialVersionUID = 8914045868352637352L;
	
	public Error(String s, Object ... o)
	{
		super(String.format(s, o));
	}
	
	public Error(Throwable t)
	{
		super(t);
	}
}
