package dave.actor.error;

public class UnhandledMessageError extends Error
{
	private static final long serialVersionUID = 8815221390486473670L;

	public UnhandledMessageError(String s) { super("%s", s); }
}
