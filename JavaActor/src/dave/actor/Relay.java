package dave.actor;

public interface Relay
{
	public abstract void accept(Message p);
}
