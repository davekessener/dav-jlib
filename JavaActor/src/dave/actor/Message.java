package dave.actor;

import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import dave.actor.error.Error;
import dave.json.Container;
import dave.json.JSON;
import dave.json.JsonObject;
import dave.json.JsonValue;
import dave.json.Loader;
import dave.json.Saveable;
import dave.json.Saver;

@Container
public class Message implements Saveable
{
	public final UUID id, sequence;
	public final ActorAddress from, to;
	public final Object payload;
	public final Type type;
	
	private Message(UUID id, UUID sequence, ActorAddress from, ActorAddress to, Type type, Object payload)
	{
		this.id = id;
		this.sequence = sequence;
		this.from = from;
		this.to = to;
		this.type = type;
		this.payload = payload;
	}
	
	@Override
	@Saver
	public JsonValue save( )
	{
		JsonObject json = new JsonObject();
		
		json.putString("id", id.toString());
		if(sequence != null) json.putString("sequence", sequence.toString());
		json.put("from", JSON.serialize(from));
		json.put("to", JSON.serialize(to));
		json.put("payload", JSON.serialize(payload));
		json.putString("type", type.toString());
		
		return json;
	}
	
	@Loader
	public static Message load(JsonValue json)
	{
		JsonObject o = (JsonObject) json;
		
		UUID id = UUID.fromString(o.getString("id"));
		UUID seq = o.contains("sequence") ? UUID.fromString(o.getString("sequence")) : null;
		ActorAddress from = (ActorAddress) JSON.deserialize(o.get("from"));
		ActorAddress to = (ActorAddress) JSON.deserialize(o.get("to"));
		Object payload = JSON.deserialize(o.get("payload"));
		Type type = Type.valueOf(o.getString("type"));
		
		return new Message(id, seq, from, to, type, payload);
	}
	
	public Object[] getRequest( )
	{
		if(type != Type.REQUEST)
			throw new UnsupportedOperationException();
		
		return (Object[]) payload;
	}
	
	@SuppressWarnings("unchecked")
	public <T> T getArgument(int i)
	{
		return (T) getRequest()[i];
	}
	
	public Object getReply( )
	{
		if(type != Type.REPLY)
			throw new UnsupportedOperationException();
		
		return payload;
	}
	
	public Sys getSystemMessage( )
	{
		if(type != Type.SYSTEM)
			throw new UnsupportedOperationException();
		
		return ((SystemMessage) payload).type;
	}
	
	public Object[] getSystemArguments( )
	{
		if(type != Type.SYSTEM)
			throw new UnsupportedOperationException();
		
		return ((SystemMessage) payload).args;
	}
	
	public Error getError( )
	{
		if(type != Type.ERROR)
			throw new UnsupportedOperationException();
		
		return (Error) payload;
	}
	
	public static Message request(ActorAddress from, ActorAddress to, Object[] args)
	{
		return new Message(UUID.randomUUID(), null, from, to, Type.REQUEST, args);
	}

	public static Message redirect(Message msg, ActorAddress to) { return redirect(msg, msg.from, to); }
	public static Message redirect(Message msg, ActorAddress from, ActorAddress to)
	{
		return new Message(msg.id, msg.sequence, from, to, msg.type, msg.payload);
	}
	
	public static Message reply(Message msg, Object r)
	{
		return new Message(UUID.randomUUID(), msg.id, msg.to, msg.from, Type.REPLY, r);
	}
	
	public static Message system(ActorAddress to, Sys msg, Object ... a)
	{
		return new Message(null, null, null, to, Type.SYSTEM, new SystemMessage(msg, a));
	}
	
	public static Message error(Message msg, Error e)
	{
		return new Message(UUID.randomUUID(), msg.id, msg.to, msg.from, Type.ERROR, e);
	}
	
	@Override
	public String toString( )
	{
		StringBuilder sb = new StringBuilder();
		
		sb.append('{').append(from.toString()).append(" -> ")
		  .append(to.toString()).append(" [").append(type)
		  .append("] (");
		
		if(payload != null)
		{
			if(payload.getClass().isArray())
			{
				sb.append(Stream.of((Object[]) payload).map(Object::toString).collect(Collectors.joining(", ")));
			}
			else
			{
				sb.append(payload.toString());
			}
		}
		
		sb.append(") ").append(id.toString());
		
		if(sequence != null)
		{
			sb.append(":").append(sequence.toString());
		}
		
		sb.append('}');
		
		return sb.toString();
	}
	
	public static enum Type
	{
		SYSTEM,
		REQUEST,
		REPLY,
		ERROR
	}
	
	public static enum Sys
	{
		SHUTDOWN
	}
	
	private static class SystemMessage
	{
		public final Sys type;
		public final Object[] args;
		
		public SystemMessage(Sys type, Object[] args)
		{
			this.type = type;
			this.args = args;
		}
	}
}
