package dave.actor;

public class Timeout
{
	private final long mDuration;
	private final long mStart;
	
	public Timeout(long t)
	{
		if(t <= 0)
			throw new IllegalArgumentException();
		
		mDuration = t;
		mStart = System.currentTimeMillis();
	}
	
	public Timeout(long s, long t)
	{
		if(t <= 0 || s < 0)
			throw new IllegalArgumentException();
		
		mDuration = t;
		mStart = s;
	}
	
	public long getDuration( )
	{
		return mDuration;
	}
	
	public long getStart( )
	{
		return mStart;
	}
	
	public long getLeft( )
	{
		return Math.max(0, mDuration - (System.currentTimeMillis() - mStart));
	}
}
