package dave.actor;

import java.util.HashMap;
import java.util.Map;

public class DynamicReceiver<T extends Atom> implements Receiver
{
	private final Map<Atom, Receiver> mHandlers;
	private final Receiver mDefault;
	
	public DynamicReceiver(Receiver def)
	{
		mHandlers = new HashMap<>();
		mDefault = def;
	}
	
	public DynamicReceiver<T> register(Atom v, Receiver r)
	{
		mHandlers.put(v, r);
		
		return this;
	}

	@Override
	public void receive(Exchange x, Object[] args)
	{
		Receiver r = null;
		
		if(args.length > 0 && (args[0] instanceof Atom))
		{
			r = mHandlers.get((Atom) args[0]);
		}
		
		(r == null ? mDefault : r).receive(x, args);
	}
}
