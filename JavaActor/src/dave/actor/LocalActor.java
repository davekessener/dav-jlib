package dave.actor;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

import dave.actor.error.Error;
import dave.util.log.Logger;
import dave.util.log.Severity;

public class LocalActor extends BaseActor implements ConcreteActor, ResponseHandler
{
	private final Relay mNet;
	private final BlockingQueue<Message> mMailbox;
	private final Map<UUID, Callback> mConversations;
	private final ExecutorService mAsync;
	private final Map<Message.Sys, BiConsumer<Message.Sys, Object[]>> mSysHandler;
	private Consumer<Error> mErrorHandler;
	private Message mBuffer;
	private Receiver mReceiver;
	private boolean mRunning;
	
	public LocalActor(String id, ActorAddress a, Relay n)
	{
		super(id, a);
		
		mNet = n;
		mMailbox = new LinkedBlockingQueue<>();
		mConversations = new HashMap<>();
		mSysHandler = new HashMap<>();
		mAsync = Executors.newSingleThreadExecutor();
		mBuffer = null;
		mRunning = true;
		
		mErrorHandler = e -> LOG.log(Severity.ERROR, "[%s] Unhandled error %s", address().toString(), e.getMessage());
		
		mSysHandler.put(Message.Sys.SHUTDOWN, (t, o) -> mRunning = false);
	}
	
	@Override
	public Response send(ActorHandle other, Object ... a)
	{
		if(a.length == 0)
			throw new IllegalArgumentException();
		
		Message p = Message.request(address(), other.address(), a);
		
		mNet.accept(p);
		
		return new BasicResponse(this, p);
	}

	@Override
	public void accept(Message message)
	{
		mMailbox.add(message);
	}
	
	@Override
	public void set(Receiver r)
	{
		mReceiver = r;
	}

	@Override
	public Void call( ) throws InterruptedException
	{
		while(true)
		{
			Message message = mMailbox.take();
			
			if(message.type == Message.Type.SYSTEM)
			{
				mSysHandler.get(message.getSystemMessage()).accept(message.getSystemMessage(), message.getSystemArguments());
			}
			else if(message.sequence == null)
			{
				if(message.type == Message.Type.ERROR)
				{
					mErrorHandler.accept(message.getError());
				}
				else
				{
					try
					{
						mReceiver.receive(a -> mNet.accept(Message.reply(message, a)), message.getRequest());
					}
					catch(Error e)
					{
						mNet.accept(Message.error(message, e));
					}
				}
			}
			else
			{
				Callback cb = mConversations.remove(message.sequence);
				
				if(cb == null)
				{
					mBuffer = message;
				}
				else
				{
					handleReply(message, cb);
				}
			}
			
			if(!mRunning)
				return null;
		}
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void handleReply(Message msg, Callback cb)
	{
		Consumer trick = (Consumer) cb.callback;
		
		if(cb.timeout != null)
		{
			cb.timeout.cancel(false);
		}
		
		if(msg.type == Message.Type.REPLY)
		{
			trick.accept(msg.getReply());
		}
		else if(cb.error != null)
		{
			cb.error.accept(msg.getError());
		}
		else
		{
			mErrorHandler.accept(msg.getError());
		}
	}

	@Override
	public void listen(Message msg, Consumer<?> cb, Timeout to, Consumer<Error> err)
	{
		if(mBuffer != null && msg.id.equals(mBuffer.sequence))
		{
			handleReply(mBuffer, new Callback(cb, null, err));
			mBuffer = null;
		}
		else
		{
			Future<?> f = null;
			
			if(to != null)
			{
				f = mAsync.submit(new Deferred(to, () -> accept(Message.error(msg, new TimeoutError(msg.id)))));
			}
			
			mConversations.put(msg.id, new Callback(cb, f, err));
		}
	}
	
	@Override
	public Object waitFor(UUID id)
	{
		return null; // TODO
	}
	
	private static class Callback
	{
		public final Consumer<?> callback;
		public final Consumer<Error> error;
		public final Future<?> timeout;
		
		public Callback(Consumer<?> callback, Future<?> timeout, Consumer<Error> error)
		{
			this.callback = callback;
			this.timeout = timeout;
			this.error = error;
		}
	}
	
	private static final Logger LOG = Logger.get("actor");
}
