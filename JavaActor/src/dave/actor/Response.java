package dave.actor;

import java.util.function.Consumer;

import dave.actor.error.Error;

public interface Response
{
	public abstract void receive(Consumer<?> cb, Timeout to, Consumer<Error> err);
	public abstract <T> T get( );
	
	public default void receive(Consumer<?> cb) { receive(cb, null, null); }
	public default void receive(Consumer<?> cb, Timeout to) { receive(cb, to, null); }
	public default void receive(Consumer<?> cb, Consumer<Error> err) { receive(cb, null, err); }
}
