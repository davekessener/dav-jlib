package dave.actor;

public class BaseActor implements ActorHandle
{
	private final String mID;
	private final ActorAddress mAddress;
	
	public BaseActor(String id, ActorAddress a)
	{
		mID = id;
		mAddress = a;
	}

	@Override
	public String getID()
	{
		return mID;
	}

	@Override
	public ActorAddress address()
	{
		return mAddress;
	}
}
