package dave.actor;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import dave.json.JsonObject;
import dave.json.JsonValue;
import dave.net.server.PacketedServer;
import dave.net.server.Server;
import dave.util.Identifiable;
import dave.util.SevereException;
import dave.util.log.Logger;
import dave.util.log.Severity;

public class Broker implements Identifiable, dave.util.Actor
{
	private static final Map<UUID, Broker> sBrokers = new ConcurrentHashMap<>();
	private static final Map<UUID, InetSocketAddress> sRemotes = new ConcurrentHashMap<>();
	
	private final UUID mID;
	private final Map<String, ConcreteActor> mActors;
	private final Map<String, Future<?>> mCallbacks;
	private final ExecutorService mAsync;
	private PacketedServer mServer;
	
	public Broker( )
	{
		mID = UUID.randomUUID();
		mActors = new ConcurrentHashMap<>();
		mCallbacks = new ConcurrentHashMap<>();
		mAsync = Executors.newCachedThreadPool();
	}
	
	@Override
	public String getID( )
	{
		return mID.toString();
	}
	
	@Override
	public void start( )
	{
		sBrokers.put(mID, this);
		
		if(mServer != null)
		{
			mServer.start();
		}
		
		LOG.log(Severity.INFO, "Started broker %s (%s)", mID.toString(), ActorUtils.to_s(mID));
	}
	
	@Override
	public void stop( )
	{
		if(mServer != null)
		{
			mServer.stop();
		}
		
		sBrokers.remove(mID);
		
		LOG.log(Severity.INFO, "Stopped broker %s", mID.toString());
	}
	
	public int publish(int port)
	{
		try
		{
			mServer = Server.createUDPServer(port, p -> accept(load(p)));
			
			return ((InetSocketAddress) mServer.getAddress()).getPort();
		}
		catch(IOException e)
		{
			throw new SevereException(e);
		}
	}
	
	public void unpublish( )
	{
		mServer.stop();
		mServer = null;
	}
	
	public ActorHandle remote(String id, String host, int port)
	{
		if(mServer == null)
			throw new IllegalStateException();
		
		try
		{
			InetSocketAddress a = new InetSocketAddress(InetAddress.getByName(host), port);
			
			return new BaseActor(id, new RemoteActorAddress(id, a));
		}
		catch(UnknownHostException e)
		{
			throw new SevereException(e);
		}
	}
	
	public ActorHandle spawn(String id, Function<Actor, Receiver> cb)
	{
		ConcreteActor a = new LocalActor(id, new LocalActorAddress(id, mID), this::accept);
		
		mActors.put(id, a);

		Receiver r = cb.apply(a);
		
		a.set(r);

		mCallbacks.put(id, mAsync.submit(a));

		return a;
	}
	
	public void unspawn(ActorHandle h)
	{
		try
		{
			mActors.remove(h.getID()).accept(Message.system(h.address(), Message.Sys.SHUTDOWN));
			mCallbacks.remove(h.getID()).get();
		}
		catch(InterruptedException | ExecutionException e)
		{
			throw new SevereException(e);
		}
	}
	
	private void process(Message p)
	{
		LOG.log("%s", to_s(p));
		
		ConcreteActor a = mActors.get(p.to.getID());
		
		if(a != null)
		{
			a.accept(p);
		}
		else
		{
			System.out.println(mActors.entrySet().stream().map(e -> "" + e.getKey() + ":" + e.getValue().getID()).collect(Collectors.joining(", ")));
			LOG.log(Severity.ERROR, "Unknown actor %s!", p.to.getID());
		}
	}
	
	private void accept(Message p)
	{
		if(p.to instanceof LocalActorAddress)
		{
			LocalActorAddress to = (LocalActorAddress) p.to;
			
			if(to.getSystem().equals(mID))
			{
				process(p);
			}
			else
			{
				Broker o = sBrokers.get(to.getSystem());
				
				if(o != null)
				{
					o.accept(p);
				}
				else
				{
					InetSocketAddress r = sRemotes.get(to.getSystem());
					
					if(r != null)
					{
						send(to.getSystem(), r, p);
					}
					else
					{
						LOG.log(Severity.ERROR, "Unknown system %s!", to.getSystem().toString());
					}
				}
			}
		}
		else if(p.to instanceof RemoteActorAddress)
		{
			RemoteActorAddress to = (RemoteActorAddress) p.to;
			
			send(null, to.getRemote(), p);
		}
		else
		{
			LOG.log(Severity.ERROR, "Unsupported address type: %s!", p.to.toString());
		}
	}
	
	private void send(UUID sys, InetSocketAddress r, Message p)
	{
		LOG.log("%s", to_s(p));
		
		try
		{
			mServer.send(r, save(p));
		}
		catch(IOException e)
		{
			String s = (sys == null) ? "" : (sys.toString() + "@");
			
			LOG.log(Severity.ERROR, "Failed to deliver to remote %s[%s]:%d (%s)",
					s, r.getHostString(), r.getPort(), e.getMessage());
		}
	}
	
	private JsonValue save(Message p)
	{
		JsonObject json = new JsonObject();
		
		json.putString("id", mID.toString());
		json.put("message", p.save());
		
		return json;
	}
	
	private Message load(Server.Datagram p)
	{
		JsonObject o = (JsonObject) p.payload;
		UUID id = UUID.fromString(o.getString("id"));
		Message msg = Message.load(o.get("message"));
		
		if(!id.equals(mID))
		{
			sRemotes.put(id, p.source);
		}
		
		return Message.redirect(msg, new LocalActorAddress(msg.to.getID(), mID));
	}
	
	private String to_s(Message p)
	{
		StringBuilder sb = new StringBuilder();
		
		sb.append('{').append(to_s(p.from)).append(" -> ").append(to_s(p.to))
		  .append(" [").append(p.type).append("] (").append(to_s(p.payload)).append(") ")
		  .append(ActorUtils.to_s(p.id));
		
		if(p.sequence != null)
		{
			sb.append(':').append(ActorUtils.to_s(p.sequence));
		}
		
		sb.append('}');
		
		return sb.toString();
	}
	
	private String to_s(ActorAddress a)
	{
		if(a instanceof LocalActorAddress)
		{
			UUID r = ((LocalActorAddress) a).getSystem();
			
			return r.equals(mID) ? a.getID() : String.format("%s@%s", a.getID(), ActorUtils.to_s(r));
		}
		else if(a instanceof RemoteActorAddress)
		{
			InetSocketAddress r = ((RemoteActorAddress) a).getRemote();
			
			return String.format("%s@[%s]:%d", a.getID(), r.getHostString(), r.getPort());
		}
		else
		{
			LOG.log(Severity.WARNING, "Unsupported address type: %s!", a.toString());
			
			return a.toString();
		}
	}

	private static String to_s(Object o)
	{
		String r = "" + o;
		
		if(o != null && o.getClass().isArray())
		{
			r = Stream.of((Object[]) o).map(Broker::to_s).collect(Collectors.joining(", "));
		}
		
		return r;
	}
	
	private static final Logger LOG = Logger.get("net");
}
