package dave.actor;

import java.util.concurrent.Callable;

public interface ConcreteActor extends Actor, Callable<Void>
{
	public abstract void accept(Message message);
	public abstract void set(Receiver r);
}
