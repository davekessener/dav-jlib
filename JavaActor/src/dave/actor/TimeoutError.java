package dave.actor;

import java.util.UUID;

import dave.actor.error.Error;

public class TimeoutError extends Error
{
	private static final long serialVersionUID = -2821223884763317655L;

	public TimeoutError(UUID id) { super("%s", id.toString()); }
}
