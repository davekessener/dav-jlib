package dave.actor;

import java.util.UUID;

public final class ActorUtils
{
	public static String to_s(UUID id) { return String.format("%04x", hash(id)); }
	
	public static int hash(UUID id)
	{
		long v = id.getLeastSignificantBits() ^ id.getMostSignificantBits();
		
		v = (v >>> 32) ^ (v & 0xFFFFFFFFl);
		v = (v >>> 16) ^ (v & 0xFFFF);
		
		return (int) v;
	}
	
	private ActorUtils( ) { }
}
