package dave.actor;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;

import dave.json.Container;
import dave.json.JsonObject;
import dave.json.JsonValue;
import dave.json.Loader;
import dave.json.Saveable;
import dave.json.Saver;
import dave.util.SevereException;

@Container
public class RemoteActorAddress extends BaseActorAddress implements Saveable
{
	private final InetSocketAddress mRemote;
	
	public RemoteActorAddress(String id, InetSocketAddress a)
	{
		super(id);
		
		mRemote = a;
	}
	
	public InetSocketAddress getRemote( )
	{
		return mRemote;
	}
	
	@Override
	public String toString( )
	{
		return String.format("%s@[%s]:%d", getID(), mRemote.getHostString(), mRemote.getPort());
	}
	
	@Override
	@Saver
	public JsonValue save( )
	{
		JsonObject json = new JsonObject();
		
		json.putString("id", getID());
		json.putString("host", mRemote.getHostString());
		json.putInt("port", mRemote.getPort());
		
		return json;
	}
	
	@Loader
	public static RemoteActorAddress load(JsonValue json)
	{
		JsonObject o = (JsonObject) json;
		
		String id = o.getString("id");
		InetAddress host = null;
		int port = o.getInt("port");

		try
		{
			host = InetAddress.getByName(o.getString("host"));
		}
		catch (UnknownHostException e)
		{
			throw new SevereException(e);
		}

		return new RemoteActorAddress(id, new InetSocketAddress(host, port));
	}
}
