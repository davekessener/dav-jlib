package dave.actor;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import dave.actor.error.UnhandledMessageError;

public class InvalidReceiver implements Receiver
{
	@Override
	public void receive(Exchange x, Object[] args)
	{
		throw new UnhandledMessageError(Stream.of(args).map(o -> ("" + o)).collect(Collectors.joining(", ")));
	}
}
