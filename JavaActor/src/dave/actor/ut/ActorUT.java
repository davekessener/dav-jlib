package dave.actor.ut;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import dave.actor.ActorHandle;
import dave.actor.Atom;
import dave.actor.Broker;
import dave.actor.DynamicReceiver;
import dave.actor.InvalidReceiver;
import dave.actor.TypedReceiver;
import dave.util.SevereException;
import dave.util.ShutdownService;
import dave.util.ShutdownService.Priority;
import dave.util.log.LogBase;

public class ActorUT
{
	@Before
	public void setup( )
	{
		LogBase.INSTANCE.registerSink(e -> true, new LogBase.DefaultSink());
		
		LogBase.INSTANCE.start();
		
		ShutdownService.INSTANCE.register(Priority.LAST, LogBase.INSTANCE::stop);
	}
	
	@After
	public void shutdown( )
	{
		ShutdownService.INSTANCE.shutdown();
	}

	private static enum OP implements Atom
	{
		FACTORIAL
	}
	
	public static interface ICalc
	{
		public abstract int add(int a, int b);
		public abstract int sub(int a, int b);
		public abstract int mul(int a, int b);
		public abstract int div(int a, int b);
	}
	
	public static class Calc implements ICalc
	{
		@Override
		public int add(int a, int b)
		{
			return a + b;
		}

		@Override
		public int sub(int a, int b)
		{
			return a - b;
		}

		@Override
		public int mul(int a, int b)
		{
			return a * b;
		}

		@Override
		public int div(int a, int b)
		{
			return a / b;
		}
	}
	
	@Test
	public void test( )
	{
		int[] r = { 0 };
		
		Broker[] sys = new Broker[] { new Broker(), new Broker() };
		
		int[] port = { sys[0].publish(0), sys[1].publish(0) };
		
		sys[0].start();
		sys[1].start();
		
		ActorHandle calc = sys[0].spawn("calc", a -> new TypedReceiver(ICalc.class, new Calc()));
		ActorHandle r_calc = sys[1].remote("calc", "127.0.0.1", port[0]);
		
		ActorHandle handle = sys[1].spawn("factorizer", actor -> {
			actor.send(actor, OP.FACTORIAL, 5).receive(rr -> r[0] = (Integer) rr);
			
			return new DynamicReceiver<OP>(new InvalidReceiver())
				.register(OP.FACTORIAL, (x, a) -> {
					Integer v = (Integer) a[1];
					
					if(v == 1)
					{
						x.answer(v);
					}
					else
					{
						actor.send(r_calc, "sub", v, 1)
							.receive(r1 -> actor.send(actor, OP.FACTORIAL, r1)
							.receive(r2 -> actor.send(calc, "mul", v, r2)
							.receive(x::answer)));
					}
				})
			;
		});
		
		sleep(50);

		sys[0].unspawn(calc);
		sys[1].unspawn(handle);
		
		sys[0].stop();
		sys[1].stop();
		
		assertEquals(r[0], 5 * 4 * 3 * 2 * 1);
	}
	
	private static void sleep(int ms) { try { Thread.sleep(ms); } catch(InterruptedException e) { throw new SevereException(e); } }
}
