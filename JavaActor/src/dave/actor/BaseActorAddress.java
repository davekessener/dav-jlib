package dave.actor;

public class BaseActorAddress implements ActorAddress
{
	private final String mID;
	
	protected BaseActorAddress(String id)
	{
		mID = id;
	}
	
	@Override
	public String getID( )
	{
		return mID;
	}
	
	@Override
	public String toString( )
	{
		return mID;
	}
}
