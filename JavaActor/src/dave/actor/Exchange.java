package dave.actor;

public interface Exchange
{
	public abstract void answer(Object o);
}
