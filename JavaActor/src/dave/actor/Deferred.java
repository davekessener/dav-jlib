package dave.actor;

import java.util.concurrent.Callable;

public class Deferred implements Callable<Void>
{
	public static interface Callback { void run( ) throws Exception; }
	
	private final Callback mCallback;
	private final Timeout mTimeout;
	
	public Deferred(Timeout to, Callback cb)
	{
		mCallback = cb;
		mTimeout = to;
	}
	
	@Override
	public Void call() throws Exception
	{
		Thread.sleep(mTimeout.getLeft());
		
		mCallback.run();
		
		return null;
	}
}
