package dave.actor;

public interface Actor extends ActorHandle
{
	public abstract Response send(ActorHandle other, Object ... o);
}
