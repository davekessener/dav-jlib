package dave.actor;

import java.util.function.Consumer;

import dave.actor.error.Error;

public class BasicResponse implements Response
{
	private final ResponseHandler mCallback;
	private final Message mMessage;
	private final long mTimestamp;
	
	public BasicResponse(ResponseHandler cb, Message src)
	{
		mCallback = cb;
		mMessage = src;
		mTimestamp = System.currentTimeMillis();
	}

	@Override
	public void receive(Consumer<?> cb, Timeout to, Consumer<Error> err)
	{
		mCallback.listen(mMessage, cb, (to == null ? null : new Timeout(mTimestamp, to.getDuration())), err);
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T get()
	{
		return (T) mCallback.waitFor(mMessage.id);
	}
}
