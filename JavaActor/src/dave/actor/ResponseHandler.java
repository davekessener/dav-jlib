package dave.actor;

import java.util.UUID;
import java.util.function.Consumer;

import dave.actor.error.Error;

public interface ResponseHandler
{
	public abstract void listen(Message msg, Consumer<?> cb, Timeout to, Consumer<Error> err);
	public abstract Object waitFor(UUID id);
}
