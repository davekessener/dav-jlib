package dave.actor;

import java.util.UUID;

import dave.json.Container;
import dave.json.JsonObject;
import dave.json.JsonValue;
import dave.json.Loader;
import dave.json.Saveable;
import dave.json.Saver;

@Container
public class LocalActorAddress extends BaseActorAddress implements Saveable
{
	private final UUID mSystem;
	
	public LocalActorAddress(String id, UUID sys)
	{
		super(id);
		
		mSystem = sys;
	}
	
	public UUID getSystem( )
	{
		return mSystem;
	}
	
	@Override
	public String toString( )
	{
		return getID() + "@" + mSystem;
	}
	
	@Override
	@Saver
	public JsonValue save( )
	{
		JsonObject json = new JsonObject();
		
		json.putString("id", getID());
		json.putString("system", mSystem.toString());
		
		return json;
	}
	
	@Loader
	public static LocalActorAddress load(JsonValue json)
	{
		JsonObject o = (JsonObject) json;
		
		String id = o.getString("id");
		UUID sys = UUID.fromString(o.getString("system"));
		
		return new LocalActorAddress(id, sys);
	}
}
