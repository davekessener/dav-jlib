package dave.actor;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import dave.actor.error.UnsupportedOperationError;
import dave.util.ImmutableMapBuilder;
import dave.util.Utils;

public class TypedReceiver implements Receiver
{
	private final Class<?> mInterface;
	private final Object mReceiver;
	
	public TypedReceiver(Class<?> i, Object o)
	{
		if(!i.isInterface())
			throw new IllegalArgumentException(i.getCanonicalName());
		if(!i.isInstance(o))
			throw new IllegalArgumentException();
		
		mInterface = i;
		mReceiver = o;
	}

	@Override
	public void receive(Exchange x, Object[] args)
	{ try {
		String all = Stream.of(args).map(o -> "" + o).collect(Collectors.joining(", "));
		
		if(args.length == 0 || !(args[0] instanceof String))
			throw new UnsupportedOperationError(all);
		
		String name = (String) args[0];
		
		List<Method> pot = Stream.of(mInterface.getMethods())
			.filter(m -> m.getName().equals(name))
			.filter(m -> m.getParameterCount() == args.length - 1)
			.filter(m -> Utils.stream_with_index(m.getParameterTypes())
				.allMatch(p -> {
					Class<?> t = p.first;
					Object a = args[p.second + 1];
					
					return t.isPrimitive() ?
							(t.isAssignableFrom(a.getClass()) || WRAPPER.get(t).isAssignableFrom(a.getClass())) :
							(a == null || t.isAssignableFrom(a.getClass()));
				}))
			.collect(Collectors.toList());
		
		if(pot.size() != 1)
			throw new UnsupportedOperationError(all);
		
		try
		{
			Method m = pot.get(0);
			Object r = m.invoke(mReceiver, Arrays.copyOfRange(args, 1, args.length));
			
			if(!m.getReturnType().equals(Void.TYPE))
			{
				x.answer(r);
			}
		}
		catch(IllegalAccessException | IllegalArgumentException | InvocationTargetException e)
		{
			throw new UnsupportedOperationError(e.getMessage());
		}
		
		} catch(UnsupportedOperationError e) { System.out.println("" + (int.class.isInstance(7))); e.printStackTrace(); throw e; }
	}
	
	private static final Map<Class<?>, Class<?>> WRAPPER = new ImmutableMapBuilder<Class<?>, Class<?>>()
		.put(boolean.class, Boolean.class)
		.put(byte.class, Byte.class)
		.put(char.class, Character.class)
		.put(double.class, Double.class)
		.put(float.class, Float.class)
		.put(int.class, Integer.class)
		.put(long.class, Long.class)
		.put(short.class, Short.class)
		.put(void.class, Void.class)
		.build();
}
