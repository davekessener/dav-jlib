package dave.work;

import java.util.Objects;

import dave.json.Container;
import dave.json.JsonObject;
import dave.json.JsonValue;
import dave.json.Loader;
import dave.json.Saveable;
import dave.json.Saver;

@Container
public class Message implements Saveable
{
	public final Task task;
	public final JsonValue payload;
	public final Address from, to;
	
	public Message(Address from, Address to, Task task, JsonValue payload)
	{
		this.task = task;
		this.payload = payload;
		this.from = from;
		this.to = to;
	}
	
	@Override
	@Saver
	public JsonValue save()
	{
		JsonObject json = new JsonObject();
		
		json.put("task", task.save());
		json.put("payload", payload);
		json.putString("from", from.getID());
		json.putString("to", to.getID());
		
		return json;
	}
	
	@Loader
	public static Message load(JsonValue json)
	{
		JsonObject o = (JsonObject) json;
		
		Task task = Task.load(o.get("task"));
		JsonValue payload = o.get("payload");
		Address from = new LocalAddress(o.getString("from"));
		Address to = new LocalAddress(o.getString("to"));
		
		return new Message(from, to, task, payload);
	}
	
	@Override
	public String toString()
	{
		return "{" + from + " -> " + to + ": " + task + " | " + payload + "}";
	}
	
	@Override
	public int hashCode()
	{
		return (task.hashCode() * 3) ^ (Objects.hashCode(payload) * 7) ^ (from.hashCode() * 13) ^ (to.hashCode() * 17);
	}
	
	@Override
	public boolean equals(Object o)
	{
		if(o instanceof Message)
		{
			Message m = (Message) o;
			
			return task.equals(m.task) && Objects.equals(payload, m.payload) && from.equals(m.from) && to.equals(m.to);
		}
		
		return false;
	}
}
