package dave.work;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import dave.net.server.Connection;
import dave.util.log.Logger;
import dave.util.log.Severity;

public class ConnectionManager
{
	private final Map<String, Connection> mConnections;
	private final Map<String, Connection> mOpen;
	
	public ConnectionManager()
	{
		mConnections = new HashMap<>();
		mOpen = new HashMap<>();
	}
	
	public Connection get(String id, InetSocketAddress a)
	{
		String k = computeKey(id, a);
		Connection c = mOpen.get(k);
		
		if(c == null)
		{
			c = mConnections.get(a.toString());
			
			if(c != null)
			{
				mOpen.put(k, c);
			}
		}
		
		return c;
	}
	
	public void add(String id, Connection c)
	{
		InetSocketAddress a = c.participants()[1];
		
		mConnections.put(a.toString(), c);
		mOpen.put(computeKey(id, a), c);
	}
	
	public void remove(String id, InetSocketAddress a)
	{
		mOpen.remove(computeKey(id, a));
	}
	
	public void update()
	{
		for(Iterator<Map.Entry<String, Connection>> i = mConnections.entrySet().iterator() ; i.hasNext() ;)
		{
			Connection c = i.next().getValue();
			InetSocketAddress a = c.participants()[1];
			
			if(!mOpen.containsValue(c))
			{
				i.remove();
				
				try
				{
					c.close();
				}
				catch(IOException e)
				{
					LOG.log(Severity.WARNING, "IO Error while closing connection to %s: %s!", a, e.getMessage());
				}
			}
		}
	}
	
	private static final String computeKey(String id, InetSocketAddress a) { return "(" + id + ")@" + a.toString(); }
	
	private static final Logger LOG = Logger.get("cmanager");
}
