package dave.work;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import dave.json.Container;
import dave.json.JsonString;
import dave.json.JsonValue;
import dave.json.Loader;
import dave.json.Saveable;
import dave.json.Saver;
import dave.util.Identifiable;

@Container
public class Task implements Identifiable, Saveable
{
	private static final Map<String, Task> sTasks = new HashMap<>();
	
	private final Task mSuper;
	private final String mID;
	private final List<Task> mChildren;

	public Task(String id) { this(null, id); }
	public Task(Task t, String id)
	{
		mSuper = t;
		mID = id;
		mChildren = new LinkedList<>();
		
		if(mSuper != null)
		{
			mSuper.mChildren.add(0, this);
		}
		
		sTasks.put(getID(), this);
	}
	
	@Override
	@Saver
	public JsonValue save()
	{
		return new JsonString(getID());
	}
	
	@Loader
	public static Task load(JsonValue json)
	{
		return sTasks.get(((JsonString) json).get());
	}
	
	public String getTopic() { return (mSuper == null ? mID : mSuper.getID()); }
	public boolean isFinal() { return (mSuper == null ? true : mSuper.mChildren.get(0).equals(this)); }
	
	public int compareTo(Task t)
	{
		String id1 = getID();
		String id2 = t.getID();
		
		if(id1.length() < id2.length())
		{
			String x = id1; id1 = id2; id2 = x;
		}
		
		if(id1.startsWith(id2))
		{
			return (int) id2.substring(id1.length()).chars().filter(c -> c == '.').count();
		}
		else
		{
			return -1;
		}
	}

	@Override
	public String getID()
	{
		return (mSuper == null ? mID : mSuper.getID() + "." + mID);
	}
}
