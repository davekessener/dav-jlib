package dave.work;

import java.util.function.Consumer;

import dave.util.Actor;
import dave.util.Identifiable;

public interface Node extends Identifiable, Actor, Consumer<Message>
{
}
