package dave.work;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

import dave.json.JsonValue;
import dave.util.log.Logger;
import dave.util.log.Severity;

public abstract class BaseNode implements Node
{
	private final String mID;
	private final Map<Task, Consumer<Message>> mCallbacks;
	private final Consumer<Message> mGateway;
	
	public BaseNode(String id, Consumer<Message> f)
	{
		mID = id;
		mCallbacks = new HashMap<>();
		mGateway = f;
	}
	
	@Override
	public String getID()
	{
		return mID;
	}
	
	protected void register(Task t, Consumer<Message> f)
	{
		mCallbacks.put(t, f);
	}
	
	protected void send(String to, Task t) { send(to, t, null); }
	protected void send(String to, Task t, JsonValue p) { send(new LocalAddress(to), t, p); }
	protected void send(Address to, Task t) { send(to, t, null); }
	protected void send(Address to, Task t, JsonValue p)
	{
		mGateway.accept(new Message(new LocalAddress(mID), to, t, p));
	}
	
	@Override
	public void accept(Message m)
	{
		Consumer<Message> cb = null;
		int d = Integer.MAX_VALUE;
		
		for(Map.Entry<Task, Consumer<Message>> e : mCallbacks.entrySet())
		{
			int t = e.getKey().compareTo(m.task);
			
			if(t >= 0 && t < d)
			{
				cb = e.getValue();
				d = t;
			}
		}
		
		if(cb != null)
		{
			cb.accept(m);
		}
		else
		{
			LOG.log(Severity.ERROR, "No handler for message %s!", m);
		}
	}
	
	private static final Logger LOG = Logger.get("node");
}
