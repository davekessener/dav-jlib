package dave.work;

public abstract class BaseRelay implements Relay
{
	private final String mID;
	
	public BaseRelay(String id)
	{
		mID = id;
	}
	
	@Override
	public String getID()
	{
		return mID;
	}
}
