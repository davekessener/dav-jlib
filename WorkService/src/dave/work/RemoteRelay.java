package dave.work;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Consumer;

import dave.net.server.Connection;
import dave.net.server.Server;
import dave.util.log.Logger;
import dave.util.log.Severity;

public abstract class RemoteRelay extends BaseRelay
{
	private final ExecutorService mAsync;
	private final Server mServer;
	private final ConnectionManager mConnections;
	private final Consumer<Message> mCallback;
	
	public RemoteRelay(int port, Consumer<Message> f)
	{
		super("remote");
		
		mAsync = Executors.newCachedThreadPool();
		mServer = listen(port);
		mConnections = new ConnectionManager();
		mCallback = f;
	}
	
	protected abstract Connection connect(InetSocketAddress a);
	protected abstract Server listen(int port);
	
	protected void add(Connection c)
	{
		mAsync.submit(() -> {
			InetSocketAddress r = c.participants()[1];
			
			try
			{
				Message initial = Message.load(c.receive());
				String id = computeId(initial);
				
				mConnections.add(id, c);
				
				updateConnections(id, initial, r);
				
				try
				{
					while(true)
					{
						Message p = Message.load(c.receive());
						
						updateConnections(computeId(p), p, r);
						
						mCallback.accept(p);
					}
				}
				catch(IOException e) { }
			}
			catch(IOException e)
			{
				LOG.log(Severity.ERROR, "IO Error while receiving data from %s!", r);
			}
		});
	}
	
	@Override
	public void start()
	{
		mServer.start();
	}
	
	@Override
	public void stop()
	{
		mAsync.shutdown();
		mServer.stop();
	}

	@Override
	public void accept(Message t)
	{
		InetSocketAddress a = ((RemoteAddress) t.to).getRemote();
		String id = computeId(t);
		Connection c = mConnections.get(id, a);
		
		if(c == null)
		{
			mConnections.add(id, c = connect(a));
		}
		
		updateConnections(id, t, a);
		
		try
		{
			c.send(t.save());
		}
		catch (IOException e)
		{
			LOG.log(Severity.ERROR, "Failed to send packet to %s: %s! | %s", a, e.getMessage(), t);
		}
	}
	
	private void updateConnections(String id, Message t, InetSocketAddress a)
	{
		if(t.task.isFinal())
		{
			mConnections.remove(id, a);
		}
		
		mConnections.update();
		
	}
	
	private static String computeId(Message t)
	{
		String a = t.from.getID();
		String b = t.to.getID();
		String v = t.task.getTopic();
		
		if(a.compareTo(b) < 0)
		{
			String x = a; a = b; b = x;
		}
		
		return a + "->" + b + ":" + v;
	}
	
	private static final Logger LOG = Logger.get("r-relay");
}
