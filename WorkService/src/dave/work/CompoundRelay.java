package dave.work;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Predicate;

import dave.util.log.Logger;
import dave.util.log.Severity;

public class CompoundRelay extends BaseRelay
{
	private final Map<Predicate<Address>, Relay> mRelays;
	
	public CompoundRelay()
	{
		super("compound");
		
		mRelays = new HashMap<>();
	}
	
	public void register(Predicate<Address> f, Relay r)
	{
		mRelays.put(f, r);
	}

	@Override
	public void accept(Message t)
	{
		LOG.log("Routing %s", t);
		
		Optional<Relay> r = mRelays.entrySet().stream()
			.filter(e -> e.getKey().test(t.to))
			.map(e -> e.getValue())
			.findFirst();
		
		if(r.isPresent())
		{
			r.get().accept(t);
		}
		else
		{
			LOG.log(Severity.ERROR, "No relay for %s!", t);
		}
	}
	
	private static final Logger LOG = Logger.get("c-relay");
}
