package dave.work;

import java.util.function.Consumer;

import dave.util.Actor;
import dave.util.Identifiable;

public interface Relay extends Identifiable, Actor, Consumer<Message>
{
	default void start() { }
	default void stop() { }
}
