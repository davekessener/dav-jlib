package dave.work;

import java.net.InetSocketAddress;

public class RemoteAddress implements Address
{
	private final String mID;
	private final InetSocketAddress mRemote;
	
	public RemoteAddress(String id, InetSocketAddress a)
	{
		mID = id;
		mRemote = a;
	}
	
	public String getNode() { return mID; }
	public InetSocketAddress getRemote() { return mRemote; }

	@Override
	public String getID()
	{
		return mID;
	}
	
	@Override
	public String toString()
	{
		return mID + "@[" + mRemote.getHostString() + "]:" + mRemote.getPort();
	}
	
	@Override
	public int hashCode()
	{
		return (mID.hashCode() * 3) ^ (mRemote.hashCode() * 13);
	}
	
	@Override
	public boolean equals(Object o)
	{
		if(o instanceof RemoteAddress)
		{
			RemoteAddress a = (RemoteAddress) o;
			
			return mID.equals(a.mID) && mRemote.equals(a.mRemote);
		}
		
		return false;
	}
}
