package dave.work;

public class LocalAddress implements Address
{
	private final String mID;
	
	public LocalAddress(String id)
	{
		mID = id;
	}
	
	@Override
	public String getID()
	{
		return mID;
	}
	
	@Override
	public String toString()
	{
		return mID;
	}
	
	@Override
	public int hashCode()
	{
		return mID.hashCode() * 41;
	}
	
	@Override
	public boolean equals(Object o)
	{
		if(o instanceof LocalAddress)
		{
			LocalAddress a = (LocalAddress) o;
			
			return mID.equals(a.mID);
		}
		
		return false;
	}
}
