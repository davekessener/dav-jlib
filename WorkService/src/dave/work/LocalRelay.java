package dave.work;

import java.util.HashMap;
import java.util.Map;

import dave.util.log.Logger;
import dave.util.log.Severity;

public class LocalRelay extends BaseRelay
{
	private final Map<String, Node> mNodes;
	
	public LocalRelay()
	{
		super("local");
		
		mNodes = new HashMap<>();
	}
	
	@Override
	public void accept(Message t)
	{
		LocalAddress a = (LocalAddress) t.to;
		Node n = mNodes.get(a.getID());
		
		if(n != null)
		{
			n.accept(t);
		}
		else
		{
			LOG.log(Severity.ERROR, "No such node '%s'! (%s)", a.getID(), t);
		}
	}
	
	private static final Logger LOG = Logger.get("l-relay");
}
