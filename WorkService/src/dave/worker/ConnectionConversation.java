package dave.worker;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

import javax.net.ssl.SSLContext;

import dave.json.JsonValue;
import dave.json.SevereIOException;
import dave.net.server.Connection;
import dave.net.server.TCPConnection;
import dave.security.SecurityUtils;
import dave.util.ThreadedConversation;
import dave.util.Transmitter;
import dave.util.relay.Overloaded;

public class ConnectionConversation extends ThreadedConversation<InetSocketAddress, JsonValue>
{
	public ConnectionConversation(SSLContext ssl)
	{
		super(a -> new ConnectionAdapter(new TCPConnection(SecurityUtils.connect(ssl, a.getHostString(), a.getPort()), new DiscriminatingTransceiver())));
	}
	
	public Sequence join(Connection c) { return super.join(new ConnectionAdapter(c)); }
	
	@Override
	protected Sequence create(Transmitter<JsonValue> c, Consumer<? super JsonValue> f)
	{
		return new CSequence(c, f);
	}
	
	public class CSequence extends Sequence
	{
		public CSequence(Transmitter<JsonValue> c, Consumer<? super JsonValue> e)
		{
			super(c, e);
		}
		
		@Overloaded
		public Sequence await(String t, BiConsumer<Sequence, JsonValue> f)
		{
			return super.await(new TopicMatcher(t), f);
		}
	}
	
	private static class ConnectionAdapter implements Transmitter<JsonValue>
	{
		private final Connection mConnection;
		
		public ConnectionAdapter(Connection c)
		{
			mConnection = c;
		}

		@Override
		public void send(JsonValue v)
		{
			try
			{
				mConnection.send(v);
			}
			catch(IOException e)
			{
				throw new SevereIOException(e);
			}
		}
	
		@Override
		public JsonValue receive()
		{
			try
			{
				return mConnection.receive();
			}
			catch(IOException e)
			{
				throw new SevereIOException(e);
			}
		}
		
		@Override
		public void destroy()
		{
			try
			{
				mConnection.close();
			}
			catch(IOException e)
			{
				throw new SevereIOException(e);
			}
		}
	}
}
