package dave.worker;

import java.io.File;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.security.KeyStore;

import javax.net.ssl.SSLContext;

import dave.net.server.Connection;
import dave.net.server.TCPConnection;
import dave.security.SecurityUtils;

public interface WorkConfig
{
	public static final int SERVICE_PORT = 10000;
	
	String password( );
	String keystore( );

	default String plainPassword()
	{
		return SecurityUtils.simpleDecrypt(password());
	}

	default KeyStore getAuthStore( ) { return getKeyStore(keystore()); }
	default KeyStore getKeyStore(String fn)
	{
		return SecurityUtils.loadKeystore(new File(fn), plainPassword());
	}
	
	default SSLContext getContext( ) { return getContext(getAuthStore()); }
	default SSLContext getContext(KeyStore ks) { return getContext(ks, ks); }
	default SSLContext getContext(KeyStore auth, KeyStore trust)
	{
		return SecurityUtils.generateContext(auth, trust, plainPassword());
	}

	default Connection connect(InetSocketAddress host, SSLContext ssl) throws IOException
	{
		return new TCPConnection(SecurityUtils.connect(ssl, host.getHostString(), host.getPort()), new DiscriminatingTransceiver());
	}
}
