package dave.worker;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.file.Files;
import java.util.stream.Collectors;

import javax.net.ssl.SSLContext;

import dave.json.JsonObject;
import dave.json.JsonValue;
import dave.json.StringBuffer;
import dave.net.NetUtils;
import dave.net.server.Connection;
import dave.net.server.DatagramServer;
import dave.net.server.SSLServer;
import dave.net.server.SSLSocketProxy;
import dave.net.server.Server;
import dave.net.server.Server.Datagram;
import dave.net.server.Server.Handler;
import dave.security.SecurityUtils;
import dave.util.SevereException;
import dave.util.Utils;

public interface ServerConfig extends WorkConfig
{
	public static enum Type
	{
		WORKER,
		MANAGER
	}
	
	String type( );
	String nic( );
	int multicast_port( );
	String multicast_group( );
	String client_library( );
	
	default JsonObject clients( )
	{
		try
		{
			File f = new File(client_library());
			String encrypted = Files.readAllLines(f.toPath()).stream().collect(Collectors.joining(""));
			String decrypted = SecurityUtils.simpleDecrypt(encrypted, password());
			
			return (JsonObject) JsonValue.read(new StringBuffer(decrypted));
		}
		catch(IOException e)
		{
			throw new SevereException(e);
		}
	}
	
	default Type getType() { return Type.valueOf(type()); }
	
	default InetAddress getMulticastGroup( )
	{
		return Utils.wrap(() -> InetAddress.getByName(multicast_group()));
	}
	
	default InetSocketAddress getMulticastAddress( )
	{
		return Utils.wrap(() -> new InetSocketAddress(NetUtils.getMulticastAddress(nic()), multicast_port()));
	}
	
	default DatagramServer<?> multicast(Handler<Datagram> f) throws IOException
	{
		return Server.createMulticastServer(getMulticastGroup(), getMulticastAddress(), f);
	}
	
	default Server listen(int port, Handler<Connection> f) throws IOException { return listen(getContext(), port, f); }
	default Server listen(SSLContext ssl, int port, Handler<Connection> f) throws IOException
	{
		return new SSLServer(new SSLSocketProxy(SecurityUtils.listen(ssl, port)), new DiscriminatingTransceiver(), f);
	}
}
