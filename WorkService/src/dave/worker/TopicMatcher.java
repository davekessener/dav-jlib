package dave.worker;

import java.util.function.Predicate;

import dave.json.JsonObject;
import dave.json.JsonString;
import dave.json.JsonValue;

public class TopicMatcher implements Predicate<JsonValue>
{
	private final String mTopic;
	
	public TopicMatcher(String t)
	{
		mTopic = t;
	}
	
	@Override
	public boolean test(JsonValue v)
	{
		if(v instanceof JsonObject)
		{
			JsonObject o = (JsonObject) v;
			
			if(o.contains("topic"))
			{
				JsonValue t = o.get("topic");
				
				if(t instanceof JsonString)
				{
					JsonString s = (JsonString) t;
					
					return s.get().equals(mTopic);
				}
			}
		}
		
		return false;
	}
}
