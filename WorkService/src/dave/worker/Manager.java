package dave.worker;

import java.io.IOException;
import java.net.InetSocketAddress;

import javax.net.ssl.SSLContext;

import dave.json.JsonBuilder;
import dave.json.JsonObject;
import dave.json.JsonString;
import dave.json.JsonValue;
import dave.json.SevereIOException;
import dave.net.server.Connection;
import dave.net.server.DatagramServer;
import dave.net.server.Server;
import dave.security.SecurityUtils;
import dave.util.Actor;
import dave.util.SevereException;
import dave.util.Utils;
import dave.util.command.CmdEngine;
import dave.util.command.SimpleCommand;
import dave.util.config.Configuration;
import dave.util.log.Logger;
import dave.util.log.Severity;
import dave.util.stream.EachWithIndex;

public class Manager implements Actor, AutoCloseable
{
	private final Configuration<ServerConfig> mConfig;
	private final DatagramServer<?> mMulticast;
	private final Server mServer;
	private final ConnectionConversation mConversation;
	private final JsonObject mOwnAddress;
	private final JsonObject mManifests;
	
	public Manager(Configuration<ServerConfig> config) throws IOException
	{
		SSLContext ssl = config.proxy().getContext();
		
		mConfig = config;
		mManifests = mConfig.proxy().clients();
		mMulticast = mConfig.proxy().multicast(datagram -> {});
		mServer = mConfig.proxy().listen(ssl, 0, this::process);
		mConversation = new ConnectionConversation(ssl);
		
		InetSocketAddress a = (InetSocketAddress) mServer.getAddress();
		
		mOwnAddress = (new JsonBuilder())
			.putString("host", a.getHostString())
			.putInt("port", a.getPort())
			.toJSON();
	}
	
	public InetSocketAddress listeningAddress( ) { return (InetSocketAddress) mServer.getAddress(); }
	public InetSocketAddress multicastAddress( ) { return (InetSocketAddress) mMulticast.getAddress(); }
	
	private void process(Connection c)
	{
		String cid = getPeerID(c);
		
		LOG.log(Severity.INFO, "Connection from %s", cid);
		
		mConversation.join(c)
			.await(new TopicMatcher(Topics.STATUS_REPLY), (q, v) -> {
				LOG.log(Severity.INFO, "From %s received %s", c.socket().getRemoteSocketAddress().toString(), v.toString());
			})
			.await(new TopicMatcher(Topics.JOB_OFFER), (q, v) -> {
				Job job = new Job(cid, (JsonObject) v);
				JsonObject entry = mManifests.getObject(cid);
				
				q.say(Topic.create(Topics.JOB_MANIFEST, entry.get("manifest")))
				.await(new TopicMatcher(Topics.JOB_MANIFEST), ($, m) -> {
					String manifest = SecurityUtils.simpleDecrypt(((JsonString) m).get(), mConfig.proxy().password());
					String hash = Utils.hash(manifest.getBytes(Utils.CHARSET));
					
					if(!hash.equals(entry.getString("hash")))
					{
						LOG.log(Severity.ERROR, "User %s manipulated manifest file!", cid);
						LOG.log(Severity.ERROR, "Expected md5 hash %s, but got %s instead!", entry.getString("hash"), hash);
						LOG.log(Severity.ERROR, ">>> BEGIN MANIFEST");
						LOG.log(Severity.ERROR, "%s", manifest);
						LOG.log(Severity.ERROR, ">>> END MANIFEST");
						
						throw new IllegalStateException("User " + cid + " manipulated manifest file!");
					}
					else
					{
						job.config().getArray("data").stream()
							.map(new EachWithIndex<>())
							.map(e -> Utils.wrap(() -> new Resource(job, e.index, e.element, c.receive())))
							.forEach(r -> job.addResource(r));
						
						JsonObject response = (new JsonBuilder()).putInt("id", job.id()).toJSON();
						q.say(Topic.create(Topics.JOB_ACCEPT, response)).run();
						
						LOG.log(Severity.INFO, "Accepted new job %d from %s.", job.id(), job.client());
					}
				})
				.run();
			})
			.run();
	}
	
	private static String getPeerID(Connection c) { return Utils.hash(WorkUtils.getKey(c).getEncoded()); }

	@Override
	public void start()
	{
		mConversation.start();
		mMulticast.start();
		mServer.start();
	}

	@Override
	public void stop()
	{
		mServer.stop();
		mMulticast.stop();
		mConversation.stop();
	}

	@Override
	public void close() throws Exception
	{
		stop();
	}
	
	public void collectStatus()
	{
		LOG.log(Severity.INFO, "Querying status ...");
		
		multicast(Topic.create(Topics.STATUS_QUERY, mOwnAddress));
	}
	
	private void multicast(JsonValue payload)
	{
		try
		{
			mMulticast.send(null, payload);
		}
		catch(IOException e)
		{
			throw new SevereIOException(e);
		}
	}
	
	public static void run(Configuration<ServerConfig> c)
	{
		try (Manager self = new Manager(c))
		{
			boolean[] running = { true };
			CmdEngine e = new CmdEngine();
			
			e.add(new SimpleCommand("quit", "stop the manager and ends the program", () -> running[0] = false));
			e.add(new SimpleCommand("status", "queries network for status", () -> self.collectStatus()));
			
			self.start();

			System.out.println("WorkService MANAGER");
			
			e.run(">", System.in, () -> running[0]);
		}
		catch(Exception e)
		{
			throw new SevereException(e);
		}
	}
	
	private static final Logger LOG = Logger.get("manager");
}
