package dave.worker;

import dave.json.JsonObject;
import dave.json.JsonValue;

public final class Topic
{
	public static JsonValue create(String topic) { return create(topic, null); }
	public static JsonValue create(String topic, JsonValue payload)
	{
		JsonObject json = new JsonObject();
		
		json.putString("topic", topic);
		json.put("content", payload);
		
		return json;
	}
	
	private Topic( ) { }
}
