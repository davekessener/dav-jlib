package dave.worker;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import dave.json.JsonValue;
import dave.json.Printer;
import dave.util.SevereException;
import dave.util.Utils;

public class JsonData extends JsonValue
{
	private final byte[] mData;
	
	public JsonData(byte[] data)
	{
		mData = data;
	}
	
	public byte[] data() { return mData; }

	@Override
	public void write(Printer out)
	{
		throw new UnsupportedOperationException();
	}
	
	@Override
	public String toString()
	{
		return String.format("{%dB binary data | MD5 %s}", mData.length, Utils.hash(mData));
	}
	
	public static JsonData fromFile(String fn)
	{
		try
		{
			return new JsonData(Files.readAllBytes((new File(fn)).toPath()));
		}
		catch(IOException e)
		{
			throw new SevereException(e);
		}
	}
}
