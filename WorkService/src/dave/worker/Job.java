package dave.worker;

import java.util.ArrayList;
import java.util.List;

import dave.json.JsonObject;
import dave.json.JsonValue;

public class Job
{
	private final Task mRoot;
	private final String mClient;
	private final JsonObject mConfig;
	private final List<Resource> mResources;
	
	public Job(String client, JsonObject conf)
	{
		mRoot = new Task(nextID());
		mClient = client;
		mConfig = conf;
		mResources = new ArrayList<>();
	}
	
	public int id( ) { return mRoot.id; }
	public String client( ) { return mClient; }
	public JsonObject config( ) { return mConfig; }
	
	public void addResource(Resource r)
	{
		mResources.add(r);
	}
	
	public static enum State
	{
		SCHEDULED,
		RUNNING,
		CANCELED,
		ERROR,
		DONE
	}
	
	public static class Task
	{
		public final int id;
		public final List<Task> children;
		public JsonValue result;
		public State state;
		
		public Task(int id)
		{
			this.id = id;
			this.children = new ArrayList<>();
			this.state = State.SCHEDULED;
		}
	}
	
	private static synchronized int nextID() { return sJID++; }
	
	private static int sJID = 0;
}
