package dave.worker;

import dave.util.Actor;
import dave.util.Utils;

public class Agent implements Actor
{
	private final Thread mThread;
	private boolean mRunning;
	
	public Agent()
	{
		mThread = new Thread(this::run);
		mRunning = false;
	}

	@Override
	public void start()
	{
		mRunning = true;
		mThread.start();
	}

	@Override
	public void stop()
	{
		mRunning = false;
		
		Utils.wrap(() -> mThread.join());
	}

	private void run()
	{
		while(mRunning)
		{
		}
	}
}
