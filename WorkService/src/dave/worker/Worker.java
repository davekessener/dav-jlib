package dave.worker;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

import dave.json.JsonObject;
import dave.json.JsonValue;
import dave.net.server.DatagramServer;
import dave.net.server.Server.Datagram;
import dave.util.Actor;
import dave.util.SevereException;
import dave.util.command.CmdEngine;
import dave.util.command.SimpleCommand;
import dave.util.config.Configuration;
import dave.util.log.Logger;

public class Worker implements Actor, AutoCloseable
{
	private final Configuration<ServerConfig> mConfig;
	private final DatagramServer<?> mMulticast;
	private final ConnectionConversation mConversation;
	private final Map<String, Consumer<Datagram>> mCallbacks;
	
	public Worker(Configuration<ServerConfig> config) throws IOException
	{
		mConfig = config;
		
		mMulticast = mConfig.proxy().multicast(this::handleDatagram);
		mConversation = new ConnectionConversation(mConfig.proxy().getContext());
		mCallbacks = new HashMap<>();
		
		mCallbacks.put(Topics.STATUS_QUERY, this::handleStatus);
	}
	
	private void handleDatagram(Datagram p)
	{
		mCallbacks.entrySet().stream()
			.filter(e -> (new TopicMatcher(e.getKey())).test(p.payload))
			.map(e -> e.getValue())
			.findFirst()
			.orElseThrow(() -> new UnsupportedOperationException(p.payload.toString()))
			.accept(p);
	}
	
	private void handleStatus(Datagram p)
	{
		LOG.log("Received status request from %s", p.source);
		
		JsonObject json = (JsonObject) p.payload;
		JsonObject remote = json.getObject("content");
		InetAddress host = p.source.getAddress();
		int port = remote.getInt("port");
		
		mConversation.start(new InetSocketAddress(host, port))
			.say(Topic.create(Topics.STATUS_REPLY, status()))
			.run();
	}
	
	private JsonValue status()
	{
		JsonObject status = new JsonObject();
		
		status.putString("message", "everything is ok");
		
		return status;
	}
	
	@Override
	public void start()
	{
		mConversation.start();
		mMulticast.start();
	}
	
	@Override
	public void stop()
	{
		mMulticast.stop();
		mConversation.stop();
	}
	
	@Override
	public void close() throws Exception
	{
		stop();
	}
	
	public static void run(Configuration<ServerConfig> c)
	{
		try (Worker self = new Worker(c))
		{
			boolean[] running = { true };
			CmdEngine e = new CmdEngine();
			
			e.add(new SimpleCommand("quit", "stop the worker and ends the program", () -> running[0] = false));
			
			self.start();
			System.out.println("WorkService WORKER");
			
			e.run(">", System.in, () -> running[0]);
		}
		catch(Exception e)
		{
			throw new SevereException(e);
		}
	}
	
	private static final Logger LOG = Logger.get("worker");
}
