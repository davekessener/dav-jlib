package dave.worker;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import dave.arguments.Arguments;
import dave.arguments.Option;
import dave.arguments.Option.OptionBuilder;
import dave.arguments.Parser;
import dave.json.JsonUtils;
import dave.json.PrettyPrinter;
import dave.security.SecurityUtils;
import dave.util.ShutdownService;
import dave.util.TransformingConsumer;
import dave.util.config.Configuration;
import dave.util.log.LogBase;
import dave.util.log.SimpleFormatter;
import dave.util.log.Spacer;
import dave.util.Utils;
import dave.util.ShutdownService.Priority;

public class Start
{
	public static void main(String[] args)
	{
		LogBase.INSTANCE.registerSink(e -> true, new TransformingConsumer<>(new Spacer(v -> System.out.println(v), 1500, Utils.repeat("=", 200)), new SimpleFormatter()));
		LogBase.INSTANCE.start();
		
		ShutdownService.INSTANCE.register(Priority.LAST, () -> LogBase.INSTANCE.stop());
		
		try
		{
			run(args);
		}
		catch(Throwable e)
		{
			e.printStackTrace();
		}
		
		ShutdownService.INSTANCE.shutdown();
	}
	
	private static void run(String[] args) throws Exception
	{
		Option o_setup = (new OptionBuilder("setup")).hasValue(true).build();
		Option o_config = (new OptionBuilder("config")).setDefault(CONFIG).build();
		Parser p = new Parser(o_setup, o_config);
		Arguments a = p.parse(args);
		String config_fn = a.getArgument(o_config);
		
		if(a.hasArgument(o_setup))
		{
			String pw = a.getArgument(o_setup);
			
			System.out.println("Generating config file " + config_fn + " with password " + pw + ".");
			
			setup(config_fn, pw);
		}
		else
		{
			Configuration<ServerConfig> config = readConfig(config_fn);
			
			switch(config.proxy().getType())
			{
				case WORKER:
					startWorker(config);
					break;
					
				case MANAGER:
					startManager(config);
					break;
					
				default:
					throw new IllegalStateException("" + config.proxy().type());
			}
		}
	}
	
	private static void setup(String fn, String pw) throws IOException
	{
		Configuration<ServerConfig> c = new Configuration<>(ServerConfig.class);
		
		if(pw.length() < 6)
			throw new IllegalArgumentException("Password must be at least 6 characters long!");
		
		c.set("type", ServerConfig.Type.WORKER.toString());
		c.set("password", SecurityUtils.simpleEncrypt(pw));
		c.set("keystore", KEYSTORE);
		c.set("nic", NIC);
		c.set("multicast_port", MC_PORT);
		c.set("multicast_group", GROUP);

		try (FileWriter out = new FileWriter(new File(fn)))
		{
			out.write(c.save().toString(new PrettyPrinter()));
		}
	}
	
	private static void startWorker(Configuration<ServerConfig> config)
	{
		Worker.run(config);
	}
	
	private static void startManager(Configuration<ServerConfig> config)
	{
		Manager.run(config);
	}
	
	private static Configuration<ServerConfig> readConfig(String fn) throws IOException
	{
		Configuration<ServerConfig> config = new Configuration<>(ServerConfig.class);
		
		config.load(JsonUtils.fromFile(new File(fn)));
		
		return config;
	}
	
	private static final String KEYSTORE = "auth.jks";
	private static final String NIC = "eth0";
	private static final int MC_PORT = 9999;
	private static final String GROUP = "239.0.0.0";
	private static final String CONFIG = "config.json";
}
