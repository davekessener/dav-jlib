package dave.worker;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.Map;

import dave.json.JsonObject;
import dave.json.JsonValue;
import dave.net.server.Connection;
import dave.net.server.Server;
import dave.net.server.TCPServer;
import dave.net.server.TCPSocketProxy;
import dave.net.server.SimpleTransceiver;
import dave.util.Actor;
import dave.util.log.Logger;
import dave.util.log.Severity;

public abstract class ServiceProvider implements Actor, AutoCloseable
{
	private final Server mServer;
	private final Map<String, Callback> mCallbacks;
	private boolean mRunning;
	
	public static interface Callback { void handle(Connection c, JsonValue v) throws IOException; }
	public static interface Wrap { void handle(JsonValue v) throws IOException; }
	
	protected ServiceProvider(int port) throws IOException
	{
		InetSocketAddress a = new InetSocketAddress(InetAddress.getLoopbackAddress(), port);
		
		mServer = new TCPServer(new TCPSocketProxy(a), new SimpleTransceiver(), this::handle);
		mCallbacks = new HashMap<>();
		mRunning = false;
	}
	
	public boolean running() { return mRunning; }
	public void shutdown() { mRunning = false; }
	
	protected void register(String cmd, Wrap f) { register(cmd, (c, v) -> f.handle(v)); }
	protected void register(String cmd, Callback f) { mCallbacks.put(cmd, f); }
	
	private void handle(Connection c)
	{
		if(!running())
			return;
		
		try
		{
			JsonObject o = (JsonObject) c.receive();
	
			Callback cb = mCallbacks.get(o.getString("command"));
			
			if(cb == null)
			{
				LOG.log(Severity.ERROR, "Received invalid instruction: %s", o);
			}
			else
			{
				cb.handle(c, o);
			}
		}
		catch(IOException e)
		{
			LOG.log(Severity.ERROR, "I/O Error while talking to %s!", c.socket().getRemoteSocketAddress());
		}
	}

	@Override
	public void start()
	{
		mRunning = true;
		mServer.start();
	}

	@Override
	public void stop()
	{
		mRunning = false;
		mServer.stop();
	}

	@Override
	public void close() throws Exception
	{
		stop();
	}
	
	private static final Logger LOG = Logger.get("service");
}
