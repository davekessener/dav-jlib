package dave.worker;

public final class Topics
{
	public static final String STATUS_QUERY = "status.query";
	public static final String STATUS_REPLY = "status.reply";
	
	public static final String JOB_OFFER    = "job.offer";
	public static final String JOB_ACCEPT   = "job.accept";
	public static final String JOB_QUERY    = "job.query";
	public static final String JOB_STATUS   = "job.status";
	public static final String JOB_RESULT   = "job.result";
	public static final String JOB_CANCEL   = "job.cancel";
	public static final String JOB_MANIFEST = "job.manifest";

	public static final String TASK_NOTIFY  = "task.notify";
	public static final String TASK_OFFER   = "task.offer";
	public static final String TASK_ACCEPT  = "task.accept";
	public static final String TASK_DECLINE = "task.decline";
	public static final String TASK_QUERY   = "task.query";
	public static final String TASK_STATUS  = "task.status";
	public static final String TASK_CANCEL  = "task.cancel";
	public static final String TASK_RESULT  = "task.result";
	
	public static final class Service
	{
		public static final String STATUS_QUERY = "service.status.query";
		public static final String STATUS_REPLY = "service.status.reply";
		
		private Service( ) { }
	}
	
	private Topics( ) { }
}
