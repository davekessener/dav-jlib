package dave.worker;

import java.net.Socket;
import java.security.PublicKey;

import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSocket;

import dave.net.server.Connection;
import dave.util.SevereException;

public final class WorkUtils
{
	public static PublicKey getKey(Connection c)
	{
		Socket s = c.socket();
		
		if(!(s instanceof SSLSocket))
			throw new IllegalArgumentException();
		
		try
		{
			return ((SSLSocket) s).getSession().getPeerCertificates()[0].getPublicKey();
		}
		catch(SSLPeerUnverifiedException e)
		{
			throw new SevereException(e);
		}
	}
	
	private WorkUtils( ) { }
}
