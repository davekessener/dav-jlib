package dave.worker;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import dave.json.JsonObject;
import dave.json.JsonValue;

public class Resource
{
	public final Type type;
	public final Path file;
	
	public Resource(Job job, int idx, JsonValue i, JsonValue d) throws IOException
	{
		JsonObject info = (JsonObject) i;
		JsonData data = (JsonData) d;
		
		type = Type.valueOf(info.getString("type"));
		file = Files.createTempFile("res_" + job.id() + "_" + idx + "_" + job.client() + "_", "_" + System.currentTimeMillis());
		
		Files.write(file, data.data());
	}
	
	public static enum Type
	{
		PROGRAM,
		DATA
	}
}
