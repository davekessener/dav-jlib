package dave.worker;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import dave.json.JsonValue;
import dave.net.server.SimpleTransceiver;

public class DiscriminatingTransceiver extends SimpleTransceiver
{
	@Override
	public void send(OutputStream out, JsonValue json) throws IOException
	{
		if(json instanceof JsonData)
		{
			byte[] data = ((JsonData) json).data();
			int l = data.length;
			
			out.write(FLAG_DATA);
			out.write(l);
			out.write(l >> 8);
			out.write(l >> 16);
			out.write(l >> 24);
			out.write(data);
		}
		else
		{
			out.write(FLAG_JSON);
			
			super.send(out, json);
		}
	}

	@Override
	public JsonValue receive(InputStream in) throws IOException
	{
		switch(in.read())
		{
			case FLAG_JSON:
				return super.receive(in);
				
			case FLAG_DATA:
				return receiveData(in);
				
			default:
				throw new IOException("Corrupted data!");
		}
	}
	
	private JsonValue receiveData(InputStream in) throws IOException
	{
		int l = 0;

		l |= in.read();
		l |= in.read() << 8;
		l |= in.read() << 16;
		l |= in.read() << 24;
		
		byte[] data = new byte[l];
		
		in.read(data);
		
		return new JsonData(data);
	}
	
	private static final byte FLAG_JSON = 0x77;
	private static final byte FLAG_DATA = 0x2A;
}
