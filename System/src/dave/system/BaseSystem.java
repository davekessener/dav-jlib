package dave.system;

import java.util.List;

import dave.net.NetUtils;
import dave.system.common.Unit;
import dave.system.net.Relay;
import dave.system.task.Task;
import dave.util.Actor;

public abstract class BaseSystem implements Actor
{
	private final Relay<Task> mNetwork;
	private final List<Unit> mUnits;
	
	protected BaseSystem(Relay<Task> network, List<Unit> units)
	{
		mNetwork = network;
		mUnits = units;
	}
	
	protected Relay<Task> getNetwork( )
	{
		return mNetwork;
	}
	
	@Override
	public void start( )
	{
		mNetwork.start();
		
		NetUtils.sleep(DELAY);
		
		for(Unit u : mUnits)
		{
			u.beforeStart();
		}
		
		NetUtils.sleep(DELAY);
		
		for(Unit u : mUnits)
		{
			u.start();
		}
		
		NetUtils.sleep(DELAY);
		
		for(Unit u : mUnits)
		{
			u.afterStart();
		}
	}
	
	@Override
	public void stop( )
	{
		for(Unit u : mUnits)
		{
			u.beforeStop();
		}
		
		NetUtils.sleep(DELAY);

		for(Unit u : mUnits)
		{
			u.stop();
		}
		
		NetUtils.sleep(DELAY);
		
		for(Unit u : mUnits)
		{
			u.afterStop();
		}
		
		NetUtils.sleep(DELAY);
		
		mNetwork.stop();
	}
	
	private static int DELAY = 1000;
}
