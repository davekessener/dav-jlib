package dave.system.db;

import java.util.HashSet;
import java.util.Set;

import dave.json.JSON;
import dave.system.common.Database;
import dave.util.Identifiable;
import dave.util.log.Logger;
import dave.util.log.Severity;
import dave.util.persistence.Storage;

public class StorageDatabase<T extends Identifiable> implements Database<T>
{
	private final Storage mBuffer;
	private final Set<String> mTmp = new HashSet<>();
	
	public StorageDatabase(Storage b)
	{
		mBuffer = b;
	}
	
	@Override
	public boolean has(String id)
	{
		return mBuffer.has(id);
	}

	@Override
	public void store(T dna)
	{
		mBuffer.store(dna.getID(), JSON.serialize(dna));
		mTmp.add(dna.getID());
		
		Logger.DEFAULT.log("StorageDB saved %s [%d]", dna.getID(), mTmp.size());
	}

	@SuppressWarnings("unchecked")
	@Override
	public T retrieve(String id)
	{
		T r = null;
		
		if(mBuffer.has(id))
		{
			r = (T) JSON.deserialize(mBuffer.retrieve(id));
		}
		
		return r;
	}

	@Override
	public void remove(String id)
	{
		if(mBuffer.has(id))
		{
			mBuffer.remove(id);
			mTmp.remove(id);
			
			Logger.DEFAULT.log("StorageDB removed %s [%d]", id, mTmp.size());
		}
		else
		{
			Logger.DEFAULT.log(Severity.WARNING, "Tried to remove unknown %s!", id);
		}
	}
}
