package dave.system.db;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;

import dave.json.JSON;
import dave.json.JsonArray;
import dave.json.JsonString;
import dave.system.common.Database;
import dave.util.Identifiable;
import dave.util.persistence.Storage;

public class FixedDatabase<T extends Identifiable> implements Database<T>, Cloneable
{
	private final List<T> mBuffer;
	private final Consumer<T> mDrain;
	private final int mCapacity;
	
	public FixedDatabase(int c) { this(c, o -> {}); }
	public FixedDatabase(int c, Consumer<T> drain)
	{
		mBuffer = new LinkedList<>();
		mDrain = drain;
		mCapacity = c;
	}
	
	private FixedDatabase(FixedDatabase<T> s)
	{
		mBuffer = new LinkedList<>();
		mDrain = s.mDrain;
		mCapacity = s.mCapacity;
		
		mBuffer.addAll(s.mBuffer);
	}
	
	@Override
	public FixedDatabase<T> clone( )
	{
		return new FixedDatabase<>(this);
	}
	
	@Override
	public void save(Storage io)
	{
		JsonArray ids = new JsonArray();
		
		mBuffer.stream().forEach(e -> {
			final String id = e.getID();
			
			ids.add(new JsonString(id));
			io.store(id, JSON.serialize(e));
		});
		
		io.store("content", ids);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void load(Storage io)
	{
		if(io.has("content"))
		{
			mBuffer.clear();
			
			((JsonArray) io.retrieve("content")).stream().forEach(e -> mBuffer.add((T) JSON.deserialize(io.retrieve(((JsonString) e).get()))));
		}
	}
	
	@Override
	public boolean has(String id)
	{
		return retrieve(id) != null;
	}

	@Override
	public void store(T dna)
	{
		mBuffer.remove(dna);
		mBuffer.add(dna);
		
		if(mBuffer.size() > mCapacity)
		{
			mDrain.accept(mBuffer.remove(0));
		}
	}

	@Override
	public T retrieve(String id)
	{
		for(Iterator<T> i = mBuffer.iterator() ; i.hasNext() ;)
		{
			T o = i.next();
			
			if(o.getID().equals(id))
			{
				i.remove();
				mBuffer.add(o);
				
				return o;
			}
		}
		
		return null;
	}

	@Override
	public void remove(String id)
	{
		T o = retrieve(id);
		
		if(o != null)
		{
			mBuffer.remove(o);
		}
	}
}
