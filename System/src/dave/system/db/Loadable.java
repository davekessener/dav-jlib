package dave.system.db;

import dave.util.persistence.Storage;

public interface Loadable
{
	public abstract void load(Storage io);
}
