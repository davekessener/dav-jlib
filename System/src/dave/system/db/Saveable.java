package dave.system.db;

import dave.util.persistence.Storage;

public interface Saveable
{
	public abstract void save(Storage io);
}
