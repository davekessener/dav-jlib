package dave.system.db;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import dave.system.common.Database;
import dave.system.common.Manager;
import dave.system.common.Sink;
import dave.system.common.Source;
import dave.util.Identifiable;
import dave.util.persistence.Storage;

public class ModularDatabase<T extends Identifiable> implements Database<T>
{
	private final List<Sink<T>> mSinks = new ArrayList<>();
	private final List<Source<T>> mSources = new ArrayList<>();
	private final List<Manager<T>> mManagers = new ArrayList<>();
	private final List<Saveable> mSavers = new ArrayList<>();
	private final List<Loadable> mLoaders = new ArrayList<>();
	
	@SafeVarargs
	public final ModularDatabase<T> addSinks(Sink<T> ... s)
	{
		mSinks.addAll(Arrays.asList(s));
		
		return this;
	}
	
	@SafeVarargs
	public final ModularDatabase<T> addSources(Source<T> ... s)
	{
		mSources.addAll(Arrays.asList(s));
		
		return this;
	}
	
	@SafeVarargs
	public final ModularDatabase<T> addManager(Manager<T> ... m)
	{
		mManagers.addAll(Arrays.asList(m));
		
		return this;
	}
	
	public final ModularDatabase<T> addSavers(Saveable ... s)
	{
		mSavers.addAll(Arrays.asList(s));
		
		return this;
	}
	
	public final ModularDatabase<T> addLoaders(Loadable ... l)
	{
		mLoaders.addAll(Arrays.asList(l));
		
		return this;
	}
	
	@Override
	public boolean has(String id)
	{
		for(Source<T> s : mSources)
		{
			if(s.has(id))
			{
				return true;
			}
		}
		
		return false;
	}

	@Override
	public void store(T dna)
	{
		for(Sink<T> s : mSinks)
		{
			s.store(dna);
		}
	}

	@Override
	public T retrieve(String id)
	{
		T o = null;
		
		for(Source<T> s : mSources)
		{
			if((o = s.retrieve(id)) != null) break;
		}
		
		return o;
	}

	@Override
	public void remove(String id)
	{
		for(Manager<T> m : mManagers)
		{
			m.remove(id);
		}
	}
	
	@Override
	public void save(Storage io)
	{
		for(Saveable s : mSavers)
		{
			s.save(io);
		}
	}
	
	@Override
	public void load(Storage io)
	{
		for(Loadable l : mLoaders)
		{
			l.load(io);
		}
	}
}
