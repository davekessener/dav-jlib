package dave.system.db;

import dave.system.common.Sink;
import dave.system.common.Source;
import dave.util.Identifiable;

public class BubbleSource<T extends Identifiable> implements Source<T>
{
	private final Sink<T> mMain;
	private final Source<T> mReserve;
	
	public BubbleSource(Sink<T> main, Source<T> reserve)
	{
		mMain = main;
		mReserve = reserve;
	}
	
	@Override
	public T retrieve(String id)
	{
		T o = mReserve.retrieve(id);
		
		if(o != null)
		{
			mMain.store(o);
		}
		
		return o;
	}

	@Override
	public boolean has(String id)
	{
		return mReserve.has(id);
	}
}
