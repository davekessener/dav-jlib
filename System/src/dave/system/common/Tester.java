package dave.system.common;

import dave.nn.dna.DNA;

public interface Tester
{
	public abstract int score(DNA dna);
}
