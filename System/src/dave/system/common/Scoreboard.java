package dave.system.common;

import dave.json.Loadable;
import dave.json.Saveable;
import dave.system.Result;
import dave.util.Streamable;

public interface Scoreboard extends Streamable<Result>, Saveable, Loadable
{
	public abstract String update(Result r);
	public abstract String get(float p);
	public abstract int highScore( );
	public abstract int size( );
	public abstract boolean remove(String id);
	public abstract boolean contains(String id);
	public abstract void cull( );
}
