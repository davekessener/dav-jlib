package dave.system.common;

import dave.util.Identifiable;

public interface Manager<T extends Identifiable>
{
	public abstract void remove(String id);
}
