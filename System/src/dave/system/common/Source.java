package dave.system.common;

import dave.util.Identifiable;

public interface Source<T extends Identifiable>
{
	public abstract T retrieve(String id);
	public abstract boolean has(String id);
}
