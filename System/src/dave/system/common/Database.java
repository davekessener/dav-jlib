package dave.system.common;

import dave.system.db.Loadable;
import dave.system.db.Saveable;
import dave.util.Identifiable;
import dave.util.persistence.Storage;

public interface Database<T extends Identifiable> extends Sink<T>, Source<T>, Manager<T>, Saveable, Loadable
{
	public default void save(Storage io) { }
	public default void load(Storage io) { }
}
