package dave.system.common;

import dave.util.Identifiable;

public interface Sink<T extends Identifiable>
{
	public abstract void store(T o);
}
