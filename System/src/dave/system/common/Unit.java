package dave.system.common;

import java.util.function.Consumer;

import dave.system.net.Node;
import dave.system.net.Packet;
import dave.system.task.Task;
import dave.util.Actor;

public interface Unit extends Consumer<Packet<Task>>, Actor
{
	public abstract Node<Task> getNode( );
	
	public default void beforeStart( ) { }
	public default void afterStart( ) { }
	public default void beforeStop( ) { }
	public default void afterStop( ) { }
}
