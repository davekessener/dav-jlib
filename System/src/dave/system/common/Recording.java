package dave.system.common;

import dave.util.Streamable;

public interface Recording extends Streamable<boolean[]>
{
	public abstract void update(int tc, boolean[] e);
}
