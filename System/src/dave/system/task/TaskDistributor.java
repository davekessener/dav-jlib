package dave.system.task;

import java.util.function.Consumer;

import dave.util.log.Logger;
import dave.system.net.Packet;

public class TaskDistributor implements Consumer<Packet<Task>>
{
	private final TaskMap<Consumer<Packet<Task>>> mProcessors;
	
	public TaskDistributor( ) { this(new LoggingConsumer<Packet<Task>>(Logger.DEFAULT)); }
	public TaskDistributor(Consumer<Packet<Task>> d)
	{
		mProcessors = new TaskMap<>(d);
	}

	protected void preProcessCallback( ) { }
	protected void postProcessCallback( ) { }
	
	public void registerPacketHandler(String task, Consumer<Packet<Task>> p) { mProcessors.put(task, null, p); }
	public void registerPacketHandler(String task, String action, Consumer<Packet<Task>> p) { mProcessors.put(task, action, p); }

	@Override
	public void accept(Packet<Task> p)
	{
		preProcessCallback();
		mProcessors.get(p.getContent()).accept(p);
		postProcessCallback();
	}
}
