package dave.system.task;

import java.util.function.Consumer;

import dave.util.log.Logger;
import dave.util.log.Severity;
import dave.system.unit.PublisherUnit.Message;

public class TaskBasedMessageProcessor implements Consumer<Message>
{
	private final TaskMap<Consumer<Message>> mProcessors;
	
	public TaskBasedMessageProcessor( ) { this(new LoggingConsumer<Message>(Logger.DEFAULT)); }
	public TaskBasedMessageProcessor(Consumer<Message> d)
	{
		mProcessors = new TaskMap<>(d);
	}
	
	public TaskBasedMessageProcessor register(String task, Consumer<Message> f)
	{
		mProcessors.put(task, null, f);
		
		return this;
	}
	
	public TaskBasedMessageProcessor register(String task, String action, Consumer<Message> f)
	{
		mProcessors.put(task, action, f);
		
		return this;
	}

	@Override
	public void accept(Message msg)
	{
		if(!(msg.data instanceof Task))
		{
			Logger.DEFAULT.log(Severity.ERROR, "Not a task-based message: %s!", msg);
		}
		else
		{
			mProcessors.get((Task) msg.data).accept(msg);
		}
	}
}
