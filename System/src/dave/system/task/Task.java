package dave.system.task;

import java.util.Objects;

import dave.json.Container;
import dave.json.JSON;
import dave.json.JsonObject;
import dave.json.JsonValue;
import dave.json.Loader;
import dave.json.Saveable;
import dave.json.Saver;
import dave.util.Identifiable;

@Container
public class Task implements Identifiable, Saveable
{
	private final String mID;
	private final String mAction;
	private final long mSession;
	private final Object mData;
	
	public Task(String id, String action, long session) { this(id, action, session, null); }
	public Task(String id, String action, long session, Object data)
	{
		mID = id;
		mAction = action;
		mSession = session;
		mData = data;
		
		if(!JSON.canSerialize(mData))
			throw new IllegalArgumentException("" + mData);
	}
	
	public Task(Task t, String action) { this(t, action, null); }
	public Task(Task t, String action, Object data)
	{
		mID = t.getID();
		mAction = action;
		mSession = t.getSession();
		mData = data;
		
		if(!JSON.canSerialize(mData))
			throw new IllegalArgumentException("" + mData);
	}
	
	@Override
	@Saver
	public JsonValue save( )
	{
		JsonObject json = new JsonObject();
		
		json.putString("id", mID);
		json.putString("action", mAction);
		json.putLong("session", mSession);
		json.put("data", JSON.serialize(mData));
		
		return json;
	}
	
	@Loader
	public static Task load(JsonValue json)
	{
		JsonObject o = (JsonObject) json;
		
		String id = o.getString("id");
		String action = o.getString("action");
		Long session = o.getLong("session");
		Object data = JSON.deserialize(o.get("data"));
		
		return new Task(id, action, session, data);
	}
	
	@Override
	public int hashCode( )
	{
		return mID.hashCode() ^ mAction.hashCode() ^ ((int) mSession) ^ ((int) (mSession >> 32)) ^ mData.hashCode();
	}
	
	@Override
	public boolean equals(Object o)
	{
		if(o instanceof Task)
		{
			Task t = (Task) o;
			
			return t.getID().equals(mID) &&
					t.getAction().equals(mAction) &&
					t.getSession() == mSession &&
					Objects.deepEquals(t.getData(), mData);
		}
		
		return false;
	}
	
	@Override
	public String toString( )
	{
		return toString(true);
	}
	
	public String toString(boolean explicit)
	{
		String d = "" + mData;
		
		if(d.length() > 100) d = d.substring(0, 100) + " ...";
		
		return explicit ? String.format("%s (%s) [%d] '%s'", mID, mAction, mSession, d) : mID;
	}
	
	@Override
	public String getID( )
	{
		return mID;
	}

	public String getAction( )
	{
		return mAction;
	}

	public long getSession( )
	{
		return mSession;
	}

	public Object getData( )
	{
		return mData;
	}
}
