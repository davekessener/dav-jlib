package dave.system.task;

public final class Tasks
{
	public static final class FetchDNA
	{
		public static final String ID = "fetch-dna";
		
		public static final String ACTION_REQUEST = "request";
		public static final String ACTION_OFFER = "offer";
		public static final String ACTION_SOLICIT = "solicit";
		public static final String STATUS_INVALID = "invalid-id";
		
		private FetchDNA( ) { }
	}
	
	public static final class Score
	{
		public static final String ID = "query-score";
		
		public static final String ACTION_REQUEST = "request";
		public static final String ACTION_OFFER = "offer";
		public static final String ACTION_SOLICIT = "solicit";
		public static final String ACTION_CULL = "cull";
		public static final String STATUS_EMPTY = "empty";
		
		private Score( ) { }
	}
	
	public static final class QueryDB
	{
		public static final String ID = "query-db";
		
		public static final String ACTION_RETRIEVE = "retrieve";
		public static final String ACTION_OFFER = "offer";
		public static final String ACTION_STORE = "store";
		public static final String ACTION_REMOVE = "remove";
		public static final String ACTION_LOAD = "load";
		public static final String ACTION_QUERY = "query";
		public static final String ACTION_DUMP = "dump";
		public static final String STATUS_KNOWN = "known";
		public static final String STATUS_MISSING = "missing";
		
		private QueryDB( ) { }
	}
	
	public static final class Report
	{
		public static final String ID = "report";
		
		public static final String ACTION_REPORT = "report-result";
		public static final String ACTION_INFORM = "inform-result";
		public static final String ACTION_SAVE = "save";
		public static final String ACTION_DELETE = "delete";
		
		private Report( ) { }
	}
	
	public static final class Publish
	{
		public static final String ID = "publisher";
		
		public static final String ACTION_SUBSCRIBE = "subscribe";
		public static final String ACTION_UNSUBSCRIBE = "unsubscribe";
		public static final String ACTION_PUBLISH = "publish";
		public static final String ACTION_RECEIVE = "receive";
		
		private Publish( ) { }
	}
	
	public static final class Timer
	{
		public static final String ID = "timer";
		
		public static final String ACTION_SCHEDULE = "schedule";
		public static final String ACTION_CONFIRM = "confirm";
		public static final String ACTION_REMOVE = "remove";
		
		private Timer( ) { }
	}
	
	public static final class Backup
	{
		public static final String ID = "backup";
		
		public static final String ACTION_TRIGGER = "trigger";
		public static final String ACTION_SAVE = "save";
		public static final String ACTION_LOAD = "load";
		
		private Backup( ) { }
	}
	
	public static final class Internal
	{
		public static final String ID = "internal";
		
		public static final String ACTION_WAKEUP = "wake-up";
		
		private Internal( ) { }
	}
	
	public static final class Status
	{
		public static final String ID = "status";
		
		public static final String ACTION_GATHER = "gather";
		public static final String ACTION_REQUEST = "request";
		public static final String ACTION_REPORT = "report";
		public static final String ACTION_PUBLISH = "publish";
		
		private Status( ) { }
	}
	
	private Tasks( ) { }
}
