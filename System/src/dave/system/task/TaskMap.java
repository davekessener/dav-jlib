package dave.system.task;

import java.util.HashMap;
import java.util.Map;

public class TaskMap<T>
{
	private final Map<TaskID, T> mMap;
	private final T mDefault;
	
	public TaskMap(T d)
	{
		mMap = new HashMap<>();
		mDefault = d;
	}
	
	public void put(String id, String action, T value)
	{
		mMap.put(new TaskID(id, action), value);
	}
	
	public T get(Task t)
	{
		T tp = mMap.get(new TaskID(t.getID(), t.getAction()));
		
		if(tp == null)
		{
			tp = mMap.get(new TaskID(t.getID(), null));
		}
		
		if(tp == null)
		{
			tp = mDefault;
		}
		
		return tp;
	}

	private static class TaskID
	{
		public final String id;
		public final String action;
		
		public TaskID(String id, String action)
		{
			this.id = id;
			this.action = action;
		}
		
		@Override
		public int hashCode( )
		{
			return id.hashCode() ^ (action == null ? 0 : action.hashCode());
		}
		
		@Override
		public boolean equals(Object o)
		{
			if(o instanceof TaskID)
			{
				TaskID tid = (TaskID) o;
				
				return tid.id.equals(id) && (action == null ? tid.action == action : action.equals(tid.action));
			}
			
			return false;
		}
	}
}
