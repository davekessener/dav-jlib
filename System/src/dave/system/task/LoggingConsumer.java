package dave.system.task;

import java.util.function.Consumer;

import dave.util.log.Logger;
import dave.util.log.Severity;

public class LoggingConsumer<T> implements Consumer<T>
{
	private final Logger mLog;
	
	public LoggingConsumer(Logger log)
	{
		mLog = log;
	}
	
	@Override
	public void accept(T t)
	{
		mLog.log(Severity.WARNING, "Received unexpected %s: %s", (t == null ? "-/-" : t.getClass().getName()), "" + t);
	}
}
