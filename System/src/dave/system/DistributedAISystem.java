package dave.system;

import java.io.File;
import java.net.InetSocketAddress;
import java.util.List;
import java.util.function.Consumer;

import dave.system.common.Database;
import dave.system.common.Scoreboard;
import dave.system.common.Tester;
import dave.system.common.Unit;
import dave.system.net.Nodes;
import dave.system.net.Relay;
import dave.system.task.Task;
import dave.system.SystemBuilder.Module;
import dave.json.JsonValue;
import dave.nn.dna.DNA;
import dave.nn.dna.Prototype;
import dave.system.SystemBuilder.BaseModule;
import dave.system.SystemBuilder.NetworkModule;
import dave.system.SystemBuilder.ManagerModule;
import dave.system.SystemBuilder.ServerNetworkModule;
import dave.system.SystemBuilder.LocalClientModule;
import dave.system.SystemBuilder.LocalServerModule;
import dave.system.SystemBuilder.Wrapper;
import dave.system.SystemBuilder.DistributingWrapper;
import dave.util.config.Option;
import dave.util.config.OptionHash;
import dave.util.persistence.file.AlternatingFileWriter;

public final class DistributedAISystem extends BaseSystem
{
	protected DistributedAISystem(Relay<Task> network, List<Unit> units)
	{
		super(network, units);
	}
	
	@SuppressWarnings("unchecked")
	public static BaseSystem buildStandalone(OptionHash<Params> config, InetSocketAddress mca, Prototype p, Tester t, Database<DNA> db, Scoreboard s)
	{
		SystemBuilder builder = new SystemBuilder((Wrapper) config.get(Params.IPC));
		Module base = new BaseModule();
		Module network = new NetworkModule(mca);
		Module tcp = new ServerNetworkModule();
		Module client = new LocalClientModule(
			t,
			Nodes.BREEDER,
			(int) config.get(Params.TESTER_COUNT),
			(int) config.get(Params.POOL_CAPACITY));
		Module server = new LocalServerModule(p, db, s,
			(Consumer<JsonValue>) config.get(Params.HIGHSCORE_SAVER),
			(float) config.get(Params.MUTATION_CHANCE));
		Module manager = new ManagerModule((int) config.get(Params.STATUS_DELTA));
		
		return builder.install(base).install(network).install(tcp).install(client).install(server).install(manager).build();
	}
	
	public static BaseSystem buildManager(InetSocketAddress mca, int p)
	{
		SystemBuilder builder = new SystemBuilder(new DistributingWrapper());
		Module base = new BaseModule();
		Module network = new NetworkModule(mca);
		Module manager = new ManagerModule(p);
		
		return builder.install(base).install(network).install(manager).build();
	}
	
	
	public static enum Params implements Option
	{
		IPC(new DistributingWrapper()),
		POOL_CAPACITY(10),
		TESTER_COUNT(4),
		MUTATION_CHANCE(0.05f),
		TEST_INTENSITY(0),
		ONESHOT(false),
		NIC(null),
		FRAME_SKIP(5),
		SCOREBOARD_SIZE(100),
		STATUS_DELTA(5 * 60),
		HIGHSCORE_SAVER(new AlternatingFileWriter(() -> new File("highscore-" + System.currentTimeMillis() + ".json")));
		
		private final Object mDef;
		
		private Params(Object d)
		{
			mDef = d;
		}

		@Override
		public Object getDefault( )
		{
			return mDef;
		}
	}
}
