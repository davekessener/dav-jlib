package dave.system.unit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import dave.json.Container;
import dave.json.JSON;
import dave.json.JsonBuilder;
import dave.json.JsonObject;
import dave.json.JsonValue;
import dave.json.Loader;
import dave.json.Saver;
import dave.system.net.Address;
import dave.system.net.Node;
import dave.system.net.Packet;
import dave.system.task.Task;
import dave.system.task.Tasks;
import dave.util.log.Logger;
import dave.util.log.Severity;

public class PublisherUnit extends BaseUnit
{
	private final Map<String, List<Address>> mLUT;
	
	public PublisherUnit(Node<Task> node)
	{
		super(node);
		
		mLUT = new HashMap<>();

		registerPacketHandler(Tasks.Publish.ID, Tasks.Publish.ACTION_SUBSCRIBE, this::handleSubscribe);
		registerPacketHandler(Tasks.Publish.ID, Tasks.Publish.ACTION_UNSUBSCRIBE, this::handleUnsubscribe);
		registerPacketHandler(Tasks.Publish.ID, Tasks.Publish.ACTION_PUBLISH, this::handlePublish);
	}

	@Override
	public JsonValue getStatus( )
	{
		return (new JsonBuilder()).putInt("channels", mLUT.size()).putInt("subscribers", mLUT.entrySet().stream().mapToInt(e -> e.getValue().size()).sum()).toJSON();
	}
	
	private void handleSubscribe(Packet<Task> p)
	{
		String channel = (String) p.getContent().getData();
		
		getSubscribers(channel).add(p.getSender());
		
		LOG.log(Severity.INFO, "Node %s subscribed to channel %s.", p.getSender(), channel);
	}
	
	private void handleUnsubscribe(Packet<Task> p)
	{
		String channel = (String) p.getContent().getData();
		
		getSubscribers(channel).remove(p.getSender());
		
		LOG.log(Severity.INFO, "Node %s unsubscribed from channel %s.", p.getSender(), channel);
	}
	
	private void handlePublish(Packet<Task> p)
	{
		Message msg = (Message) p.getContent().getData();
		Task t = new Task(Tasks.Publish.ID, Tasks.Publish.ACTION_RECEIVE, newSession(), new Message(p.getSender(), msg));
		
		LOG.log(Severity.INFO, "Node %s publishes on channel %s.", p.getSender(), msg.channel);
		
		for(Address subscriber : getSubscribers(msg.channel))
		{
			if(!subscriber.equals(p.getSender()))
			{
				getNode().send(subscriber, t);
			}
		}
	}
	
	private List<Address> getSubscribers(String channel)
	{
		List<Address> subscribers = mLUT.get(channel);
		
		if(subscribers == null)
		{
			mLUT.put(channel, subscribers = new ArrayList<>());
		}

		return subscribers;
	}
	
	public static final class Channels
	{
		public static final String DNA_DISTRIBUTION = "dna-distribution";
		public static final String BACKUP = "backup";
		public static final String SYSTEM = "system";
		
		private Channels( ) { }
	}
	
	@Container
	public static class Message
	{
		public final Address source;
		public final String channel;
		public final Object data;
		
		public Message(Address source, Message msg)
		{
			this.source = source;
			this.channel = msg.channel;
			this.data = msg.data;
		}
		
		public Message(String channel, Object data)
		{
			this.source = null;
			this.channel = channel;
			this.data = data;
		}
		
		@Override
		public String toString( )
		{
			return String.format("Message{from %s on %s: %s}", source, channel, data.toString());
		}
		
		@Saver
		public JsonValue save( )
		{
			JsonObject json = new JsonObject();
			
			json.put("source", JSON.serialize(source));
			json.putString("channel", channel);
			json.put("data", JSON.serialize(data));
			
			return json;
		}
		
		@Loader
		public static Message load(JsonValue json)
		{
			JsonObject o = (JsonObject) json;
			
			Address source = (Address) JSON.deserialize(o.get("source"));
			String channel = o.getString("channel");
			Object data = JSON.deserialize(o.get("dat"));
			
			return new Message(source, new Message(channel, data));
		}
	}
	
	private static final Logger LOG = Logger.get("u-publish");
}
