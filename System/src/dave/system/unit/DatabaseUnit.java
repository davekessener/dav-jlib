package dave.system.unit;

import java.net.InetAddress;
import java.net.Socket;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Consumer;

import dave.json.Container;
import dave.json.JSON;
import dave.json.JsonConstant;
import dave.json.JsonObject;
import dave.json.JsonValue;
import dave.json.Loader;
import dave.json.Saver;
import dave.net.server.Connection;
import dave.net.server.TCPConnection;
import dave.nn.dna.DNA;
import dave.system.SystemUtils;
import dave.system.common.Database;
import dave.system.net.Address;
import dave.system.net.LocalAddress;
import dave.system.net.Node;
import dave.system.net.Nodes;
import dave.system.net.Packet;
import dave.system.net.UniqueAddress;
import dave.system.task.Task;
import dave.system.task.Tasks;
import dave.util.log.Logger;
import dave.util.log.Severity;
import dave.util.Utils;

public class DatabaseUnit extends BaseUnit
{
	private final Database<DNA> mDatabase;
	private final ExecutorService mAsync;
	private final Consumer<JsonValue> mPersistence;
	private final Set<String> mHighscores;
	
	public DatabaseUnit(Node<Task> node, Consumer<JsonValue> io, Database<DNA> db)
	{
		super(node);
		
		mDatabase = db;
		mAsync = Executors.newFixedThreadPool(5);
		mPersistence = io;
		mHighscores = new HashSet<>();
		
		onStop(this::stopProcessor);

		registerPacketHandler(Tasks.QueryDB.ID, Tasks.QueryDB.ACTION_STORE, this::handleStore);
		registerPacketHandler(Tasks.QueryDB.ID, Tasks.QueryDB.ACTION_RETRIEVE, this::handleRetrieve);
		registerPacketHandler(Tasks.QueryDB.ID, Tasks.QueryDB.ACTION_REMOVE, this::handleRemove);
		registerPacketHandler(Tasks.QueryDB.ID, Tasks.QueryDB.ACTION_QUERY, this::handleQuery);
		registerPacketHandler(Tasks.QueryDB.ID, Tasks.QueryDB.ACTION_LOAD, this::handleLoad);
		registerPacketHandler(Tasks.QueryDB.ID, Tasks.QueryDB.ACTION_DUMP, this::handleDump);
	}
	
	@Override
	public JsonValue getStatus( )
	{
		return JsonConstant.NULL;
	}
	
	private void stopProcessor( )
	{
		mAsync.shutdownNow();
	}
	
	private void handleStore(Packet<Task> p)
	{
		final DNA dna = (DNA) p.getContent().getData();
		final String id = dna.getID();
		
		mDatabase.store(dna);
		
		if((p.getSender() instanceof LocalAddress) && p.getSender().getNodeID().equals(getNode().getID()))
		{
			getNode().send(Nodes.SCORE, new Task(Tasks.QueryDB.ID, Tasks.QueryDB.STATUS_KNOWN, newSession(), id));
		}
		
		if(mHighscores.contains(id))
		{
			mHighscores.remove(id);
			mPersistence.accept(dna.save());
		}
	}
	
	private void handleRetrieve(Packet<Task> p)
	{
		Task t = p.getContent();
		String id = (String) t.getData();
		DNA dna = mDatabase.retrieve(id);
	
		if(dna == null)
		{
			t = new Task(t, Tasks.QueryDB.STATUS_MISSING, id);
		}
		else
		{
			t = new Task(t, Tasks.QueryDB.ACTION_OFFER, dna);
		}
		
		getNode().send(p.getSender(), t);
	}
	
	private void handleRemove(Packet<Task> p)
	{
		mDatabase.remove((String) p.getContent().getData());
	}
	
	private void handleQuery(Packet<Task> p)
	{
		String id = (String) p.getContent().getData();
		Task t = new Task(p.getContent(), mDatabase.has(id) ? Tasks.QueryDB.STATUS_KNOWN : Tasks.QueryDB.STATUS_MISSING, id);
		
		getNode().send(p.getSender(), t);
	}
	
	private void handleLoad(Packet<Task> p)
	{
		final RemoteRecord record = (RemoteRecord) p.getContent().getData();
		final InetAddress remote = ((UniqueAddress) record.remote).host.getAddress();
		
		if(!mAsync.isShutdown())
		{
			mAsync.submit(Utils.wrap(() -> {
				Connection c = new TCPConnection(new Socket(remote, SystemUtils.TCP_PORT));
				Task t = new Task(Tasks.QueryDB.ID, Tasks.QueryDB.ACTION_RETRIEVE, newSession(), record.id);
				Packet<Task> packet = new Packet<>(Address.NIL, new LocalAddress(Nodes.DATABASE), t);
				
				c.send(packet.save());
				
				packet = Packet.load(c.receive());
				t = packet.getContent();
				
				if(t.getAction().equals(Tasks.QueryDB.ACTION_OFFER))
				{
					getNode().send(Nodes.DATABASE, new Task(t, Tasks.QueryDB.ACTION_STORE, t.getData()));
				}
				else
				{
					LOG.log(Severity.WARNING, "Failed to retrieve %s from %s! [%s]",  record.id, record.remote, packet.toString());
					
					getNode().send(Nodes.SCORE, new Task(t, Tasks.QueryDB.STATUS_MISSING, record.id));
				}
				
				c.close();
			}));
		}
	}
	
	private void handleDump(Packet<Task> p)
	{
		String id = (String) p.getContent().getData();
		DNA dna = mDatabase.retrieve(id);
		
		if(dna == null)
		{
			mHighscores.add(id);
		}
		else
		{
			mPersistence.accept(dna.save());
		}
	}
	
// # --------------------------------------------------------------------------
	
	@Container
	public static class RemoteRecord
	{
		public final Address remote;
		public final String id;
		
		public RemoteRecord(Address remote, String id)
		{
			this.remote = remote;
			this.id = id;
		}
		
		@Saver
		public JsonValue save( )
		{
			JsonObject json = new JsonObject();
			
			json.put("remote", JSON.serialize(remote));
			json.putString("id", id);
			
			return json;
		}
		
		@Loader
		public static RemoteRecord load(JsonValue json)
		{
			JsonObject o = (JsonObject) json;
			
			Address remote = (Address) JSON.deserialize(o.get("remote"));
			String id = o.getString("id");
			
			return new RemoteRecord(remote, id);
		}
		
		@Override
		public String toString( )
		{
			return "Record{" + id + "--" + remote + "}";
		}
	}
	
	private static final Logger LOG = Logger.get("u-db");
}
