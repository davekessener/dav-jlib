package dave.system.unit;

import java.io.IOException;

import dave.json.JsonBuilder;
import dave.json.JsonValue;
import dave.net.server.Connection;
import dave.system.SystemBuilder;
import dave.system.net.Node;
import dave.system.net.Nodes;
import dave.system.net.Packet;
import dave.system.task.Task;

public class ConnectionUnit extends BaseUnit
{
	private static int NEXT_ID = 0;
	
	private final SystemBuilder mNetwork;
	private final Connection mConnection;
	private final Thread mThread;
	private int mSent, mReceived;
	
	public ConnectionUnit(SystemBuilder net, Connection c)
	{
		super(new Node<>(Nodes.REMOTE + "-" + System.currentTimeMillis() + "-" + (NEXT_ID++)));
		
		mNetwork = net;
		mConnection = c;
		mSent = mReceived = 0;
		mThread = new Thread(this::run);
		
		onStart(mThread::start);
	}
	
	private void run( )
	{
		try
		{
			while(true)
			{
				Packet<Task> p = Packet.load(mConnection.receive());
				
				++mReceived;
				
				getNode().send(p.getRecipient(), p.getContent());
			}
		}
		catch(IOException e)
		{
			mNetwork.remove(this);
		}
	}
	
	@Override
	public JsonValue getStatus( )
	{
		return (new JsonBuilder()).putInt("up", mSent).putInt("down", mReceived).toJSON();
	}
	
	@Override
	public void accept(Packet<Task> p)
	{
		++mSent;
		
		try
		{
			mConnection.send(p.save());
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}
}
