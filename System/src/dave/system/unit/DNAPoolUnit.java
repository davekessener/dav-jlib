package dave.system.unit;

import java.util.ArrayList;
import java.util.List;

import dave.json.JsonBuilder;
import dave.json.JsonValue;
import dave.nn.dna.DNA;
import dave.system.net.Node;
import dave.system.net.Packet;
import dave.system.task.Task;
import dave.system.task.Tasks;
import dave.util.log.Logger;
import dave.util.log.Severity;

public class DNAPoolUnit extends BaseUnit
{
	private final List<DNA> mPool;
	private final List<Packet<Task>> mWaiting;
	private final String mSource;
	private final int mCapacity;
	private int mOpenRequests;
	
	public DNAPoolUnit(Node<Task> node, String source, int cap)
	{
		super(node);
		
		mCapacity = cap;
		mPool = new ArrayList<>();
		mWaiting = new ArrayList<>();
		mOpenRequests = 0;
		mSource = source;
		
		afterStart(this::sendRequest);
		
		registerPacketHandler(Tasks.FetchDNA.ID, Tasks.FetchDNA.ACTION_OFFER, p -> handleOffer(p));
		registerPacketHandler(Tasks.FetchDNA.ID, Tasks.FetchDNA.ACTION_REQUEST, p -> handleRequest(p));
	}

	@Override
	public JsonValue getStatus( )
	{
		return (new JsonBuilder()).putInt("available", mPool.size()).putInt("clients", mWaiting.size()).putInt("pending", mOpenRequests).toJSON();
	}
	
	@Override
	protected void postProcessCallback( )
	{
		while(!mWaiting.isEmpty() && !mPool.isEmpty())
		{
			sendDNA(mWaiting.remove(0));
		}
		
		while(mPool.size() + mOpenRequests < mCapacity)
		{
			sendRequest();
		}
	}
	
	private void handleOffer(Packet<Task> p)
	{
		DNA dna = (DNA) p.getContent().getData();
		
		--mOpenRequests;
		
		if(mPool.size() == mCapacity)
		{
			LOG.log(Severity.WARNING, "Received DNA [%s] from %s, but buffer is full!", dna.getID(), p.getSender());
		}
		else
		{
			mPool.add(dna);
		}
	}
	
	private void handleRequest(Packet<Task> p)
	{
		Task t = p.getContent();
		String id = (String) t.getData();
		
		if(id == null)
		{
			mWaiting.add(p);
		}
		else
		{
			getNode().send(p.getSender(), new Task(p.getContent(), Tasks.FetchDNA.STATUS_INVALID));
		}
	}
	
	private void sendDNA(Packet<Task> p)
	{
		getNode().send(p.getSender(), new Task(p.getContent(), Tasks.FetchDNA.ACTION_OFFER, mPool.remove(0)));
	}

	private void sendRequest( )
	{
		++mOpenRequests;
		getNode().send(mSource, new Task(Tasks.FetchDNA.ID, Tasks.FetchDNA.ACTION_REQUEST, newSession()));
	}
	
	private static final Logger LOG = Logger.get("u-pool");
}
