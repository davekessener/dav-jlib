package dave.system.unit;

import java.util.HashSet;
import java.util.Set;

import dave.json.JsonBuilder;
import dave.json.JsonValue;
import dave.system.Result;
import dave.system.common.Scoreboard;
import dave.system.net.BroadcastAddress;
import dave.system.net.Node;
import dave.system.net.Nodes;
import dave.system.net.Packet;
import dave.system.task.Task;
import dave.system.task.Tasks;
import dave.util.log.Logger;
import dave.util.log.Severity;

public class ScoreUnit extends BaseUnit
{
	private final Scoreboard mScore;
	private final Set<Result> mPending;
	
	public ScoreUnit(Node<Task> node, Scoreboard score)
	{
		super(node);
		
		mScore = score;
		mPending = new HashSet<>();
		
		registerPacketHandler(Tasks.Report.ID, Tasks.Report.ACTION_REPORT, this::handleResult);
		registerPacketHandler(Tasks.Report.ID, Tasks.Report.ACTION_INFORM, this::handleInform);
		registerPacketHandler(Tasks.Score.ID, Tasks.Score.ACTION_REQUEST, this::handleRequest);
		registerPacketHandler(Tasks.Score.ID, Tasks.Score.ACTION_SOLICIT, this::handleSolicitation);
		registerPacketHandler(Tasks.Score.ID, Tasks.Score.ACTION_CULL, this::handleCull);
		registerPacketHandler(Tasks.QueryDB.ID, Tasks.QueryDB.STATUS_KNOWN, this::handleKnown);
		registerPacketHandler(Tasks.QueryDB.ID, Tasks.QueryDB.STATUS_MISSING, this::handleMissing);
		
		onStart(this::solicit);
		afterStart(this::scheduleCulling);
	}

	private void solicit( )
	{
		getNode().send(new BroadcastAddress(Nodes.SCORE),
			new Task(Tasks.Score.ID, Tasks.Score.ACTION_SOLICIT, newSession(), mScore.highScore()));
	}
	
	private void scheduleCulling( )
	{
		getNode().send(Nodes.TIMER, new Task(
			Tasks.Timer.ID, Tasks.Timer.ACTION_SCHEDULE, newSession(),
				new TimerUnit.FutureTask(CULL_TASK, getNode().getID(), new Task(
					Tasks.Score.ID, Tasks.Score.ACTION_CULL, newSession()), CULL_PERIOD)));
	}
	
	private void handleSolicitation(Packet<Task> p)
	{
		int score = mScore.highScore();
		int cutoff = ((Number) p.getContent().getData()).intValue();
		
		if(score > cutoff)
		{
			getNode().send(p.getSender(),
				new Task(Tasks.Report.ID, Tasks.Report.ACTION_INFORM, newSession(),
					new Result(mScore.get(0), score)));
		}
		else if(score < cutoff)
		{
			getNode().send(p.getSender(), new Task(p.getContent(), Tasks.Score.ACTION_SOLICIT, score));
		}
	}
	
	private void handleInform(Packet<Task> p)
	{
		Result r = (Result) p.getContent().getData();
		
		mPending.add(r);
		
		getNode().send(Nodes.DATABASE, new Task(Tasks.QueryDB.ID, Tasks.QueryDB.ACTION_LOAD, newSession(),
			new DatabaseUnit.RemoteRecord(p.getSender(), r.dna)));
	}
	
	private void handleKnown(Packet<Task> p)
	{
		String id = (String) p.getContent().getData();
		
		for(Result r : mPending)
		{
			if(r.dna.equals(id))
			{
				mPending.remove(r);
				updateScore(r, false);
				
				return;
			}
		}
	}
	
	private void handleCull(Packet<Task> p)
	{
		LOG.log(Severity.INFO, "Culling score!");
		
		mScore.cull();
		
		mScore.stream().forEach(e -> getNode().send(Nodes.DATABASE,
			new Task(Tasks.QueryDB.ID, Tasks.QueryDB.ACTION_QUERY, newSession(), e.dna)));
		
		scheduleCulling();
	}
	
	@Override
	public JsonValue getStatus( )
	{
		return (new JsonBuilder()).putInt("highscore", mScore.highScore()).toJSON();
	}
	
	private void handleResult(Packet<Task> p)
	{
		updateScore((Result) p.getContent().getData(), true);
	}
	
	private void updateScore(Result r, boolean broadcast)
	{
		if(mScore.contains(r.dna)) return;
		
		final boolean new_high_score = r.score > mScore.highScore();
		final String removed = mScore.update(r);
		final boolean added = !r.dna.equals(removed);
		
		if(removed != null && added)
		{
			getNode().send(Nodes.DATABASE, new Task(Tasks.QueryDB.ID, Tasks.QueryDB.ACTION_REMOVE, newSession(), removed));
		}
		
		if(new_high_score)
		{
			LOG.log(Severity.INFO, "New highscore: %d!", mScore.highScore());
			
			getNode().send(Nodes.DATABASE, new Task(Tasks.QueryDB.ID, Tasks.QueryDB.ACTION_DUMP, newSession(), r.dna));
		}

		if(broadcast)
		{
			String msg = added ? Tasks.Report.ACTION_SAVE : Tasks.Report.ACTION_DELETE;
			
			getNode().send(Nodes.BREEDER, new Task(Tasks.Report.ID, msg, newSession(), r.dna));
			
			if(new_high_score)
			{
				getNode().send(new BroadcastAddress(Nodes.SCORE),
					new Task(Tasks.Report.ID, Tasks.Report.ACTION_INFORM, newSession(), r));
			}
		}
	}
	
	private void handleRequest(Packet<Task> p)
	{
		Task t = (Task) p.getContent();
		String id = mScore.get((float) t.getData());
		
		if(id != null)
		{
			t = new Task(t, Tasks.Score.ACTION_OFFER, id);
		}
		else
		{
			t = new Task(t, Tasks.Score.STATUS_EMPTY);
		}
		
		getNode().send(p.getSender(), t);
	}
	
	private void handleMissing(Packet<Task> p)
	{
		final String id = (String) p.getContent().getData();
		
		for(Result r : mPending)
		{
			if(r.dna.equals(id))
			{
				mPending.remove(r);
				return;
			}
		}
		
		mScore.remove(id);
	}
	
	private static final String CULL_TASK = "cull";
	private static final int CULL_PERIOD = 1 * 60 * 60; // 1h
	private static final Logger LOG = Logger.get("u-score");
}
