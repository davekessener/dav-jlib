package dave.system.unit;

import dave.util.persistence.Storage;
import dave.util.persistence.StorageAdapter;
import dave.system.net.Node;
import dave.system.task.Task;
import dave.system.task.TaskBasedMessageProcessor;
import dave.system.task.Tasks;
import dave.system.unit.PublisherUnit.Channels;
import dave.system.unit.PublisherUnit.Message;

public abstract class StorageBackedUnit extends BaseUnit
{
	private final Storage mBackup;
	
	public StorageBackedUnit(Node<Task> node, Storage backup)
	{
		super(node);
		
		mBackup = new StorageAdapter(backup, getNode().getID());
		
		registerMessageHandler(Channels.BACKUP, (new TaskBasedMessageProcessor())
			.register(Tasks.Backup.ID, Tasks.Backup.ACTION_LOAD, this::handleLoad)
			.register(Tasks.Backup.ID, Tasks.Backup.ACTION_SAVE, this::handleSave));
		
		onStart(this::request);
	}
	
	protected abstract void load(Storage io);
	protected abstract void save(Storage io);
	
	private void request( )
	{
		subscribe(Channels.BACKUP);
	}
	
	private void handleLoad(Message msg)
	{
		load(mBackup);
	}
	
	private void handleSave(Message msg)
	{
		save(mBackup);
	}
}
