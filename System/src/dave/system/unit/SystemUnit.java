package dave.system.unit;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

import dave.Main;
import dave.json.Container;
import dave.json.JsonBuilder;
import dave.json.JsonCollectors;
import dave.json.JsonObject;
import dave.json.JsonValue;
import dave.json.Loader;
import dave.json.Saveable;
import dave.json.Saver;
import dave.system.net.Node;
import dave.system.net.Nodes;
import dave.system.net.Packet;
import dave.system.task.Task;
import dave.system.task.Tasks;
import dave.system.unit.PublisherUnit.Channels;
import dave.util.io.SevereIOException;
import dave.util.log.LogBase;
import dave.util.log.Severity;

public class SystemUnit extends BaseUnit
{
	private final Map<String, LogEntry> mLog;
	private final String mHostname;
	private Severity mMaxLevel;
	
	public SystemUnit(Node<Task> node)
	{
		super(node);
		
		mLog = new HashMap<>();
		mMaxLevel = Severity.NONE;
		
		try
		{
			mHostname = InetAddress.getLocalHost().getHostName();
		}
		catch(UnknownHostException e)
		{
			throw new SevereIOException(e);
		}
		
		LogBase.INSTANCE.registerSink(e -> true, e -> {
			if(e.severity.level() > mMaxLevel.level())
			{
				mMaxLevel = e.severity;
			}
		});
		
		onStart(() -> scheduleRequests(1));
		
		registerPacketHandler(Tasks.Status.ID, Tasks.Status.ACTION_GATHER, p -> requestReports());
		registerPacketHandler(Tasks.Status.ID, Tasks.Status.ACTION_REPORT, this::handleReport);
		registerPacketHandler(Tasks.Status.ID, Tasks.Status.ACTION_REQUEST, this::handleRequest);
	}

	@Override
	public JsonValue getStatus( )
	{
		final String status =
			(mMaxLevel.level() >= Severity.ERROR.level() ? "CRITICAL" :
			(mMaxLevel.level() >= Severity.WARNING.level() ? "WARNING" : 
			"normal"));
		
		return (new JsonBuilder()).putString("host", mHostname).putInt("version", Main.VERSION).putString("status", status).toJSON();
	}
	
	private void scheduleRequests(int p)
	{
		getNode().send(Nodes.TIMER,
			new Task(Tasks.Timer.ID, Tasks.Timer.ACTION_SCHEDULE, 0,
				new TimerUnit.FutureTask(TASK_ID, getNode().getID(),
					new Task(Tasks.Status.ID, Tasks.Status.ACTION_GATHER, 0),
			p)));
	}
	
	private void requestReports( )
	{
		final String id = getNode().getID();
		
		mLog.clear();
		
		mLog.put(id, new LogEntry(id, System.currentTimeMillis(), getStatus()));
		
		publish(Channels.SYSTEM, new Task(Tasks.Status.ID, Tasks.Status.ACTION_REQUEST, newSession()));
		
		scheduleRequests(TASK_PERIOD);
	}
	
	private void handleReport(Packet<Task> p)
	{
		String node = p.getSender().getNodeID();
		JsonValue message = (JsonValue) p.getContent().getData();
		
		mLog.put(node, new LogEntry(node, System.currentTimeMillis(), message));
	}
	
	private void handleRequest(Packet<Task> p)
	{
		JsonValue log = mLog.values().stream().map(LogEntry::save).collect(JsonCollectors.ofArray());
		
		getNode().send(p.getSender(), new Task(p.getContent(), Tasks.Status.ACTION_PUBLISH, log));
	}
	
	@Container
	public static class LogEntry implements Saveable
	{
		public final String node;
		public final long timestamp;
		public final JsonValue message;
		
		public LogEntry(String node, long timestamp, JsonValue message)
		{
			this.node = node;
			this.timestamp = timestamp;
			this.message = message;
		}
		
		@Override
		@Saver
		public JsonValue save( )
		{
			JsonObject json = new JsonObject();
			
			json.putString("node", node);
			json.putLong("timestamp", timestamp);
			json.put("message", message);
			
			return json;
		}
		
		@Loader
		public static LogEntry load(JsonValue json)
		{
			JsonObject o = (JsonObject) json;
			
			String node = o.getString("node");
			long timestamp = o.getLong("timestamp");
			JsonValue message = o.get("message");
			
			return new LogEntry(node, timestamp, message);
		}
	}
	
	private static final String TASK_ID = "report-status";
	private static final int TASK_PERIOD = 1 * 60;
}
