package dave.system.unit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

import dave.util.log.Logger;
import dave.util.log.Severity;
import dave.json.JsonValue;
import dave.system.common.Unit;
import dave.system.net.Node;
import dave.system.net.Nodes;
import dave.system.net.Packet;
import dave.system.task.Task;
import dave.system.task.TaskBasedMessageProcessor;
import dave.system.task.TaskDistributor;
import dave.system.task.Tasks;
import dave.system.unit.PublisherUnit.Channels;
import dave.system.unit.PublisherUnit.Message;

public abstract class BaseUnit extends TaskDistributor implements Unit
{
	private final Node<Task> mNode;
	private final Map<String, Consumer<Message>> mLUT;
	private final List<Runnable> mPreStart, mOnStart, mPostStart;
	private final List<Runnable> mPreStop, mOnStop, mPostStop;
	private long mSession = 0;
	
	public BaseUnit(Node<Task> node)
	{
		mNode = node;
		mLUT = new HashMap<>();
		mPreStart = new ArrayList<>();
		mOnStart = new ArrayList<>();
		mPostStart = new ArrayList<>();
		mPreStop = new ArrayList<>();
		mOnStop = new ArrayList<>();
		mPostStop = new ArrayList<>();
		
		registerPacketHandler(Tasks.Publish.ID, Tasks.Publish.ACTION_RECEIVE, this::handleReceive);
		registerMessageHandler(Channels.SYSTEM, (new TaskBasedMessageProcessor()).register(Tasks.Status.ID, Tasks.Status.ACTION_REQUEST, this::handleStatusReport));
		
		mNode.setClient(old -> this);
	}
	
	protected long newSession( ) { return mSession++; }
	
	@Override
	public Node<Task> getNode( )
	{
		return mNode;
	}
	
	@Override
	public final void beforeStart( )
	{
		subscribe(Channels.SYSTEM);
		
		for(Runnable r : mPreStart)
		{
			r.run();
		}
	}

	@Override
	public final void start( )
	{
		for(Runnable r : mOnStart)
		{
			r.run();
		}
	}
	
	@Override
	public final void afterStart( )
	{
		for(Runnable r : mPostStart)
		{
			r.run();
		}
	}
	
	@Override
	public final void beforeStop( )
	{
		for(Runnable r : mPreStop)
		{
			r.run();
		}
	}

	@Override
	public final void stop( )
	{
		for(Runnable r : mOnStop)
		{
			r.run();
		}
	}
	
	@Override
	public final void afterStop( )
	{
		for(Runnable r : mPostStop)
		{
			r.run();
		}
	}
	
	protected abstract JsonValue getStatus( );
	
	protected void beforeStart(Runnable r) { mPreStart.add(0, r); }
	protected void onStart(Runnable r) { mOnStart.add(r); }
	protected void afterStart(Runnable r) { mPostStart.add(r); }
	protected void beforeStop(Runnable r) { mPreStop.add(0, r); }
	protected void onStop(Runnable r) { mOnStop.add(r); }
	protected void afterStop(Runnable r) { mPostStop.add(r); }
	
	private void handleReceive(Packet<Task> p)
	{
		Message msg = (Message) p.getContent().getData();
		
		Consumer<Message> cb = mLUT.get(msg.channel);
		
		if(cb != null)
		{
			cb.accept(msg);
		}
		else
		{
			Logger.DEFAULT.log(Severity.WARNING, "Received message from %s on channel %s with no handler registered!", msg.source, msg.channel);
		}
	}
	
	private void handleStatusReport(Message msg)
	{
		getNode().send(Nodes.SYSTEM, new Task(Tasks.Status.ID, Tasks.Status.ACTION_REPORT, newSession(), getStatus()));
	}
	
	protected void subscribe(String channel)
	{
		getNode().send(Nodes.PUBLISHER,
				new Task(Tasks.Publish.ID, Tasks.Publish.ACTION_SUBSCRIBE, newSession(), channel));
	}
	
	protected void unsubscribe(String channel)
	{
		getNode().send(Nodes.PUBLISHER,
				new Task(Tasks.Publish.ID, Tasks.Publish.ACTION_UNSUBSCRIBE, newSession(), channel));
	}
	
	protected void publish(String channel, Object content)
	{
		getNode().send(Nodes.PUBLISHER,
			new Task(Tasks.Publish.ID, Tasks.Publish.ACTION_PUBLISH, newSession(),
				new Message(channel, content)));
	}
	
	protected void registerMessageHandler(String channel, Consumer<Message> cb)
	{
		if(mLUT.put(channel, cb) != null)
			throw new IllegalStateException(channel);
	}
}
