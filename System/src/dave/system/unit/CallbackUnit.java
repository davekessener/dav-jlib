package dave.system.unit;

import java.util.function.Consumer;

import dave.json.JsonConstant;
import dave.json.JsonValue;
import dave.system.common.Unit;
import dave.system.net.Node;
import dave.system.task.Task;

public class CallbackUnit extends BaseUnit
{
	private final Consumer<Unit> mCallback;
	
	public CallbackUnit(Node<Task> node, Consumer<Unit> cb)
	{
		super(node);
		
		mCallback = cb;
		
		afterStart(() -> mCallback.accept(this));
	}

	@Override
	protected JsonValue getStatus( )
	{
		return JsonConstant.NULL;
	}
}
