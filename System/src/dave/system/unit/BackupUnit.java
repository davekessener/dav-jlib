package dave.system.unit;

import java.io.File;
import java.util.function.Consumer;

import dave.json.JsonBuilder;
import dave.json.JsonValue;
import dave.system.net.Node;
import dave.system.net.Nodes;
import dave.system.net.Packet;
import dave.system.task.Task;
import dave.system.task.Tasks;
import dave.system.unit.PublisherUnit.Channels;
import dave.util.log.Logger;
import dave.util.log.Severity;
import dave.util.Producer;
import dave.util.io.FileIO;
import dave.util.persistence.Storage;
import dave.util.persistence.file.IOStorage;

public class BackupUnit extends BaseUnit
{
	private final Producer<File> mGen;
	private final int mPeriod;
	private final Consumer<Storage> mStorage;
	private File mOld, mCurrent;
	
	public BackupUnit(Node<Task> node, Consumer<Storage> io, File o, Producer<File> p, int d)
	{
		super(node);
		
		mGen = p;
		mPeriod = d;
		mOld = mCurrent = null;
		mStorage = io;

		registerPacketHandler(Tasks.Backup.ID, Tasks.Backup.ACTION_TRIGGER, this::handleTrigger);
		
		onStart(() -> scheduleBroadcast(mPeriod));
		beforeStop(() -> handleTrigger(null));
		afterStop(this::cleanup);
		
		if(o != null)
		{
			LOG.log(Severity.INFO, "Loading backup from %s", o.getName());
			
			mCurrent = o;
			mStorage.accept(new IOStorage(() -> new FileIO(mCurrent)));
			
			afterStart(() -> publish(Channels.BACKUP, new Task(Tasks.Backup.ID, Tasks.Backup.ACTION_LOAD, newSession())));
		}
		else
		{
			turnover(mGen.produce());
		}
	}
	
	@Override
	public JsonValue getStatus( )
	{
		return (new JsonBuilder()).putString("file", mCurrent == null ? "NULL" : mCurrent.getName()).toJSON();
	}
	
	private void scheduleBroadcast(int p)
	{
		getNode().send(Nodes.TIMER,
			new Task(Tasks.Timer.ID, Tasks.Timer.ACTION_SCHEDULE, newSession(),
				new TimerUnit.FutureTask(TIMER_ID, Nodes.BACKUP,
					new Task(Tasks.Backup.ID, Tasks.Backup.ACTION_TRIGGER, 0),
				p)));
	}
	
	private void cleanup( )
	{
		if(mOld != null)
		{
			mOld.deleteOnExit();
			mOld = null;
		}
	}
	
	private void turnover(File f)
	{
		if(mOld != null)
		{
			mOld.delete();
		}
		
		f.delete();
		
		mOld = mCurrent;
		mCurrent = f;
		mStorage.accept(new IOStorage(() -> new FileIO(mCurrent)));
	}

	private void handleTrigger(Packet<Task> p)
	{
		turnover(mGen.produce());
		
		publish(Channels.BACKUP, new Task(Tasks.Backup.ID, Tasks.Backup.ACTION_SAVE, newSession()));
		
		scheduleBroadcast(mPeriod);
	}

	private static final String TIMER_ID = "save-all";
	private static final Logger LOG = Logger.get("u-backup");
}
