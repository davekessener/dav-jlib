package dave.system.unit;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

import dave.json.JsonBuilder;
import dave.json.JsonValue;
import dave.nn.dna.DNA;
import dave.nn.dna.Prototype;
import dave.system.net.Node;
import dave.system.net.Nodes;
import dave.system.net.Packet;
import dave.system.task.Task;
import dave.system.task.Tasks;
import dave.util.RNG;
import dave.util.XoRoRNG;

public class BreederUnit extends BaseUnit
{
	private final List<DNA> mEntries;
	private final Queue<Packet<Task>> mRequests;
	private final RNG mRandom;
	private final Prototype mProto;
	private final float mChance;
	private final Queue<DNA> mDads;
	private State mState;
	
	private static enum State
	{
		INIT,
		READY,
		WAITING
	}
	
	public BreederUnit(Node<Task> node, Prototype p, float chance)
	{
		super(node);
		
		mEntries = new ArrayList<>();
		mRequests = new ArrayDeque<>();
		mRandom = new XoRoRNG();
		mProto = p;
		mChance = 1.0f + chance;
		mDads = new ArrayDeque<DNA>();
		mState = State.INIT;

		registerPacketHandler(Tasks.FetchDNA.ID, Tasks.FetchDNA.ACTION_REQUEST, this::handleRequest);
		registerPacketHandler(Tasks.FetchDNA.ID, Tasks.FetchDNA.ACTION_OFFER, this::handleDNAOffer);
		registerPacketHandler(Tasks.QueryDB.ID, Tasks.QueryDB.ACTION_OFFER, this::handleDNAOffer);
		registerPacketHandler(Tasks.QueryDB.ID, Tasks.QueryDB.STATUS_MISSING, this::handleMissing);
		registerPacketHandler(Tasks.Score.ID, Tasks.Score.ACTION_OFFER, this::handleIDOffer);
		registerPacketHandler(Tasks.Score.ID, Tasks.Score.STATUS_EMPTY, this::onEmptyDNA);
		registerPacketHandler(Tasks.Report.ID, Tasks.Report.ACTION_SAVE, this::handleSave);
		registerPacketHandler(Tasks.Report.ID, Tasks.Report.ACTION_DELETE, this::handleDelete);
	}

	@Override
	public JsonValue getStatus( )
	{
		return (new JsonBuilder()).putInt("entries", mEntries.size()).putInt("pending", mRequests.size()).putString("state", mState.toString()).toJSON();
	}
	
	private void tick( )
	{
		float v = (float) (mChance * mRandom.nextDouble());
		
		if(mDads.size() >= 2)
		{
			Packet<Task> p = mRequests.poll();
			DNA dad_0 = mDads.poll(), dad_1 = mDads.poll();
			DNA dna = DNA.procreate(dad_0, dad_1, mRandom, mChance);
			
			mEntries.add(dna);
			getNode().send(p.getSender(), new Task(p.getContent(), Tasks.FetchDNA.ACTION_OFFER, dna));
			mState = State.INIT;
		}
		else if(v >= 1.0)
		{
			prepare(create());
		}
		else
		{
			getNode().send(Nodes.SCORE, new Task(Tasks.Score.ID, Tasks.Score.ACTION_REQUEST, newSession(), v * v));
			mState = State.WAITING;
		}
	}
	
	private void store(DNA dna)
	{
		getNode().send(Nodes.DATABASE, new Task(Tasks.QueryDB.ID, Tasks.QueryDB.ACTION_STORE, newSession(), dna));
	}
	
	private DNA create( )
	{
		return new DNA(mProto, mRandom);
	}
	
	private void prepare(DNA dna)
	{
		mDads.add(dna);
		mState = State.READY;
	}
	
	@Override
	protected void postProcessCallback( )
	{
		while(!mRequests.isEmpty() && mState != State.WAITING)
		{
			if(mState == State.INIT)
			{
				mState = State.READY;
			}
	
			while(mState == State.READY) tick();
		}
	}
	
	private void handleRequest(Packet<Task> p)
	{
		mRequests.add(p);
	}
	
	private void handleDNAOffer(Packet<Task> p)
	{
		prepare((DNA) p.getContent().getData());
	}
	
	private void handleMissing(Packet<Task> p)
	{
		getNode().send(Nodes.SCORE, p.getContent());
		
		prepare(create());
	}
	
	private void handleIDOffer(Packet<Task> p)
	{
		getNode().send(Nodes.DATABASE, 
			new Task(Tasks.QueryDB.ID, Tasks.QueryDB.ACTION_RETRIEVE, newSession(), p.getContent().getData()));
	}
	
	private void onEmptyDNA(Packet<Task> p)
	{
		prepare(create());
	}

	private void handleSave(Packet<Task> p)
	{
		String id = (String) p.getContent().getData();
		
		for(DNA dna : mEntries)
		{
			if(dna.getID().equals(id))
			{
				store(dna);
				mEntries.remove(dna);
				
				return;
			}
		}
	}
	
	private void handleDelete(Packet<Task> p)
	{
		String id = (String) p.getContent().getData();
		
		for(DNA dna : mEntries)
		{
			if(dna.getID().equals(id))
			{
				mEntries.remove(dna);
				break;
			}
		}
	}
}
