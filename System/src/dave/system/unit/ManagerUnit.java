package dave.system.unit;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import dave.json.JsonArray;
import dave.json.JsonBuilder;
import dave.json.JsonCollectors;
import dave.json.JsonValue;
import dave.json.Saveable;
import dave.system.net.Address;
import dave.system.net.BroadcastAddress;
import dave.system.net.Node;
import dave.system.net.Nodes;
import dave.system.net.Packet;
import dave.system.task.Task;
import dave.system.task.Tasks;
import dave.system.unit.SystemUnit.LogEntry;
import dave.util.log.Logger;
import dave.util.log.Severity;
import dave.util.log.SimpleFormatter;

public class ManagerUnit extends BaseUnit
{
	private final int mPeriod;
	private final Set<Client> mClients;
	
	public ManagerUnit(Node<Task> node, int p)
	{
		super(node);
		
		mPeriod = p;
		mClients = new HashSet<>();
		
		registerPacketHandler(Tasks.Status.ID, Tasks.Status.ACTION_GATHER, this::handleGather);
		registerPacketHandler(Tasks.Status.ID, Tasks.Status.ACTION_PUBLISH, this::handlePublish);
		
		afterStart(() -> scheduleReports(5));
	}
	
	private void scheduleReports(int p)
	{
		getNode().send(Nodes.TIMER, new Task(
			Tasks.Timer.ID, Tasks.Timer.ACTION_SCHEDULE, 0,
			new TimerUnit.FutureTask("collect-stati",
				getNode().getID(), new Task(
					Tasks.Status.ID, Tasks.Status.ACTION_GATHER, newSession()),
			p)));
	}

	@Override
	protected JsonValue getStatus( )
	{
		return (new JsonBuilder()).put("clients", mClients.stream().map(Client::save).collect(JsonCollectors.ofArray())).toJSON();
	}
	
	private void handleGather(Packet<Task> p)
	{
		final Task t = new Task(Tasks.Status.ID, Tasks.Status.ACTION_REQUEST, newSession());
		
		getNode().send(new BroadcastAddress(Nodes.SYSTEM), t);
		getNode().send(Nodes.SYSTEM, t);
		
		scheduleReports(mPeriod);
	}
	
	private void handlePublish(Packet<Task> p)
	{
		LOG.log(Severity.INFO, "Received report from %s", p.getSender());
		
		((JsonArray) p.getContent().getData()).stream().map(LogEntry::load).forEach(e -> {
			LOG.log(Severity.INFO, "[%12s] %s", e.node, e.message.toString());
		});
		
		mClients.removeIf(e -> e.client.equals(p.getSender()));
		mClients.add(new Client(p.getSender(), new Date()));
	}
	
	private static class Client implements Saveable
	{
		public final Address client;
		public final Date last;
		
		public Client(Address client, Date last)
		{
			this.client = client;
			this.last = last;
		}
		
		@Override
		public JsonValue save( )
		{
			return (new JsonBuilder())
				.putString("address", client.toString())
				.putString("timestamp", SimpleFormatter.FORMAT.format(last)).toJSON();
		}
	}
	
	private static final Logger LOG = Logger.get("u-status");
}
