package dave.system.unit;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import dave.json.Container;
import dave.json.JSON;
import dave.json.JsonBuilder;
import dave.json.JsonObject;
import dave.json.JsonValue;
import dave.json.Loader;
import dave.json.Saver;
import dave.system.net.Node;
import dave.system.net.Packet;
import dave.system.task.Task;
import dave.system.task.Tasks;
import dave.util.Identifiable;
import dave.util.log.Logger;
import dave.util.log.Severity;

public class TimerUnit extends BaseUnit
{
	private final ScheduledExecutorService mTimer;
	private final Map<String, ScheduledFuture<?>> mHandles;
	
	public TimerUnit(Node<Task> node)
	{
		super(node);
		
		mTimer = Executors.newSingleThreadScheduledExecutor();
		mHandles = Collections.synchronizedMap(new HashMap<>());
		
		onStop(this::shutdown);

		registerPacketHandler(Tasks.Timer.ID, Tasks.Timer.ACTION_SCHEDULE, this::handleSchedule);
		registerPacketHandler(Tasks.Timer.ID, Tasks.Timer.ACTION_REMOVE, this::handleRemove);
	}
	
	private void shutdown( )
	{
		mHandles.clear();
		mTimer.shutdownNow();
	}
	
	@Override
	public JsonValue getStatus( )
	{
		return (new JsonBuilder()).putInt("events", mHandles.size()).toJSON();
	}

	private void handleSchedule(Packet<Task> p)
	{
		final FutureTask t = (FutureTask) p.getContent().getData();
		final String id = p.getSender() + "." + t.id;
		
		mHandles.put(id, mTimer.schedule(() -> {
			getNode().send(t.target, t.payload);
			mHandles.remove(id);
		}, t.delay, TimeUnit.SECONDS));
		
		LOG.log(Severity.INFO, "Task %s was registered to run in %d seconds.", id, t.delay);
	}

	private void handleRemove(Packet<Task> p)
	{
		String id = p.getSender() + "." + (String) p.getContent().getData();
		ScheduledFuture<?> f = mHandles.remove(id);
		
		if(f != null)
		{
			f.cancel(false);
		}
		
		LOG.log(Severity.INFO, "Task %s was removed.%s", id, (f == null ? " [FAIL]" : ""));
	}
	
	@Container
	public static class FutureTask implements Identifiable
	{
		public final String id;
		public final String target;
		public final Task payload;
		public final int delay;
		
		public FutureTask(String id, String target, Task payload, int delay)
		{
			this.id = id;
			this.target = target;
			this.payload = payload;
			this.delay = delay;
		}

		@Override
		public String getID( )
		{
			return id;
		}
		
		@Override
		public String toString( )
		{
			return String.format("FutureTask{%s @%d to %s (%s - %s)}", id, delay, target, payload.getID(), payload.getAction());
		}

		@Saver
		public JsonValue save( )
		{
			JsonObject json = new JsonObject();
			
			json.putString("id", id);
			json.putString("target", target);
			json.put("payload", JSON.serialize(payload));
			json.putInt("delay", delay);
			
			return json;
		}

		@Loader
		public static FutureTask load(JsonValue json)
		{
			JsonObject o = (JsonObject) json;
			
			String id = o.getString("id");
			String target = o.getString("target");
			Task payload = (Task) JSON.deserialize(o.get("payload"));
			int delay = o.getInt("delay");
			
			return new FutureTask(id, target, payload, delay);
		}
	}
	
	private static final Logger LOG = Logger.get("u-timer");
}
