package dave.system.unit;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import dave.json.JsonBuilder;
import dave.json.JsonValue;
import dave.nn.dna.DNA;
import dave.system.Result;
import dave.system.common.Tester;
import dave.system.net.Node;
import dave.system.net.Nodes;
import dave.system.net.Packet;
import dave.system.task.Task;
import dave.system.task.Tasks;
import dave.util.log.Logger;
import dave.util.log.Severity;

public class TesterUnit extends BaseUnit
{
	private final Tester mTester;
	private final ExecutorService mAsync;
	private final BlockingQueue<Entry> mResults;
	private final int mCapacity;
	private final long mStart;
	private int mSentRequests;
	private long mResultCurrent, mResultTotal;
	private boolean mRunning;
	
	public TesterUnit(Node<Task> node, int cap, Tester t)
	{
		super(node);
		
		mTester = t;
		mCapacity = cap;
		mSentRequests = 0;
		mResultCurrent = 0;
		mResultTotal = 0;
		mStart = System.currentTimeMillis();
		mAsync = Executors.newFixedThreadPool(mCapacity + 1);
		mResults = new ArrayBlockingQueue<>(mCapacity);
		mRunning = false;
		
		afterStart(this::startProcessor);
		beforeStop(this::stopProcessor);
		
		registerPacketHandler(Tasks.FetchDNA.ID, Tasks.FetchDNA.ACTION_OFFER, this::handleOffer);
	}
	
	@Override
	public JsonValue getStatus( )
	{
		long d = System.currentTimeMillis() - mStart;
		long c = mResultCurrent; mResultCurrent = 0;
		
		mResultTotal += c;
		
		double avg = mResultTotal * 60000 / (double) d;
		
		return (new JsonBuilder()).putLong("tests", c).putNumber("average", avg).putInt("running", mResults.size()).putInt("pending", mSentRequests).toJSON();
	}
	
	private void startProcessor( )
	{
		mRunning = true;
		request();
		mAsync.submit(this::process);
	}
	
	private void stopProcessor( )
	{
		mRunning = false;
		mAsync.shutdownNow();
	}
	
	private void process( )
	{
		try
		{
			while(mRunning)
			{
				Entry entry = mResults.take();
				
				if(mRunning) try
				{
					getNode().send(Nodes.SCORE,
						new Task(Tasks.Report.ID, Tasks.Report.ACTION_REPORT, newSession(),
							new Result(entry.dna, entry.score.get())));
				}
				catch(ExecutionException e)
				{
					LOG.log(Severity.ERROR, "DNA %s threw an exception (%s) during testing!", entry.dna, e.getMessage());
					e.printStackTrace();
				}
				
				++mResultCurrent;
				
				request();
			}
		}
		catch(InterruptedException e)
		{
			LOG.log(Severity.INFO, "Shutting down testing unit!");
		}
	}
	
	private void request( )
	{
		while(mSentRequests < mResults.remainingCapacity())
		{
			++mSentRequests;
			getNode().send(Nodes.DNA_POOL,
				new Task(Tasks.FetchDNA.ID, Tasks.FetchDNA.ACTION_REQUEST, newSession()));
		}
	}
	
	private void handleOffer(Packet<Task> p)
	{
		if(mAsync.isShutdown()) return;
		
		if(mResults.remainingCapacity() > 0)
		{
			DNA dna = (DNA) p.getContent().getData();
			
			--mSentRequests;
			
			mResults.add(new Entry(dna.getID(), mAsync.submit(() -> mTester.score(dna))));
		}
		else
		{
			LOG.log(Severity.WARNING, "Trying to run an additional dna: %s !", p);
		}
	}
	
	private static class Entry
	{
		public final String dna;
		public final Future<Integer> score;
		
		public Entry(String dna, Future<Integer> score)
		{
			this.dna = dna;
			this.score = score;
		}
	}
	
	private static final Logger LOG = Logger.get("u-test");
}
