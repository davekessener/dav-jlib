package dave.system.client;

import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import dave.json.Container;
import dave.json.JsonArray;
import dave.json.JsonObject;
import dave.json.JsonValue;
import dave.json.Loader;
import dave.json.Saveable;
import dave.json.Saver;
import dave.system.common.Recording;

@Container
public class SimpleRecording implements Recording, Saveable
{
	private final List<Entry> mRecord = new LinkedList<>();
	private Entry mCurrent = null;
	
	@Override
	public Iterator<boolean[]> iterator( )
	{
		return new Iterator<boolean[]>() {
			private final Iterator<Entry> mmIter = mRecord.iterator();
			private Entry mmCurrent = mmIter.next();
			private int mmTickCount = 0;
			
			@Override
			public boolean[] next( )
			{
				++mmTickCount;
				
				if(mmIter.hasNext() && mmTickCount >= mmCurrent.next)
				{
					mmCurrent = mmIter.next();
				}
				
				return mmCurrent.content;
			}
			
			@Override
			public boolean hasNext( )
			{
				return true;
			}
		};
	}

	@Override
	public void update(int tc, boolean[] e)
	{
		if(mCurrent == null || !Arrays.equals(e, mCurrent.content))
		{
			if(mCurrent != null)
			{
				mCurrent.next = tc;
			}
			
			mRecord.add(mCurrent = new Entry(e));
		}
	}
	
	private void add(Entry e)
	{
		mRecord.add(mCurrent = e);
	}
	
	@Override
	@Saver
	public JsonValue save( )
	{
		JsonArray json = new JsonArray();
		
		mRecord.stream().forEachOrdered(e -> json.add(e.save()));
		
		return json;
	}
	
	@Loader
	public static SimpleRecording load(JsonValue json)
	{
		SimpleRecording r = new SimpleRecording();
		
		((JsonArray) json).stream().forEachOrdered(v -> r.add(Entry.load(v)));
		
		return r;
	}

	private static final class Entry implements Saveable
	{
		public int next;
		public final boolean[] content;
		
		public Entry(boolean[] c)
		{
			content = c;
		}
		
		@Override
		public JsonValue save( )
		{
			JsonObject json = new JsonObject();
			
			json.putInt("next", next);
			json.put("content", new JsonArray(content));
			
			return json;
		}
		
		public static Entry load(JsonValue json)
		{
			JsonObject o = (JsonObject) json;
			
			Entry r = new Entry(o.getArray("content").asBooleans());
			
			r.next = o.getInt("next");
			
			return r;
		}
	}
}
