package dave.system.client;

import dave.nn.NeuralNetwork;
import dave.nn.conv.WritableAllocator;
import dave.nn.dna.DNA;
import dave.util.Transformer;

public class NeuralNetworkAdapter implements Transformer<DNA, NeuralNetwork>
{
	private final WritableAllocator mSource;
	private final NeuralNetwork mNet;
	
	public NeuralNetworkAdapter(WritableAllocator a, NeuralNetwork nn)
	{
		mSource = a;
		mNet = nn;
	}

	@Override
	public NeuralNetwork create(DNA source)
	{
		mSource.setContent(source.getBase());
		
		return mNet;
	}
}
