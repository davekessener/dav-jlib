package dave.system.client;

import dave.game.IEngine;
import dave.game.Key;

import dave.game.GameUtils;
import dave.nn.NeuralNetwork;
import dave.nn.dna.DNA;
import dave.system.common.Recording;
import dave.system.common.Tester;
import dave.util.Producer;
import dave.util.Transformer;

public class ModuleTester implements Tester
{
	private final Transformer<DNA, NeuralNetwork> mAI;
	private final Producer<IEngine> mGameCreator;
	private final Transformer<Float, Boolean> mKeyTransform;
	private final Producer<HeartbeatMonitor> mMonitor;
	private final Recording mRecording;
	private final int mFrameSkip;
	
	public ModuleTester(
		int s,
		Transformer<DNA, NeuralNetwork> ai,
		Producer<IEngine> p,
		Producer<HeartbeatMonitor> m,
		Transformer<Float, Boolean> f)
	{
		mAI = ai;
		mGameCreator  = p;
		mKeyTransform = f;
		mMonitor = m;
		mRecording = new SimpleRecording();
		mFrameSkip = s;
	}
	
	public Recording getRecord( ) { return mRecording; }

	@Override
	public int score(DNA dna)
	{
		IEngine game = mGameCreator.produce();
		NeuralNetwork ai = mAI.create(dna);
		HeartbeatMonitor monitor = mMonitor.produce();
		
		for(int i = 0 ; game.isAlive() && monitor.isAlive(game) ; ++i)
		{
			game.tick();
			
			if(i % mFrameSkip == 0)
			{
				game.draw();
				
				float[] raw = ai.calculate(GameUtils.transform(game.getScreen()));
				boolean[] btns = new boolean[raw.length];
				
				for(int j = 0 ; j < raw.length ; ++j)
				{
					btns[j] = mKeyTransform.create(raw[j]);
				}
				
				mRecording.update(i, btns);
				
				for(int j = 0 ; j < btns.length ; ++j)
				{
					game.setKeyState(Key.values()[j], btns[j]);
				}
			}
		}
		
		return game.score();
	}

	public static interface HeartbeatMonitor { boolean isAlive(IEngine game); }
	
	public static class LifetimeMonitor implements HeartbeatMonitor
	{
		@Override
		public boolean isAlive(IEngine game)
		{
			return game.isAlive();
		}
	}
	
	public static class CountdownMonitor implements HeartbeatMonitor
	{
		private final int mCountdown;
		private int mLastScore;
		private int mCounter;
		
		public CountdownMonitor(int c)
		{
			mCountdown = c;
			mLastScore = 0;
			mCounter = mCountdown;
		}
		
		@Override
		public boolean isAlive(IEngine game)
		{
			int s = game.score();
			
			if(mLastScore == s)
			{
				if(mCounter > 0) --mCounter;
			}
			else
			{
				mCounter = mCountdown;
			}
			
			mLastScore = s;
			
			return mCounter > 0;
		}
	}
}
