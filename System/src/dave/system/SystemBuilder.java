package dave.system;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

import dave.json.JsonValue;
import dave.net.server.PacketedServer;
import dave.net.server.Server;
import dave.nn.dna.DNA;
import dave.nn.dna.Prototype;
import dave.system.common.Database;
import dave.system.common.Scoreboard;
import dave.system.common.Tester;
import dave.system.common.Unit;
import dave.system.net.Address;
import dave.system.net.BroadcastAddress;
import dave.system.net.BroadcastServerRelay;
import dave.system.net.FakeRelay;
import dave.system.net.LayeredRelay;
import dave.system.net.LocalRelay;
import dave.system.net.Node;
import dave.system.net.NodeRegistry;
import dave.system.net.NodeSynchronizer;
import dave.system.net.Nodes;
import dave.system.net.Packet;
import dave.system.net.Relay;
import dave.system.net.ServerRelay;
import dave.system.net.UniqueAddress;
import dave.system.task.Task;
import dave.system.unit.BreederUnit;
import dave.system.unit.ConnectionUnit;
import dave.system.unit.DNAPoolUnit;
import dave.system.unit.DatabaseUnit;
import dave.system.unit.ManagerUnit;
import dave.system.unit.PublisherUnit;
import dave.system.unit.ScoreUnit;
import dave.system.unit.SystemUnit;
import dave.system.unit.TesterUnit;
import dave.system.unit.TimerUnit;
import dave.util.io.SevereIOException;

public class SystemBuilder
{
	private final Wrapper mSynchronizer;
	private final LayeredRelay<Task> mNetwork;
	private final LocalRelay<Task> mLocal;
	private final List<Unit> mUnits;
	private final Consumer<Packet<Task>> mServer;
	
	public SystemBuilder(Wrapper f)
	{
		mSynchronizer = f;
		mLocal = new LocalRelay<>();
		mNetwork = new LayeredRelay<>(mLocal);
		mUnits = new ArrayList<>();
		mServer = mSynchronizer.wrap(p -> mNetwork.accept(p));
	}
	
	protected Consumer<Packet<Task>> getNetwork( )
	{
		return mServer;
	}
	
	protected NodeRegistry<Task> getRegistry( )
	{
		return mLocal;
	}
	
	public SystemBuilder install(Unit unit)
	{
		mUnits.add(unit);
		
		mLocal.register(unit.getNode());
		
		unit.getNode().setClient(old -> mSynchronizer.wrap(old));
		unit.getNode().setServer(old -> mServer);
		
		return this;
	}
	
	public void remove(Unit unit)
	{
		unit.getNode().setClient(old -> (p -> {}));
		unit.getNode().setServer(old -> (p -> {}));
		
		mLocal.deregister(unit.getNode());
		
		mUnits.remove(unit);
	}
	
	public SystemBuilder install(Module module)
	{
		module.installOn(this);
		
		return this;
	}
	
	public SystemBuilder install(Predicate<Address> f, Relay<Task> cb)
	{
		mNetwork.register(f, cb);
		
		return this;
	}
	
	public BaseSystem build( )
	{
		return new DistributedAISystem(mNetwork, mUnits);
	}

// # --------------------------------------------------------------------------
	
	public static interface Module
	{
		public abstract void installOn(SystemBuilder builder);
	}
	
	public static class BaseModule implements Module
	{
		@Override
		public void installOn(SystemBuilder builder)
		{
			builder.install(new SystemUnit(new Node<>(Nodes.SYSTEM)));
			builder.install(new PublisherUnit(new Node<>(Nodes.PUBLISHER)));
			builder.install(new TimerUnit(new Node<>(Nodes.TIMER)));
		}
	}
	
	public static class LocalClientModule implements Module
	{
		private final Tester mTest;
		private final int mTestCap, mPoolCap;
		private final String mSource;
		
		public LocalClientModule(Tester t, String s, int tc, int pc)
		{
			mTest = t;
			mTestCap = tc;
			mPoolCap = pc;
			mSource = s;
		}
		
		@Override
		public void installOn(SystemBuilder builder)
		{
			builder.install(new TesterUnit(new Node<>(Nodes.TESTER), mTestCap, mTest));
			builder.install(new DNAPoolUnit(new Node<>(Nodes.DNA_POOL), mSource, mPoolCap));
		}
	}
	
	public static class LocalServerModule implements Module
	{
		private final Prototype mProto;
		private final float mChance;
		private final Database<DNA> mDatabase;
		private final Scoreboard mScore;
		private final Consumer<JsonValue> mPersistance;
		
		public LocalServerModule(Prototype p, Database<DNA> db, Scoreboard s, Consumer<JsonValue> dbp, float c)
		{
			mProto = p;
			mPersistance = dbp;
			mChance = c;
			mDatabase = db;
			mScore = s;
		}

		@Override
		public void installOn(SystemBuilder builder)
		{
			builder.install(new BreederUnit(new Node<>(Nodes.BREEDER), mProto, mChance));
			builder.install(new DatabaseUnit(new Node<>(Nodes.DATABASE), mPersistance, mDatabase));
			builder.install(new ScoreUnit(new Node<>(Nodes.SCORE), mScore));
		}
	}
	
	public static class NetworkModule implements Module
	{
		private final InetSocketAddress mMC;
		
		public NetworkModule(InetSocketAddress mc)
		{
			mMC = mc;
		}
		
		@Override
		public void installOn(SystemBuilder builder)
		{
			try
			{
				final Consumer<Packet<Task>> network = builder.getNetwork();
				final Server.Handler<Server.Datagram> f = p -> network.accept(SystemUtils.transform(p));
				final PacketedServer udp = Server.createUDPServer(SystemUtils.UDP_PORT, f);
				final PacketedServer mc = Server.createMulticastServer(SystemUtils.MULTICAST_ADDR, mMC, f);
				
				builder.install(a -> (a instanceof UniqueAddress), new ServerRelay<>(udp));
				builder.install(a -> (a instanceof BroadcastAddress), new BroadcastServerRelay<>(mc));
			}
			catch(IOException e)
			{
				throw new SevereIOException(e);
			}
		}
	}
	
	public static class ServerNetworkModule implements Module
	{
		@Override
		public void installOn(SystemBuilder builder)
		{
			try
			{
				final Server tcp = Server.createTCPServer(SystemUtils.TCP_PORT, c -> {
					Unit unit = new ConnectionUnit(builder, c);
					
					builder.install(unit);
					
					unit.start();
				});
				
				builder.install(a -> false, new FakeRelay<>(tcp));
			}
			catch(IOException e)
			{
				throw new SevereIOException(e);
			}
		}
	}
	
	public static class ManagerModule implements Module
	{
		private final int mPeriod;
		
		public ManagerModule(int p)
		{
			mPeriod = p;
		}
		
		@Override
		public void installOn(SystemBuilder builder)
		{
			builder.install(new ManagerUnit(new Node<>(Nodes.MANAGER), mPeriod));
		}
	}

// # --------------------------------------------------------------------------
	
	public static interface Wrapper
	{
		public abstract Consumer<Packet<Task>> wrap(Consumer<Packet<Task>> source);
	}
	
	public static class DefaultWrapper implements Wrapper
	{
		@Override
		public Consumer<Packet<Task>> wrap(Consumer<Packet<Task>> source)
		{
			return source;
		}
	}
	
	public static class DistributingWrapper implements Wrapper
	{
		@Override
		public Consumer<Packet<Task>> wrap(Consumer<Packet<Task>> source)
		{
			return new NodeSynchronizer<>(source);
		}
	}

}
