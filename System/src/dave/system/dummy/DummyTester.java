package dave.system.dummy;

import java.util.Random;

import dave.nn.dna.DNA;
import dave.system.common.Tester;

public class DummyTester implements Tester
{
	private final Random mRandom = new Random(System.currentTimeMillis());
	
	@Override
	public int score(DNA dna)
	{
		return mRandom.nextInt(1000);
	}
}
