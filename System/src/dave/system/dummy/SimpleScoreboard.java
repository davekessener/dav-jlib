package dave.system.dummy;

import java.util.Iterator;

import dave.json.JsonArray;
import dave.json.JsonValue;
import dave.system.Result;
import dave.system.common.Scoreboard;

public class SimpleScoreboard implements Scoreboard
{
	private final int mCapacity;
	private final Element mRoot;
	private int mSize;
	private int mHighScore;
	
	public SimpleScoreboard(int capacity)
	{
		mRoot = new Element(null);
		mCapacity = capacity;
		mSize = 0;
		mHighScore = 0;
	}
	
	@Override
	public JsonValue save( )
	{
		JsonArray json = new JsonArray();
		
		for(Element e = mRoot.next ; e != null ; e = e.next)
		{
			json.add(e.value.save());
		}
		
		return json;
	}
	
	@Override
	public void load(JsonValue json)
	{
		mRoot.next = null;
		mSize = 0;
		mHighScore = 0;
		
		((JsonArray) json).stream().forEach(e -> update(Result.load(e)));
	}
	
	@Override
	public void cull( )
	{
		if(mSize == mCapacity)
		{
			int n = mSize / 2 - 3;
			
			for(Element e = mRoot ; n > 0 ; e = e.next, --n)
			{
				e.next = e.next.next;
				--mSize;
			}
		}
	}
	
	@Override
	public int size( )
	{
		return mSize;
	}
	
	@Override
	public boolean contains(String id)
	{
		for(Element e = mRoot.next ; e != null ; e = e.next)
		{
			if(e.value.dna.equals(id)) return true;
		}
		
		return false;
	}
	
	@Override
	public boolean remove(String id)
	{
		for(Element e = mRoot ; e != null ; e = e.next)
		{
			if(e.next != null && e.next.value.dna.equals(id))
			{
				e.next = e.next.next;
				--mSize;
				
				return true;
			}
		}
		
		return false;
	}
	
	@Override
	public Iterator<Result> iterator( )
	{
		return new Iterator<Result>() {
			private Element e = mRoot;
			
			@Override
			public Result next( )
			{
				e = e.next;
				
				return e.value;
			}
			
			@Override
			public boolean hasNext( )
			{
				return e.next != null;
			}
		};
	}
	
	@Override
	public int highScore( )
	{
		return mHighScore;
	}
	
	@Override
	public String get(float p)
	{
		int idx = (int) Math.floor(p * mCapacity);
		
		if(idx < 0 || idx >= mCapacity)
			throw new ArrayIndexOutOfBoundsException();
		
		if(idx >= mSize)
			return null;
		
		idx = (mSize - 1) - idx;
		
		Element e = mRoot.next;
		
		while(idx-- > 0) e = e.next;
		
		return e.value.dna;
	}

	@Override
	public String update(Result r)
	{
		String removed = null;
		
		for(Element e = mRoot ; e != null ; e = e.next)
		{
			if(e.next != null && e.next.value.dna.equals(r.dna))
			{
				e.next = e.next.next;
				--mSize;
				break;
			}
		}
		
		for(Element e = mRoot ; e != null ; e = e.next)
		{
			if(e.next == null || e.next.value.score > r.score)
			{
				e.next = new Element(r, e.next);
				++mSize;
				break;
			}
		}
		
		if(mSize > mCapacity)
		{
			removed = mRoot.next.value.dna;
			mRoot.next = mRoot.next.next;
			--mSize;
		}
		
		mHighScore = Math.max(mHighScore, r.score);
		
		return removed;
	}
	
	private static class Element
	{
		public Element next;
		public Result value;
		
		public Element(Result dna) { this(dna, null); }
		public Element(Result dna, Element next)
		{
			this.value = dna;
			this.next = next;
		}
	}
}
