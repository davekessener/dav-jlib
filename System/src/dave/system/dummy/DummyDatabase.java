package dave.system.dummy;

import java.util.HashMap;
import java.util.Map;

import dave.json.Container;
import dave.system.common.Database;
import dave.util.Identifiable;

@Container
public class DummyDatabase<T extends Identifiable> implements Database<T>
{
	private final Map<String, T> mDB = new HashMap<>();
	
	@Override
	public boolean has(String id)
	{
		return mDB.containsKey(id);
	}
	
	@Override
	public void store(T dna)
	{
		mDB.put(dna.getID(), dna);
	}

	@Override
	public T retrieve(String id)
	{
		T dna = mDB.get(id);
		
		if(dna == null)
			throw new ArrayIndexOutOfBoundsException();
		
		return dna;
	}

	@Override
	public void remove(String id)
	{
		mDB.remove(id);
	}
}
