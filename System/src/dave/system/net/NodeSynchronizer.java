package dave.system.net;

import java.util.function.Consumer;

import dave.util.Distributor;
import dave.util.ShutdownService;

public class NodeSynchronizer<T> implements Consumer<Packet<T>>
{
	private final Distributor<Packet<T>> mThread;
	
	public NodeSynchronizer(Consumer<Packet<T>> cb)
	{
		mThread = new Distributor<>(p -> cb.accept(p));
		
		mThread.start();
		
		ShutdownService.INSTANCE.register(mThread::stop);
	}

	@Override
	public void accept(Packet<T> p)
	{
		mThread.accept(p);
	}
}
