package dave.system.net;

import dave.json.Container;
import dave.json.JsonString;
import dave.json.JsonValue;
import dave.json.Loader;
import dave.json.Saver;

@Container
public class BroadcastAddress implements Address
{
	private final String mNode;
	
	public BroadcastAddress(String n)
	{
		mNode = n;
	}
	
	@Saver
	public JsonValue save( )
	{
		return new JsonString(mNode);
	}
	
	@Loader
	public static BroadcastAddress load(JsonValue json)
	{
		return new BroadcastAddress(((JsonString) json).get());
	}

	@Override
	public String getNodeID( )
	{
		return mNode;
	}
	
	@Override
	public int hashCode( )
	{
		return mNode.hashCode() ^ 0x84826594;
	}
	
	@Override
	public boolean equals(Object o)
	{
		if(o instanceof BroadcastAddress)
		{
			return mNode.equals(((LocalAddress) o).node);
		}
		
		return false;
	}
	
	@Override
	public String toString( )
	{
		return "*/" + mNode;
	}
}
