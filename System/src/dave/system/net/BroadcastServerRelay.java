package dave.system.net;

import java.net.SocketAddress;

import dave.net.server.PacketedServer;

public class BroadcastServerRelay<T> extends ServerRelay<T>
{
	public BroadcastServerRelay(PacketedServer s)
	{
		super(s);
	}
	
	@Override
	protected SocketAddress findRemote(Address a)
	{
		return null;
	}
}
