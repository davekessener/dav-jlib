package dave.system.net;

public interface Address
{
	public abstract String getNodeID( );
	
	public static final Address NIL = new LocalAddress("-/-");
}
