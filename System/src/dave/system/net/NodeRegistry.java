package dave.system.net;

public interface NodeRegistry<T>
{
	public abstract void register(Node<T> n);
	public abstract void deregister(Node<T> n);
}
