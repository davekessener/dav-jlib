package dave.system.net;

import java.util.function.Consumer;

import dave.util.log.Logger;
import dave.system.task.LoggingConsumer;
import dave.util.Identifiable;

public class Node<T> implements Identifiable, Consumer<Packet<T>>
{
	public static interface Nester<T> { Consumer<T> nest(Consumer<T> old); }
	
	private final Address mSelf;
	private Consumer<Packet<T>> mClient, mServer;
	
	public Node(String id) { this(id, new LoggingConsumer<>(Logger.DEFAULT), new LoggingConsumer<>(Logger.DEFAULT)); }
	public Node(String id, Consumer<Packet<T>> server) { this(id, new LoggingConsumer<>(Logger.DEFAULT), server); }
	public Node(String id, Consumer<Packet<T>> client, Consumer<Packet<T>> server)
	{
		mSelf = new LocalAddress(id);
		mClient = client;
		mServer = server;
	}

	@Override
	public String getID( )
	{
		return mSelf.getNodeID();
	}

	@Override
	public void accept(Packet<T> t)
	{
		mClient.accept(t);
	}
	
	public void send(String to, T content)
	{
		mServer.accept(new Packet<>(mSelf, new LocalAddress(to), content));
	}
	
	public void send(Address to, T content)
	{
		mServer.accept(new Packet<>(mSelf, to, content));
	}
	
	public void setClient(Nester<Packet<T>> cb) { mClient = cb.nest(mClient); }
	public void setServer(Nester<Packet<T>> cb) { mServer = cb.nest(mServer); }
}
