package dave.system.net;

import dave.json.Container;
import dave.json.JsonString;
import dave.json.JsonValue;
import dave.json.Loader;
import dave.json.Saver;

@Container
public class LocalAddress implements Address
{
	public final String node;
	
	public LocalAddress(String node)
	{
		this.node = node;
	}
	
	@Override
	public String getNodeID( )
	{
		return node;
	}

	@Saver
	public JsonValue save( )
	{
		return new JsonString(node);
	}
	
	@Loader
	public static LocalAddress load(JsonValue json)
	{
		return new LocalAddress(((JsonString) json).get());
	}
	
	@Override
	public int hashCode( )
	{
		return node.hashCode() ^ 0x41424344;
	}
	
	@Override
	public boolean equals(Object o)
	{
		if(o instanceof LocalAddress)
		{
			return node.equals(((LocalAddress) o).node);
		}
		
		return false;
	}
	
	@Override
	public String toString( )
	{
		return node;
	}
}
