package dave.system.net;

import java.io.IOException;
import java.net.SocketAddress;

import dave.net.server.PacketedServer;
import dave.util.log.Logger;
import dave.util.io.SevereIOException;

public class ServerRelay<T> extends FakeRelay<T, PacketedServer>
{
	public ServerRelay(PacketedServer s)
	{
		super(s);
	}
	
	protected SocketAddress findRemote(Address a)
	{
		return ((UniqueAddress) a).host;
	}
	
	@Override
	public void accept(Packet<T> t)
	{
		LOG.log("%s", t.toString());
		
		try
		{
			getImplementation().send(findRemote(t.getRecipient()), t.save());
		}
		catch(IOException e)
		{
			throw new SevereIOException(e);
		}
	}
	
	private static final Logger LOG = Logger.get("relay");
}
