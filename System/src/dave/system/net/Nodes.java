package dave.system.net;

public final class Nodes
{
	public static final String SYSTEM = "system";
	public static final String MANAGER = "manager";
	public static final String PUBLISHER = "publisher";
	public static final String DNA_POOL = "dna-pool";
	public static final String BREEDER = "breeder";
	public static final String SCORE = "scoreboard";
	public static final String DATABASE = "database";
	public static final String TESTER = "tester";
	public static final String TIMER = "timer";
	public static final String REMOTE = "remote";
	public static final String BACKUP = "backup";
	public static final String CALLBACK = "task-executor";
	
	private Nodes( ) { }
}
