package dave.system.net;

import java.util.HashMap;
import java.util.Map;

import dave.util.log.Logger;
import dave.util.log.Severity;

public class LocalRelay<T> implements Relay<T>, NodeRegistry<T>
{
	private final Map<String, Node<T>> mNodes = new HashMap<>();
	
	@Override
	public void register(Node<T> n)
	{
		String id = n.getID();
		
		if(mNodes.containsKey(id))
			throw new IllegalArgumentException(id);
		
		LOG.log(Severity.INFO, "Registering new node %s.", id);
		
		mNodes.put(id, n);
	}
	
	@Override
	public void deregister(Node<T> n)
	{
		String id = n.getID();

		LOG.log(Severity.INFO, "Removing node %s.", id);
		
		if(mNodes.remove(id) != n)
			throw new IllegalArgumentException(id);
	}

	@Override
	public void accept(Packet<T> p)
	{
		Node<T> n = mNodes.get(p.getRecipient().getNodeID());
		
		LOG.log("%s%s", p, (n == null ? " [FAIL]" : ""));
		
		if(n != null)
		{
			n.accept(p);
		}
	}
	
	private static final Logger LOG = Logger.get("relay");
}
