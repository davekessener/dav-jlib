package dave.system.net;

import dave.json.Container;
import dave.json.JSON;
import dave.json.JsonObject;
import dave.json.JsonValue;
import dave.json.Loader;
import dave.json.Saveable;
import dave.json.Saver;

@Container
public class Packet<T> implements Saveable
{
	private final Address mFrom, mTo;
	private final T mPayload;
	
	public Packet(Address from, Address to, T payload)
	{
		mFrom = from;
		mTo = to;
		mPayload = payload;
	}
	
	public Address getSender( ) { return mFrom; }
	public Address getRecipient( ) { return mTo; }
	public T getContent( ) { return mPayload; }
	
	@Override
	@Saver
	public JsonValue save( )
	{
		JsonObject json = new JsonObject();
		
		json.put("from", JSON.serialize(mFrom));
		json.put("to", JSON.serialize(mTo));
		json.put("content", JSON.serialize(mPayload));
		
		return json;
	}
	
	@SuppressWarnings("unchecked")
	@Loader
	public static <T> Packet<T> load(JsonValue json)
	{
		JsonObject o = (JsonObject) json;
		
		Address from = (Address) JSON.deserialize(o.get("from"));
		Address to = (Address) JSON.deserialize(o.get("to"));
		Object content = JSON.deserialize(o.get("content"));
		
		return new Packet<>(from, to, (T) content);
	}
	
	@Override
	public int hashCode( )
	{
		return mFrom.hashCode() ^ mTo.hashCode() ^ mPayload.hashCode();
	}
	
	@Override
	public boolean equals(Object o)
	{
		if(o instanceof Packet)
		{
			Packet<?> p = (Packet<?>) o;
			
			return p.mFrom.equals(mFrom) && p.mTo.equals(mTo) && p.mPayload.equals(mPayload);
		}
		
		return false;
	}
	
	@Override
	public String toString( )
	{
		return String.format("{%s -> %s: %s}", mFrom, mTo, mPayload);
	}
}
