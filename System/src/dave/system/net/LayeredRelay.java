package dave.system.net;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Predicate;

public class LayeredRelay<T> implements Relay<T>
{
	private final List<Entry<T>> mRelays;

	@Override
	public void start( )
	{
		for(Entry<T> e : mRelays)
		{
			e.callback.start();
		}
	}

	@Override
	public void stop( )
	{
		for(Entry<T> e : mRelays)
		{
			e.callback.stop();
		}
	}
	
	public LayeredRelay(Relay<T> backup)
	{
		mRelays = new LinkedList<>();
		
		mRelays.add(new Entry<>(a -> true, backup));
	}
	
	public LayeredRelay<T> register(Predicate<Address> f, Relay<T> cb)
	{
		mRelays.add(0, new Entry<>(f, cb));
		
		return this;
	}

	@Override
	public void accept(Packet<T> t)
	{
		for(Entry<T> e : mRelays)
		{
			if(e.filter.test(t.getRecipient()))
			{
				e.callback.accept(t);
				
				break;
			}
		}
	}
	
	private static final class Entry<T>
	{
		public final Predicate<Address> filter;
		public final Relay<T> callback;
		
		public Entry(Predicate<Address> f, Relay<T> r)
		{
			filter = f;
			callback = r;
		}
	}
}
