package dave.system.net;

import java.util.function.Consumer;

import dave.util.Actor;

public interface Relay<T> extends Consumer<Packet<T>>, Actor
{
	public default void start( ) { }
	public default void stop( ) { }
}
