package dave.system.net;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;

import dave.json.Container;
import dave.json.JsonObject;
import dave.json.JsonValue;
import dave.json.Loader;
import dave.json.Saver;

@Container
public class UniqueAddress implements Address
{
	public final String node;
	public final InetSocketAddress host;
	
	public UniqueAddress(String node, InetSocketAddress host)
	{
		this.node = node;
		this.host = host;
	}
	
	@Override
	public String getNodeID( )
	{
		return node;
	}

	@Saver
	public JsonValue save( )
	{
		JsonObject json = new JsonObject();
		
		json.putString("node", node);
		json.putString("ip", host.getAddress().getHostAddress());
		json.putInt("port", host.getPort());
		
		return json;
	}
	
	@Loader
	public static UniqueAddress load(JsonValue json)
	{
		try
		{
			JsonObject o = (JsonObject) json;
			
			String node = o.getString("node");
			String ip = o.getString("ip");
			int port = o.getInt("port");
			
			InetSocketAddress host = new InetSocketAddress(InetAddress.getByName(ip), port);
			
			return new UniqueAddress(node, host);
		}
		catch(UnknownHostException e)
		{
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public int hashCode( )
	{
		return node.hashCode() ^ host.hashCode() ^ 0x54535251;
	}
	
	@Override
	public boolean equals(Object o)
	{
		if(o instanceof UniqueAddress)
		{
			UniqueAddress a = (UniqueAddress) o;
			
			return node.equals(a.node) && host.equals(a.host);
		}
		
		return false;
	}
	
	@Override
	public String toString( )
	{
		return node + "@" + host.getHostString() + ":" + host.getPort();
	}
}
