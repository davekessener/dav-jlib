package dave.system.net;

import dave.util.Actor;

public class FakeRelay<T, A extends Actor> implements Relay<T>
{
	private final A mHook;
	
	public FakeRelay(A cb)
	{
		mHook = cb;
	}
	
	protected A getImplementation( ) { return mHook; }
	
	@Override
	public void start( )
	{
		mHook.start();
	}
	
	@Override
	public void stop( )
	{
		mHook.stop();
	}

	@Override
	public void accept(Packet<T> t)
	{
	}
}
