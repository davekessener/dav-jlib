package dave.system;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import dave.json.JsonValue;
import dave.net.NetUtils;
import dave.net.server.Server.Datagram;
import dave.nn.builder.ConvolutionLayerTemplate;
import dave.nn.builder.FeedForwardLayerTemplate;
import dave.nn.builder.LayerTemplate;
import dave.nn.builder.NetworkTemplate;
import dave.nn.builder.PoolingLayerTemplate;
import dave.nn.builder.RectifyLayerTemplate;
import dave.nn.builder.ReductionLayerTemplate;
import dave.nn.conv.MaxPool;
import dave.nn.conv.MaxRect;
import dave.nn.dna.Range;
import dave.system.net.Address;
import dave.system.net.BroadcastAddress;
import dave.system.net.LocalAddress;
import dave.system.net.Nodes;
import dave.system.net.Packet;
import dave.system.net.UniqueAddress;
import dave.system.task.Task;
import dave.system.task.Tasks;
import dave.util.Utils;
import dave.util.io.SevereIOException;
import dave.util.math.Vec3;

public final class SystemUtils
{
	public static <T> Packet<T> transform(Datagram data)
	{
		Packet<T> p = Packet.load(data.payload);
		Address from = new UniqueAddress(p.getSender().getNodeID(), data.source);
		Address to = new LocalAddress(p.getRecipient().getNodeID());
		
		return new Packet<>(from, to, p.getContent());
	}

	private static class Entry
	{
		public final NetworkInterface nic;
		public final Future<Integer> callback;
		
		public Entry(NetworkInterface nic, Future<Integer> callback)
		{
			this.nic = nic;
			this.callback = callback;
		}
	}

	public static NetworkInterface findActiveNetwork( ) throws SocketException
	{
//		InetSocketAddress group = new InetSocketAddress(SystemUtils.MULTICAST_ADDR, SystemUtils.MULTICAST_PORT);
		Task task = new Task(Tasks.Status.ID, Tasks.Status.ACTION_REQUEST, 0);
		Packet<Task> packet = new Packet<>(new LocalAddress(Nodes.SYSTEM), new BroadcastAddress(Nodes.SYSTEM), task);
		JsonValue json = packet.save();
		
		Predicate<Entry> filter = entry -> {
			try
			{
				return entry.callback.get() > 0;
			}
			catch(ExecutionException | InterruptedException e)
			{
				return false;
			}
		};

		List<NetworkInterface> nics = Utils.stream(NetworkInterface.getNetworkInterfaces())
			.map(nic -> new Entry(nic, NetUtils.queryNIC(nic, SystemUtils.MULTICAST_ADDR, SystemUtils.MULTICAST_PORT, json))).collect(Collectors.toList())
			.stream().filter(filter).map(e -> e.nic).collect(Collectors.toList());
		
		if(nics.isEmpty())
		{
			throw new SevereIOException("Couldn't find an active network!");
		}
		else if(nics.size() > 1)
		{
			throw new SevereIOException("Found multiple active networks: " + nics.stream().map(nic -> nic.getName() + " (" + nic.getDisplayName() + ")").collect(Collectors.joining(", ")));
		}
		else
		{
			return nics.get(0);
		}
	}
	
	public static float MIN = -1.0f, MAX = 1.0f;
	public static final int SCREEN_WIDTH = 160, SCREEN_HEIGHT = 144;
	public static final NetworkTemplate TEMPLATE = new NetworkTemplate(
		new Vec3(SCREEN_WIDTH, SCREEN_HEIGHT, 1),
		new Range(MIN, MAX),
		new LayerTemplate[] {
			new ConvolutionLayerTemplate(4, 7),
			new RectifyLayerTemplate(new MaxRect()),
			new PoolingLayerTemplate(new MaxPool(2)),
			new ReductionLayerTemplate(2),

			new ConvolutionLayerTemplate(4, 16),
			new RectifyLayerTemplate(new MaxRect()),
			new PoolingLayerTemplate(new MaxPool(2)),
			new ReductionLayerTemplate(4),

			new ConvolutionLayerTemplate(2, 4),
			new RectifyLayerTemplate(new MaxRect()),
			new PoolingLayerTemplate(new MaxPool(2)),
			new ReductionLayerTemplate(4),
			
			new FeedForwardLayerTemplate(100),
			new RectifyLayerTemplate(new MaxRect()),
			
			new FeedForwardLayerTemplate(8)
	});

	
	public static final int TCP_PORT = 32000;
	public static final int UDP_PORT = 32001;
	public static final int MULTICAST_PORT = 32002;
	public static final InetAddress MULTICAST_ADDR = defaultMulticastAddress();
	
	private static InetAddress defaultMulticastAddress( )
	{
		try
		{
			return InetAddress.getByName("232.0.0.0");
		}
		catch(UnknownHostException e)
		{
			throw new RuntimeException(e);
		}
	}
	
	private SystemUtils( ) { }
}
