package dave.system;

import dave.json.Container;
import dave.json.JsonObject;
import dave.json.JsonValue;
import dave.json.Loader;
import dave.json.Saveable;
import dave.json.Saver;
import dave.nn.dna.DNA;

@Container
public class Result implements Saveable
{
	public final String dna;
	public final int score;
	
	public Result(DNA dna, int score) { this(dna.getID(), score); }
	public Result(String dna, int score)
	{
		this.dna = dna;
		this.score = score;
	}
	
	@Override
	@Saver
	public JsonValue save( )
	{
		JsonObject json = new JsonObject();
		
		json.putString("dna", dna);
		json.putInt("score", score);
		
		return json;
	}
	
	@Loader
	public static Result load(JsonValue json)
	{
		JsonObject o = (JsonObject) json;
		
		String dna = o.getString("dna");
		int score = o.getInt("score");
		
		return new Result(dna, score);
	}
	
	@Override
	public int hashCode( )
	{
		return score ^ dna.hashCode();
	}
	
	@Override
	public boolean equals(Object o)
	{
		if(o instanceof Result)
		{
			return dna.equals(((Result) o).dna) && score == ((Result) o).score;
		}
		
		return false;
	}
	
	@Override
	public String toString( )
	{
		return String.format("{dna=\"%s\", score=%d}", dna, score);
	}
}
