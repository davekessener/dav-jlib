package dave;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.NetworkInterface;
import java.util.Scanner;
import java.util.function.Consumer;

import dave.game.GameUtils;
import dave.game.demo.Demo;
import dave.net.NetUtils;
import dave.nn.conv.BasicAllocator;
import dave.nn.conv.Network;
import dave.nn.dna.DNA;
import dave.nn.dna.Prototype;
import dave.system.BaseSystem;
import dave.system.DistributedAISystem;
import dave.system.SystemUtils;
import dave.system.DistributedAISystem.Params;
import dave.system.client.ModuleTester;
import dave.system.client.NeuralNetworkAdapter;
import dave.system.client.ModuleTester.CountdownMonitor;
import dave.system.common.Database;
import dave.system.common.Scoreboard;
import dave.system.common.Tester;
import dave.system.db.FixedDatabase;
import dave.system.dummy.SimpleScoreboard;
import dave.util.log.Entry;
import dave.util.log.LogBase;
import dave.util.log.Logger;
import dave.util.log.Severity;
import dave.util.log.SimpleFormatter;
import dave.util.log.Spacer;
import dave.util.log.TimeSegmentedSink;
import dave.util.Proxify;
import dave.util.ShutdownService;
import dave.util.TransformingConsumer;
import dave.util.Utils;
import dave.util.ShutdownService.Priority;
import dave.util.Transformer;
import dave.util.config.OptionHash;

public class Main
{
	private static InetAddress getMulticastAddress(String[] args) throws NumberFormatException, IOException
	{
		NetworkInterface nic = null;
		
		if(args.length > 0)
		{
			nic = Utils.stream(NetworkInterface.getNetworkInterfaces()).filter(i -> i.getName().equals(args[0])).findFirst().get();
		}
		else
		{
			nic = SystemUtils.findActiveNetwork();
		}
		
		Logger.DEFAULT.log(Severity.INFO, "Selected NIC %s (%s) for multicast.", nic.getName(), nic.getDisplayName());
		
		return NetUtils.getAnyAddress(nic);
	}
	
	public static void main(String[] args)
	{
		Transformer<Consumer<String>, Consumer<Entry>> make = s -> new TransformingConsumer<>(new Spacer(s, 1500, Utils.repeat("=", 200)), new SimpleFormatter());
		
		LogBase.INSTANCE.registerSink(e -> (e.severity.level() >= Severity.INFO.level()), make.create(s -> System.out.println(s)));
		LogBase.INSTANCE.registerSink(e -> true, make.create(new TimeSegmentedSink(() -> new File("log-" + System.currentTimeMillis() + ".log"), 15 * 60 * 1000)));
		
		LogBase.INSTANCE.start();
		ShutdownService.INSTANCE.register(Priority.LAST, LogBase.INSTANCE::stop);
		
		Logger.DEFAULT.log(Severity.INFO, "VERSION %d", VERSION);
		
		try
		{
			final InetSocketAddress mca = new InetSocketAddress(getMulticastAddress(args), SystemUtils.MULTICAST_PORT);
			final OptionHash<Params> config = new OptionHash<>();
			final BasicAllocator alloc = new BasicAllocator();
			final Network nn = SystemUtils.TEMPLATE.generate(alloc);
			final Prototype proto = new Prototype(alloc.size(), SystemUtils.TEMPLATE.getValueRange());
			final Tester t = new ModuleTester(5, new NeuralNetworkAdapter(alloc, nn), Demo::new, () -> new CountdownMonitor(1000), GameUtils::decide);
			final Scoreboard s = new SimpleScoreboard(100);
			final Database<DNA> db = Proxify.synchronize(Database.class, new FixedDatabase<>(110, o -> {}));
			
			Logger.DEFAULT.log(Severity.INFO, "Setting up ...");
			
			BaseSystem sys = DistributedAISystem.buildStandalone(config, mca, proto, t, db, s);

			Logger.DEFAULT.log(Severity.INFO, "Starting ...");
			
			sys.start();
			
			try(Scanner in = new Scanner(System.in))
			{
				for(String line ; true ;)
				{
					line = in.nextLine();
					
					if(line.startsWith("q")) break;
				}
			}
			
			Logger.DEFAULT.log(Severity.INFO, "Stopping ...");
			
			sys.stop();
			
			Logger.DEFAULT.log(Severity.INFO, "[DONE]");
		}
		catch(Throwable e)
		{
			e.printStackTrace();
		}
		
		ShutdownService.INSTANCE.shutdown();
		
		System.out.println("Goodbye.");
	}
	
	public static final int VERSION = 10;
}
