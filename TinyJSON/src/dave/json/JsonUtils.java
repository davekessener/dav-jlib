package dave.json;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public final class JsonUtils
{
	private static final Set<Class<?>> PRIMITIVE_CLASSES = new HashSet<>(Arrays.asList(Boolean.class, Byte.class, Character.class, Short.class, Integer.class, Long.class, Float.class, Double.class, Void.class));
	
	public static JsonValue parse(String json)
	{
		return JsonValue.read(new StringBuffer(json));
	}
	
	public static JsonValue fromFile(File f) throws IOException
	{
		return JsonValue.read(new StringBuffer(Files.readAllLines(f.toPath()).stream().collect(Collectors.joining(" "))));
	}
	
	public static void toFile(JsonValue json, File f) throws IOException { toFile(json, f, new PrettyPrinter()); }
	public static void toFile(JsonValue json, File f, Printer p) throws IOException
	{
		try (FileOutputStream out = new FileOutputStream(f))
		{
			out.write(json.toString(p).getBytes());
		}
	}
	
	public static boolean isPrimitive(Object o)
	{
		return o == null || o.getClass().isPrimitive() || o.getClass().isArray() || PRIMITIVE_CLASSES.contains(o.getClass());
	}
	
	public static void skipWS(Buffer in) { while((in.lookAhead() == ' ' || in.lookAhead() == '\t' || in.lookAhead() == '\n')) in.pop(); }

	public static JsonValue merge(JsonValue b, JsonValue c)
	{
		if((c instanceof JsonObject) && (b instanceof JsonObject))
		{
			JsonObject r = new JsonObject();
			JsonObject base = (JsonObject) b;
			JsonObject change = (JsonObject) c;
			
			base.keySet().forEach(id -> r.put(id, base.get(id)));
			
			change.keySet().forEach(key -> {
				JsonValue o = change.get(key);
				
				if(r.contains(key))
				{
					o = merge(r.get(key), o);
				}
				
				r.put(key, o);
			});
			
			return r;
		}
		else
		{
			return c;
		}
	}
	
	public static Restorable wrap(Saveable save, Loadable load)
	{
		return new Restorable() {
			@Override
			public JsonValue save()
			{
				return save.save();
			}
			
			@Override
			public void load(JsonValue json)
			{
				load.load(json);
			}
			
			@Override
			public String toString()
			{
				return String.format("JSON-%08x", hashCode());
			}
		};
	}
	
	private JsonUtils( ) { }
}
