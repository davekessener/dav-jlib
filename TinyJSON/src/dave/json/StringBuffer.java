package dave.json;


public class StringBuffer implements Buffer
{
	private final char[] mContent;
	private int mIdx;
	
	public StringBuffer(String s)
	{
		mContent = s.toCharArray();
		mIdx = 0;
	}

	@Override
	public char lookAhead()
	{
		if(mIdx >= mContent.length)
			throw new IllegalStateException();
		
		return mContent[mIdx];
	}

	@Override
	public void pop()
	{
		if(mIdx >= mContent.length)
			throw new IllegalStateException();
		
		++mIdx;
	}
}
