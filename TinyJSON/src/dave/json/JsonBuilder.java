package dave.json;

public class JsonBuilder
{
	private final JsonObject mRoot = new JsonObject();
	
	public JsonBuilder put(String id, JsonValue json)
	{
		mRoot.put(id, json);
		
		return this;
	}
	
	public JsonBuilder putString(String id, String v)
	{
		mRoot.putString(id, v);
		
		return this;
	}
	
	public JsonBuilder putInt(String id, int v)
	{
		mRoot.putInt(id, v);
		
		return this;
	}
	
	public JsonBuilder putLong(String id, long v)
	{
		mRoot.putLong(id, v);
		
		return this;
	}
	
	public JsonBuilder putNumber(String id, double v)
	{
		mRoot.putNumber(id, v);
		
		return this;
	}
	
	public JsonObject toJSON( )
	{
		return mRoot;
	}
}
