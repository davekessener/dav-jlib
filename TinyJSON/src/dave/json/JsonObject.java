package dave.json;


import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.stream.Stream;

public class JsonObject extends JsonValue
{
	private final Map<String, JsonValue> mContent;
	
	public JsonObject( )
	{
		mContent = new LinkedHashMap<>();
	}
	
	public int size( )
	{
		return mContent.size();
	}
	
	public Set<String> keySet( )
	{
		return mContent.keySet();
	}
	
	public Stream<Map.Entry<String, JsonValue>> stream( )
	{
		return mContent.entrySet().stream();
	}
	
	public void forEach(BiConsumer<? super String, ? super JsonValue> f)
	{
		mContent.forEach(f);
	}
	
	public void put(String id, JsonValue v)
	{
		mContent.put(id, v == null ? JsonConstant.NULL : v);
	}
	
	public boolean contains(String id)
	{
		return mContent.containsKey(id);
	}
	
	public JsonValue get(String id)
	{
		JsonValue v = mContent.get(id);
		
		if(v == null)
			throw new IllegalArgumentException("No such tag: " + id);
		
		return v == JsonConstant.NULL ? null : v;
	}
	
	public void putString(String id, String v) { put(id, v == null ? JsonConstant.NULL : new JsonString(v)); }
	public void putNumber(String id, double v) { put(id, new JsonNumber(v)); }
	public void putInt(String id, long v) { put(id, new JsonNumber(v)); }
	public void putLong(String id, long v) { putInt(id, v); }
	public void putBool(String id, boolean v) { put(id, v ? JsonConstant.TRUE : JsonConstant.FALSE); }
	
	public String getString(String id) { JsonValue e = get(id); return e == null ? null : ((JsonString) e).get(); }
	public double getNumber(String id) { JsonValue e = get(id); return e == null ? null : ((JsonNumber) e).getDouble(); }
	public double getDouble(String id) { JsonValue e = get(id); return e == null ? null : ((JsonNumber) e).getDouble(); }
	public float getFloat(String id) { JsonValue e = get(id); return e == null ? null : (float) ((JsonNumber) e).getDouble(); }
	public int getInt(String id) { JsonValue e = get(id); return e == null ? null : ((JsonNumber) e).getInt(); }
	public long getLong(String id) { JsonValue e = get(id); return e == null ? null : ((JsonNumber) e).getLong(); }
	public JsonArray getArray(String id) { JsonValue e = get(id); return e == null ? null : (JsonArray) e; }
	public JsonObject getObject(String id) { JsonValue e = get(id); return e == null ? null : (JsonObject) e; }
	
	public boolean getBool(String id)
	{
		JsonValue v = get(id);
		
		if(!(v instanceof JsonConstant))
			throw new IllegalArgumentException("Not a bool: " + id);
		
		return v == JsonConstant.TRUE;
	}

	@Override
	public int hashCode( )
	{
		return mContent.size() * 69;
	}
	
	@Override
	public boolean equals(Object o)
	{
		if(o instanceof JsonObject)
		{
			return mContent.equals(((JsonObject) o).mContent);
		}
		
		return false;
	}
	
	@Override
	public void write(Printer out)
	{
		out.accept("{");
		
		boolean f = true;
		for(Entry<String, JsonValue> e : mContent.entrySet())
		{
			if(!f) out.accept(",");
			
			out.accept(JsonString.encode(e.getKey()));
			out.accept(":");
			e.getValue().write(out);
			
			f = false;
		}
		
		out.accept("}");
	}
	
	protected static JsonObject readObject(Buffer in)
	{
		if(in.lookAhead() != '{')
			throw new IllegalArgumentException();
		
		in.pop();
		
		JsonUtils.skipWS(in);
		
		JsonObject obj = new JsonObject();
		
		while(in.lookAhead() != '}')
		{
			String id = JsonString.decode(in);
			
			JsonUtils.skipWS(in);
			
			if(in.lookAhead() != ':')
				throw new IllegalArgumentException();
			
			in.pop();
			JsonUtils.skipWS(in);
			
			obj.put(id, JsonValue.read(in));
			
			JsonUtils.skipWS(in);
			
			if(in.lookAhead() == ',')
			{
				in.pop();
				JsonUtils.skipWS(in);
			}
			else if(in.lookAhead() != '}')
			{
				throw new IllegalArgumentException();
			}
		}
		
		in.pop();
		
		return obj;
	}
}
