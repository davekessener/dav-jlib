package dave.json;

public interface Restorable extends Saveable, Loadable
{
}
