package dave.json;

public interface Printer
{
	public abstract void accept(String s);
}
