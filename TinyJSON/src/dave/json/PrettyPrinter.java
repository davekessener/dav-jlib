package dave.json;


import java.util.ArrayList;
import java.util.List;

public class PrettyPrinter implements Printer
{
	private List<String> mBuf = new ArrayList<>();
	private String mResult = null;
	
	public static String convert(JsonValue json)
	{
		Printer pp = new PrettyPrinter();
		
		json.write(pp);
		
		return pp.toString();
	}
	
	@Override
	public void accept(String s)
	{
		mBuf.add(s);
	}
	
	@Override
	public String toString( )
	{
		if(mResult == null)
		{
			mResult = eval();
			mBuf = null;
		}
		
		return mResult;
	}
	
	private String eval( )
	{
		StringBuilder sb = new StringBuilder();
		int indent = 0;
		boolean shouldNL = false, sameLine = false;
		
		for(int i = 0 ; i < mBuf.size() ; ++i)
		{
			String s = mBuf.get(i);
			
			if(s.equals("[") && isSingleLine(i))
			{
				if(shouldNL) newline(sb, indent);
				
				sb.append('[');
				while(!s.equals("]"))
				{
					s = mBuf.get(++i);
					
					if(!s.equals(","))
					{
						sb.append(' ');
					}
					
					sb.append(s);
				}
				
				shouldNL = true;
			}
			else if(s.equals(","))
			{
				sb.append(',');
				shouldNL = true;
			}
			else if(s.equals("[") || s.equals("{"))
			{
				String nxt = mBuf.get(i + 1);
				
				if(nxt.equals("]") || nxt.equals("}"))
				{
					if(shouldNL) newline(sb, indent);
					sb.append(s).append(' ').append(nxt);
					++i;
					shouldNL = false;
				}
				else if(sameLine || i == 0)
				{
					sb.append(s);
					++indent;
					shouldNL = true;
				}
				else
				{
					newline(sb, indent++);
					sb.append(s);
					shouldNL = true;
				}
			}
			else if(s.equals("]") || s.equals("}"))
			{
				newline(sb, --indent);
				sb.append(s);
				shouldNL = true;
			}
			else if(s.equals(":"))
			{
				sb.append(" : ");
				shouldNL = false;
			}
			else
			{
				if(shouldNL)
				{
					newline(sb, indent);
				}
				
				sb.append(s);
				shouldNL = false;
			}
			
			sameLine = s.equals(":");
		}
		
		return sb.toString();
	}
	
	private boolean isSingleLine(int i)
	{
		while(true)
		{
			String s = mBuf.get(++i);
			
			if(s.equals("]")) break;
			
			if(!(s.equals(",") || s.startsWith("-") || (s.charAt(0) >= '0' && s.charAt(0) <= '9')))
			{
				return false;
			}
		}
		
		return true;
	}
	
	private static void newline(StringBuilder sb, int indent)
	{
		sb.append('\n');
		
		for(int i = 0 ; i < indent ; ++i)
		{
			sb.append(INDENT);
		}
	}
	
	private static final String INDENT = "    ";
}
