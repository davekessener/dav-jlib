package dave.json;


public class JsonString extends JsonValue
{
	private String mContent;
	
	public JsonString(String s)
	{
		if(s == null)
			throw new NullPointerException();
		
		mContent = s;
	}
	
	public String get( ) { return mContent; }
	public void set(String s) { mContent = s; }
	
	@Override
	public int hashCode( )
	{
		return mContent.hashCode() * 413;
	}
	
	@Override
	public boolean equals(Object o)
	{
		if(o instanceof JsonString)
		{
			return mContent.equals(((JsonString) o).mContent);
		}
		
		return false;
	}

	@Override
	public void write(Printer out)
	{
		out.accept(encode(mContent));
	}
	
	protected static JsonString readString(Buffer in)
	{
		return new JsonString(decode(in));
	}
	
	public static String encode(String b)
	{
		StringBuilder sb = new StringBuilder();
		
		sb.append('"');
		b.chars().forEach(c -> {
			switch(c)
			{
				case '\n':
					sb.append("\\n");
					break;
					
				case '\t':
					sb.append("\\t");
					break;
					
				case '"':
					sb.append("\\\"");
					break;
					
				case '\\':
					sb.append("\\\\");
					break;
					
				default:
					sb.append((char) c);
			}
		});
		sb.append('"');
		
		return sb.toString();
	}
	
	public static String decode(Buffer b)
	{
		StringBuilder sb = new StringBuilder();
		
		if(b.lookAhead() != '"')
			throw new IllegalArgumentException();
		
		b.pop();
		
		for(char c = '\0' ; (c = b.lookAhead()) != '"' ; b.pop())
		{
			if(c == '\\')
			{
				b.pop();
				switch(b.lookAhead())
				{
					case 'n':
						sb.append('\n');
						break;
						
					case 't':
						sb.append('\t');
						break;
						
					case '"':
						sb.append('"');
						break;
						
					case '\\':
						sb.append('\\');
						break;
						
					case '/':
						sb.append('/');
						break;
						
					default:
						throw new IllegalArgumentException("Invalid escape sequence: \\" + b.lookAhead());
				}
			}
			else
			{
				sb.append(c);
			}
		}
		
		b.pop();
		
		return sb.toString();
	}
}
