package dave.json;


public interface Saveable
{
	public abstract JsonValue save( );
}
