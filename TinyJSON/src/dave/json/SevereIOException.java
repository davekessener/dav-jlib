package dave.json;

import java.io.IOException;

public class SevereIOException extends RuntimeException
{
	private static final long serialVersionUID = -3350210992727603928L;
	
	public SevereIOException( )
	{
	}
	
	public SevereIOException(String msg)
	{
		super(msg);
	}
	
	public SevereIOException(IOException t)
	{
		super(t);
	}
}
