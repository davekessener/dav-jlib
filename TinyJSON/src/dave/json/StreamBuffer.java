package dave.json;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

public class StreamBuffer implements Buffer
{
	private final Reader mIn;
	private int mLook;
	
	public StreamBuffer(InputStream in)
	{
		mIn = new InputStreamReader(in);
		mLook = -2;
	}
	
	@Override
	public char lookAhead()
	{
		if(mLook == -1)
			throw new IllegalStateException();
		
		if(mLook == -2) try
		{
			mLook = mIn.read();
		}
		catch(IOException e)
		{
			throw new SevereIOException(e);
		}
		
		return (char) mLook;
	}

	@Override
	public void pop()
	{
		mLook = -2;
	}
}
