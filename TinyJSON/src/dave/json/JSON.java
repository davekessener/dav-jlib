package dave.json;

import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Stream;

public final class JSON
{
	public static boolean canSerialize(Object o)
	{
		return JsonUtils.isPrimitive(o)
			|| (o instanceof String)
			|| (o instanceof JsonValue)
			|| (o instanceof Saveable && o instanceof Loadable)
			|| o.getClass().isAnnotationPresent(Container.class)
			|| o.getClass().isAnnotationPresent(JsonSerializable.class)
			|| (o.getClass().isArray() && Arrays.stream((Object[]) o).allMatch(e -> canSerialize(e)));
	}
	
	public static JsonValue serialize(Object o)
	{
		if(o == null)
		{
			return JsonConstant.NULL;
		}
		else if(o instanceof String)
		{
			return new JsonString((String) o);
		}
		else if(o instanceof JsonValue)
		{
			JsonObject json = new JsonObject();
			
			json.put("content", (JsonValue) o);
			
			return json;
		}
		else if(o.getClass().isArray())
		{
			JsonObject json = new JsonObject();
			Object[] os = (Object[]) o;
			JsonArray a = new JsonArray();
			String c = o.getClass().getComponentType().getName();
			
			for(int i = 0 ; i < os.length ; ++i)
			{
				a.add(JSON.serialize(os[i]));
			}

			json.putString("array", c);
			json.put("content", a);
			
			return json;
		}
		else if(o.getClass().equals(Boolean.class))
		{
			return ((Boolean) o).booleanValue() ? JsonConstant.TRUE : JsonConstant.FALSE;
		}
		else if(JsonUtils.isPrimitive(o))
		{
			Number n = (Number) o;
			
			if((o instanceof Double) || (o instanceof Float))
			{
				return new JsonNumber(n.doubleValue());
			}
			else
			{
				return new JsonNumber(n.longValue());
			}
		}
		else if(o instanceof Saveable)
		{
			JsonObject json = new JsonObject();
			
			json.putString("class", o.getClass().getName());
			json.put("content", ((Saveable) o).save());
			
			return json;
		}
		else if(o.getClass().isEnum())
		{
			JsonObject json = new JsonObject();
			
			json.putString("enum", o.getClass().getName());
			json.putString("value", o.toString());
			
			return json;
		}
		else if(o.getClass().getName() != null)
		{
			Class<?> c = o.getClass();
			
			if(c.isAnnotationPresent(Container.class))
			{
				Method[] ms = o.getClass().getMethods();
				
				for(int i = 0 ; i < ms.length ; ++i)
				{
					if(ms[i].isAnnotationPresent(Saver.class))
					{
						JsonObject json = new JsonObject();
						
						json.putString("class", o.getClass().getName());
						
						try
						{
							json.put("content", (JsonValue) ms[i].invoke(o));
						}
						catch(IllegalAccessException | IllegalArgumentException | InvocationTargetException e)
						{
							throw new RuntimeException(e);
						}
						
						return json;
					}
				}
			}
			else if(c.isAnnotationPresent(JsonSerializable.class))
			{
				JsonObject json = new JsonObject();
				JsonObject fields = new JsonObject();
				
				Stream.of(c.getConstructors())
					.filter(cc -> cc.isAnnotationPresent(JsonSerializer.class))
					.findFirst()
					.map(cc -> cc.getAnnotation(JsonSerializer.class))
					.map(a -> Stream.of(a.value()))
					.ifPresent(s -> s.forEach(id -> fields.put(id, serialize(getField(c, o, id)))));
				
				json.putString("class", c.getName());
				json.put("fields", fields);
				
				return json;
			}
		}
		
		throw new IllegalArgumentException("Not serialisable: " + o.toString());
	}
	
	private static Object getField(Class<?> c, Object o, String id)
	{
		try
		{
			Field f = c.getDeclaredField(id);
			
			f.setAccessible(true);
			
			return f.get(o);
		}
		catch(NoSuchFieldException e)
		{
			if(c.equals(Object.class))
			{
				throw new RuntimeException(e);
			}
			else
			{
				return getField(c.getSuperclass(), o, id);
			}
		}
		catch(IllegalArgumentException | IllegalAccessException | SecurityException e)
		{
			throw new RuntimeException(e);
		}
	}
	
	public static Object deserialize(JsonValue json)
	{
		if(json == JsonConstant.NULL)
		{
			return null;
		}
		else if(json == JsonConstant.TRUE)
		{
			return true;
		}
		else if(json == JsonConstant.FALSE)
		{
			return false;
		}
		else if(json instanceof JsonNumber)
		{
			JsonNumber n = (JsonNumber) json;
			
			return n.isInt() ?
				((n.getLong() > (long) Integer.MAX_VALUE || n.getLong() < (long) Integer.MIN_VALUE) ?
						(Object) Long.valueOf(n.getLong()) :
						(Object) Integer.valueOf((int) n.getLong())) :
				(Object) Double.valueOf(n.getDouble());
		}
		else if(json instanceof JsonString)
		{
			return ((JsonString) json).get();
		}
		else try
		{
			JsonObject o = (JsonObject) json;
			
			if(o.contains("class"))
			{
				Class<?> c = Class.forName(o.getString("class"));
				
				if(o.contains("content"))
				{
					JsonValue v = o.get("content");
					
					Method[] m = c.getMethods();
					
					for(int i = 0 ; i < m.length ; ++i)
					{
						if(m[i].isAnnotationPresent(Loader.class))
						{
							return m[i].invoke(null, v);
						}
					}
					
					if(Loadable.class.isAssignableFrom(c))
					{
						Loadable r = (Loadable) c.getDeclaredConstructor().newInstance();
						
						r.load(v);
						
						return r;
					}
				}
				else if(o.contains("fields"))
				{
					JsonObject fields = o.getObject("fields");
					
					Optional<Constructor<?>> cc = Stream.of(c.getConstructors())
						.filter(e -> e.isAnnotationPresent(JsonSerializer.class))
						.findFirst();
					
					if(cc.isPresent())
					{
						return cc.get().newInstance(
							fields.keySet().stream()
								.map(id -> deserialize(fields.get(id)))
								.toArray());
					}
					else if(fields.size() == 0)
					{
						return c.getDeclaredConstructor().newInstance();
					}
				}
			}
			else if(o.contains("array"))
			{
				JsonArray v = o.getArray("content");
				Object a = Array.newInstance(Class.forName(o.getString("array")), v.size());
				
				for(int i = 0 ; i < v.size() ; ++i)
				{
					Array.set(a, i, JSON.deserialize(v.get(i)));
				}
				
				return a;
			}
			else if(o.contains("enum"))
			{
				Class<?> c = Class.forName(o.getString("enum"));
				
				if(!c.isEnum())
				{
					throw new IllegalStateException(o.getString("enum") + " is not an enum!");
				}
				
				try
				{
					return c.getDeclaredMethod("valueOf", String.class).invoke(null, o.getString("value"));
				}
				catch(NoSuchMethodException | SecurityException e)
				{
					throw new IllegalStateException(e);
				}
			}
			else
			{
				return o.get("content");
			}
			
			throw new IllegalArgumentException(o.toString(new BasicPrinter()));
		}
		catch(IllegalAccessException | IllegalArgumentException | InvocationTargetException | ClassNotFoundException | InstantiationException | NoSuchMethodException | SecurityException e)
		{
			throw new RuntimeException(e);
		}
	}
	
	private JSON( ) { }
}
