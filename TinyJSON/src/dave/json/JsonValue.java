package dave.json;

public abstract class JsonValue
{
	public abstract void write(Printer out);
	
	public String toString(Printer p)
	{
		write(p);
		
		return p.toString();
	}
	
	@Override
	public String toString( )
	{
		return toString(new BasicPrinter());
	}
	
	public static JsonValue read(Buffer in)
	{
		JsonUtils.skipWS(in);
		
		char c = in.lookAhead();
		
		if((c >= '0' && c <= '9') || c == '-')
		{
			return JsonNumber.readNumber(in);
		}
		switch(c)
		{
			case '{':
				return JsonObject.readObject(in);
				
			case '[':
				return JsonArray.readArray(in);
				
			case '"':
				return JsonString.readString(in);
				
			default:
				return JsonConstant.readConstant(in);
		}
	}
}
