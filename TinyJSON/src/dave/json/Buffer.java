package dave.json;


public interface Buffer
{
	public abstract char lookAhead( );
	public abstract void pop( );
}
