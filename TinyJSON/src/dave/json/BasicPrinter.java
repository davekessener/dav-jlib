package dave.json;


public class BasicPrinter implements Printer
{
	private final StringBuilder mBuf = new StringBuilder();

	@Override
	public void accept(String s)
	{
		mBuf.append(s);
	}
	
	@Override
	public String toString( )
	{
		return mBuf.toString();
	}
}
