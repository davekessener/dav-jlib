package dave.json;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class JsonArray extends JsonValue implements Iterable<JsonValue>
{
	private final List<JsonValue> mContent;
	
	public JsonArray( )
	{
		mContent = new ArrayList<>();
	}
	
	public JsonArray(JsonValue[] a) { this(); addArray(a); }
	public JsonArray(Saveable[] a)  { this(); addArray(a); }
	public JsonArray(double[] a)    { this(); addArray(a); }
	public JsonArray(int[] a)       { this(); addArray(a); }
	public JsonArray(short[] a)     { this(); addArray(a); }
	public JsonArray(float[] a)     { this(); addArray(a); }
	public JsonArray(boolean[] a)   { this(); addArray(a); }
	public JsonArray(String[] a)    { this(); addArray(a); }
	
	public void add(JsonValue v)
	{
		mContent.add(v == null ? JsonConstant.NULL : v);
	}
	
	public void addAll(JsonArray a)
	{
		mContent.addAll(a.mContent);
	}
	
	public int size( )
	{
		return mContent.size();
	}
	
	@Override
	public Iterator<JsonValue> iterator( )
	{
		return mContent.iterator();
	}
	
	public Stream<JsonValue> stream( )
	{
		return StreamSupport.stream(this.spliterator(), false);
	}
	
	public JsonValue get(int i) { return mContent.get(i); }
	public int getInt(int i) { return ((JsonNumber) get(i)).getInt(); }
	public double getDouble(int i) { return ((JsonNumber) get(i)).getDouble(); }
	public float getFloat(int i) { return (float) ((JsonNumber) get(i)).getDouble(); }
	public String getString(int i) { return ((JsonString) get(i)).get(); }
	public JsonObject getObject(int i) { return (JsonObject) get(i); }
	public JsonArray getArray(int i) { return (JsonArray) get(i); }

	public void addNumber(double d)
	{
		add(new JsonNumber(d));
	}
	
	public void addString(String s)
	{
		add(s == null ? JsonConstant.NULL : new JsonString(s));
	}

	public void addArray(double[] d)
	{
		for(int i = 0 ; i < d.length ; ++i)
		{
			add(new JsonNumber(d[i]));
		}
	}
	
	public void addArray(int[] d)
	{
		for(int i = 0 ; i < d.length ; ++i)
		{
			add(new JsonNumber(d[i]));
		}
	}
	
	public void addArray(short[] d)
	{
		for(int i = 0 ; i < d.length ; ++i)
		{
			add(new JsonNumber(d[i]));
		}
	}

	public void addArray(float[] d)
	{
		for(int i = 0 ; i < d.length ; ++i)
		{
			add(new JsonNumber(d[i]));
		}
	}
	
	public void addArray(boolean[] a)
	{
		for(int i = 0 ; i < a.length ; ++i)
		{
			add(a[i] ? JsonConstant.TRUE : JsonConstant.FALSE);
		}
	}
	
	public void addArray(String[] s)
	{
		for(int i = 0 ; i < s.length ; ++i)
		{
			add(new JsonString(s[i]));
		}
	}
	
	public void addArray(Saveable[] s)
	{
		for(int i = 0 ; i < s.length ; ++i)
		{
			add(s[i].save());
		}
	}
	
	public void addArray(JsonValue[] s)
	{
		for(int i = 0 ; i < 0 ; ++i)
		{
			add(s[i]);
		}
	}
	
	public float[] asFloats( )
	{
		float[] d = new float[mContent.size()];
		
		for(int i = 0 ; i < d.length ; ++i)
		{
			d[i] = (float) ((JsonNumber) mContent.get(i)).getDouble();
		}
		
		return d;
	}

	public double[] asDoubles( )
	{
		double[] d = new double[mContent.size()];
		
		for(int i = 0 ; i < d.length ; ++i)
		{
			d[i] = ((JsonNumber) mContent.get(i)).getDouble();
		}
		
		return d;
	}
	
	public int[] asInts( )
	{
		int[] d = new int[mContent.size()];
		
		for(int i = 0 ; i < d.length ; ++i)
		{
			d[i] = ((JsonNumber) mContent.get(i)).getInt();
		}
		
		return d;
	}
	
	public short[] asShorts( )
	{
		int[] r = asInts();
		short[] d = new short[r.length];
		
		for(int i = 0 ; i < r.length ; ++i)
		{
			d[i] = (short) r[i];
		}
		
		return d;
	}
	
	public boolean[] asBooleans( )
	{
		boolean[] a = new boolean[mContent.size()];
		
		for(int i = 0 ; i < a.length ; ++i)
		{
			JsonConstant e = (JsonConstant) mContent.get(i);
			
			a[i] = (e == JsonConstant.TRUE);
		}
		
		return a;
	}
	
	public String[] asStrings( )
	{
		String[] r = new String[mContent.size()];
		
		for(int i = 0 ; i < r.length ; ++i)
		{
			JsonValue v = mContent.get(i);
			
			r[i] = (v == JsonConstant.NULL) ? null : ((JsonString) v).get();
		}
		
		return r;
	}
	
	@Override
	public int hashCode( )
	{
		return mContent.size() * 1337;
	}
	
	@Override
	public boolean equals(Object o)
	{
		if(o instanceof JsonArray)
		{
			return mContent.equals(((JsonArray) o).mContent);
		}
		
		return false;
	}

	@Override
	public void write(Printer out)
	{
		out.accept("[");
		
		boolean f = true;
		for(JsonValue v : mContent)
		{
			if(!f) out.accept(",");
			v.write(out);
			f = false;
		}
		
		out.accept("]");
	}
	
	protected static JsonArray readArray(Buffer in)
	{
		if(in.lookAhead() != '[')
			throw new IllegalArgumentException();
		
		JsonArray r = new JsonArray();
		
		in.pop();
		
		JsonUtils.skipWS(in);
		
		while(in.lookAhead() != ']')
		{
			r.add(JsonValue.read(in));
			
			JsonUtils.skipWS(in);
			
			if(in.lookAhead() == ',')
			{
				in.pop();
				JsonUtils.skipWS(in);
			}
			else if(in.lookAhead() != ']')
			{
				throw new IllegalArgumentException("" + in.lookAhead());
			}
		}
		
		in.pop();
		
		return r;
	}
}
