package dave.json;

import java.util.Map;
import java.util.function.BiConsumer;
import java.util.stream.Collector;

public final class JsonCollectors
{
	public static Collector<JsonValue, JsonArray, JsonArray> ofArray( )
	{
		return Collector.of(
			() -> new JsonArray(),
			(BiConsumer<JsonArray, JsonValue>) (a, v) -> a.add(v),
			(a0, a1) -> {
				a0.addAll(a1);
				return a0;
			});
	}
	
	public static <T> Collector<Map.Entry<T, JsonValue>, JsonObject, JsonObject> ofObject()
	{
		return ofObject((o, e) -> o.put(e.getKey().toString(), e.getValue()));
	}
	
	public static <T> Collector<T, JsonObject, JsonObject> ofObject(BiConsumer<JsonObject, T> f)
	{
		return Collector.of(() -> new JsonObject(), f, (o0, o1) -> {
			o1.keySet().forEach(id -> o0.put(id, o1.get(id)));
			return o0;
		});
	}
	
	private JsonCollectors( ) { }
}
