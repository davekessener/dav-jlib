package dave.json;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;

public class StreamPrinter implements Printer
{
	private final OutputStream mOut;
	private final Charset mCharset;
	
	public StreamPrinter(OutputStream out, Charset c)
	{
		if(out == null)
			throw new NullPointerException();
		
		mOut = out;
		mCharset = c;
	}
	
	public void flush( )
	{
		try
		{
			mOut.flush();
		}
		catch(IOException e)
		{
			throw new UncheckedIOException(e);
		}
	}

	@Override
	public void accept(String s)
	{
		try
		{
			mOut.write(s.getBytes(mCharset));
		}
		catch(IOException e)
		{
			throw new UncheckedIOException(e);
		}
	}
	
	public static class UncheckedIOException extends RuntimeException
	{
		private static final long serialVersionUID = -2878591418893932117L;
		
		public final Throwable cause;
		
		public UncheckedIOException(Throwable e)
		{
			cause = e;
		}
	}
	
	public static final Charset CHARSET = Charset.forName("UTF-8");
}
