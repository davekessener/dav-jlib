package dave.json;


public class JsonNumber extends JsonValue
{
	private final double mFloat;
	private final long mInt;
	private final boolean mIsInt;
	
	public JsonNumber(double v)
	{
		if(v == (long) v)
		{
			mFloat = 0;
			mInt = (long) v;
			mIsInt = true;
		}
		else
		{
			mFloat = v;
			mInt = 0;
			mIsInt = false;
		}
	}
	
	public JsonNumber(long v)
	{
		mFloat = 0;
		mInt = v;
		mIsInt = true;
	}
	
	@Override
	public int hashCode( )
	{
		return (mIsInt ? Long.valueOf(mInt).hashCode() : Double.valueOf(mFloat).hashCode()) ^ 0x11223344;
	}
	
	@Override
	public boolean equals(Object o)
	{
		if(o instanceof JsonNumber)
		{
			JsonNumber n = (JsonNumber) o;
			
			return n.mIsInt == mIsInt && n.mFloat == mFloat && n.mInt == mInt;
		}
		
		return false;
	}
	
	public boolean isInt( ) { return mIsInt; }
	
	public int getInt( ) { if(!isInt()) throw new IllegalStateException(); return (int) mInt; }
	public long getLong( ) { if(!isInt()) throw new IllegalStateException(); return mInt; }
	public double getDouble( ) { return mIsInt ? mInt : mFloat; }
	
	@Override
	public void write(Printer out)
	{
		String s = null;
		
		if(mIsInt)
		{
			s = "" + mInt;
		}
		else
		{
			String t = String.format("%.24g", mFloat).replaceAll(",", ".");
			
			int e = t.length();
			
			while(e > 0 && t.charAt(e - 1) == '0') --e;
			
			if(e > 0 && t.charAt(e - 1) == '.') -- e;
			
			s = t.substring(0, e);
		}
		
		out.accept(s);
	}
	
	protected static JsonNumber readNumber(Buffer in)
	{
		StringBuilder sb = new StringBuilder();
		boolean is_int = true;
		
		int p = 0;
		while(true)
		{
			switch(p)
			{
				case 0:
					if(in.lookAhead() == '-')
					{
						sb.append('-'); in.pop();
					}
					++p;
					break;
				case 1:
					if(in.lookAhead() == '.')
					{
						sb.append('.'); in.pop();
						is_int = false;
					}
					else if(in.lookAhead() == 'e' || in.lookAhead() == 'E')
					{
						sb.append('E'); in.pop();
						
						if(in.lookAhead() == '-')
						{
							sb.append('-'); in.pop();
						}
					}
					break;
				default:
					break;
			}
			
			if(in.lookAhead() < '0' || in.lookAhead() > '9') break;
			
			sb.append(in.lookAhead());
			in.pop();
		}
		
		JsonNumber r = null;
		
		if(is_int)
		{
			r = new JsonNumber(Long.parseLong(sb.toString()));
		}
		else
		{
			r = new JsonNumber(Double.parseDouble(sb.toString()));
		}
		
		return r;
	}
}
