package dave.json;


public final class JsonConstant extends JsonValue
{
	private final String mName;
	
	private JsonConstant(String name)
	{
		mName = name;
	}

	@Override
	public void write(Printer out)
	{
		out.accept(mName);
	}
	
	public static JsonConstant of(boolean f) { return f ? TRUE : FALSE; }
	
	protected static JsonConstant readConstant(Buffer in)
	{
		StringBuilder sb = new StringBuilder();
		
		while(in.lookAhead() >= 'a' && in.lookAhead() <= 'z')
		{
			sb.append(in.lookAhead());
			in.pop();
		}
		
		String id = sb.toString();
		
		if(id.equals(NULL.mName)) return NULL;
		else if(id.equals(TRUE.mName)) return TRUE;
		else if(id.equals(FALSE.mName)) return FALSE;
		else throw new IllegalArgumentException(id);
	}

	public static final JsonConstant NULL = new JsonConstant("null");
	public static final JsonConstant TRUE = new JsonConstant("true");
	public static final JsonConstant FALSE = new JsonConstant("false");
}
