package dave.json;

public interface Loadable
{
	public abstract void load(JsonValue json);
}
