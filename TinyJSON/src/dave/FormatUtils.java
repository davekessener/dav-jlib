package dave;

import dave.json.JsonArray;
import dave.json.JsonObject;
import dave.json.JsonString;
import dave.json.JsonValue;
import dave.xml.XmlNode;

public final class FormatUtils
{
	public static JsonValue toJSON(XmlNode node)
	{
		JsonObject json = new JsonObject();
		JsonObject attr = new JsonObject();
		JsonArray content = new JsonArray();

		node.attributes().forEach(e -> attr.putString(e.getKey(), e.getValue()));
		
		node.content().forEach(e -> {
			if(e instanceof XmlNode)
			{
				content.add(toJSON((XmlNode) e));
			}
			else
			{
				content.add(new JsonString((String) e));
			}
		});
		
		json.putString("id", node.id());
		json.put("attributes", attr);
		json.put("content", content);
		
		return json;
	}
	
	public static XmlNode toXML(JsonValue json)
	{
		JsonObject o = (JsonObject) json;
		XmlNode node = new XmlNode(o.getString("id"));
		
		o.getObject("attributes").forEach((id, a) -> node.setAttribute(id, ((JsonString) a).get()));
		o.getArray("content").forEach(v -> {
			if(v instanceof JsonString)
			{
				node.addContent(((JsonString) v).get());
			}
			else
			{
				node.addContent(toXML(v));
			}
		});
		
		return node;
	}
	
	private FormatUtils( ) { }
}
