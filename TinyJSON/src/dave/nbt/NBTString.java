package dave.nbt;

import java.nio.charset.Charset;

import dave.util.Deserializer;
import dave.util.Serializer;

public class NBTString extends NBTTag
{
	private String mValue;
	
	public NBTString() { this(""); }
	public NBTString(String v)
	{
		super(Type.STRING);
		
		set(v);
	}
	
	public void set(String v)
	{
		if(v == null)
			throw new NullPointerException();
		
		mValue = v;
	}
	
	public String get()
	{
		return mValue;
	}
	
	@Override
	public void serialize(Serializer io)
	{
		serializeString(io, mValue);
	}
	
	public static NBTString deserialize(Deserializer io)
	{
		return new NBTString(deserializeString(io));
	}
	
	public static void serializeString(Serializer io, String v)
	{
		io.writeShort((short) v.length());
		io.write(v.getBytes(CHARSET));
	}
	
	public static String deserializeString(Deserializer io)
	{
		int l = io.readShort();
		
		return l == 0 ? "" : new String(io.read(l), CHARSET);
	}
	
	public static final Charset CHARSET = Charset.forName("UTF-8");
}
