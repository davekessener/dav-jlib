package dave.nbt;

import java.util.ArrayList;
import java.util.List;

import dave.util.Deserializer;
import dave.util.Serializer;

public class NBTByteArray extends NBTTag
{
	private final List<Byte> mContent;
	
	public NBTByteArray()
	{
		super(Type.BYTE_ARRAY);
		
		mContent = new ArrayList<>();
	}
	
	public int size() { return mContent.size(); }
	public byte get(int i) { return mContent.get(i); }
	public void add(byte v) { mContent.add(v); }
	public void set(byte[] a) { mContent.clear(); for(int i = 0 ; i < a.length ; ++i) add(a[i]); }

	@Override
	public void serialize(Serializer io)
	{
		io.writeInt(mContent.size());
		mContent.forEach(b -> io.writeByte(b));
	}
	
	public static NBTByteArray deserialize(Deserializer io)
	{
		NBTByteArray a = new NBTByteArray();
		
		a.set(io.read(io.readInt()));
		
		return a;
	}
}
