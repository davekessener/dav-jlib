package dave.nbt;

import dave.util.Deserializer;
import dave.util.Serializer;

public class NBTFloat extends NBTTag
{
	private float mValue;
	
	public NBTFloat() { this((float) 0); }
	public NBTFloat(float v)
	{
		super(Type.FLOAT);
		
		mValue = v;
	}

	public float get() { return mValue; }
	public void set(float v) { mValue = v; }
	
	@Override
	public void serialize(Serializer io)
	{
		io.writeFloat(mValue);
	}
	
	public static NBTFloat deserialize(Deserializer io)
	{
		return new NBTFloat(io.readFloat());
	}
}
