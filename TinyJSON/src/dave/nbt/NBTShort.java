package dave.nbt;

import dave.util.Deserializer;
import dave.util.Serializer;

public class NBTShort extends NBTTag
{
	private short mValue;
	
	public NBTShort() { this((short) 0); }
	public NBTShort(short v)
	{
		super(Type.SHORT);
		
		mValue = v;
	}

	public short get() { return mValue; }
	public void set(short v) { mValue = v; }
	
	@Override
	public void serialize(Serializer io)
	{
		io.writeShort(mValue);
	}
	
	public static NBTShort deserialize(Deserializer io)
	{
		return new NBTShort(io.readShort());
	}
}
