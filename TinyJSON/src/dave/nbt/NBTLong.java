package dave.nbt;

import dave.util.Deserializer;
import dave.util.Serializer;

public class NBTLong extends NBTTag
{
	private long mValue;
	
	public NBTLong() { this((long) 0); }
	public NBTLong(long v)
	{
		super(Type.LONG);
		
		mValue = v;
	}

	public long get() { return mValue; }
	public void set(long v) { mValue = v; }
	
	@Override
	public void serialize(Serializer io)
	{
		io.writeLong(mValue);
	}
	
	public static NBTLong deserialize(Deserializer io)
	{
		return new NBTLong(io.readLong());
	}
}
