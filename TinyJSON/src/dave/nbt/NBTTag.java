package dave.nbt;

import dave.util.Deserializer;
import dave.util.Serializer;

public abstract class NBTTag
{
	private final Type mType;
	
	protected NBTTag(Type t)
	{
		mType = t;
	}
	
	public Type type()
	{
		return mType;
	}
	
	public abstract void serialize(Serializer io);
	
	protected static NBTTag doDeserialize(Type t, Deserializer io)
	{
		switch(t)
		{
			case END:
				throw new IllegalArgumentException();
			
			case BYTE:
				return NBTByte.deserialize(io);
				
			case SHORT:
				return NBTShort.deserialize(io);
				
			case INT:
				return NBTInt.deserialize(io);
				
			case LONG:
				return NBTLong.deserialize(io);
				
			case FLOAT:
				return NBTFloat.deserialize(io);
				
			case DOUBLE:
				return NBTDouble.deserialize(io);
				
			case LIST:
				return NBTList.deserialize(io);
				
			case COMPOUND:
				return NBTCompound.deserialize(io);
				
			case BYTE_ARRAY:
				return NBTByteArray.deserialize(io);
				
			case INT_ARRAY:
				return NBTIntArray.deserialize(io);
				
			case STRING:
				return NBTString.deserialize(io);
		}
		
		throw new IllegalArgumentException();
	}
	
	protected void writeType(Serializer io) { serializeType(io, mType); }
	protected static void serializeType(Serializer io, Type t) { io.writeByte((byte) t.ordinal()); }
}
