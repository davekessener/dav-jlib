package dave.nbt;

import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;

import dave.util.Deserializer;
import dave.util.Serializer;

public class NBTCompound extends NBTTag
{
	private final Map<String, NBTTag> mTags;
	
	public NBTCompound()
	{
		super(Type.COMPOUND);
		
		mTags = new HashMap<>();
	}
	
	public void put(String name, NBTTag tag) { mTags.put(name, tag); }
	
	public NBTTag get(String name)
	{
		NBTTag tag = mTags.get(name);
		
		if(tag == null)
			throw new NoSuchElementException(name);
		
		return tag;
	}
	
	public void putByte(String name, byte v) { put(name, new NBTByte(v)); }
	public void putShort(String name, short v) { put(name, new NBTShort(v)); }
	public void putInt(String name, int v) { put(name, new NBTInt(v)); }
	public void putLong(String name, long v) { put(name, new NBTLong(v)); }
	public void putFloat(String name, float v) { put(name, new NBTFloat(v)); }
	public void putDouble(String name, double v) { put(name, new NBTDouble(v)); }

	public byte getByte(String name) { return ((NBTByte) get(name)).get(); }
	public short getShort(String name) { return ((NBTShort) get(name)).get(); }
	public int getInt(String name) { return ((NBTInt) get(name)).get(); }
	public long getLong(String name) { return ((NBTLong) get(name)).get(); }
	public float getFloat(String name) { return ((NBTFloat) get(name)).get(); }
	public double getDouble(String name) { return ((NBTDouble) get(name)).get(); }

	@Override
	public void serialize(Serializer io)
	{
		mTags.entrySet().forEach(e -> {
			e.getValue().writeType(io);
			NBTString.serializeString(io, e.getKey());
			e.getValue().serialize(io);
		});
		NBTTag.serializeType(io, Type.END);
	}
	
	public static NBTCompound deserialize(Deserializer io)
	{
		NBTCompound r = new NBTCompound();
		
		while(true)
		{
			Type t = Type.values()[(int) io.readByte()];
			
			if(t == Type.END)
				break;
			
			String name = NBTString.deserializeString(io);
			NBTTag tag = NBTTag.doDeserialize(t, io);
			
			r.put(name, tag);
		}
		
		return r;
	}
}
