package dave.nbt;

import dave.util.Deserializer;
import dave.util.Serializer;

public class NBTInt extends NBTTag
{
	private int mValue;
	
	public NBTInt() { this((int) 0); }
	public NBTInt(int v)
	{
		super(Type.INT);
		
		mValue = v;
	}

	public int get() { return mValue; }
	public void set(int v) { mValue = v; }
	
	@Override
	public void serialize(Serializer io)
	{
		io.writeInt(mValue);
	}
	
	public static NBTInt deserialize(Deserializer io)
	{
		return new NBTInt(io.readInt());
	}
}
