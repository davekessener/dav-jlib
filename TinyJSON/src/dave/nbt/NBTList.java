package dave.nbt;

import java.util.ArrayList;
import java.util.List;

import dave.util.Deserializer;
import dave.util.Serializer;

public class NBTList extends NBTTag
{
	private final Type mType;
	private final List<NBTTag> mTags;
	
	public NBTList(Type t)
	{
		super(Type.LIST);
		
		mType = t;
		mTags = new ArrayList<>();
	}
	
	public void add(NBTTag tag)
	{
		if(tag.type() != mType)
			throw new IllegalArgumentException("Expected " + mType + ", but got " + tag.type());
		
		mTags.add(tag);
	}
	
	public void remove(NBTTag tag)
	{
		mTags.remove(tag);
	}

	@Override
	public void serialize(Serializer io)
	{
		NBTTag.serializeType(io, mType);
		io.writeInt(mTags.size());
		mTags.forEach(t -> t.serialize(io));
	}
	
	public static NBTList deserialize(Deserializer io)
	{
		Type t = Type.values()[(int) io.readByte()];
		int l = io.readInt();
		NBTList a = new NBTList(t);
		
		for(int i = 0 ; i < l ; ++i)
		{
			a.add(NBTTag.doDeserialize(t, io));
		}
		
		return a;
	}
}
