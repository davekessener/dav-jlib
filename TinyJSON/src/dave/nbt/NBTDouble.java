package dave.nbt;

import dave.util.Deserializer;
import dave.util.Serializer;

public class NBTDouble extends NBTTag
{
	private double mValue;
	
	public NBTDouble() { this((double) 0); }
	public NBTDouble(double v)
	{
		super(Type.DOUBLE);
		
		mValue = v;
	}

	public double get() { return mValue; }
	public void set(double v) { mValue = v; }
	
	@Override
	public void serialize(Serializer io)
	{
		io.writeDouble(mValue);
	}
	
	public static NBTDouble deserialize(Deserializer io)
	{
		return new NBTDouble(io.readDouble());
	}
}
