package dave.nbt;

import dave.util.Deserializer;
import dave.util.Serializer;

public class NBTByte extends NBTTag
{
	private byte mValue;
	
	public NBTByte() { this((byte) 0); }
	public NBTByte(byte v)
	{
		super(Type.BYTE);
		
		mValue = v;
	}
	
	public byte get() { return mValue; }
	public void set(byte v) { mValue = v; }
	
	@Override
	public void serialize(Serializer io)
	{
		io.writeByte(mValue);
	}
	
	public static NBTByte deserialize(Deserializer io)
	{
		return new NBTByte(io.readByte());
	}
}
