package dave.nbt;

import java.util.ArrayList;
import java.util.List;

import dave.util.Deserializer;
import dave.util.Serializer;

public class NBTIntArray extends NBTTag
{
	private final List<Integer> mContent;
	
	public NBTIntArray()
	{
		super(Type.INT_ARRAY);
		
		mContent = new ArrayList<>();
	}

	public int size() { return mContent.size(); }
	public int get(int i) { return mContent.get(i); }
	public void add(int v) { mContent.add(v); }
	public void set(int[] a) { mContent.clear(); for(int i = 0 ; i < a.length ; ++i) add(a[i]); }

	@Override
	public void serialize(Serializer io)
	{
		io.writeInt(mContent.size());
		mContent.forEach(i -> io.writeInt(i));
	}
	
	public static NBTIntArray deserialize(Deserializer io)
	{
		NBTIntArray a = new NBTIntArray();
		int l = io.readInt();
		
		for(int i = 0 ; i < l ; ++i)
		{
			a.add(io.readInt());
		}
		
		return a;
	}
}
