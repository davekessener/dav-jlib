package dave.xml;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;

import dave.json.SevereIOException;

public final class XmlUtils
{
	public static XmlNode fromFile(File f) throws IOException
	{
		try (FileInputStream in = new FileInputStream(f))
		{
			return XmlScanner.scan(() -> {
				try
				{
					return in.read();
				}
				catch(IOException e)
				{
					throw new SevereIOException(e);
				}
			});
		}
		catch(SevereIOException e)
		{
			throw (IOException) e.getCause();
		}
	}
	
	public static void toFile(XmlNode node, File f) throws IOException { toFile(node, f, CHARSET); }
	public static void toFile(XmlNode node, File f, Charset cs) throws IOException
	{
		try (FileOutputStream out = new FileOutputStream(f))
		{
			(new PrettyPrinter(v -> {
				try
				{
					out.write(v.getBytes(cs));
				}
				catch(IOException e)
				{
					throw new SevereIOException(e);
				}
			})).process(node);
		}
		catch(SevereIOException e)
		{
			throw (IOException) e.getCause();
		}
	}
	
	public static final Charset CHARSET = Charset.forName("UTF-8");
	
	private XmlUtils( ) { }
}
