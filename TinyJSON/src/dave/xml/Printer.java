package dave.xml;

public interface Printer
{
	public abstract void process(XmlNode node);
	
	public static String escape(String s)
	{
		char[] cs = s.toCharArray();
		StringBuilder sb = new StringBuilder();
		
		for(int i = 0 ; i < cs.length ; ++i)
		{
			switch(cs[i])
			{
				case '<':
					sb.append("&lt;");
					break;
					
				case '>':
					sb.append("&gt;");
					break;
					
				case '\'':
					sb.append("&apos;");
					break;
					
				case '"':
					sb.append("&quot;");
					break;
					
				case '&':
					sb.append("&amp;");
					break;
					
				default:
					sb.append(cs[i]);
					break;
			}
		}
		
		return sb.toString();
	}
	
	public static String unescape(String s)
	{
		s = s.replaceAll("&lt;", "<");
		s = s.replaceAll("&gt;", ">");
		s = s.replaceAll("&apos;", "'");
		s = s.replaceAll("&quot;", "\"");
		s = s.replaceAll("&amp;", "&");
		
		return s;
	}
}
