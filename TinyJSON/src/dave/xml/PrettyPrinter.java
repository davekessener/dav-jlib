package dave.xml;

import java.util.function.Consumer;

public class PrettyPrinter implements Printer
{
	private final Consumer<String> mCallback;
	private final String mIndent;
	
	public PrettyPrinter(Consumer<String> cb) { this(cb, "    "); }
	public PrettyPrinter(Consumer<String> cb, String idt)
	{
		mCallback = cb;
		mIndent = idt;
	}
	
	@Override
	public void process(XmlNode node)
	{
		process(node, "");
	}
	
	private void process(XmlNode node, String idt)
	{
		boolean root = node.id().isEmpty();
		String nidt = (root ? idt : idt + mIndent);
		StringBuilder sb = new StringBuilder();
		
		if(!root)
		{
			sb.append(idt).append("<").append(node.type()).append(node.id());
		}
		
		boolean[] f = new boolean[] { false, false };
		
		f[1] = node.content().findFirst().isPresent();
		
		String end = (f[1] ? ">" : "/>");
		
		if(!node.type().isEmpty())
		{
			end = node.type() + ">";
		}
		
		if(!root)
		{
			node.attributes().forEach(e -> {
				f[0] = true;
				
				sb.append("\n");
				mCallback.accept(sb.toString());
				sb.setLength(0);
				
				sb.append(nidt)
				  .append(e.getKey())
				  .append("=\"")
				  .append(e.getValue())
				  .append("\"");
			});
			
			if(f[0])
			{
				sb.append("\n").append(idt);
			}
			else if(!f[1])
			{
				sb.append(" ");
			}
			
			sb.append(end).append("\n");
			
			mCallback.accept(sb.toString());
			sb.setLength(0);
		}
		
		node.content().forEach(e -> {
			if(e instanceof String)
			{
				sb.append(nidt).append(Printer.escape((String) e)).append("\n");
				mCallback.accept(sb.toString());
				sb.setLength(0);
			}
			else if(e instanceof XmlNode)
			{
				process((XmlNode) e, nidt);
			}
			else
			{
				throw new RuntimeException("" + e);
			}
		});
		
		if(!root && f[1])
		{
			sb.append(idt).append("</").append(node.id()).append(">\n");
			mCallback.accept(sb.toString());
		}
	}
}
