package dave.xml;

import java.util.function.Consumer;

public class BasicPrinter implements Printer
{
	private final Consumer<String> mCallback;
	
	public BasicPrinter(Consumer<String> cb)
	{
		mCallback = cb;
	}

	@Override
	public void process(XmlNode node)
	{
		boolean root = node.id().isEmpty();
		StringBuilder sb = new StringBuilder();
		
		if(!root)
		{
			sb.append("<").append(node.type()).append(node.id());
			
			node.attributes().forEach(e -> {
				sb.append(" ").append(e.getKey()).append("=\"").append(e.getValue()).append("\"");
			});
		}
		
		boolean[] f = new boolean[] { false, false };
		
		f[1] = node.content().findFirst().isPresent();

		String end = (node.type().isEmpty() ? "/" : node.type());
		
		if(!root)
		{
			if(f[1])
			{
				sb.append(node.type()).append(">");
			}
			else
			{
				sb.append(" ").append(end).append(">");
			}
		}
		
		mCallback.accept(sb.toString());
		sb.setLength(0);
		
		node.content().forEach(e -> {
			if(e instanceof String)
			{
				if(f[0]) sb.append(" ");
				sb.append(Printer.escape((String) e));
				f[0] = true;
				mCallback.accept(sb.toString());
				sb.setLength(0);
			}
			else if(e instanceof XmlNode)
			{
				process((XmlNode) e);
				f[0] = false;
			}
			else
			{
				throw new RuntimeException("" + e);
			}
		});
		
		if(!root && f[1])
		{
			sb.append("<").append(end).append(node.id()).append(">");
			mCallback.accept(sb.toString());
		}
	}
}
