package dave.xml;

import java.util.LinkedList;
import java.util.Queue;
import java.util.function.IntSupplier;

public final class XmlScanner
{
	private final Input mInput;
	
	private XmlScanner(IntSupplier in)
	{
		mInput = new CompoundInput(new WrapInput(in), new StaticInput("</root>"));
	}
	
	public static XmlNode scan(IntSupplier cb)
	{
		return (new XmlScanner(cb)).scanAll();
	}
	
	private XmlNode scanAll()
	{
		try
		{
			XmlNode root = new XmlNode("root");
			
			scanBody(root);
			
			return new XmlNode("", root);
		}
		catch(InternalException e)
		{
			throw new RuntimeException(String.format("While parsing XML data: %s @%d", e.getMessage(), mInput.position()));
		}
	}
	
	private XmlNode scanNode(String id, String t)
	{
		XmlNode r = new XmlNode(id, t);
		
		if(scanArgs(r))
		{
			scanBody(r);
		}
		
		return r;
	}
	
	private boolean scanArgs(XmlNode root)
	{
		Token t = next();
		
		while(t.type == Type.ID)
		{
			String id = t.value;
			
			t = next();
			
			if(t.type != Type.SYM || !t.value.equals("="))
				throw new InternalException("Expected '=', not " + t);
			
			t = next();
			
			if(t.type != Type.STR)
				throw new InternalException("Expected string, not " + t);
			
			root.setAttribute(id, t.value);
			
			t = next();
		}
		
		if(t.type != Type.SYM)
			throw new InternalException("Expected symbol, not " + t);
		
		if(t.value.endsWith(">"))
		{
			if(t.value.equals(">"))
			{
				return true;
			}
			else if(t.value.startsWith(root.type()))
			{
				return false;
			}
		}
		
		throw new InternalException("Expected end, not " + t);
	}
	
	private void scanBody(XmlNode root)
	{
		StringBuilder sb = new StringBuilder();
		
		while(true)
		{
			Token t = next();
			
			if(t.type == Type.ID)
			{
				if(sb.length() > 0) sb.append(' ');
				sb.append(t.value);
			}
			else if(t.type == Type.STR)
			{
				if(sb.length() > 0) sb.append(' ');
				sb.append('"').append(t.value).append('"');
			}
			else
			{
				if(t.value.startsWith("<"))
				{
					String tp = t.value.substring(1);
					
					t = next();
					
					if(sb.length() > 0)
					{
						root.addContent(Printer.unescape(sb.toString()));
						sb.setLength(0);
					}
					
					if(t.type == Type.SYM && t.value.equals("/"))
					{
						t = next();
						
						if(t.type != Type.ID || !t.value.equals(root.id()))
							throw new InternalException("Expected closing element for " + root.id() + ", not " + t);
						
						t = next();
						
						if(t.type != Type.SYM || !t.value.equals(">"))
							throw new InternalException("Expected > not " + t);
						
						break;
					}
					else if(t.type == Type.ID)
					{
						root.addContent(scanNode(t.value, tp));
					}
					else
					{
						throw new InternalException("Did not expect " + t);
					}
				}
				else
				{
					if(sb.length() > 0) sb.append(' ');
					sb.append(t.value);
				}
			}
		}
	}
	
	private Token next()
	{
		mInput.skip();
		
		char c = mInput.peek();
		
		switch(c)
		{
			case '<':
				return nextOpen();
				
			case '>':
			case '/':
			case '?':
				return nextClose(c);
				
			case '=':
				return new Token(Type.SYM, "" + mInput.get());
				
			case '"':
				return nextString();
				
			default:
				return nextID();
		}
	}
	
	private Token nextOpen()
	{
		String r = "<";
		
		mInput.get();
		
		if(mInput.peek() == '?')
		{
			mInput.get();
			
			r = "<?";
		}
		else if(mInput.peek() == '!')
		{
			mInput.get();
			
			if(mInput.get() != '-' || mInput.get() != '-')
				throw new InternalException("Invalid comment!");
			
			return nextComment();
		}
		
		return new Token(Type.SYM, r);
	}
	
	private Token nextClose(char v)
	{
		mInput.get();
		
		if(!mInput.empty() && mInput.peek() == '>')
		{
			mInput.get();
			
			return new Token(Type.SYM, v + ">");
		}
		else
		{
			return new Token(Type.SYM, "" + v);
		}
	}
	
	private Token nextComment()
	{
		StringBuilder sb = new StringBuilder();
		
		sb.append("<!--");
		
		char c = '\0';
		
		while(true)
		{
			char t = mInput.get();
			
			if(c == '-' && t == '-')
			{
				c = mInput.get();
				
				if(c != '>')
					throw new InternalException("Invalid occurence of -- in comment!");
				
				return next();
			}
			
			c = t;
		}
	}
	
	private Token nextString()
	{
		if(mInput.get() != '"')
			throw new InternalException("Strings must start with '\"'!");
		
		StringBuilder sb = new StringBuilder();
		
		for(char c ; (c = mInput.get()) != '"' ;)
		{
			if(c == '\\')
			{
				switch(c = mInput.get())
				{
					case 'n':
						sb.append('\n');
						break;
						
					case 't':
						sb.append('\t');
						break;
						
					case '0':
						sb.append('\0');
						break;
						
					case 'r':
						sb.append('\r');
						break;
						
					case 'b':
						sb.append('\b');
						break;
						
					case '"':
						sb.append('"');
						break;
						
					case '\'':
						sb.append('\'');
						break;
						
					default:
						throw new InternalException("Invalid escape sequence \\" + c);
				}
			}
			else
			{
				sb.append(c);
			}
		}
		
		return new Token(Type.STR, sb.toString());
	}
	
	private Token nextID()
	{
		StringBuilder sb = new StringBuilder();
		
		for(char c ; is_id(c = mInput.peek()) ; mInput.get())
		{
			sb.append(c);
		}
		
		if(sb.length() == 0)
			throw new InternalException("Cannot have id length 0!");
		
		return new Token(Type.ID, sb.toString());
	}
	
	private static class Token
	{
		public final Type type;
		public final String value;
		
		public Token(Type type, String value)
		{
			this.type = type;
			this.value = value;
		}
		
		@Override
		public String toString()
		{
			return String.format("'%s' [%s]", value, type);
		}
	}
	
	private static enum Type
	{
		ID,
		SYM,
		STR
	}
	
	private static interface Input
	{
		public abstract boolean empty( );
		public abstract int position( );
		public abstract char peek( );
		public abstract char get( );
		
		public default void skip()
		{
			while(!empty() && is_ws(peek())) get();
		}
	}
	
	private static class StaticInput implements Input
	{
		private final char[] mContent;
		private int mIdx;
		
		public StaticInput(String s)
		{
			mContent = s.toCharArray();
			mIdx = 0;
		}
		
		@Override
		public boolean empty()
		{
			return mIdx >= mContent.length;
		}

		@Override
		public int position()
		{
			return mIdx;
		}

		@Override
		public char peek()
		{
			return mContent[mIdx];
		}

		@Override
		public char get()
		{
			return mContent[mIdx++];
		}
	}
	
	private static class WrapInput implements Input
	{
		private final IntSupplier mCB;
		private int mNext, mPos;
		
		public WrapInput(IntSupplier cb)
		{
			mCB = cb;
			mNext = cb.getAsInt();
			mPos = 0;
		}
		
		public boolean empty() { return mNext == C_EOS; }
		public int position() { return mPos; }
		
		public char peek()
		{
			if(mNext == C_EOS)
				throw new InternalException("Unexpected EOS!");
			
			if(mNext < 0 || mNext > Character.MAX_CODE_POINT)
				throw new InternalException("invalid char: " + mNext);
			
			return (char) mNext;
		}
		
		public char get()
		{
			char r = peek();
			
			mNext = mCB.getAsInt();
			++mPos;
			
			return r;
		}
	}
	
	private static class CompoundInput implements Input
	{
		private final Queue<Input> mInputs;
		private int mPos;
		
		public CompoundInput(Input ... ins)
		{
			mInputs = new LinkedList<>();
			mPos = 0;
			
			for(int i = 0 ; i < ins.length ; ++i)
			{
				add(ins[i]);
			}
		}
		
		public void add(Input in)
		{
			mInputs.add(in);
		}

		@Override
		public boolean empty()
		{
			return mInputs.isEmpty();
		}

		@Override
		public int position()
		{
			return mPos;
		}

		@Override
		public char peek()
		{
			return mInputs.element().peek();
		}

		@Override
		public char get()
		{
			char c = mInputs.element().get();
			
			++mPos;
			
			if(mInputs.element().empty())
			{
				mInputs.poll();
			}
			
			return c;
		}
	}
	
	private static class InternalException extends RuntimeException
	{
		private static final long serialVersionUID = 7360290670422702814L;

		public InternalException(String s) { super(s); }
	}
	
	private static boolean is_ws(int c) { return c == ' ' || c == '\t' || c == '\n' || c == '\r'; }
	private static boolean is_sym(int c) { return c == '"' || c == '<' || c == '>' || c == '/' || c == '='; }
	private static boolean is_id(int c) { return !is_ws(c) && !is_sym(c); }
	
	private static final int C_EOS = -1;
}
