package dave.xml;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.stream.Stream;

public class XmlNode
{
	private final String mID, mType;
	private final Map<String, String> mAttributes;
	private final List<Object> mContent;
	
	public XmlNode(String id) { this(id, ""); }
	public XmlNode(String id, String type)
	{
		if(id == null || type == null)
			throw new NullPointerException();
		
		mID = id;
		mType = type;
		mAttributes = new LinkedHashMap<>();
		mContent = new ArrayList<>();
	}
	
	public XmlNode(String id, XmlNode xml)
	{
		if(id == null)
			throw new NullPointerException();
		
		mID = id;
		mType = xml.mType;
		mAttributes = new LinkedHashMap<>(xml.mAttributes);
		mContent = new ArrayList<>(xml.mContent);
	}
	
	public String id() { return mID; }
	public String type() { return mType; }
	
	public Stream<Object> content() { return mContent.stream(); }
	public Stream<XmlNode> children() { return content().filter(e -> (e instanceof XmlNode)).map(e -> (XmlNode) e); }
	public Stream<String> text() { return content().filter(e -> (e instanceof String)).map(e -> (String) e); }
	
	public Stream<Map.Entry<String, String>> attributes() { return mAttributes.entrySet().stream(); }
	
	public XmlNode find(String path)
	{
		String[] p = path.split("/");
		
		XmlNode n = this;
		
		for(int i = 0 ; i < p.length ; ++i)
		{
			if(p[i].isEmpty()) continue;
			
			String id = p[i];
			
			n = n.children().filter(nn -> nn.id().equals(id)).findFirst().get();
		}
		
		return n;
	}
	
	public void addContent(String v)
	{
		if(v.isEmpty())
			throw new IllegalArgumentException("Cannot accept empty strings!");
		
		mContent.add(v);
	}
	
	public void addContent(XmlNode v)
	{
		if(v == null)
			throw new IllegalArgumentException("Cannot accept null!");
		
		mContent.add(v);
	}
	
	public void removeContent(Object v)
	{
		if(!mContent.remove(v))
			throw new NoSuchElementException("" + v);
	}
	
	public void setAttribute(String id, String v)
	{
		if(id.isEmpty())
			throw new IllegalArgumentException("Attribute ID cannot be empty!");
		if(v == null)
			throw new IllegalArgumentException("null is an invalid attribute!");
		
		mAttributes.put(id, v);
	}

	public String getAttribute(String id)
	{
		if(id.isEmpty())
			throw new IllegalArgumentException("Attribute ID cannot be empty!");
		
		String v = mAttributes.get(id);
		
		if(v == null)
			throw new NoSuchElementException(id);
		
		return v;
	}
	
	public boolean hasAttribute(String id)
	{
		if(id.isEmpty())
			throw new IllegalArgumentException("Attribute ID cannot be empty!");
		
		return mAttributes.containsKey(id);
	}
	
	public void removeAttribute(String id)
	{
		if(id.isEmpty())
			throw new IllegalArgumentException("Attribute ID cannot be empty!");
		
		if(mAttributes.remove(id) == null)
			throw new NoSuchElementException(id);
	}
	
	@Override
	public boolean equals(Object o)
	{
		if(o instanceof XmlNode)
		{
			XmlNode n = (XmlNode) o;
			
			return mID.equals(n.mID) && Objects.equals(mType, n.mType) && mAttributes.equals(n.mAttributes) && mContent.equals(n.mContent);
		}
		
		return false;
	}
	
	@Override
	public int hashCode()
	{
		return mID.hashCode() ^ mAttributes.hashCode() ^ mContent.hashCode();
	}
	
	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		
		(new BasicPrinter(v -> sb.append(v))).process(this);
		
		return sb.toString();
	}
}
