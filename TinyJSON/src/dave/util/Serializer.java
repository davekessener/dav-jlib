package dave.util;

public interface Serializer
{
	public abstract void writeByte(byte v);
	
	public default void write(byte[] v, int o, int l)
	{
		for(int i = o ; i < l + o ; ++i)
		{
			writeByte(v[i]);
		}
	}
	
	public default void write(byte[] v) { write(v, 0, v.length); }
	
	public default void writeBool(boolean f) { writeByte((byte) (f ? 1 : 0)); }
	public default void writeShort(short v) { writeByte((byte) (v & 0xFF)); writeByte((byte) ((v >> 8) & 0xFF)); }
	public default void writeInt(int v) { writeShort((short) (v & 0xFFFF)); writeShort((short) ((v >> 16) & 0xFFFF)); }
	public default void writeLong(long v) { writeInt((int) (v & 0xFFFFFFFF)); writeInt((int) ((v >> 32) & 0xFFFFFFFF)); }
	
	public default void writeFloat(float v) { writeInt(Float.floatToIntBits(v)); }
	public default void writeDouble(double v) { writeLong(Double.doubleToLongBits(v)); }
}
