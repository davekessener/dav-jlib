package dave.util;

public interface Deserializer
{
	public abstract byte readByte( );
	
	public default void read(byte[] v, int o, int l)
	{
		for(int i = o ; i < l + o ; ++i)
		{
			v[i] = readByte();
		}
	}
	
	public default void read(byte[] v) { read(v, 0, v.length); }
	public default byte[] read(int l) { byte[] v = new byte[l]; read(v); return v; }
	
	public default boolean readBool( ) { return readByte() == 1; }
	public default short readShort( ) { short lo = (short) (readByte() & 0xFF); short hi = (short) (readByte() & 0xFF); return (short) ((hi << 8) | lo); }
	public default int readInt( ) { int lo = readShort() & 0xFFFF; int hi = readShort() & 0xFFFF; return (int) ((hi << 16) | lo); }
	public default long readLong( ) { long lo = readInt() & 0xFFFFFFFFl; long hi = readInt() & 0xFFFFFFFFl; return (long) ((hi << 32l) | lo); }
	
	public default float readFloat( ) { return Float.intBitsToFloat(readInt()); }
	public default double readDouble( ) { return Double.longBitsToDouble(readLong()); }
}
