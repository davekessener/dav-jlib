package dave.util;

import java.io.IOException;
import java.io.OutputStream;

import dave.json.SevereIOException;

public class StreamSerializer implements Serializer
{
	private final OutputStream mOut;
	
	public StreamSerializer(OutputStream out)
	{
		mOut = out;
	}

	@Override
	public void writeByte(byte v)
	{
		try
		{
			mOut.write(v);
		}
		catch(IOException e)
		{
			throw new SevereIOException(e);
		}
	}
}
