package dave.util;

import java.io.IOException;
import java.io.InputStream;

import dave.json.SevereIOException;

public class StreamDeserializer implements Deserializer
{
	private final InputStream mIn;
	
	public StreamDeserializer(InputStream in)
	{
		mIn = in;
	}

	@Override
	public byte readByte()
	{
		try
		{
			int v = mIn.read();
			
			if(v < 0 || v > 0xFF)
				throw new SevereIOException();
			
			return (byte) v;
		}
		catch(IOException e)
		{
			throw new SevereIOException(e);
		}
	}
}
