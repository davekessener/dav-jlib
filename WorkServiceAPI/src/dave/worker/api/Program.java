package dave.worker.api;

import dave.json.JsonValue;

public interface Program
{
	public abstract void run(JsonValue args) throws Exception;
}
