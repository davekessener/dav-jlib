package dave.db;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;

import dave.db.api.DataType;
import dave.db.api.DatabaseConnection;
import dave.db.api.Many;
import dave.db.api.Record;
import dave.db.api.RecordFactory;
import dave.db.api.RecordSet;
import dave.db.api.Row;
import dave.db.api.SQLError;
import dave.db.api.Transitive;

public class AbstractRecordFactory implements RecordFactory
{
	private final DatabaseConnection mCon;
	private final Set<String> mKnownTables;
	private final Set<String> mKnownAssocs;
	private final Map<String, Record> mCache;
	
	public AbstractRecordFactory(DatabaseConnection c)
	{
		mCon = c;
		
		mKnownTables = new HashSet<>();
		mKnownAssocs = new HashSet<>();
		mCache = new HashMap<>();
	}

	@Override
	public <T extends Record> T create(Class<T> c, Row data)
	{
		checkTable(c);
		
		return retrieve(c, mCon.insertRow(c.getSimpleName(), data));
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T extends Record> T retrieve(Class<T> c, int id)
	{
		checkTable(c);
		
		String ref = c.getSimpleName() + "_" + id;
		
		Record r = mCache.get(ref);
		
		if(r == null)
		{
			mCache.put(ref, r = (Record) Proxy.newProxyInstance(c.getClassLoader(), new Class<?>[] { c }, new Handler(c, id)));
		}
		
		return (T) r;
	}
	
	@Override
	public <T extends Record> void registerTable(Class<T> c)
	{
		checkTable(c);
	}
	
	@Override
	public <T extends Record> Stream<T> all(Class<T> c)
	{
		return mCon.select(c.getSimpleName(), null).stream().map(row -> retrieve(c, (Integer) row.get("id")));
	}
	
	private void checkTable(Class<? extends Record> c)
	{
		String tbl = c.getSimpleName();
		
		if(!mKnownTables.contains(tbl))
		{
			mKnownTables.add(tbl);
			
			if(!mCon.checkTableExists(tbl))
			{
				RecordInterface iface = extractFields(c);
				
				mCon.createTable(tbl);
				iface.fields.entrySet().forEach(e -> mCon.addColumnToTable(tbl, e.getKey(), e.getValue()));
				
				iface.assocs.values().stream().forEach(e -> {
					checkTable(e.other(c));
					
					if(e.type == AssocType.ONE)
					{
						mCon.addReference(tbl, e.other(c).getSimpleName());
					}
					else
					{
						checkAssociation(c, e);
					}
				});
			}
		}
	}
	
	private void checkAssociation(Class<? extends Record> c, RecordAssoc a)
	{
		String tbl = a.getTableName(c);
		
		if(tbl == null) return;
		
		if(!mKnownAssocs.contains(tbl))
		{
			mKnownAssocs.add(tbl);
			
			if(!mCon.checkTableExists(tbl))
			{
				mCon.createTable(tbl);

				mCon.addReference(tbl, a.from.getSimpleName());
				mCon.addReference(tbl, a.to.getSimpleName());
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public static final RecordInterface extractFields(Class<? extends Record> c)
	{
		Map<String, DataType> fields = new HashMap<>();
		Map<String, RecordAssoc> assocs = new HashMap<>(); 
		
		Stream.of(c.getDeclaredMethods()).forEach(m -> {
			if(m.isAnnotationPresent(Many.class))
			{
				Class<? extends Record> t = m.getAnnotation(Many.class).value();
				
				assocs.put(m.getName(), new RecordAssoc(c, t, AssocType.MANY));
			}
			else if(m.isAnnotationPresent(Transitive.class))
			{
				Class<? extends Record> t = m.getAnnotation(Transitive.class).value();
				
				assocs.put(m.getName(), new RecordAssoc(c, t, AssocType.MANYMANY));
			}
			else
			{
				if(m.getReturnType() != void.class) return;
				if(m.getParameterCount() != 1) return;
				
				Class<?> t = m.getParameters()[0].getType();
				
				Method getter = Stream.of(c.getDeclaredMethods())
					.filter(g -> g.getName().equals(m.getName()))
					.filter(g -> g.getParameterCount() == 0)
					.filter(g -> g.getReturnType().equals(t))
					.findFirst().orElse(null);
				
				if(getter == null) return;
				
				if(Record.class.isAssignableFrom(t))
				{
					assocs.put(m.getName(), new RecordAssoc(c, (Class<? extends Record>) t, AssocType.ONE));
				}
				else
				{
					fields.put(m.getName(), DataType.fromJavaType(t));
				}
			}
		});
		
		return new RecordInterface(fields, assocs);
	}
	
	public static enum AssocType
	{
		ONE,
		MANY,
		MANYMANY
	}
	
	public static class RecordAssoc
	{
		public final Class<? extends Record> from, to;
		public final AssocType type;
		
		public RecordAssoc(Class<? extends Record> a, Class<? extends Record> b, AssocType t)
		{
			int c = a.getSimpleName().toLowerCase().compareTo(b.getSimpleName().toLowerCase());
			
			this.from = (c < 0 ? a : b);
			this.to = (c < 0 ? b : a);
			this.type = t;
		}
		
		public String getTableName(Class<? extends Record> c)
		{
			switch(type)
			{
				case ONE:
					return null;
					
				case MANY:
					return String.format("%s_%s", c.getSimpleName(), other(c).getSimpleName());
					
				case MANYMANY:
					return String.format("%s_X_%s", from.getSimpleName(), to.getSimpleName());
					
				default:
					throw new RuntimeException("" + type);
			}
		}
		
		public Class<? extends Record> other(Class<? extends Record> c)
		{
			return from.equals(c) ? to : from;
		}
	}
	
	public static class RecordInterface
	{
		public final Map<String, DataType> fields;
		public final Map<String, RecordAssoc> assocs;
		
		public RecordInterface(Map<String, DataType> fields, Map<String, RecordAssoc> assocs)
		{
			this.fields = fields;
			this.assocs = assocs;
		}
	}

	private class Handler implements InvocationHandler
	{
		private final Class<? extends Record> mmClass;
		private final int mmID;
		private final RecordInterface mmIface;
		private final Map<String, Object> mmData;
		
		public Handler(Class<? extends Record> c, int id)
		{
			mmClass = c;
			mmID = id;
			
			mmIface = extractFields(c);
			mmData = new HashMap<>();
			
			List<Row> rows = mCon.select(c.getSimpleName(), "id = ?", id);
			
			if(rows.size() != 1)
				throw new SQLError(new SQLException("In table " + c.getSimpleName() + " id=" + id + " invalid row count: " + rows.size()));
			
			rows.get(0).entries().forEach(e -> mmData.put(e.first, e.second));
		}

		@Override
		public Object invoke(Object proxy, Method method, Object[] args) throws Throwable
		{
			String field = method.getName();
			
			if(field.equals("getID"))
			{
				return mmID;
			}
			else if(field.equals("getRecordClass"))
			{
				return mmClass;
			}
			else if(field.equals("getFactory"))
			{
				return AbstractRecordFactory.this;
			}
			else if(field.equals("equals"))
			{
				Record r = (Record) args[0];
				
				return mmID == r.getID() && mmClass.equals(r.getRecordClass());
			}
			else
			{
				boolean getter = method.getParameterCount() == 0;
				RecordAssoc ref = mmIface.assocs.get(field);
				DataType t = mmIface.fields.get(field);
				
				if(t == null && ref == null)
				{
					return method.invoke(this, args);
				}
				else
				{
					if(getter)
					{
						return getter(field, ref);
					}
					else
					{
						setter(field, ref, args[0]);
					}
				}
			}
			
			return null;
		}
		
		private Object getter(String field, RecordAssoc a)
		{
			if(a == null)
			{
				return mmData.get(field);
			}
			else switch(a.type)
			{
				case ONE:
				{
					Object id = mmData.get(a.other(mmClass).toString() + "_id");
					
					return id == null ? null : AbstractRecordFactory.this.retrieve(a.other(mmClass), (Integer) id);
				}
				
				case MANY:
				case MANYMANY:
					return generateSet(a.getTableName(mmClass), a.other(mmClass));
				
				default:
					throw new RuntimeException("" + a.type);
			}
		}
		
		private RecordSet<Record> generateSet(String table, Class<? extends Record> o)
		{
			return new VarRecSet<Record>(
				() -> mCon.select(table, String.format("%s_id = ?", mmClass.getSimpleName()), mmID).stream()
					.map(row -> AbstractRecordFactory.this.retrieve(o, (Integer) row.get(o.getSimpleName() + "_id"))),
				e -> !mCon.select(table, String.format("%s_id = ? AND %s_id = ?", mmClass.getSimpleName(), o.getSimpleName()), mmID, e.getID()).isEmpty(),
				e -> mCon.insertRow(table, (new Row()).add(mmClass.getSimpleName() + "_id", mmID).add(o.getSimpleName() + "_id", e.getID())),
				e -> mCon.select(table, String.format("%s_id = ? AND %s_id = ?", mmClass.getSimpleName(), o.getSimpleName()), mmID, e.getID())
					.forEach(row -> mCon.deleteRow(table, (Integer) row.get("id")))
			);
		}
		
		private void setter(String field, RecordAssoc a, Object v)
		{
			if(a == null)
			{
				mmData.put(field, v);
				mCon.updateRow(mmClass.getSimpleName(), mmID, (new Row()).add(field, v));
			}
			else switch(a.type)
			{
				case ONE:
				{
					int id = ((Record) v).getID();
					
					mmData.put(a.other(mmClass).toString() + "_id", id);
					mCon.updateRow(mmClass.getSimpleName(), mmID, (new Row()).add(a.other(mmClass).getSimpleName() + "_id", id));
				}
				break;
				
				default:
					throw new RuntimeException("" + a.type);
			}
		}
	}
}
