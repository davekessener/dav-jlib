package dave.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import dave.db.api.DataType;
import dave.util.log.Logger;

public class H2SQLConnection implements SQLConnection
{
	private final Connection mCon;
	
	public H2SQLConnection(String path) { this(path, "", ""); }
	public H2SQLConnection(String path, String user, String pass)
	{
		mCon = DBUtils.wrap(() -> DriverManager.getConnection(path, user, pass));
	}
	
	@Override
	public void close()
	{
		DBUtils.wrap(() -> mCon.close());
	}
	
	@Override
	public ResultSet execute(String sql, Object ... args)
	{
		LOG.log("[H2] %s%s", sql, args.length == 0 ? "" : " (" + Stream.of(args).map(DataType::safeAutoToSQL).collect(Collectors.joining(", ")) + ")");
		
		return DBUtils.wrap(() -> {
			PreparedStatement s = mCon.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			
			for(int i = 0 ; i < args.length ; ++i)
			{
				s.setObject(i + 1, args[i]);
			}
			
			s.execute();
			
			ResultSet r = s.getResultSet();
			
			if(r == null)
			{
				r = s.getGeneratedKeys();
			}
			
			return r;
		});
	}
	
	private static final Logger LOG = Logger.get("h2-sql");
}
