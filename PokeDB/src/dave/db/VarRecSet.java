package dave.db;

import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Stream;

import dave.db.api.Record;
import dave.db.api.RecordSet;
import dave.util.Producer;

public class VarRecSet<T extends Record> implements RecordSet<T>
{
	private final boolean mMutable;
	private final Consumer<T> mAdd, mRem;
	private final Predicate<T> mContains;
	private final Producer<Stream<T>> mSet;
	
	public VarRecSet(Producer<Stream<T>> s, Predicate<T> c)
	{
		mMutable = false;
		
		mAdd = mRem = null;
		
		mContains = c;
		mSet = s;
	}
	
	public VarRecSet(Producer<Stream<T>> s, Predicate<T> c, Consumer<T> add, Consumer<T> rem)
	{
		mMutable = true;
		
		mAdd = add;
		mRem = rem;
		
		mContains = c;
		mSet = s;
	}

	@Override
	public boolean isMutable()
	{
		return mMutable;
	}

	@Override
	public Stream<T> stream()
	{
		return mSet.produce();
	}

	@Override
	public void add(T record)
	{
		if(!isMutable()) throw new IllegalStateException("Immutable set: " + record);
		
		if(!contains(record))
		{
			mAdd.accept(record);
		}
	}

	@Override
	public void remove(T record)
	{
		if(!isMutable()) throw new IllegalStateException("Immutable set: " + record);
		
		mRem.accept(record);
	}

	@Override
	public boolean contains(T record)
	{
		return mContains.test(record);
	}
}
