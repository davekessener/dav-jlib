package dave.db.api;

import java.util.stream.Stream;

public interface RecordFactory
{
	public abstract <T extends Record> void registerTable(Class<T> c);
	public abstract <T extends Record> T retrieve(Class<T> c, int id);
	public abstract <T extends Record> T create(Class<T> c, Row data);
	
	public abstract <T extends Record> Stream<T> all(Class<T> c);
	
	public default <T extends Record> T create(Class<T> c) { return create(c, new Row()); }
}
