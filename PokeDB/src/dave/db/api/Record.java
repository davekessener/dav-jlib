package dave.db.api;

public interface Record
{
	public abstract int getID( );
	public abstract Class<? extends Record> getRecordClass( );
	public abstract RecordFactory getFactory( );
}
