package dave.db.api;

import java.util.stream.Stream;

public interface RecordSet<T extends Record>
{
	public abstract boolean isMutable( );
	
	public abstract Stream<T> stream( );
	
	public abstract void add(T record);
	public abstract void remove(T record);
	
	public abstract boolean contains(T record);
}
