package dave.db.api;

import java.util.List;

public interface DatabaseConnection
{
	public abstract boolean checkTableExists(String name);
	public abstract void createTable(String name);
	public abstract void removeTable(String name);
	
	public abstract void addColumnToTable(String table, String col, DataType t, String ... mods);
	public abstract void removeColumnFromTable(String table, String col);
	public abstract void addReference(String table, String ref_table);
	
	public abstract int insertRow(String table, Row row);
	public abstract void deleteRow(String table, int id);
	
	public abstract List<Row> select(String table, String cond, Object ... args);
	public abstract void updateRow(String table, int id, Row row);
	
	public abstract void close( );
}
