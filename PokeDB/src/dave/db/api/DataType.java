package dave.db.api;

import java.sql.SQLException;
import java.util.function.Function;

public enum DataType
{
	INT("INTEGER", Object::toString),
	REAL("DOUBLE", Object::toString),
	STR("VARCHAR(65535)", o -> String.format("'%s'", o));
	
	public final String sql_type;
	public final Function<Object, String> sql_converter;
	
	public String toSQL(Object o) { return sql_converter.apply(o); }
	
	private DataType(String sql_type, Function<Object, String> sql_converter)
	{
		this.sql_type = sql_type;
		this.sql_converter = sql_converter;
	}
	
	public static String safeAutoToSQL(Object o) { return o == null ? "null" : autoToSQL(o); }
	
	public static String autoToSQL(Object o)
	{
		if(o instanceof String)
		{
			return STR.toSQL(o);
		}
		else if(o instanceof Integer)
		{
			return INT.toSQL(o);
		}
		else if(o instanceof Double)
		{
			return REAL.toSQL(o);
		}
		else
		{
			throw new SQLError(new SQLException("Invalid java type: " + (o == null ? "null" : o.getClass())));
		}
	}
	
	public static DataType parseSQLType(int t)
	{
		switch(t)
		{
			case java.sql.Types.INTEGER:
			case java.sql.Types.BIGINT:
				return INT;
				
			case java.sql.Types.DOUBLE:
				return REAL;
				
			case java.sql.Types.VARCHAR:
				return STR;
				
			default:
				throw new SQLError(new SQLException("unsupported sql type: " + t));
		}
	}
	
	public static DataType fromJavaType(Class<?> c)
	{
		if(c.equals(int.class))
		{
			return INT;
		}
		else if(c.equals(double.class))
		{
			return REAL;
		}
		else if(c.equals(String.class))
		{
			return STR;
		}
		else
		{
			throw new IllegalArgumentException("No such type: " + c.getCanonicalName());
		}
	}
}
