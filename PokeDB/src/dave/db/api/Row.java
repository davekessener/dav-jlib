package dave.db.api;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

import dave.util.Utils;

public class Row
{
	private final Map<String, Object> mData = new HashMap<>();
	
	public Row add(String name, Object v)
	{
		mData.put(name.toLowerCase(), v);
		
		return this;
	}
	
	public Stream<String> keys() { return mData.keySet().stream(); }
	public Stream<Object> values() { return mData.values().stream(); }
	public Stream<Utils.Pair<String, Object>> entries() { return mData.entrySet().stream().map(Utils::pair); }
	
	public Object get(String name) { return mData.get(name); }
}
