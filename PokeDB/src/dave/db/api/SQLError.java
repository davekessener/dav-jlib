package dave.db.api;

import java.sql.SQLException;

public class SQLError extends RuntimeException
{
	private static final long serialVersionUID = -2797764448878220784L;

	public final SQLException e;
	
	public SQLError(SQLException e)
	{
		super(e);
		
		this.e = e;
	}
}
