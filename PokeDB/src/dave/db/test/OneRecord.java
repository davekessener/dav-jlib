package dave.db.test;

import dave.db.api.Record;

public interface OneRecord extends Record
{
	public abstract TestRecord tr( );
	public abstract void tr(TestRecord v);
}
