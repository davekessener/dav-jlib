package dave.db.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import dave.db.AbstractRecordFactory;
import dave.db.H2SQLConnection;
import dave.db.SQLAdapter;
import dave.db.api.DataType;
import dave.db.api.DatabaseConnection;
import dave.db.api.RecordFactory;
import dave.db.api.Row;
import dave.util.log.LogBase;
import dave.util.log.LogSink;
import dave.util.log.Logger;

public class DBConUT
{
	private DatabaseConnection c = null;
	
	@BeforeClass
	public static void init()
	{
		LogBase.INSTANCE.registerSink(e -> true, LogSink.build());
		
		LogBase.INSTANCE.start();
	}
	
	@AfterClass
	public static void shutdown()
	{
		LogBase.INSTANCE.stop();
	}
	
	@Before
	public void setup()
	{
		c = createConnection();
	}
	
	@After
	public void teardown()
	{
		c.close();
		c = null;
	}
	
	@Test
	public void testDB()
	{
		Logger.DEFAULT.log("=== DB =======================================");
		
		String tbl = "testtbl";
		
		assertFalse(c.checkTableExists(tbl));
		
		c.createTable(tbl);
		
		c.addColumnToTable(tbl, "name", DataType.STR);
		c.addColumnToTable(tbl, "age", DataType.INT);

		assertTrue(c.checkTableExists(tbl));
		
		int id1 = c.insertRow(tbl, (new Row()).add("name", "Sam").add("age", 17));
		c.insertRow(tbl, (new Row()).add("name", "Alex").add("age", 23));
		c.insertRow(tbl, (new Row()).add("name", "Sebastian").add("age", 18));
		int id2 = c.insertRow(tbl, (new Row()).add("name", "Scott").add("age", 16));
		c.insertRow(tbl, (new Row()).add("name", "Methusalem").add("age", 99));
		
		List<Row> rows = c.select(tbl, "age < ?", 18);
		
		assertEquals(2, rows.size());
		
		Row sam = rows.get(0);
		Row scott = rows.get(1);
		
		assertEquals(1, sam.get("id"));
		assertEquals(17, sam.get("age"));
		assertEquals("Sam", sam.get("name"));
		
		assertEquals(4, scott.get("id"));
		assertEquals(16, scott.get("age"));
		assertEquals("Scott", scott.get("name"));
		
		assertEquals(1, id1);
		assertEquals(4, id2);
	}
	
	@Test
	public void testFieldExtraction()
	{
		Logger.DEFAULT.log("=== Extract =======================================");
		
		AbstractRecordFactory.RecordInterface iface = AbstractRecordFactory.extractFields(TestRecord.class);
		
		assertEquals(2, iface.fields.size());
		assertEquals(DataType.STR, iface.fields.get("name"));
		assertEquals(DataType.INT, iface.fields.get("age"));
	}
	
	@Test
	public void testRecords()
	{
		Logger.DEFAULT.log("=== Records =======================================");
		
		RecordFactory factory = new AbstractRecordFactory(c);
		
		TestRecord user = factory.create(TestRecord.class);
		
		assertTrue(user.equals(user));
		
		user.age(17);
		user.name("Sam");
		
		List<Row> rows = c.select(TestRecord.class.getSimpleName(), "id > ?", 0);
		
		assertEquals(1, rows.size());
		assertEquals(17, rows.get(0).get("age"));
		assertEquals("Sam", rows.get(0).get("name"));
	}
	
	@Test
	public void testOneAssoc()
	{
		Logger.DEFAULT.log("=== OneAssoc =======================================");
		
		RecordFactory factory = new AbstractRecordFactory(c);
		
		OneRecord rec = factory.create(OneRecord.class);
		TestRecord e = factory.create(TestRecord.class);
		
		assertEquals(null, rec.tr());
		
		rec.tr(e);
		
		assertEquals(e, rec.tr());
	}
	
	@Test
	public void testManyAssoc()
	{
		Logger.DEFAULT.log("=== ManyAssoc =======================================");
		
		RecordFactory factory = new AbstractRecordFactory(c);
		
		ManyRecord a = factory.create(ManyRecord.class);
		OtherRecord b = factory.create(OtherRecord.class);
		TestRecord c = factory.create(TestRecord.class);
		
		assertFalse(a.others().contains(b));
		assertFalse(a.tests().contains(c));
		
		a.others().add(b);
		a.tests().add(c);
		
		assertTrue(a.others().contains(b));
		assertTrue(b.others().contains(a));
		assertTrue(a.tests().contains(c));
	}
	
	@Test
	public void testFactoryUtils()
	{
		Logger.DEFAULT.log("=== FactoryUtils =======================================");
		
		RecordFactory factory = new AbstractRecordFactory(c);
		
		factory.create(TestRecord.class, (new Row()).add("name", "Sam").add("age", 17));
		factory.create(TestRecord.class, (new Row()).add("name", "Alex").add("age", 23));
		factory.create(TestRecord.class, (new Row()).add("name", "Vincent").add("age", 18));
		factory.create(TestRecord.class, (new Row()).add("name", "Scott").add("age", 16));
		factory.create(TestRecord.class, (new Row()).add("name", "Methusalem").add("age", 99));
		
		assertEquals(5, factory.all(TestRecord.class).count());
	}
	
	private static DatabaseConnection createConnection()
	{
		return new SQLAdapter(new H2SQLConnection("jdbc:h2:mem:myDb"));
	}
}
