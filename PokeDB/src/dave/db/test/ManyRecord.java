package dave.db.test;

import dave.db.api.Many;
import dave.db.api.Record;
import dave.db.api.RecordSet;
import dave.db.api.Transitive;

public interface ManyRecord extends Record
{
	@Transitive(OtherRecord.class)
	public abstract RecordSet<OtherRecord> others( );
	
	@Many(TestRecord.class)
	public abstract RecordSet<TestRecord> tests( );
}
