package dave.db.test;

import dave.db.api.Record;

public interface TestRecord extends Record
{
	public abstract String name( );
	public abstract void name(String name);
	
	public abstract int age( );
	public abstract void age(int age);
}
