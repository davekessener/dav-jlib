package dave.db.test;

import dave.db.api.Record;
import dave.db.api.RecordSet;
import dave.db.api.Transitive;

public interface OtherRecord extends Record
{
	@Transitive(ManyRecord.class)
	public abstract RecordSet<ManyRecord> others( );
}
