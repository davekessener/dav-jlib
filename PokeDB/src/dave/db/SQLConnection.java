package dave.db;

import java.sql.ResultSet;

public interface SQLConnection
{
	public abstract ResultSet execute(String sql, Object ... args);
	public abstract void close( );
}
