package dave.db;

import java.io.File;
import java.sql.SQLException;
import java.util.concurrent.Callable;

import dave.db.api.SQLError;
import dave.util.Utils;

public final class DBUtils
{
	public static H2SQLConnection openH2DB(String ... path)
	{
		return new H2SQLConnection(path.length == 0 ? "jdbc:h2:mem:RAMDB" : "jdbc:h2:" + Utils.join(path, File.separator));
	}
	
	public static <T> T wrap(Callable<T> f)
	{
		try
		{
			return f.call();
		}
		catch(SQLException e)
		{
			throw new SQLError(e);
		}
		catch(Exception e)
		{
			throw new RuntimeException(e);
		}
	}
	
	public static void wrap(ThrowingRunnable f)
	{
		try
		{
			f.run();
		}
		catch(SQLException e)
		{
			throw new SQLError(e);
		}
		catch(Exception e)
		{
			throw new RuntimeException(e);
		}
	}
	
	public static interface ThrowingRunnable { void run() throws Exception; }
	
	private DBUtils( ) { }
}
