package dave.db;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import dave.db.api.DataType;
import dave.db.api.DatabaseConnection;
import dave.db.api.Row;
import dave.util.Utils;

public class SQLAdapter implements DatabaseConnection
{
	private final SQLConnection mCon;
	
	public SQLAdapter(SQLConnection con)
	{
		mCon = con;
	}
	
	@Override
	public void close()
	{
		mCon.close();
	}
	
	@Override
	public boolean checkTableExists(String name)
	{
		ResultSet r = mCon.execute(String.format("SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = ?"), name.toUpperCase());
		
		return DBUtils.wrap(() -> r.next());
	}

	@Override
	public void createTable(String name)
	{
		mCon.execute(String.format("CREATE TABLE %s (id INTEGER NOT NULL AUTO_INCREMENT, PRIMARY KEY (id))", name));
	}

	@Override
	public void removeTable(String name)
	{
		mCon.execute(String.format("DROP TABLE %s", name));
	}

	@Override
	public void addColumnToTable(String table, String col, DataType t, String ... mods)
	{
		mCon.execute(String.format("ALTER TABLE %s ADD %s %s%s", table, col, t.sql_type, mods.length == 0 ? "" : " " + Utils.join(mods, "")));
	}

	@Override
	public void removeColumnFromTable(String table, String col)
	{
		mCon.execute(String.format("ALTER TABLE %s DROP COLUMN %s", table, col));
	}
	
	@Override
	public void addReference(String table, String ref_table)
	{
		addColumnToTable(table, ref_table + "_id", DataType.INT);
		
		mCon.execute(String.format("ALTER TABLE %s ADD FOREIGN KEY (%s) REFERENCES %s(id)", table, ref_table + "_id", ref_table));
	}

	@Override
	public int insertRow(String table, Row row)
	{
		ResultSet r = mCon.execute(String.format("INSERT INTO %s (%s) VALUES (%s)",
				table,
				row.keys().collect(Collectors.joining(",")),
				row.values().map(e -> "?").collect(Collectors.joining(","))),
			row.values().toArray());
		
		return DBUtils.wrap(() -> {
			r.next();
			
			return (Integer) r.getInt(1);
		});
	}

	@Override
	public void deleteRow(String table, int id)
	{
		mCon.execute(String.format("DELETE FROM %s WHERE id = ?", table), id);
	}

	@Override
	public List<Row> select(String table, String cond, Object ... args)
	{
		StringBuilder cmd = new StringBuilder();
		
		cmd.append("SELECT * FROM ").append(table);
		
		if(cond != null)
		{
			cmd.append(" WHERE ").append(cond);
		}
		
		ResultSet r = mCon.execute(cmd.toString(), args);
		
		List<Row> results = new ArrayList<>();
		
		DBUtils.wrap(() -> {
			ResultSetMetaData meta = r.getMetaData();
			
			while(r.next())
			{
				Row row = new Row();
				
				for(int i = 1 ; i <= meta.getColumnCount() ; ++i)
				{
					String name = meta.getColumnName(i);
					Object val = r.getObject(i);
					
					row.add(name, val);
				}
				
				results.add(row);
			}
		});
		
		return results;
	}

	@Override
	public void updateRow(String table, int id, Row row)
	{
		mCon.execute(String.format("UPDATE %s SET %s WHERE id = ?",
				table,
				row.keys().map(k -> String.format("%s = ?", k)).collect(Collectors.joining(", "))),
			Utils.append(row.values().toArray(), id));
	}
}
