package dave.game.demo;

import dave.game.Key;

public class PlayerComponent extends Component
{
	private final DataComponent mData;
	private final PhysicsComponent mPhysics;
	private int mJCnt;
	private boolean mHasJumped;
	
	public PlayerComponent(Entity e, DataComponent d, PhysicsComponent ph)
	{
		super(e);
		
		mData = d;
		mPhysics = ph;
		mJCnt = 0;
		mHasJumped = false;
	}

	@Override
	public void update( )
	{
		if(mPhysics.isOnGround())
		{
			if(mData.world.getGame().getKeyState(Key.A))
			{
				if(!mHasJumped)
				{
					mJCnt = 25;
					mHasJumped = true;
				}
			}
			else
			{
				mHasJumped = false;
			}
		}
		
		if(mJCnt > 0)
		{
			if(mJCnt > 10 || mData.world.getGame().getKeyState(Key.A))
			{
				mPhysics.getVelocity().addY(-JUMP_D - 20);
				--mJCnt;
			}
			else
			{
				mJCnt = 0;
			}
		}
		
		boolean go_left = mData.world.getGame().getKeyState(Key.LEFT);
		boolean go_right = mData.world.getGame().getKeyState(Key.RIGHT);
		int f = mData.world.getGame().getKeyState(Key.B) ? 2 : 1;
		
		mPhysics.setMaxSpeed(PhysicsComponent.MAX_SPEED * f);
		
		if(go_left && !go_right)
		{
			mPhysics.getVelocity().addX(-SPEED * f);
		}
		if(go_right && !go_left)
		{
			mPhysics.getVelocity().addX(SPEED * f);
		}
		if(!go_left && !go_right)
		{
			mPhysics.getVelocity().setX(mPhysics.getVelocity().getX() / 2);
		}
	}
	
	private static final int SPEED = 3;
	private static final int JUMP_D = 15;
}
