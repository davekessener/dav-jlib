package dave.game.demo;

import dave.game.IDisplay;
import dave.util.math.Vec2;

public class DataComponent extends Component
{
	public final World world;
	public AABB aabb;
	public Sprite sprite;
	public Vec2 offset;
	
	public DataComponent(Entity e, World w)
	{
		super(e);
		
		world = w;
	}

	@Override
	public void draw(IDisplay s, Vec2 o)
	{
		if(sprite != null)
		{
			Vec2 p = Vec2.sub(aabb.origin(), o);
			
			if(offset != null) p.add(offset);
			
			sprite.draw(s, p);
		}
	}
}
