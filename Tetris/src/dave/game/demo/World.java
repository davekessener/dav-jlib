package dave.game.demo;

import java.util.ArrayList;
import java.util.List;

import dave.game.IEngine;
import dave.game.IDisplay;
import dave.util.math.Vec2;

public class World
{
	private final IEngine mGame;
	private final TileMap mMap;
	private final List<Entity> mEntities;
	private final Vec2 mPos;
	private final List<AABB> mTerrain;
	private final List<Event> mEvents;
	
	public World(IEngine g, TileMap m)
	{
		mGame = g;
		mMap = m;
		mEntities = new ArrayList<>();
		mEvents = new ArrayList<>();
		mPos = new Vec2(0, 0);
		mTerrain = mMap.generateTerrain();
	}
	
	public IEngine getGame( ) { return mGame; }
	public List<Entity> getEntities( ) { return mEntities; }
	public List<Event> getEvents( ) { return mEvents; }
	public Vec2 getPosition( ) { return mPos; }
	public List<AABB> getTerrain( ) { return mTerrain; }
	
	public void update( )
	{
		mEntities.forEach(e -> { if(e.isAlive()) e.update(); });
		mEntities.removeIf(e -> !e.isAlive());
		
		for(Event e : mEvents)
		{
			if(e.canExecute(mPos.getX()))
			{
				e.execute(this);
			}
		}
		
		mEvents.removeIf(e -> e.canExecute(mPos.getX()));
	}
	
	public void draw(IDisplay s)
	{
		mMap.draw(s, mPos);
		
		for(Entity e : mEntities)
		{
			e.draw(s, mPos);
		}
	}
	
	public static interface Callback { public abstract void execute(World w); }
	
	public static class Event
	{
		private final int mTrigger;
		private final Callback mCallback;
		
		public Event(int t, Callback cb)
		{
			mTrigger = t;
			mCallback = cb;
		}
		
		public boolean canExecute(int x) { return x >= mTrigger; }
		public void execute(World w) { mCallback.execute(w); }
	}
}
