package dave.game.demo;

import dave.util.math.Vec2;

public class EnemyEntity extends PhysicalEntity
{
	private int mDir;
	
	public EnemyEntity(World w, Sprite s, Vec2 p)
	{
		super(w, s, p);
		
		mDir = RIGHT;
	}
	
	private void toggle( ) { mDir = (mDir == LEFT ? RIGHT : LEFT); }
	
	@Override
	public void update( )
	{
		super.update();
		
		if(getPhysics().getVelocity().getX() == 0) toggle();
		
		getPhysics().getVelocity().setX(mDir * SPEED);
	}
	
	@Override
	protected void collide(PhysicalEntity e)
	{
		if(e instanceof EnemyEntity)
		{
			mDir = getData().aabb.origin().getX() < e.getData().aabb.origin().getX() ? LEFT : RIGHT;
			getPhysics().getVelocity().setX(mDir * SPEED);
		}
		else if(e instanceof PlayerEntity)
		{
			((PlayerEntity) e).collide(this);
		}
	}
	
	public static final int LEFT = -1, RIGHT = 1;
	private static final int SPEED = 24;
}
