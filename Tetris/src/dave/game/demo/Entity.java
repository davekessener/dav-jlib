package dave.game.demo;

import java.util.ArrayList;
import java.util.List;

import dave.game.IDisplay;
import dave.util.math.Vec2;

public abstract class Entity
{
	private final List<Component> mComponents = new ArrayList<>();
	private boolean mAlive = true;
	
	protected void add(Component c) { mComponents.add(c); }
	
	public void kill( ) { mAlive = false; }
	public boolean isAlive( ) { return mAlive; }
	
	public void update( )
	{
		for(Component c : mComponents)
		{
			c.update();
		}
	}
	
	public void draw(IDisplay s, Vec2 o)
	{
		for(Component c : mComponents)
		{
			c.draw(s, o);
		}
	}
}
