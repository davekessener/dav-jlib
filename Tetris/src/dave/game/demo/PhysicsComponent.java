package dave.game.demo;

import dave.util.math.Vec2;

public class PhysicsComponent extends Component
{
	private final DataComponent mData;
	private final Vec2 mV, mT;
	private boolean mOnGround;
	private int mSpeedLimit;
	
	public PhysicsComponent(PhysicalEntity e, DataComponent data)
	{
		super(e);
		
		mData = data;
		mV = new Vec2(0, 0);
		mT = new Vec2(0, 0);
		mOnGround = false;
		mSpeedLimit = MAX_SPEED;
	}
	
	public boolean isOnGround( ) { return mOnGround; }
	public void setMaxSpeed(int v) { mSpeedLimit = v; }
	public Vec2 getVelocity( ) { return mV; }
	
	@Override
	public void update( )
	{
		if(mData.aabb != null)
		{
			mV.addY(20);
			
			if(mV.getX() < -mSpeedLimit) mV.setX(-mSpeedLimit);
			if(mV.getX() >  mSpeedLimit) mV.setX( mSpeedLimit);
//			if(mV.getY() < -mSpeedLimit) mV.setY(-mSpeedLimit);
			if(mV.getY() >  mSpeedLimit) mV.setY( mSpeedLimit);
			
			mT.add(mV);
			
			int dx = (int) mT.getX() / TIME_SCALE, dy = (int) mT.getY() / TIME_SCALE;
			
			mT.set(new Vec2(mT.getX() % TIME_SCALE, mT.getY() % TIME_SCALE));
			
			while(dy != 0)
			{
				mOnGround = false;
				
				int ddy = dy < 0 ? -1 : 1;
				AABB nbb = AABB.apply(mData.aabb, new Vec2(0, ddy));
				
				for(AABB bb : mData.world.getTerrain())
				{
					if(nbb.intersects(bb))
					{
						mOnGround = mV.getY() > 0;
						
						mV.setY(0);
						mT.setY(0);
						dy = 0;
						
						break;
					}
				}

				if(dy != 0)
				{
					dy -= ddy;
					mData.aabb = nbb;
				}
			}
			
			while(dx != 0)
			{
				int ddx = dx < 0 ? -1 : 1;
				AABB nbb = AABB.apply(mData.aabb, new Vec2(ddx, 0));
				
				for(AABB bb : mData.world.getTerrain())
				{
					if(nbb.intersects(bb))
					{
						mV.setX(0);
						mT.setX(0);
						dx = 0;
						
						break;
					}
				}
				
				if(dx != 0)
				{
					dx -= ddx;
					mData.aabb = nbb;
				}
			}
			
			for(Entity e : mData.world.getEntities())
			{
				if(e != getEntity() && (e instanceof PhysicalEntity) &&
					((PhysicalEntity) e).getData().aabb.intersects(mData.aabb))
				{
					((PhysicalEntity) getEntity()).collide((PhysicalEntity) e);
				}
			}
		}
		
		if(mData.aabb.origin().getY() > 160) getEntity().kill();
	}
	
	public static final int MAX_SPEED = 100;
	public static final int TIME_SCALE = 100;
}
