package dave.game.demo;

import dave.util.math.Vec2;

public class PhysicalEntity extends Entity
{
	private final DataComponent mData;
	private final PhysicsComponent mPhysics;
	
	public PhysicalEntity(World w, Sprite s, Vec2 p)
	{
		mData = new DataComponent(this, w);
		
		mData.sprite = s;
		mData.aabb = AABB.fromSize(p.getX(), p.getY(), s.getWidth(), s.getHeight());
		
		mPhysics = new PhysicsComponent(this, mData);
	}
	
	protected DataComponent getData( ) { return mData; }
	protected PhysicsComponent getPhysics( ) { return mPhysics; }
	
	protected void collide(PhysicalEntity e)
	{
	}
}
