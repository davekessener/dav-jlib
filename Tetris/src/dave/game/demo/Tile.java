package dave.game.demo;

import dave.game.IDisplay;
import dave.util.math.Vec2;

public class Tile
{
	private final byte[][] mData;
	
	public Tile(byte[][] d)
	{
		mData = d;
	}
	
	public int getWidth( ) { return mData[0].length; }
	public int getHeight( ) { return mData.length; }
	
	public void draw(IDisplay s, Vec2 v) { draw(s, v.getX(), v.getY()); }
	public void draw(IDisplay s, int ox, int oy)
	{
		for(int dy = 0 ; dy < getHeight() ; ++dy)
		{
			for(int dx = 0 ; dx < getWidth() ; ++dx)
			{
				int x = ox + dx, y = oy + dy;
				
				if(x >= 0 && y >= 0 && x < s.getWidth() && y < s.getHeight())
				{
					s.setPixel(x, y, mData[dy][dx]);
				}
			}
		}
	}
}
