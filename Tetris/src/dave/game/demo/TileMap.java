package dave.game.demo;

import java.util.ArrayList;
import java.util.List;

import dave.game.IDisplay;
import dave.util.Utils;
import dave.util.math.Vec2;

public class TileMap
{
	private final Tile[] mTileSet;
	private final byte[][] mMap;
	private final int mTileWidth, mTileHeight;
	
	public TileMap(byte[][] m, Tile[] ts)
	{
		mTileSet = ts;
		mMap = m;
		
		mTileWidth = ts[0].getWidth();
		mTileHeight = ts[0].getHeight();
		
		for(int i = 0 ; i < ts.length ; ++i)
		{
			if(ts[i].getWidth() != mTileWidth || ts[i].getHeight() != mTileHeight)
				throw new IllegalArgumentException();
		}
		
		Utils.forEach(mMap, (x, y, v) -> {
			if(v < 0 || v >= mTileSet.length)
				throw new IllegalArgumentException();
		});
	}
	
	public void draw(IDisplay s, Vec2 p)
	{
		int ox = p.getX(), oy = p.getY();
		
		for(int dy = Utils.max(0, oy / mTileHeight) ; dy < Utils.min(mMap.length, (oy + s.getHeight()) / mTileHeight + 1) ; ++dy)
		{
			for(int dx = Utils.max(0, ox / mTileWidth) ; dx < Utils.min(mMap[0].length, (ox + s.getWidth()) / mTileWidth + 1) ; ++dx)
			{
				int x = dx * mTileWidth - ox, y = dy * mTileHeight - oy;
				
				mTileSet[mMap[dy][dx]].draw(s, x, y);
			}
		}
	}

	public List<AABB> generateTerrain( )
	{
		List<AABB> r = new ArrayList<>();
		
		for(int i = 0 ; i < mMap.length ; ++i)
		{
			for(int j = 0 ; j < mMap[i].length ; ++j)
			{
				if(mMap[i][j] > 0)
				{
					int k = j + 1; while(k < mMap[i].length && mMap[i][k] > 0) ++k;
					
					r.add(AABB.fromCoords(j * mTileWidth, i * mTileHeight, k * mTileWidth, (i + 1) * mTileHeight));
					
					j = k - 1;
				}
			}
		}
		
		return r;
	}
}
