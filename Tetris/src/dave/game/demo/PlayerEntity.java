package dave.game.demo;

import dave.util.math.Vec2;

public class PlayerEntity extends PhysicalEntity
{
	public PlayerEntity(World w, Sprite s, Vec2 p)
	{
		super(w, s, p);
		
		new PlayerComponent(this, super.getData(), super.getPhysics());
	}
	
	@Override
	protected void collide(PhysicalEntity e)
	{
		if(e instanceof EnemyEntity)
		{
			if(!getPhysics().isOnGround() && getPhysics().getVelocity().getY() > 0)
			{
				e.kill();
			}
			else
			{
				kill();
			}
		}
	}
}
