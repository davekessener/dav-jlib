package dave.game.demo;

import dave.game.IDisplay;
import dave.util.math.Vec2;

public abstract class Component
{
	private final Entity mEntity;
	
	public Component(Entity e)
	{
		(mEntity = e).add(this);
	}
	
	protected Entity getEntity( )
	{
		return mEntity;
	}

	public void update( ) { }
	public void draw(IDisplay s, Vec2 p) { }
}
