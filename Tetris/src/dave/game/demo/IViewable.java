package dave.game.demo;

import dave.game.IDisplay;
import dave.util.math.Vec2;

public interface IViewable
{
	public abstract void draw(IDisplay screen, Vec2 o);
	public abstract int getWidth( );
	public abstract int getHeight( );
}
