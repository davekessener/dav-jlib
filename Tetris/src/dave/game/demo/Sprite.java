package dave.game.demo;

import dave.game.IDisplay;
import dave.util.math.Vec2;

public class Sprite implements IViewable
{
	private final byte[][] mData;
	
	public Sprite(byte[][] d)
	{
		mData = d;
	}

	@Override
	public void draw(IDisplay screen, Vec2 o)
	{
		int ox = o.getX(), oy = o.getY();
		
		for(int dy = 0 ; dy < getHeight() ; ++dy)
		{
			for(int dx = 0 ; dx < getWidth() ; ++dx)
			{
				int x = ox + dx, y = oy + dy;
				byte px = mData[dy][dx];
				
				if(px != -1 && x >= 0 && y >= 0 && x < screen.getWidth() && y < screen.getHeight())
				{
					screen.setPixel(x, y, px);
				}
			}
		}
	}

	@Override
	public int getWidth()
	{
		return mData[0].length;
	}

	@Override
	public int getHeight()
	{
		return mData.length;
	}
}
