package dave.game.demo;

import dave.util.math.Vec2;

public class AABB
{
	private final Vec2 mO, mE;
	
	private AABB(int x1, int y1, int x2, int y2)
	{
		mO = new Vec2(x1, y1);
		mE = new Vec2(x2, y2);
	}
	
	public Vec2 origin( ) { return mO; }
	public Vec2 end( ) { return mE; }
	public int width( ) { return mE.getX() - mO.getX(); }
	public int height( ) { return mE.getY() - mO.getY(); }
	
	public void shiftX(int dx) { mO.addX(dx); mE.addX(dx); }
	public void shiftY(int dy) { mO.addY(dy); mE.addY(dy); }
	public void shift(int dx, int dy) { shiftX(dx); shiftY(dy); }
	
	public boolean contains(Vec2 v)
	{
		return v.getX() >= mO.getX() && v.getX() < mE.getX() && v.getY() >= mO.getY() && v.getY() < mE.getY();
	}
	
	public boolean intersects(AABB aabb)
	{
		return !(mO.getX() >= aabb.mE.getX() || mO.getY() >= aabb.mE.getY() || aabb.mO.getX() >= mE.getX() || aabb.mO.getY() >= mE.getY());
	}
	
	@Override
	public String toString( )
	{
		return String.format("{(%d, %d) -> (%d, %d)}", mO.getX(), mO.getY(), mE.getX(), mE.getY());
	}
	
	public static AABB apply(AABB bb, Vec2 v)
	{
		return fromSize(bb.mO.getX() + v.getX(), bb.mO.getY() + v.getY(), bb.width(), bb.height());
	}
	
	public static AABB fromCoords(int x1, int y1, int x2, int y2)
	{
		if(x1 > x2) { x1 ^= x2; x2 ^= x1; x1 ^= x2; }
		if(y1 > y2) { y1 ^= y2; y2 ^= y1; y1 ^= y2; }
		
		if(x1 == x2 || y1 == y2)
			throw new IllegalArgumentException();
		
		return new AABB(x1, y1, x2, y2);
	}
	
	public static AABB fromSize(int x, int y, int w, int h)
	{
		if(w < 0) { w = -w; x -= w; }
		if(h < 0) { h = -h; y -= h; }
		
		if(w == 0 || h == 0)
			throw new IllegalArgumentException();
		
		return new AABB(x, y, x + w, y + h);
	}
}
