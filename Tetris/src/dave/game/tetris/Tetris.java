package dave.game.tetris;

import java.util.Random;

import dave.game.BasicEngine;
import dave.game.Key;
import dave.game.demo.Tile;
import dave.game.demo.TileMap;
import dave.game.IDisplay;
import dave.util.Utils;
import dave.util.math.Vec2;

public class Tetris extends BasicEngine
{
	private static final int WIDTH = 160;
	private static final int HEIGHT = 144;
	
	private final Board mBoard;
	private final Random mRand;
	private boolean mDirty, mAlive;
	private Piece mActive, mNext;
	private int mTimer, mPeriod;
	private int mLastButtons;
	
	public Tetris( ) { this(System.currentTimeMillis()); }
	public Tetris(long seed)
	{
		super(WIDTH, HEIGHT);
		
		mBoard = new Board(7 * 8, 0, 12, i -> {
			mPeriod -= i;
			if(mPeriod < 1) mPeriod = 1;
			addScore(i * 100);
		});
		
		mRand = new Random(seed);
		mDirty = false;
		mAlive = true;
		mPeriod = 5;
		mTimer = mPeriod;
		mLastButtons = calcButtons();
		next();
		next();
		
		TileMap m = new TileMap(Utils.makeMap(TILEMAP), new Tile[] {
			new Tile(Utils.makeMap(TILESET[0])),
			new Tile(Utils.makeMap(TILESET[1]))
		});
		
		m.draw(display(), new Vec2(0, 0));
		
		vsync();
	}
	
	private int calcButtons( )
	{
		int b = 0;
		
		for(int i = 0 ; i < Key.values().length ; ++i)
		{
			if(getKeyState(Key.values()[i])) b |= 1 << i;
		}
		
		return b;
	}
	
	private void next( )
	{
		mActive = mNext;
		mNext = new Piece(1 * 8, 1 * 8, Piece.BLOCKS[mRand.nextInt(Piece.BLOCKS.length)]);
		
		if(mActive != null)
		{
			mActive.update(10 * 8, -6 * 8);
		}
	}
	
	private boolean wasPressed(int b, Key k)
	{
		int bm = 1 << k.ordinal();
		
		return (mLastButtons & bm) == 0 && (b & bm) != 0;
	}
	
	@Override
	public void tick()
	{
		if(mAlive)
		{
			mDirty = true;
			
			if(--mTimer == 0 || getKeyState(Key.B))
			{
				mTimer = mPeriod;
				
				mActive.update(0, 1);
				
				if(mBoard.intersects(mActive) || mActive.getBoundingBox().end().getY() > HEIGHT - 8)
				{
					mActive.update(0, -1);
					
					if(mActive.getPosition().getY() <= 0)
					{
						mAlive = false;
					}
					else
					{
						mBoard.join(mActive);
						next();
						addScore(1);
					}
					
					return;
				}
			}

			int b = calcButtons();
			int dx = 0;
			
			if(wasPressed(b, Key.LEFT)) --dx;
			if(wasPressed(b, Key.RIGHT)) ++dx;
			
			if(dx != 0)
			{
				dx *= 8;
				
				if(dx < 0 && mActive.getBoundingBox().origin().getX() + dx < 7 * 8) dx = 0;
				if(dx > 0 && mActive.getBoundingBox().end().getX() + dx > WIDTH - 8) dx = 0;
				
				if(dx != 0)
				{
					mActive.update(dx, 0);
					
					if(mBoard.intersects(mActive))
					{
						mActive.update(-dx, 0);
					}
				}
			}
			
			if(wasPressed(b, Key.A))
			{
				mActive.rotateRight();
				
				int ox = mActive.getBoundingBox().origin().getX();
				int ex = mActive.getBoundingBox().end().getX();
				int ey = mActive.getBoundingBox().end().getY();
				
				if(mBoard.intersects(mActive) || ox <= 7 * 8 || ex >= WIDTH - 8 || ey > HEIGHT - 8)
				{
					mActive.rotateLeft();
				}
			}
			
			mLastButtons = b;
		}
	}

	@Override
	public void draw()
	{
		IDisplay s = super.display();
		
		if(mDirty)
		{
			for(int y = 0 ; y < HEIGHT - 8 ; ++y)
			{
				for(int x = 7 * 8 ; x < WIDTH - 8 ; ++x)
				{
					s.setPixel(x, y, (byte) 3);
				}
			}

			for(int y = 1 * 8 ; y < 6 * 8 ; ++y)
			{
				for(int x = 1 * 8 ; x < 6 * 8 ; ++x)
				{
					s.setPixel(x, y, (byte) 3);
				}
			}
			
			mBoard.draw(s);
			mActive.draw(s);
			mNext.draw(s);
			
			vsync();
			
			mDirty = false;
		}
	}

	@Override
	public boolean isAlive()
	{
		return mAlive;
	}

	@Override
	public boolean isIdle()
	{
		return !(getKeyState(Key.LEFT) || getKeyState(Key.RIGHT));
	}
	
	private static final String[][] TILESET = {
		{
			"33333333",
			"33333333",
			"33333333",
			"33333333",
			"33333333",
			"33333333",
			"33333333",
			"33333333"
		},
		{
			"11221122",
			"22112211",
			"11221122",
			"22112211",
			"11221122",
			"22112211",
			"11221122",
			"22112211"
		},
	};
	
	private static final String[] TILEMAP = {
		"11111110000000000001",
		"10000010000000000001",
		"10000010000000000001",
		"10000010000000000001",
		"10000010000000000001",
		"10000010000000000001",
		"11111110000000000001",
		"11111110000000000001",
		"11111110000000000001",
		"11111110000000000001",
		"11111110000000000001",
		"11111110000000000001",
		"11111110000000000001",
		"11111110000000000001",
		"11111110000000000001",
		"10000010000000000001",
		"10000010000000000001",
		"11111111111111111111"
	};
}
