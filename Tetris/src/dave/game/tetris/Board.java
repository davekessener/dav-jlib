package dave.game.tetris;

import java.util.ArrayList;
import java.util.List;

import dave.game.demo.AABB;
import dave.util.math.Vec2;

public class Board extends Piece
{
	public static interface Callback { public abstract void call(int rows); }
	
	private final Callback mF;
	private final int mWidth;
	
	public Board(int x, int y, int w, Callback cb)
	{
		super(x, y, new AABB[] {});
		
		mWidth = w;
		mF = cb;
	}
	
	@Override
	public void join(Piece p)
	{
		super.join(p);
		
		List<int[]> board = new ArrayList<>();
		AABB[] aabb = super.getBB();
		
		for(int i = 0 ; i < aabb.length ; ++i)
		{
			Vec2 v = aabb[i].origin();
			int x = v.getX() / 8, y = v.getY() / 8;
			
			while(y >= board.size())
			{
				board.add(new int[mWidth]);
			}
			
			board.get(y)[x] = 1;
		}

		List<AABB> updated = new ArrayList<>();
		int deleted = 0;
		for(int y = board.size() - 1 ; y >= 0 ; --y)
		{
			int[] row = board.get(y);
			boolean delete = true;
			
			if(row == null) continue;
			
			for(int x = 0 ; x < row.length ; ++x)
			{
				if(row[x] != 1)
				{
					delete = false;
					break;
				}
			}
			
			if(delete)
			{
				board.remove(y);
				++deleted;
			}
			else
			{
				for(int x = 0 ; x < row.length ; ++x)
				{
					if(row[x] == 1)
					{
						updated.add(AABB.fromSize(x * 8, (y + deleted) * 8, 8, 8));
					}
				}
			}
		}
		
		super.setBB(updated.toArray(new AABB[updated.size()]));
		
		mF.call(deleted);
	}
}
