package dave.game.tetris;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import dave.game.demo.AABB;
import dave.game.demo.Tile;
import dave.game.IDisplay;
import dave.util.Utils;
import dave.util.math.Mat2x2;
import dave.util.math.Vec2;

public class Piece
{
	private final Vec2 mPos;
	private AABB[] mAABB;
	private AABB mBB;
	
	public Piece(int x, int y, AABB[] aabb)
	{
		mPos = new Vec2(x, y);
		mAABB = Arrays.copyOf(aabb, aabb.length);
		mBB = find();
	}
	
	public Vec2 getPosition( ) { return mPos; }
	public AABB getBoundingBox( ) { return AABB.apply(mBB, mPos); }
	
	protected AABB[] getBB( ) { return mAABB; }
	protected void setBB(AABB[] aabb) { mAABB = aabb; }
	
	public void update(int dx, int dy)
	{
		mPos.add(new Vec2(dx, dy));
	}
	
	public void draw(IDisplay s)
	{
		for(int i = 0 ; i < mAABB.length ; ++i)
		{
			AABB aabb = AABB.apply(mAABB[i], mPos);
			
			BLOCK.draw(s, aabb.origin());
		}
	}
	
	public boolean intersects(Piece p)
	{
		for(int i = 0 ; i < mAABB.length ; ++i)
		{
			for(int j = 0 ; j < p.mAABB.length ; ++j)
			{
				if(AABB.apply(mAABB[i], mPos).intersects(AABB.apply(p.mAABB[j], p.mPos)))
				{
					return true;
				}
			}
		}
		
		return false;
	}
	
	public void join(Piece p)
	{
		int l = mAABB.length;
		
		mAABB = Arrays.copyOf(mAABB, l + p.mAABB.length);
		
		for(int i = 0 ; i < p.mAABB.length ; ++i)
		{
			mAABB[l + i] = AABB.apply(p.mAABB[i], Vec2.sub(p.mPos, mPos));
		}
		
		mBB = find();
	}
	
	public void rotateLeft( )
	{
		rotate(new Mat2x2(new int[][] {
			{ 0, 1 }, { -1, 0 }
		}), new Vec2(0, 40));
	}
	
	public void rotateRight( )
	{
		rotate(new Mat2x2(new int[][] {
			{ 0, -1 }, { 1, 0 }
		}), new Vec2(40, 0));
	}
	
	private void rotate(Mat2x2 m, Vec2 t)
	{
		List<AABB> aabb = new ArrayList<>();
		
		for(int i = 0 ; i < mAABB.length ; ++i)
		{
			Vec2 o = m.apply(mAABB[i].origin());
			Vec2 e = m.apply(mAABB[i].end());
			AABB v = AABB.fromCoords(o.getX(), o.getY(), e.getX(), e.getY());
			
			aabb.add(AABB.apply(v, t));
		}
		
		mAABB = aabb.toArray(new AABB[aabb.size()]);
		mBB = find();
	}
	
	private AABB find( )
	{
		int x1 = Integer.MAX_VALUE, y1 = Integer.MAX_VALUE, x2 = Integer.MIN_VALUE, y2 = Integer.MIN_VALUE;
		
		for(int i = 0 ; i < mAABB.length ; ++i)
		{
			if(mAABB[i].origin().getX() < x1) x1 = mAABB[i].origin().getX();
			if(mAABB[i].origin().getY() < y1) y1 = mAABB[i].origin().getY();
			if(mAABB[i].end().getX() > x2) x2 = mAABB[i].end().getX();
			if(mAABB[i].end().getY() > y2) y2 = mAABB[i].end().getY();
		}
		
		return AABB.fromCoords(x1, y1, x2, y2);
	}
	
	private static final Tile BLOCK = new Tile(Utils.makeMap(new String[] {
		"10000001",
		"01111110",
		"01232320",
		"01323230",
		"01232320",
		"01323230",
		"01232320",
		"10000001"
	}));
	
	private static final AABB[] PARTS;
	
	static
	{
		List<AABB> parts = new ArrayList<>();
		
		for(int y = 0 ; y < 5 ; ++y)
		{
			for(int x = 0 ; x < 5 ; ++x)
			{
				parts.add(AABB.fromSize(x * 8, y * 8, 8, 8));
			}
		}
		
		PARTS = parts.toArray(new AABB[parts.size()]);
	}
	
	public static final AABB[] BLOCK_2x2 = new AABB[] { PARTS[1 + 1 * 5], PARTS[2 + 1 * 5], PARTS[1 + 2 * 5], PARTS[2 + 2 * 5] };
	public static final AABB[] BLOCK_5x1 = new AABB[] { PARTS[0 + 2 * 5], PARTS[1 + 2 * 5], PARTS[2 + 2 * 5], PARTS[3 + 2 * 5], PARTS[4 + 2 * 5] };
	public static final AABB[] BLOCK_3x2 = new AABB[] { PARTS[2 + 1 * 5], PARTS[1 + 2 * 5], PARTS[2 + 2 * 5], PARTS[3 + 2 * 5] };
	public static final AABB[] BLOCK_S1  = new AABB[] { PARTS[0 + 1 * 5], PARTS[1 + 1 * 5], PARTS[1 + 2 * 5], PARTS[2 + 2 * 5] };
	public static final AABB[] BLOCK_S2  = new AABB[] { PARTS[0 + 2 * 5], PARTS[1 + 2 * 5], PARTS[1 + 1 * 5], PARTS[2 + 1 * 5] };
	
	public static final AABB[][] BLOCKS = new AABB[][] { BLOCK_2x2, BLOCK_5x1, BLOCK_3x2, BLOCK_S1, BLOCK_S2 };
}
