package dave.web;

import java.util.function.Function;

public class WebPage implements WebHandler
{
	private final Function<WebRequest, WebResponse> mCallback;
	
	public WebPage(Function<WebRequest, WebResponse> f)
	{
		mCallback = f;
	}

	@Override
	public WebResponse get(WebRequest r)
	{
		return mCallback.apply(r);
	}
}
