package dave.web.jetty;

import java.io.IOException;
import java.util.function.Function;

import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;

import dave.web.WebResponse;
import dave.web.WebRequestInfo;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class JettyHandler extends AbstractHandler
{
	private final Function<WebRequestInfo, WebResponse> mCallback;
	
	public JettyHandler(Function<WebRequestInfo, WebResponse> f)
	{
		mCallback = f;
	}

	@Override
	public void handle(String path, Request req, HttpServletRequest httpreq, HttpServletResponse r)
			throws IOException, ServletException
	{
		WebRequestInfo info = new WebRequestInfo(path);
		WebResponse wr = mCallback.apply(info);
		
		r.setStatus(wr.status.code);
		r.setContentType(wr.type.http_type);
		r.getWriter().println(wr.response);
		
		req.setHandled(true);
	}
}
