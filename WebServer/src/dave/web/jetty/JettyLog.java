package dave.web.jetty;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.function.Consumer;

import org.eclipse.jetty.logging.JettyLogger;
import org.eclipse.jetty.logging.StdErrAppender;
import org.slf4j.LoggerFactory;

public class JettyLog
{
	private final PrintStream mOut;
	
	public JettyLog(PrintStream out)
	{
		mOut = out;
	}
	
	public JettyLog(Consumer<String> cb)
	{
		mOut = new PrintStream(new StreamProxy(cb), true, CHARSET);
	}
	
	public void install()
	{
		JettyLogger log = (JettyLogger) LoggerFactory.getLogger("org.eclipse.jetty");
		
		StdErrAppender append = (StdErrAppender) log.getAppender();
		
		append.setStream(mOut);
	}
	
	private static class StreamProxy extends OutputStream
	{
		private final Consumer<String> mCallback;
		private final ByteArrayOutputStream mBuffer;
		
		public StreamProxy(Consumer<String> f)
		{
			mCallback = f;
			mBuffer = new ByteArrayOutputStream();
		}
		
		@Override
		public void write(int b) throws IOException
		{
			if(b == '\n')
			{
				mCallback.accept(new String(mBuffer.toByteArray(), CHARSET));
				mBuffer.reset();
			}
			else
			{
				mBuffer.write(b);
			}
		}
	}
	
	private static final Charset CHARSET = StandardCharsets.UTF_8;
}
