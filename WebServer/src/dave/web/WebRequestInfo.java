package dave.web;

public class WebRequestInfo
{
	public final String path;
	
	public WebRequestInfo(String path)
	{
		path = path.toLowerCase();
		path = path.strip();
		if(path.startsWith("/")) path = path.substring(1);
		if(path.endsWith("/")) path = path.substring(0, path.length() - 1);
		path = path.strip();
		
		this.path = path;
	}
}
