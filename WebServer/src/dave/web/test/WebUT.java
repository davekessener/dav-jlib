package dave.web.test;

import static org.junit.Assert.assertEquals;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.eclipse.jetty.server.Server;
import org.junit.BeforeClass;
import org.junit.Test;

import dave.web.ResponseStatus;
import dave.web.ResponseType;
import dave.web.WebResponse;
import dave.web.WebServer;
import dave.web.WebUtils;
import dave.web.WebWildcard;
import dave.web.jetty.JettyLog;
import dave.web.jetty.TestHandler;

public class WebUT
{
	@BeforeClass
	public static void setup()
	{
		(new JettyLog(s -> {})).install();
	}
	
	@Test
	public void testServer() throws Exception
	{
		Server server = new Server(9000);
		
		server.setHandler(new TestHandler());
		
		try
		{
			server.start();
			
			assertEquals("<h1>Hello, World!</h1>", get("127.0.0.1:9000"));
		}
		finally
		{
			server.stop();
			server.join();
		}
	}
	
	@Test
	public void testFramework() throws MalformedURLException, IOException
	{
		WebServer server = new WebServer();
		
		server.port(8081);
		
		server.registerHandler("page1", r -> new WebResponse(ResponseType.HTML, ResponseStatus.OK, "abc123"));
		
		server.registerHandler("page2", new WebWildcard("id", r -> new WebResponse(ResponseType.HTML, ResponseStatus.OK, "id=" + r.wildcards.get("id"))));
		
		try
		{
			server.start();
			
			assertEquals("abc123", get("127.0.0.1:8081/page1"));
			assertEquals("id=17", get("127.0.0.1:8081/page2/17"));
		}
		finally
		{
			server.stop();
		}
	}
	
	@Test
	public void testHTMLEscape()
	{
		assertEquals("this &amp; that", WebUtils.escapeHTML("this & that"));
		assertEquals("1 &lt; 3 &lt; 4", WebUtils.escapeHTML("1 < 3 < 4"));
		assertEquals("is 1 &lt; 3 &amp;&amp; 4 &gt;&gt; 3 &lt; 1?", WebUtils.escapeHTML("is 1 < 3 && 4 >> 3 < 1?"));
		assertEquals("is 1 < 3 && 4 >> 3 < 1?", WebUtils.unescapeHTML(WebUtils.escapeHTML("is 1 < 3 && 4 >> 3 < 1?")));
	}
	
	private static String get(String url) throws MalformedURLException, IOException
	{
		StringBuilder sb = new StringBuilder();
		HttpURLConnection c = (HttpURLConnection) (new URL("http://" + url)).openConnection();
		
		try (BufferedReader in = new BufferedReader(new InputStreamReader(c.getInputStream())))
		{
			for(String line = null ; (line = in.readLine()) != null ;)
			{
				sb.append(line);
			}
		}
		
		return sb.toString();
	}
}
