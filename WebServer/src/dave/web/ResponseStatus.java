package dave.web;

public enum ResponseStatus
{
	OK(200),
	NOT_FOUND(404),
	INTERNAL_SERVER_ERROR(500);
	
	public final int code;
	
	private ResponseStatus(int code)
	{
		this.code = code;
	}
}
