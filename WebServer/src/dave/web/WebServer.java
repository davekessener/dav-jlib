package dave.web;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Stream;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;

import dave.util.Holder;
import dave.util.SevereException;
import dave.util.stream.StreamUtils;
import dave.web.html.HtmlBuilder;
import dave.web.jetty.JettyHandler;
import dave.web.jetty.JettyLog;

public class WebServer
{
	private final JettyLog mJettyLog;
	private final Holder<Consumer<String>> mLogCB;
	private final Holder<Function<WebRequest, WebResponse>> mOn404;
	private final Holder<Consumer<Throwable>> mOnErr;
	private final WebPath mPath;
	private final Server mServer;
	private int mPort;
	private String mListenAddr;
	
	public WebServer()
	{
		mJettyLog = new JettyLog(this::onLog);
		mLogCB = new Holder<>(null);
		mOn404 = new Holder<>(WebServer::default404Page);
		mOnErr = new Holder<>(null);
		mPath = new WebPath();
		mServer = new Server();
		
		mPort = DEFAULT_PORT;
		mListenAddr = "0.0.0.0";
	}
	
	public void setLogSink(Consumer<String> f) { mLogCB.value = f; }
	public void setErrorHandler(Consumer<Throwable> f) { mOnErr.value = f; }
	
	public int port() { return mPort; }
	public void port(int p) { mPort = p; }
	public String host() { return mListenAddr; }
	public void host(String a) { mListenAddr = a; }
	
	public void registerHandler(String key, WebHandler s)
	{
		mPath.put(key, s);
	}
	
	public void indexHandler(WebHandler s)
	{
		mPath.setDefault(s);
	}
	
	public void start()
	{
		mJettyLog.install();
		
		Optional<String> err = mPath.paths().filter(p ->
			!StreamUtils.isUnique(Stream.of(p.split("/"))
				.filter(e -> e.startsWith(":"))))
			.findAny();
		
		if(err.isPresent())
		{
			throw new IllegalStateException("Duplicate wildcard ID in path " + err.get());
		}
		
		mServer.setHandler(new JettyHandler(this::onRequest));
		
		ServerConnector c = new ServerConnector(mServer);
		c.setPort(mPort);
		c.setHost(mListenAddr);
		mServer.addConnector(c);
		
		try
		{
			mServer.start();
		}
		catch(Exception e)
		{
			throw new SevereException(e);
		}
	}
	
	public void stop()
	{
		try
		{
			mServer.stop();
			mServer.join();
		}
		catch(Exception e)
		{
			throw new SevereException(e);
		}
	}
	
	private WebResponse onRequest(WebRequestInfo info)
	{
		WebRequest wr = new WebRequest(info);
		WebResponse r = null;
		
		try
		{
			r = mPath.get(wr);
			
			if(r == null)
			{
				r = mOn404.value.apply(wr);
			}
			
			if(r == null)
			{
				throw new NullPointerException("No handler!");
			}
		}
		catch(Throwable e)
		{
			Consumer<Throwable> cb = mOnErr.value;
			
			if(cb != null)
			{
				cb.accept(e);
			}
			
			r = defaultErrorPage(wr, e);
		}
		
		return r;
	}
	
	private void onLog(String s)
	{
		Consumer<String> cb = mLogCB.value;
		
		if(cb != null)
		{
			cb.accept(s);
		}
	}

	public static WebResponse default404Page(WebRequest r)
	{
		return new WebResponse(ResponseType.HTML, ResponseStatus.NOT_FOUND, new HtmlBuilder()
				.push("html")
					.push("header")
						.push("title")
							.text("Error 404 Not Found")
						.pop()
					.pop()
					.push("body")
						.push("h1")
							.text("Path \"" + r.info.path + "\" not found!")
						.pop()
					.pop()
				.pop()
			.generate());
	}
	
	public static WebResponse defaultErrorPage(WebRequest r, Throwable e)
	{
		StringWriter sw = new StringWriter();
		
		e.printStackTrace(new PrintWriter(sw));
		
		return new WebResponse(ResponseType.HTML, ResponseStatus.INTERNAL_SERVER_ERROR, new HtmlBuilder()
				.push("html")
					.push("header")
						.push("title")
							.text("Error 500 Internal Server Error")
						.pop()
					.pop()
					.push("body")
						.push("p")
							.text(sw.toString())
						.pop()
					.pop()
				.pop()
			.generate());
	}
	
	public static final int DEFAULT_PORT = 9000;
}
