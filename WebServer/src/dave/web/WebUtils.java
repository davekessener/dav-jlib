package dave.web;

public final class WebUtils
{
	public static String escapeHTML(String text)
	{
		return text
			.replaceAll("&", "&amp;")
			.replaceAll("<", "&lt;")
			.replaceAll(">", "&gt;")
			.replaceAll("\n", "<br />")
		;
	}
	
	public static String unescapeHTML(String text)
	{
		return text
			.replaceAll("<br />", "\n")
			.replaceAll("&gt;", ">")
			.replaceAll("&lt;", "<")
			.replaceAll("&amp;", "&")
		;
	}
	
	private WebUtils( ) { }
}
