package dave.web;

import java.util.stream.Stream;

public class WebWildcard implements WebHandler
{
	private final String mName;
	private final WebHandler mSegment;
	
	public WebWildcard(String id, WebHandler s)
	{
		mName = id.toLowerCase();
		mSegment = s;
	}

	@Override
	public WebResponse get(WebRequest r)
	{
		r.add(mName);
		
		return mSegment.get(r);
	}
	
	@Override
	public Stream<String> paths()
	{
		return mSegment.paths().map(p -> ":" + mName + "/" + p);
	}
}
