package dave.web;

import java.util.function.BiFunction;

public class WebWrapper implements WebHandler
{
	private final WebHandler mSuper;
	private final BiFunction<WebResponse, WebRequest, WebResponse> mCallback;
	
	public WebWrapper(WebHandler s, BiFunction<WebResponse, WebRequest, WebResponse> f)
	{
		mSuper = s;
		mCallback = f;
	}

	@Override
	public WebResponse get(WebRequest r)
	{
		return mCallback.apply(mSuper.get(r), r);
	}
}
