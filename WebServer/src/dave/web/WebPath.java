package dave.web;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

import dave.util.stream.StreamUtils;

public class WebPath implements WebHandler
{
	private final Map<String, WebHandler> mChildren = new HashMap<>();
	private WebHandler mDefault = null;
	
	public void setDefault(WebHandler d) { mDefault = d; }

	@Override
	public WebResponse get(WebRequest r)
	{
		WebHandler s = r.done() ? mDefault : mChildren.get(r.next());
		
		if(s == null) return null;
		
		r.add();
		
		return s.get(r);
	}

	@Override
	public WebPath put(String key, WebHandler s)
	{
		mChildren.put(key.toLowerCase(), s);
		
		return this;
	}
	
	@Override
	public Stream<String> paths()
	{
		return StreamUtils.join(mChildren.entrySet().stream()
				.flatMap(e -> e.getValue().paths().map(p -> e.getKey() + "/" + p)),
			Stream.of(mDefault).filter(s -> s != null).flatMap(s -> s.paths()));
	}
}
