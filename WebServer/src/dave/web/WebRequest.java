package dave.web;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WebRequest
{
	public final List<Segment> segments;
	public final Map<String, String> wildcards;
	public final WebRequestInfo info;
	
	public WebRequest(WebRequestInfo info)
	{
		this.info = info;
		
		this.segments = new ArrayList<>();
		this.wildcards = new HashMap<>();
	}
	
	public WebRequest(List<Segment> segments, Map<String, String> wildcards, WebRequestInfo info)
	{
		this.segments = Collections.unmodifiableList(segments);
		this.wildcards = Collections.unmodifiableMap(wildcards);
		this.info = info;
	}
	
	public WebRequest finalizeRequest()
	{
		return new WebRequest(segments, wildcards, info);
	}
	
	public String next()
	{
		String[] p = info.path.split("/");
		
		return (segments.size() < p.length) ? p[segments.size()] : null;
	}
	
	public void add() { segments.add(new Segment(segments.size(), next(), null)); }
	
	public void add(String key)
	{
		String v = next();
		
		wildcards.put(key, v);
		segments.add(new Segment(segments.size(), key, v));
	}
	
	public boolean done() { return segments.size() == info.path.split("/").length; }
	
	public static final class Segment
	{
		public final int position;
		public final String key;
		public final String value;
		
		public Segment(int position, String key, String value)
		{
			this.position = position;
			this.key = key;
			this.value = value;
		}
	}
}
