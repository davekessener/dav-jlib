package dave.web.html;

public class HtmlBuilder
{
	private final HtmlTag mTag;
	private final HtmlBuilder mSuper;
	
	public HtmlBuilder()
	{
		mTag = new HtmlTag("");
		mSuper = null;
	}
	
	private HtmlBuilder(String tag, HtmlBuilder s)
	{
		mTag = new HtmlTag(tag);
		mSuper = s;
		
		mSuper.mTag.children().add(mTag);
	}
	
	public HtmlBuilder push(String tag)
	{
		return new HtmlBuilder(tag, this);
	}
	
	public HtmlBuilder pop()
	{
		return mSuper;
	}
	
	public HtmlBuilder set(String key, String v)
	{
		mTag.attributes().put(key, v);
		
		return this;
	}
	
	public HtmlBuilder text(String txt)
	{
		mTag.children().add(new TextElement(txt));
		
		return this;
	}
	
	public String generate()
	{
		return "<!DOCTYPE html>\n" + mTag.children().get(0).generate();
	}
}
