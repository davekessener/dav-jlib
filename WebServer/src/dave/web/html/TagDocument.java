package dave.web.html;

public class TagDocument extends HtmlTag
{
	private final TagHeader mHeader;
	private final TagBody mBody;
	
	public TagDocument()
	{
		super("html");
		
		mHeader = new TagHeader();
		mBody = new TagBody();
		
		children().add(mHeader);
		children().add(mBody);
	}
	
	public TagHeader header() { return mHeader; }
	public TagBody body() { return mBody; }
	
	@Override
	public String generate()
	{
		return "<!DOCTYPE html>\n" + super.generate();
	}
}
