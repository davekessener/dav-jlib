package dave.web.html;

import dave.web.WebUtils;

public class TextElement implements HtmlElement
{
	private String mContent;
	
	public TextElement()
	{
		mContent = null;
	}
	
	public TextElement(String txt)
	{
		this();
		
		setContent(txt);
	}
	
	public String getContent() { return mContent; }
	public TextElement setContent(String c)
	{
		mContent = c;
		
		return this;
	}

	@Override
	public String generate()
	{
		return WebUtils.escapeHTML("" + mContent);
	}
}
