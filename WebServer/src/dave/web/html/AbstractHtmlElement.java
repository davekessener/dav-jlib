package dave.web.html;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import dave.web.WebUtils;

public abstract class AbstractHtmlElement implements HtmlElement
{
	private final String mKey;
	private final List<HtmlElement> mChildren;
	private final Map<String, String> mAttributes;
	
	protected AbstractHtmlElement(String key)
	{
		mKey = key;
		mChildren = new ArrayList<>();
		mAttributes = new HashMap<>();
	}
	
	public List<HtmlElement> children() { return mChildren; }
	public Map<String, String> attributes() { return mAttributes; }
	
	@Override
	public String generate()
	{
		StringBuilder sb = new StringBuilder();
		
		sb.append("<").append(mKey);
		
		if(!mAttributes.isEmpty())
		{
			mAttributes.entrySet().forEach(e -> sb
				.append(" ")
				.append(e.getKey())
				.append("=\"")
				.append(WebUtils.escapeHTML(e.getValue()))
				.append("\""));
		}
		
		if(mChildren.isEmpty())
		{
			sb.append(" />");
		}
		else
		{
			sb.append(">");
			
			mChildren.forEach(c -> sb.append("\n  ").append(c.generate()));
			
			sb.append("\n</").append(mKey).append(">");
		}
		
		return sb.toString();
	}
}
