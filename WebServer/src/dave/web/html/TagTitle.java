package dave.web.html;

public class TagTitle extends HtmlTag
{
	private String mTitle;
	
	public TagTitle()
	{
		super("title");
		
		mTitle = null;
	}
	
	public String getTitle() { return mTitle; }
	
	public TagTitle setTitle(String t)
	{
		t = t.strip();
		t = (t.isBlank() ? null : t);
		
		mTitle = t;
		
		children().clear();
		children().add(new TextElement().setContent(mTitle));
		
		return this;
	}
}
