package dave.web.html;

public class TagHeader extends HtmlTag
{
	private final TagTitle mTitle;
	
	public TagHeader()
	{
		super("header");
		
		mTitle = new TagTitle();
		
		children().add(mTitle);
	}
	
	public TagTitle title() { return mTitle; }
}
