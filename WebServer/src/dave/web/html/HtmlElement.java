package dave.web.html;

public interface HtmlElement
{
	public abstract String generate( );
}
