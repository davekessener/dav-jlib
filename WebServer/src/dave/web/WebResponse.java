package dave.web;

public class WebResponse
{
	public final ResponseType type;
	public final ResponseStatus status;
	public final String response;
	
	public WebResponse(ResponseType type, ResponseStatus status, String response)
	{
		this.type = type;
		this.status = status;
		this.response = response;
	}
}
