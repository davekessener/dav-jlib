package dave.web;

public enum ResponseType
{
	HTML("text/html"),
	JSON("text/json");
	
	public final String http_type;
	
	private ResponseType(String http_type)
	{
		this.http_type = http_type;
	}
}
