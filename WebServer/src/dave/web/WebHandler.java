package dave.web;

import java.util.stream.Stream;

public interface WebHandler
{
	public abstract WebResponse get(WebRequest r);
	
	public default WebHandler put(String key, WebHandler s) { throw new UnsupportedOperationException(); }
	
	public default Stream<String> paths( ) { return Stream.empty(); }
}
