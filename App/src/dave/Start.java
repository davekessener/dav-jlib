package dave;

import dave.app.MainApp;
import dave.game.Controller;
import dave.game.IEngine;
import dave.game.demo.Demo;

public class Start
{
	public static void main(String[] args)
	{
		IEngine game = new Demo();
		Controller ctrl = new Controller(game, 10, null);
		MainApp app = new MainApp(game);
		
		app.setOnKey(e -> game.setKeyState(e.getKey(), e.isPressed()));
		
		ctrl.start();
		
		app.run();
		
		ctrl.stop();
		
		System.out.println("Goodbye!");
	}
}
