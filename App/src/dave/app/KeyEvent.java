package dave.app;

import dave.game.Key;

public class KeyEvent implements IEvent
{
	private final Key mKey;
	private final boolean mState;
	
	public KeyEvent(Key k, boolean p)
	{
		mKey = k;
		mState = p;
	}
	
	public Key getKey( )
	{
		return mKey;
	}
	
	public boolean isPressed( )
	{
		return mState;
	}
}
