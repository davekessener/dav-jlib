package dave.app.ui;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class MainUI
{
	private final JFrame mWindow;
	private final JPanel mContent;
	
	public MainUI(JPanel content)
	{
		mWindow = new JFrame("Sample text");
		mContent = content;
		
		mWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		mWindow.add(content);
		mWindow.pack();
		
		mWindow.setLocationRelativeTo(null);
	}
	
	public JFrame getUI( )
	{
		return mWindow;
	}
	
	public void update( )
	{
		mContent.repaint();
	}
}
