package dave.app.ui;

import java.awt.Color;
import java.util.Arrays;
import java.util.List;

public class Pallet
{
	private final List<Color> mColors;
	
	public Pallet(Color ... colors)
	{
		mColors = Arrays.asList(colors);
	}
	
	public Color getColor(byte px)
	{
		if(px >= mColors.size())
			throw new IllegalArgumentException();
		
		return mColors.get(px);
	}
}
