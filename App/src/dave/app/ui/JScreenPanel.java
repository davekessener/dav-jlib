package dave.app.ui;

import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.JPanel;

import dave.game.IScreen;
import dave.game.ObservableScreen;

public class JScreenPanel extends JPanel
{
	private static final long serialVersionUID = 2692441612883113347L;
	
	private final ObservableScreen mScreen;
	private final Pallet mPallet;
	private final int mScale;
	
	public JScreenPanel(Pallet p, IScreen s) { this(p, s, 1); }
	public JScreenPanel(Pallet p, IScreen s, int zoom)
	{
		mScreen = new ObservableScreen(s);
		mPallet = p;
		mScale = zoom;
	}
	
	@Override
	public Dimension getPreferredSize( )
	{
		return new Dimension(mScreen.getWidth() * mScale, mScreen.getHeight() * mScale);
	}
	
	@Override
	public void paintComponent(Graphics g)
	{
		mScreen.update((x, y, px) -> {
			g.setColor(mPallet.getColor(px));
			
			g.fillRect(x * mScale, y * mScale, mScale, mScale);
		});
	}
}
