package dave.app;

import java.awt.Color;
import java.awt.event.KeyAdapter;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.SwingUtilities;

import dave.app.ui.JScreenPanel;
import dave.app.ui.MainUI;
import dave.app.ui.Pallet;
import dave.game.IEngine;
import dave.game.Key;

public class MainApp
{
	private final IEngine mGame;
	private EventHandler<KeyEvent> mOnKey;
	private MainUI mUI;
	private State mState;
	
	public MainApp(IEngine s)
	{
		mGame = s;
		mState = State.INIT;
	}
	
	public void setOnKey(EventHandler<KeyEvent> h)
	{
		mOnKey = h;
	}
	
	public void run( )
	{
		System.out.println("Starting interactive mode ...");
		
		SwingUtilities.invokeLater(() -> {
			mUI = new MainUI(new JScreenPanel(new Pallet(COLORS), mGame.getScreen(), 4));
			
			mUI.getUI().addWindowListener(new WindowAdapter() {
				@Override
				public void windowClosing(WindowEvent e)
				{
					System.out.println("Shutting down ...");
					mState = State.DONE;
				}
			});
			
			mUI.getUI().addKeyListener(new ManualControl());
			
			mUI.getUI().setVisible(true);
			mState = State.RUNNING;
		});
		
		while(mState != State.DONE)
		{
			try { Thread.sleep(10); } catch (InterruptedException e) { }
			
			if(mState == State.RUNNING)
			{
				mUI.getUI().setTitle(String.format("The Game: %d", mGame.score()));
				mUI.update();
			}
		}
	}
	
	private static enum State
	{
		INIT,
		RUNNING,
		DONE
	}
	
	private class ManualControl extends KeyAdapter
	{
		private Key getKey(char c)
		{
			switch(c)
			{
				case 'l': return Key.A;
				case 'p': return Key.B;
				case 'k': return Key.START;
				case 'o': return Key.SELECT;
				case 'w': return Key.UP;
				case 'a': return Key.LEFT;
				case 's': return Key.DOWN;
				case 'd': return Key.RIGHT;
			}
			
			return null;
		}
		
		@Override
		public void keyPressed(java.awt.event.KeyEvent e)
		{
			if(mOnKey != null)
			{
				Key k = getKey(e.getKeyChar());
				
				if(k != null)
				{
					mOnKey.process(new KeyEvent(k, true));
				}
			}
		}
		
		@Override
		public void keyReleased(java.awt.event.KeyEvent e)
		{
			if(mOnKey != null)
			{
				Key k = getKey(e.getKeyChar());
				
				if(k != null)
				{
					mOnKey.process(new KeyEvent(k, false));
				}
			}
		}
	}
	
	private static final Color[] COLORS = new Color[] { Color.BLACK, Color.DARK_GRAY, Color.LIGHT_GRAY, Color.WHITE };
}
