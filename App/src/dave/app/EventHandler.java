package dave.app;

public interface EventHandler<E extends IEvent>
{
	public abstract void process(E e);
}
