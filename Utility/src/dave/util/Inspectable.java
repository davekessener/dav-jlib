package dave.util;

public interface Inspectable
{
	public default String inspect( ) { return toString(); }
}
