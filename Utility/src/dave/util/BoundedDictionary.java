package dave.util;

public class BoundedDictionary<K, V> extends DirtyObject implements Dictionary<K, V>
{
	public class Entry
	{
		public final K key;
		public final V value;
		public Entry next, previous;
		
		public Entry(K key, V value)
		{
			this.key = key;
			this.value = value;
		}
	}
	
	private Entry mRoot;
	private int mCapacity;
	
	public BoundedDictionary(int c)
	{
		mRoot = new Entry(null, null);
		mRoot.previous = mRoot.next = mRoot;
		mCapacity = c;
	}
	
	@Override
	public V put(K k, V v)
	{
		use();
		
		V r = remove(k);
		
		Entry e = new Entry(k, v);
		
		e.next = mRoot.next;
		e.previous = mRoot;
		
		mRoot.next.previous = e;
		mRoot.next = e;
		
		if(mCapacity == 0)
		{
			mRoot.previous = mRoot.previous.previous;
			mRoot.previous.next = mRoot;
		}
		else
		{
			--mCapacity;
		}
		
		return r;
	}
	
	@Override
	public V get(K k)
	{
		return find(k).value;
	}
	
	@Override
	public V remove(K k)
	{
		use();
		
		Entry e = find(k);
		
		if(e != mRoot)
		{
			e.previous.next = e.next;
			e.next.previous = e.previous;
			
			++mCapacity;
		}
		
		return e.value;
	}
	
	@Override
	public boolean containsKey(K k)
	{
		return find(k) != mRoot;
	}
	
	private Entry find(K k)
	{
		for(Entry e = mRoot.next ; e != mRoot ; e = e.next)
		{
			if(e.key.equals(k))
			{
				return e;
			}
		}
		
		return mRoot;
	}
}
