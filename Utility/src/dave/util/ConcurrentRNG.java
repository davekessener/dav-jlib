package dave.util;

public class ConcurrentRNG implements RNG
{
	private final RNG mSuper;
	
	public ConcurrentRNG(RNG rng)
	{
		mSuper = rng;
	}

	@Override
	public synchronized void setSeed(Seed s)
	{
		mSuper.setSeed(s);
	}

	@Override
	public synchronized Seed getSeed()
	{
		return mSuper.getSeed();
	}

	@Override
	public synchronized double nextDouble()
	{
		return mSuper.nextDouble();
	}

	@Override
	public synchronized int nextInt()
	{
		return mSuper.nextInt();
	}

	@Override
	public synchronized long nextLong()
	{
		return mSuper.nextLong();
	}
	
	@Override
	public synchronized double nextGauss()
	{
		return mSuper.nextGauss();
	}
}
