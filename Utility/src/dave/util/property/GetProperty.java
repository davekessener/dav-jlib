package dave.util.property;

public interface GetProperty<T>
{
	public abstract T get( );
}
