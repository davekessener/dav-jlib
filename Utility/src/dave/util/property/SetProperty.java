package dave.util.property;

public interface SetProperty<T>
{
	public abstract void set(T v);
}
