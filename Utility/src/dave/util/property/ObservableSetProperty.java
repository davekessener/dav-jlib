package dave.util.property;

import dave.util.Observable;

public interface ObservableSetProperty<T> extends Observable, SetProperty<T>
{
}
