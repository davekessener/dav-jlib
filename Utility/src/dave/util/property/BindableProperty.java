package dave.util.property;

import dave.util.Observer;

public class BindableProperty<T> extends ProxyObservableProperty<T>
{
	@SuppressWarnings("unchecked")
	private final Observer mObserver = o -> super.set(((GetProperty<T>) o).get());
	private final Property<ObservableProperty<T>> mObserved = new SimpleProperty<>();
	
	public BindableProperty() { }
	public BindableProperty(Property<T> p) { super(p); }
	
	public void bind(ObservableProperty<T> o)
	{
		unbind();
		
		super.set(o.get());
		
		mObserved.set(o);
		
		o.addObserver(mObserver);
	}
	
	public void unbind()
	{
		if(mObserved.get() != null)
		{
			mObserved.get().removeObserver(mObserver);
		}
		
		mObserved.set(null);
	}
	
	@Override
	public void set(T o)
	{
		if(o != get())
		{
			super.set(o);
			
			if(mObserved.get() != null)
			{
				mObserved.get().set(o);
			}
		}
	}
}
