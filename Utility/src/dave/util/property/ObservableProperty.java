package dave.util.property;

public interface ObservableProperty<T> extends Property<T>, ObservableGetProperty<T>, ObservableSetProperty<T>
{
}
