package dave.util.property;

import dave.util.BaseObservable;

public class ProxyObservableProperty<T> extends BaseObservable implements ObservableProperty<T>
{
	private final Property<T> mContainer;
	
	public ProxyObservableProperty() { this(new SimpleProperty<>()); }
	public ProxyObservableProperty(Property<T> c)
	{
		mContainer = c;
	}
	
	@Override
	public T get()
	{
		return mContainer.get();
	}

	@Override
	public void set(T v)
	{
		mContainer.set(v);
		
		alert();
	}
}
