package dave.util.property;

public class NonNullProperty<T> implements Property<T>
{
	private T mObj;
	
	public NonNullProperty(T o)
	{
		set(o);
	}

	@Override
	public T get()
	{
		return mObj;
	}

	@Override
	public void set(T v)
	{
		if(v == null)
			throw new NullPointerException();
		
		mObj = v;
	}
}
