package dave.util.property;

public class SimpleProperty<T> implements Property<T>
{
	private T mObj;
	
	public SimpleProperty() { this(null); }
	public SimpleProperty(T o)
	{
		mObj = o;
	}

	@Override
	public T get()
	{
		return mObj;
	}

	@Override
	public void set(T v)
	{
		mObj = v;
	}
}
