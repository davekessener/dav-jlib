package dave.util.property;

import dave.util.Observable;

public interface ObservableGetProperty<T> extends Observable, GetProperty<T>
{
}
