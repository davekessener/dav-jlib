package dave.util.property;

public interface Property<T> extends GetProperty<T>, SetProperty<T>
{
}
