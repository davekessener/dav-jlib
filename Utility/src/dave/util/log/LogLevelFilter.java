package dave.util.log;

import java.util.function.Predicate;

public class LogLevelFilter implements Predicate<Entry>
{
	private int mLevel;
	
	public LogLevelFilter() { this(Severity.INFO); }
	public LogLevelFilter(Severity lvl)
	{
		mLevel = lvl.level();
	}
	
	public void setThreshold(Severity s)
	{
		mLevel = s.level();
	}
	
	@Override
	public boolean test(Entry e)
	{
		return e.severity.level() >= mLevel;
	}
}
