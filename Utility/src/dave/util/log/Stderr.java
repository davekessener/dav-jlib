package dave.util.log;

public final class Stderr
{
	public static synchronized void printf(String s, Object ... o)
	{
		System.err.print(String.format(s, o));
		System.err.flush();
	}
	
	public static synchronized void println(String s)
	{
		System.err.println(s);
		System.err.flush();
	}
	
	private Stderr( ) { }
}
