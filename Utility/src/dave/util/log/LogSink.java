package dave.util.log;

import java.util.function.Consumer;

import dave.util.Transformer;
import dave.util.TransformingConsumer;
import dave.util.Utils;

public class LogSink extends TransformingConsumer<Entry, String>
{
	private LogSink(Consumer<String> cb, int t, int n, Transformer<Entry, String> f)
	{
		super(new Spacer(cb, t, Utils.repeat("=", n)), f);
	}
	
	public static LogSink build()
	{
		return (new Builder()).build();
	}
	
	public static class Builder
	{
		private Consumer<String> mCallback;
		private int mTime, mLength;
		private Transformer<Entry, String> mTransform;
		
		public Builder()
		{
			mCallback = s -> Stdout.println("# " + s);
			mTime = 1500;
			mLength = 200;
			mTransform = Utils.supportsANSI() ? new ColorFormatter() : new SimpleFormatter();
		}
		
		public Builder setCallback(Consumer<String> f) { mCallback = f; return this; }
		public Builder setTime(int t) { mTime = t; return this; }
		public Builder setLength(int l) { mLength = l; return this; }
		public Builder setTransform(Transformer<Entry, String> f) { mTransform = f; return this; }
		
		public LogSink build()
		{
			return new LogSink(mCallback, mTime, mLength, mTransform);
		}
	}
}
