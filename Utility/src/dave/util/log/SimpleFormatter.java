package dave.util.log;

import java.text.SimpleDateFormat;

import dave.util.Transformer;

public class SimpleFormatter implements Transformer<Entry, String>
{
    public static final SimpleDateFormat FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
    
	@Override
	public String create(Entry e)
	{
        StringBuilder sb = new StringBuilder();
        
        String ts = String.format("%-23s", FORMAT.format(e.timestamp));
        String caller = "[" + e.caller.file + ": " + e.caller.line + "]";
        String sev = String.format("[%5s]", e.severity);
        String log = e.logger;
        String th = String.format("(%3d)", e.caller.thread);
        
        if(log.length() < 11)
        {
        	log = String.format("%11s", log);
        }
        else if(log.length() > 11)
        {
        	log = log.substring(0, 4) + "..." + log.substring(log.length() - 4);
        }
        
        sb.append(ts).append(" ")
          .append(sev).append(" ")
          .append(log).append(" ")
          .append(th).append(" | ")
          .append(e.message);
        
        if(e.caller.file != null)
        {
        	sb.append(" ").append(caller);
        }
        
		return sb.toString().trim();
	}
}
