package dave.util.log;

public class Caller
{
	public final String file;
	public final int line;
	public final Class<?> klass;
	public final long thread;
	
	public Caller(String file, int line, Class<?> klass, long thread)
	{
		this.file = file;
		this.line = line;
		this.klass = klass;
		this.thread = thread;
	}

	public static Caller getCaller(int d)
	{
        StackTraceElement e = Thread.currentThread().getStackTrace()[d];
        Class<?> c = Object.class;
        
        try
		{
			c = Class.forName(e.getClassName());
		}
		catch(ClassNotFoundException ex)
		{
			ex.printStackTrace();
		}
        
        return new Caller(e.getFileName(), e.getLineNumber(), c, Thread.currentThread().getId());
	}
}
