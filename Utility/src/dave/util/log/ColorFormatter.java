package dave.util.log;

public class ColorFormatter extends SimpleFormatter
{
	private static final String COLOR_RED = "\u001B[31m";
	private static final String COLOR_RESET = "\u001B[0m";
	
	@Override
	public String create(Entry e)
	{
		String msg = super.create(e);
		
		if(e.severity.level() >= Severity.ERROR.level())
		{
			msg = COLOR_RED + msg + COLOR_RESET;
		}
		
		return msg;
	}
}
