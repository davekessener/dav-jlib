package dave.util.log;

import java.util.function.Consumer;

public class Spacer implements Consumer<String>
{
	private final Consumer<String> mCallback;
	private final long mDelta;
	private final String mSpacer;
	private long mLast;
	
	public Spacer(Consumer<String> cb, int ts, String sp)
	{
		mCallback = cb;
		mDelta = ts;
		mSpacer = sp;
		mLast = System.currentTimeMillis();
	}

	@Override
	public void accept(String o)
	{
		long t = System.currentTimeMillis();
		
		if(t - mLast >= mDelta)
		{
			mCallback.accept(mSpacer);
		}
		
		mCallback.accept(o);
		
		mLast = t;
	}
}
