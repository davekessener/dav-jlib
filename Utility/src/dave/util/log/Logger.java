package dave.util.log;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

import dave.util.Identifiable;

public class Logger implements Identifiable
{
	private static final Map<String, Logger> sLoggers = new HashMap<>();
	
	public static final Logger DEFAULT = get("main");
	
	private final String mID;

	private Logger(String id)
	{
		mID = id;
	}

	@Override
	public String getID( )
	{
		return mID;
	}
	
	public void info(String msg, Object ... o) { log(Severity.INFO, 1, msg, o); }
	public void warn(String msg, Object ... o) { log(Severity.WARNING, 1, msg, o); }
	public void error(String msg, Object ... o) { log(Severity.ERROR, 1, msg, o); }
	public void fatal(String msg, Object ... o) { log(Severity.FATAL, 1, msg, o); }

	public void log(int cd, String msg, Object ... o) { log(Severity.NONE, cd + 1, msg, o); }
	public void log(String msg, Object ... o) { log(Severity.NONE, 1, msg, o); }
	public void log(Severity s, String msg, Object ... o) { log(s, 1, msg, o); }
	public synchronized void log(Severity s, int cd, String msg, Object ... o)
	{
		Caller c = Caller.getCaller(cd + 2 + LogBase.INSTANCE.getStackDepth());
		Date ts = new Date();

		msg = String.format(msg, o);
		
		Stream.of(msg.split("\n")).forEach(p -> {
			LogBase.INSTANCE.log(new Entry(mID, c, s, ts, p));
		});
	}

	public static Logger get(String id)
	{
		Logger l = sLoggers.get(id);
		
		if(l == null)
		{
			sLoggers.put(id, l = new Logger(id));
		}
		
		return l;
	}
}
