package dave.util.log;

public enum Severity
{
	NONE(0, "-----"),
	INFO(1, "INFO"),
	WARNING(2, "WARN"),
	ERROR(3, "ERROR"),
	FATAL(4, "FATAL");
	
	private final String mID;
	private final int mLevel;
	
	private Severity(int l, String n)
	{
		mID = n;
		mLevel = l;
	}
	
	@Override
	public String toString( )
	{
		return mID;
	}
	
	public int level( )
	{
		return mLevel;
	}
}
