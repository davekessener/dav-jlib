package dave.util.log.service;

import dave.util.property.Property;
import dave.util.property.SimpleProperty;

public abstract class DebugObject implements LoggableObject
{
	private final Property<String> mID = new SimpleProperty<>();
	
	@Override
	public String getObjectID()
	{
		if(mID.get() == null)
		{
			mID.set(this.getClass().getSimpleName() + "-" + String.format("%08X", this.hashCode()));
		}
		
		return mID.get();
	}
	
	@Override
	public void log(String tag, String f, Object ... a)
	{
		String msg = String.format(f, a);
		String id = getObjectID();
		String s = toString();
		
		LogService.INSTANCE.log(id, tag, s, msg);
	}
}
