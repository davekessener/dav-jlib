package dave.util.log.service;

public interface LoggableObject
{
	public abstract String getObjectID( );
	public abstract void log(String tag, String f, Object ... a);
}
