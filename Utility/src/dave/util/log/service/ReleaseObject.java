package dave.util.log.service;

public abstract class ReleaseObject implements LoggableObject
{
	@Override
	public String getObjectID()
	{
		throw new UnsupportedOperationException();
	}
	
	@Override
	public void log(String tag, String f, Object ... o)
	{
	}
}
