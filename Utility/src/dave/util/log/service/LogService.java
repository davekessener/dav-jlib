package dave.util.log.service;

import java.util.ArrayList;
import java.util.List;

import dave.util.ShutdownService;
import dave.util.ShutdownService.Priority;
import dave.util.log.Logger;

public final class LogService
{
	public static final LogService INSTANCE = new LogService();
	
	private final List<Entry> mEntries;
	private State mState;
	
	private LogService()
	{
		mEntries = new ArrayList<>();
		mState = State.INIT;
	}
	
	public synchronized void log(String id, String tag, String desc, String msg)
	{
		if(mState != State.ON)
			throw new IllegalStateException();
		
		mEntries.add(new Entry(id, tag, msg, Thread.currentThread().getStackTrace(), desc));
		
		while(mEntries.size() > 10000)
		{
			mEntries.remove(0);
		}
	}
	
	public synchronized Entry retrieve(String id, String tag)
	{
		if(mState != State.ON)
			throw new IllegalStateException();
		
		return mEntries.stream()
			.filter(e -> e.id.equals(id) && e.tag.equals(tag))
			.findFirst().orElse(null);
	}
	
	public void initialize(boolean debug)
	{
		if(mState != State.INIT)
			throw new IllegalStateException();
		
		if(debug)
		{
			LOG.info("Starting log service");
			
			mState = State.ON;
			
			ShutdownService.INSTANCE.register(Priority.LATER, this::shutdown);
		}
		else
		{
			mState = State.DISABLED;
		}
	}
	
	private void shutdown()
	{
		if(mState != State.ON)
			throw new IllegalStateException();
		
		mState = State.OFF;
	}
	
	private static enum State
	{
		INIT,
		ON,
		OFF,
		DISABLED
	}
	
	public static class Entry
	{
		public final String id;
		public final String tag;
		public final String message;
		public final StackTraceElement[] caller;
		public final String description;
		
		public Entry(String id, String tag, String message, StackTraceElement[] caller, String description)
		{
			this.id = id;
			this.tag = tag;
			this.message = message;
			this.caller = caller;
			this.description = description;
		}
	}
	
	private static final Logger LOG = Logger.get("log");
}
