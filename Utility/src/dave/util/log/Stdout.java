package dave.util.log;

public final class Stdout
{
	public static synchronized void printf(String s, Object ... o)
	{
		System.out.print(String.format(s, o));
		System.out.flush();
	}
	
	public static synchronized void println(String s)
	{
		System.out.println(s);
		System.out.flush();
	}
	
	private Stdout( ) { }
}
