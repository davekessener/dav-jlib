package dave.util.log;

import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.function.Predicate;

import dave.util.Actor;
import dave.util.SevereException;
import dave.util.TransformingConsumer;
import dave.util.Utils;

public class LogBase implements Actor
{
	public static LogBase INSTANCE = new LogBase();
	
	private final List<Sink> mSinks;
	private final Thread mThread;
	private final BlockingQueue<Entry> mQueue;
	private boolean mRunning;
	private int mLogLength, mStackDepth;
	
	private LogBase( )
	{
		mSinks = Collections.synchronizedList(new ArrayList<>());
		mQueue = new LinkedBlockingQueue<>();
		mThread = new Thread(Utils.wrap(() -> run()));
		mRunning = false;
		mLogLength = 80;
		mStackDepth = 0;
	}
	
	@Override
	public void start( )
	{
		mRunning = true;
		sync();
		mThread.start();
	}
	
	@Override
	public void stop( )
	{
		try
		{
			mRunning = false;
			mThread.join(100);
		}
		catch(InterruptedException e)
		{
			throw new SevereException(e);
		}
	}
	
	protected int getStackDepth( ) { return mStackDepth; }
	
	public void registerSink(Predicate<Entry> f, Consumer<Entry> cb)
	{
		mSinks.add(new Sink(f, cb));
	}
	
	public void log(Entry e)
	{
		String msg = e.message;
		
		if(msg.length() > mLogLength)
		{
			mLogLength = Math.min(MAX_LEN, msg.length());
		}
		else if(msg.length() < mLogLength)
		{
			msg = String.format("%-" + mLogLength + "s", msg);
		}
		
		if(mRunning)
		{
			mQueue.add(new Entry(e.logger, e.caller, e.severity, e.timestamp, msg));
		}
	}
	
	private void run( ) throws InterruptedException
	{
		while(mRunning || !mQueue.isEmpty())
		{
			Entry e = mQueue.poll(10, TimeUnit.MILLISECONDS);
			
			if(e == null) continue;
			
			for(Sink s : mSinks)
			{
				if(s.filter.test(e))
				{
					s.callback.accept(e);
				}
			}
		}
	}
	
	private void sync( )
	{
		StackTraceElement[] s = Thread.currentThread().getStackTrace();
		
		for(int i = 0 ; i < s.length ; ++i)
		{
			if(s[i].getMethodName() == "sync")
			{
				mStackDepth = i;
				
				break;
			}
		}
	}
	
	private static final class Sink
	{
		public final Predicate<Entry> filter;
		public final Consumer<Entry> callback;
		
		public Sink(Predicate<Entry> filter, Consumer<Entry> callback)
		{
			this.filter = filter;
			this.callback = callback;
		}
	}

	private static final int MAX_LEN = 150;
	
	public static class DefaultSink extends TransformingConsumer<Entry, String>
	{
		public DefaultSink( ) { this(1500, System.out); }
		public DefaultSink(int d) { this(d, System.out); }
		public DefaultSink(OutputStream out) { this(1500, out); }
		public DefaultSink(int d, OutputStream out) { this(d, new PrintStream(out)); }
		
		public DefaultSink(int d, PrintStream out)
		{
			super(new Spacer(s -> out.println(s), d, Utils.repeat("=", MAX_LEN)), new SimpleFormatter());
		}
	}
}
