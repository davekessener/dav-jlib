package dave.util.log;

import java.util.Date;

public class Entry
{
	public final String logger;
	public final Caller caller;
	public final Severity severity;
	public final Date timestamp;
	public final String message;
	
	public Entry(String logger, Caller caller, Severity severity, Date timestamp, String message)
	{
		this.logger = logger;
		this.caller = caller;
		this.severity = severity;
		this.timestamp = timestamp;
		this.message = message;
	}
}
