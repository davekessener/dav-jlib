package dave.util.log;

import java.util.HashMap;
import java.util.Map;

public final class LogDatabase
{
	public static final LogDatabase INSTANCE = new LogDatabase();
	
	private final Map<Class<? extends LoggableObject>, Severity> mLoglevels = new HashMap<>();
	
	public Severity getLoglevel(Class<? extends LoggableObject> c)
	{
		Severity s = mLoglevels.get(c);
		
		if(s == null)
		{
			s = Severity.NONE;
		}
		
		return s;
	}
	
	public void setLoglevel(Class<? extends LoggableObject> c, Severity s)
	{
		mLoglevels.put(c, s);
	}
	
	private LogDatabase( ) { }
}
