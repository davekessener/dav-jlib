package dave.util.log;

import dave.util.DirtyObject;

public class LoggableObject extends DirtyObject
{
	private final Logger mLog;
	private final Severity mSeverity;
	
	public LoggableObject()
	{
		mLog = Logger.get(getClass().getSimpleName());
		mSeverity = LogDatabase.INSTANCE.getLoglevel(getClass());
	}
	
	protected Logger log() { return mLog; }
	protected void log(String msg, Object ... args) { mLog.log(mSeverity, 1, msg, args); }
}
