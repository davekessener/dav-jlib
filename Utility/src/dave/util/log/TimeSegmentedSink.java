package dave.util.log;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.function.Consumer;

import dave.util.Producer;
import dave.json.SevereIOException;

public class TimeSegmentedSink implements Consumer<String>
{
	private final Producer<File> mProd;
	private final long mDelta;
	private File[] mFiles;
	private BufferedWriter[] mOut;
	private long mLast;
	private int mIdx;
	
	public TimeSegmentedSink(Producer<File> p, long d)
	{
		mProd = p;
		mDelta = d;
		mFiles = new File[2];
		mOut = new BufferedWriter[2];
		mLast = 0;
		mIdx = 0;
		
		update();
	}
	
	@Override
	public void accept(String s)
	{
		if(System.currentTimeMillis() - mLast >= mDelta)
		{
			update();
		}
		
		try
		{
			for(int i = 0 ; i < mOut.length ; ++i)
			{
				if(mOut[i] != null)
				{
					mOut[i].write(s);
					mOut[i].newLine();
					mOut[i].flush();
				}
			}
		}
		catch(IOException e)
		{
			throw new SevereIOException(e);
		}
	}
	
	private void update( )
	{
		try
		{
			mLast = System.currentTimeMillis();
			
			if(mFiles[mIdx] != null)
			{
				mOut[mIdx].close();
				mFiles[mIdx].delete();
			}
			
			mFiles[mIdx] = mProd.produce();
			mOut[mIdx] = new BufferedWriter(new FileWriter(mFiles[mIdx]));
			
			mIdx = (mIdx + 1) % mFiles.length;
		}
		catch(IOException e)
		{
			throw new SevereIOException(e);
		}
	}
}
