package dave.util;

public interface RNG
{
	public abstract void setSeed(Seed s);
	public abstract Seed getSeed( );
	public abstract double nextDouble( );
	public abstract int nextInt( );
	public abstract long nextLong( );
	public abstract double nextGauss( );
	
	public default int nextInt(int n)
	{
		if(n == 0)
			throw new IllegalArgumentException("0 not acceptable argument to RNG::nextInt(int) !");
		
		int s = 1;
		int bm = 0x7FFFFFFF;
		
		if(n < 0)
		{
			s = -1;
		}
		
		--n;
		
		if(n == 0)
			return 0;
		
		while(true)
		{
			int t = (bm >> 1);
			
			if((n & t) != n)
				break;
			
			bm = t;
		}
		
		int v = 0;
		
		while(true)
		{
			v = nextInt() & bm;
			
			if(v <= n)
				break;
		}
		
		return s * v;
	}
}
