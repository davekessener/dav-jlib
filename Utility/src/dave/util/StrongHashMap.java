package dave.util;

import java.util.HashMap;
import java.util.NoSuchElementException;

public class StrongHashMap<K, V> extends HashMap<K, V>
{
	private static final long serialVersionUID = 626017622880648092L;
	
	@Override
	public V put(K k, V v)
	{
		if(super.put(k, v) != null)
			throw new IllegalStateException("Key overwritten: " + k);
		
		return null;
	}
	
	@Override
	public V get(Object k)
	{
		V v = super.get(k);
		
		if(v == null)
			throw new NoSuchElementException("" + k);
		
		return v;
	}
}
