package dave.util;

import java.util.ArrayList;
import java.util.List;

public class BaseObservable implements Observable
{
	private final List<Observer> mObservers = new ArrayList<>();
	
	@Override
	public void addObserver(Observer o)
	{
		mObservers.add(o);
	}
	
	@Override
	public void removeObserver(Observer o)
	{
		mObservers.remove(o);
	}
	
	protected void alert( )
	{
		for(Observer o : mObservers)
		{
			o.alert(this);
		}
	}
}
