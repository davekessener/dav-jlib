package dave.util;

public interface Transformer<S, D>
{
	public abstract D create(S source);
}
