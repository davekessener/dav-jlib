package dave.util.container;

import java.util.LinkedHashMap;
import java.util.Map;

public class BoundedMap<K, V> extends LinkedHashMap<K, V>
{
	private static final long serialVersionUID = -2482574967263570409L;
	
	private final int mSize;

	public BoundedMap(int l)
	{
		super(l * 10 / 7, 0.7f, true);
		
		mSize = l;
	}
	
	@Override
	protected boolean removeEldestEntry(Map.Entry<K, V> e)
	{
		return size() > mSize;
	}
}
