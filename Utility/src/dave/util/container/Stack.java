package dave.util.container;

import java.util.stream.Stream;

public interface Stack<T>
{
	public abstract void push(T e);
	public abstract T top( );
	public abstract T pop( );
	public abstract int size( );
	
	public default boolean isEmpty( ) { return size() == 0; }
	public default void clear( ) { while(!isEmpty()) pop(); }
	
	public default Stream<T> stream( ) { throw new UnsupportedOperationException(); }
}
