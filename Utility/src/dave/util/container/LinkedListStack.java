package dave.util.container;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Stream;

public class LinkedListStack<T> implements Stack<T>
{
	private final List<T> mStack = new LinkedList<>();

	@Override
	public void push(T e)
	{
		mStack.add(0, e);
	}

	@Override
	public T top()
	{
		return mStack.get(0);
	}

	@Override
	public T pop()
	{
		T e = top();
		
		mStack.remove(0);
		
		return e;
	}

	@Override
	public int size()
	{
		return mStack.size();
	}
	
	@Override
	public boolean isEmpty()
	{
		return mStack.isEmpty();
	}
	
	@Override
	public Stream<T> stream()
	{
		return mStack.stream();
	}
}
