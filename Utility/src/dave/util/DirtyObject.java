package dave.util;

public class DirtyObject extends Object implements Dirty
{
	private boolean mIsDirty = false;
	
	protected void use( )
	{
		mIsDirty = true;
	}

	@Override
	public boolean dirty( )
	{
		return mIsDirty;
	}

	@Override
	public void clean( )
	{
		mIsDirty = false;
	}
}
