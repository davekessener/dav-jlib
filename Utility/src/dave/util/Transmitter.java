package dave.util;

public interface Transmitter<T>
{
	public abstract void send(T v);
	public abstract T receive( );
	public default void destroy( ) { }
}
