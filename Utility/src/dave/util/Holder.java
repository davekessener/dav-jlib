package dave.util;

public class Holder<T>
{
	public T value;
	
	public Holder(T value)
	{
		this.value = value;
	}
}
