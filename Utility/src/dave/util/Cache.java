package dave.util;

public interface Cache<K, V>
{
	public abstract V load(K k);
	public abstract boolean loaded(K k);
	public abstract int capacity( );
	public abstract int size( );
	public abstract void clear( );
}