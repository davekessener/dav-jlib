package dave.util;

public interface Identifiable
{
	public abstract String getID( );
}
