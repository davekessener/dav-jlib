package dave.util.io;

public interface Closeable extends AutoCloseable
{
	public abstract void close( );
}
