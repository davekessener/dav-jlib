package dave.util.io;

public interface CloseableIO extends BasicIO, Closeable
{
}
