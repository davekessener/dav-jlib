package dave.util.io;

import java.io.IOException;
import java.io.OutputStream;

import dave.json.SevereIOException;

public class StreamOutput implements Output, Closeable, CloseableOutput
{
	private final OutputStream mOut;
	
	public StreamOutput(OutputStream out)
	{
		mOut = out;
	}

	@Override
	public void write(byte[] v, int o, int l)
	{
		try
		{
			mOut.write(v, o, l);
		}
		catch(IOException e)
		{
			throw new SevereIOException(e);
		}
	}

	@Override
	public void close()
	{
		try
		{
			mOut.close();
		}
		catch(IOException e)
		{
			throw new SevereIOException(e);
		}
	}
}
