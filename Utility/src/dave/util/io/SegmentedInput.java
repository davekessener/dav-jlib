package dave.util.io;

import dave.util.Producer;

public class SegmentedInput extends SegmentationBase<BaseInput> implements Input
{
	public SegmentedInput(BaseInput in, Producer<Integer> f)
	{
		super(in, f);
	}

	@Override
	public void read(byte[] b, int o, int l)
	{
		implementation(b, o, l);
	}

	@Override
	protected void action(byte[] b, int o, int l)
	{
		getBase().read(b, o, l);
	}

	@Override
	public boolean eos()
	{
		return getBase().eos();
	}
}
