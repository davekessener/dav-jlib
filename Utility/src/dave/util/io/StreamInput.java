package dave.util.io;

import java.io.IOException;
import java.io.InputStream;

import dave.json.SevereIOException;

public class StreamInput implements Input, Closeable, CloseableInput
{
	private final InputStream mIn;
	
	public StreamInput(InputStream in)
	{
		mIn = in;
	}
	
	@Override
	public boolean eos()
	{
		return false;
	}
	
	@Override
	public void read(byte[] b, int o, int l)
	{
		try
		{
			mIn.read(b, o, l);
		}
		catch(IOException e)
		{
			throw new SevereIOException(e);
		}
	}

	@Override
	public void close()
	{
		try
		{
			mIn.close();
		}
		catch(IOException e)
		{
			throw new SevereIOException(e);
		}
	}
}
