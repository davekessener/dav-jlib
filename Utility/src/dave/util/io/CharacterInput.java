package dave.util.io;

import java.nio.BufferOverflowException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CoderResult;

public class CharacterInput implements InputDecoder<Character>
{
	private final Input mIn;
	private final CharsetDecoder mDecode;
	private final MyByteBuffer mBuf;
	
	public CharacterInput(Input in, Charset ch)
	{
		mIn = in;
		mDecode = ch.newDecoder();
		mBuf = new MyByteBuffer(20);
	}
	
	@Override
	public Character read( )
	{
		CharBuffer out = CharBuffer.allocate(1);
		
		while(true)
		{
			mBuf.put(mIn.read());
			
			ByteBuffer in = mBuf.view();
			CoderResult c = mDecode.decode(in, out, false);
			mBuf.skip(in.position());
			
			if(c.isError() || c.isMalformed() || c.isUnmappable()) try
			{
				c.throwException();
			}
			catch(CharacterCodingException e)
			{
				throw new EncodingException(e);
			}
			
			if(out.get(0) != '\0')
				break;
			
			out.clear();
		}
		
		return out.get(0);
	}
	
	private static class MyByteBuffer
	{
		private final byte[] mBuf;
		private int mPos;
		
		public MyByteBuffer(int c)
		{
			mBuf = new byte[c];
			mPos = 0;
		}
		
		public void put(byte v)
		{
			if(mPos == mBuf.length)
				throw new BufferOverflowException();
			
			mBuf[mPos++] = v;
		}
		
		public ByteBuffer view( )
		{
			return ByteBuffer.wrap(mBuf, 0, mPos);
		}
		
		public void skip(int n)
		{
			if(n > mPos)
				throw new IllegalArgumentException();
			
			for(int i = n ; i < mPos ; ++i)
			{
				mBuf[i - n] = mBuf[i];
			}
			
			mPos -= n;
		}
	}
	
	public static class EncodingException extends RuntimeException
	{
		private static final long serialVersionUID = 7225262986471679474L;
		
		public EncodingException(Exception e)
		{
			super(e);
		}
	}
}
