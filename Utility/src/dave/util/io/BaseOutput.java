package dave.util.io;

public interface BaseOutput
{
	public abstract void write(byte[] v, int o, int l);
	
	public default void write(byte v)
	{
		write(new byte[] { v });
	}
	
	public default void write(byte[] v)
	{
		write(v, 0, v.length);
	}
}
