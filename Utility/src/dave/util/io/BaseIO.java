package dave.util.io;

public abstract class BaseIO implements IO
{
	@Override
	public byte read( )
	{
		return read(1)[0];
	}

	@Override
	public byte[] read(int l)
	{
		byte[] b = new byte[l];
		
		read(b, 0, l);
		
		return b;
	}

	@Override
	public void write(byte v)
	{
		write(new byte[] { v });
	}

	@Override
	public void write(byte[] v)
	{
		write(v, 0, v.length);
	}
}
