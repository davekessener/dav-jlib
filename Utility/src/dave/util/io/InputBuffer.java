package dave.util.io;

import java.nio.charset.Charset;

import dave.json.Buffer;

public class InputBuffer implements Buffer
{
	private final InputDecoder<Character> mIn;
	private int mSize;
	private char mLook;
	
	public InputBuffer(int s, Input in, Charset ch)
	{
		mIn = new CharacterInput(in, ch);
		mSize = s;
		
		pop();
	}
	
	@Override
	public char lookAhead( )
	{
		return mLook;
	}

	@Override
	public void pop( )
	{
		if(mSize < 0)
		{
			throw new IllegalStateException();
		}
		else if(mSize > 0)
		{
			mLook = mIn.read();
		}
		
		--mSize;
	}
}
