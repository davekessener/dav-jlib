package dave.util.io;

public class CountingInput extends CountingBase<Input> implements Input
{
	public CountingInput(Input io)
	{
		super(io);
	}

	@Override
	public void read(byte[] b, int o, int l)
	{
		implementation(b, o, l);
	}

	@Override
	protected void action(byte[] b, int o, int l)
	{
		getBase().read(b, o, l);
	}

	@Override
	public boolean eos()
	{
		return getBase().eos();
	}
}
