package dave.util.io;

public interface SeekableInput extends Input, Seekable
{
}
