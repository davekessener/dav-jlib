package dave.util.io;

public class UncheckedInterruptedException extends RuntimeException
{
	private static final long serialVersionUID = 2894394945365202430L;

	public UncheckedInterruptedException(InterruptedException e)
	{
		super(e);
	}
}
