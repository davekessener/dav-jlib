package dave.util.io;

public interface OutputEncoder<T>
{
	public abstract void write(T o);
}
