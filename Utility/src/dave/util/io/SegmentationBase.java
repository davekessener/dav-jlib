package dave.util.io;

import dave.util.Producer;

public abstract class SegmentationBase<T>
{
	private final T mIO;
	private final Producer<Integer> mCallback;
	private int mLeft;
	
	public SegmentationBase(T io, Producer<Integer> f)
	{
		mIO = io;
		mCallback = f;
		mLeft = 0;
	}
	
	protected T getBase( ) { return mIO; }
	
	protected void implementation(byte[] b, int o, int l)
	{
		if(mLeft == 0)
		{
			mLeft = mCallback.produce();
		}
		
		if(l <= mLeft)
		{
			action(b, o, l);
			mLeft -= l;
		}
		else
		{
			action(b, o, mLeft);
			o += mLeft;
			l -= mLeft;
			mLeft = 0;
			
			implementation(b, o, l);
		}
	}
	
	protected abstract void action(byte[] b, int o, int l);
}
