package dave.util.io;

public interface CloseableInput extends Input, Closeable
{
}
