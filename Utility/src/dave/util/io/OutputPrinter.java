package dave.util.io;

import java.nio.charset.Charset;

import dave.json.Printer;

public class OutputPrinter implements Printer
{
	private final Output mIO;
	private final Charset mCharset;
	
	public OutputPrinter(Output io, Charset ch)
	{
		mIO = io;
		mCharset = ch;
	}

	@Override
	public void accept(String s)
	{
		mIO.write(s.getBytes(mCharset));
	}
}
