package dave.util.io;

public abstract class CountingBase<T>
{
	private final T mBase;
	private int mCounter;
	
	public CountingBase(T base)
	{
		mBase = base;
		mCounter = 0;
	}
	
	public int getByteCount( )
	{
		return mCounter;
	}
	
	protected T getBase( ) { return mBase; }
	
	protected abstract void action(byte[] b, int o, int l);
	
	protected void implementation(byte[] b, int o, int l)
	{
		action(b, o, l);
		
		mCounter += l;
	}
}
