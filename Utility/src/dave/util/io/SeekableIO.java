package dave.util.io;

public interface SeekableIO extends BasicIO, SeekableInput, SeekableOutput
{
}
