package dave.util.io;

public interface InputDecoder<T>
{
	public abstract T read( );
}
