package dave.util.io;

public interface BaseInput
{
	public abstract void read(byte[] b, int o, int l);
	public abstract boolean eos( );
	
	public default byte read( )
	{
		return read(1)[0];
	}
	
	public default byte[] read(int l)
	{
		byte[] b = new byte[l];
		
		read(b, 0, l);
		
		return b;
	}
}
