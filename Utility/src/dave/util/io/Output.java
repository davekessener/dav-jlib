package dave.util.io;

public interface Output extends BaseOutput
{
	public default Output writeByte(int v)
	{
		return writeRawInt(v, 1);
	}
	
	public default Output writeShort(int v)
	{
		return writeRawInt(v, 2);
	}
	
	public default Output writeInt(int v)
	{
		return writeRawInt(v, 4);
	}
	
	public default Output writeLong(long v)
	{
		return writeRawInt(v, 8);
	}
	
	public default Output writeFloat(float f)
	{
		return writeRawInt(Float.floatToRawIntBits(f), 4);
	}
	
	public default Output writeDouble(double f)
	{
		return writeRawInt(Double.doubleToRawLongBits(f), 8);
	}
	
	public default Output writeRawInt(long v, int n)
	{
		while(n-- > 0)
		{
			write((byte) (v % 0x100));
			v >>>= 8;
		}
		
		return this;
	}
}
