package dave.util.io;

import dave.util.Producer;

public class SegmentedOutput extends SegmentationBase<Output> implements Output
{
	public SegmentedOutput(Output out, Producer<Integer> f)
	{
		super(out, f);
	}

	@Override
	public void write(byte[] b, int o, int l)
	{
		implementation(b, o, l);
	}

	@Override
	protected void action(byte[] b, int o, int l)
	{
		getBase().write(b, o, l);
	}
}
