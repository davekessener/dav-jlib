package dave.util.io;

import java.io.EOFException;
import java.util.Arrays;

import dave.json.SevereIOException;

public class DummyIO extends BaseIO
{
	private byte[] mBuf;
	private int mSize, mSeek;
	private boolean mClosed;
	
	public DummyIO( )
	{
		mBuf = new byte[0];
		mSize = mSeek = 0;
		mClosed = false;
	}
	
	public void reopen( )
	{
		mClosed = false;
		mSeek = 0;
	}
	
	private void writeImpl(byte v)
	{
		if(mSeek == mSize)
		{
			if(mSize == mBuf.length)
			{
				mBuf = Arrays.copyOf(mBuf, mBuf.length * 2 + 1);
			}
			
			++mSize;
		}
		
		mBuf[mSeek++] = v;
	}
	
	private byte readImpl( )
	{
		if(mSeek >= mSize)
			throw new SevereIOException(new EOFException());
		
		return mBuf[mSeek++];
	}
	
	@Override
	public boolean eos()
	{
		return mSeek < mSize;
	}

	@Override
	public void read(byte[] b, int o, int l)
	{
		if(mClosed)
			throw new SevereIOException();
		
		if(b == null || b.length < o + l)
			throw new IllegalArgumentException();
		
		while(l-- > 0)
		{
			b[o++] = readImpl();
		}
	}

	@Override
	public void write(byte[] v, int o, int l)
	{
		if(mClosed)
			throw new SevereIOException();
		
		if(v == null || v.length < o + l)
			throw new IllegalArgumentException();
		
		while(l-- > 0)
		{
			writeImpl(v[o++]);
		}
	}

	@Override
	public void close( )
	{
		mClosed = true;
	}

	@Override
	public void seek(int p)
	{
		if(mClosed)
			throw new SevereIOException();
		
		if(p < 0 || p > mSize)
			throw new IllegalArgumentException(String.format("Tried to seek to %d/%dB!", p, mSize));
		
		mSeek = p;
	}

	@Override
	public int seek( )
	{
		if(mClosed)
			throw new SevereIOException();
		
		return mSeek;
	}

	@Override
	public int size( )
	{
		if(mClosed)
			throw new SevereIOException();
		
		return mSize;
	}
}
