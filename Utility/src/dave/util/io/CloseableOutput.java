package dave.util.io;

public interface CloseableOutput extends Closeable, Output
{
}
