package dave.util.io;

import java.nio.charset.Charset;
import java.util.Arrays;

import dave.util.Utils;

public interface Input extends BaseInput
{
	public default byte readByte( )
	{
		return (byte) readRawInt(1);
	}
	
	public default short readShort( )
	{
		return (short) readRawInt(2);
	}
	
	public default int readInt( )
	{
		return (int) readRawInt(4);
	}
	
	public default long readLong( )
	{
		return readRawInt(8);
	}
	
	public default float readFloat( )
	{
		return Float.intBitsToFloat(readInt());
	}
	
	public default double readDouble( )
	{
		return Double.longBitsToDouble(readLong());
	}
	
	public default long readRawInt(int n)
	{
		byte[] r = read(n);
		long v = 0;
		
		for(int i = 0 ; i < n ; ++i)
		{
			v |= (r[i] & 0xffl) << (i * 8);
		}
		
		return v;
	}

	public default String readLine() { return readLine('\n', Utils.CHARSET); }
	public default String readLine(char d) { return readLine(d, Utils.CHARSET); }
	public default String readLine(char d, Charset cs)
	{
		byte[] r = new byte[0];
		int i = 0;
		
		while(true)
		{
			byte v = readByte();
			
			if(i == r.length)
			{
				r = Arrays.copyOf(r, r.length * 2 + 1);
			}
			
			r[i++] = v;
			
			String s = new String(Arrays.copyOf(r, i), cs);
			
			if(eos() || (s.length() > 0 && s.charAt(s.length() - 1) == d))
				return s;
		}
	}
}
