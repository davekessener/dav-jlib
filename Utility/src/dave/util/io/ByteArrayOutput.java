package dave.util.io;

import java.util.Arrays;

public class ByteArrayOutput implements Output
{
	private byte[] mBuffer;
	private int mIdx;
	
	public ByteArrayOutput() { this(100); }
	public ByteArrayOutput(int l)
	{
		mBuffer = new byte[l];
		mIdx = 0;
	}
	
	public byte[] toByteArray() { return Arrays.copyOf(mBuffer, mIdx); }
	
	@Override
	public void write(byte v)
	{
		if(mIdx >= mBuffer.length)
		{
			mBuffer = Arrays.copyOf(mBuffer, mBuffer.length * 2 + 1);
		}
		
		mBuffer[mIdx++] = v;
	}
	
	@Override
	public void write(byte[] v, int o, int l)
	{
		for(int i = 0 ; i < l ; ++i)
		{
			write(v[o + i]);
		}
	}
}
