package dave.util.io;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

import dave.json.SevereIOException;

public class FileIO extends BaseIO
{
	private final RandomAccessFile mFile;
	
	public FileIO(File f)
	{
		try
		{
			mFile = new RandomAccessFile(f.getAbsolutePath(), "rw");
		}
		catch(FileNotFoundException e)
		{
			throw new SevereIOException(e);
		}
	}
	
	@Override
	public boolean eos()
	{
		try
		{
			return mFile.getFilePointer() == mFile.length();
		}
		catch(IOException e)
		{
			throw new SevereIOException(e);
		}
	}

	@Override
	public void read(byte[] b, int o, int l)
	{
		try
		{
			mFile.readFully(b, o, l);
		}
		catch(IOException e)
		{
			throw new SevereIOException(e);
		}
	}

	@Override
	public void write(byte[] v, int o, int l)
	{
		try
		{
			mFile.write(v, o, l);
		}
		catch(IOException e)
		{
			throw new SevereIOException(e);
		}
	}

	@Override
	public void close( )
	{
		try
		{
			mFile.close();
		}
		catch(IOException e)
		{
			throw new SevereIOException(e);
		}
	}

	@Override
	public void seek(int p)
	{
		try
		{
			mFile.seek(p);
		}
		catch(IOException e)
		{
			throw new SevereIOException(e);
		}
	}

	@Override
	public int seek( )
	{
		try
		{
			return (int) mFile.getFilePointer();
		}
		catch(IOException e)
		{
			throw new SevereIOException(e);
		}
	}
	
	@Override
	public int size( )
	{
		try
		{
			return (int) mFile.length();
		}
		catch(IOException e)
		{
			throw new SevereIOException(e);
		}
	}
}
