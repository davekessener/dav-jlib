package dave.util.io;

public class CountingOutput extends CountingBase<Output> implements Output
{
	public CountingOutput(Output io)
	{
		super(io);
	}

	@Override
	public void write(byte[] v, int o, int l)
	{
		implementation(v, o, l);
	}
	
	@Override
	protected void action(byte[] b, int o, int l)
	{
		getBase().write(b, o, l);
	}
}
