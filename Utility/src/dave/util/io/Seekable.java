package dave.util.io;

public interface Seekable
{
	public abstract void seek(int p);
	public abstract int seek( );
	public abstract int size( );
}
