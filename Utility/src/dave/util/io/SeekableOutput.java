package dave.util.io;

public interface SeekableOutput extends Output, Seekable
{
}
