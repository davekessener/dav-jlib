package dave.util;

import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Stream;

public interface Association<K, V>
{
	public abstract boolean contains(K k);
	public abstract void put(K k, V v);
	public abstract V get(K k);
	public abstract void remove(K k);
	public abstract Stream<V> stream( );
	
	public default void forEach(Consumer<V> f) { stream().forEach(f); }
	
	public static interface EmptyPolicy<K, V> { V onEmpty(K k); }
	public static interface OverwritePolicy<K, V> { V onOverwrite(K k, V o, V n); }
	
	public static class OnEmptyThrowException<K, V> implements EmptyPolicy<K, V>
	{
		@Override
		public V onEmpty(K k)
		{
			throw new NoSuchElementException(Objects.toString(k));
		}
	}
	
	public static class OnEmptyReturnNull<K, V> implements EmptyPolicy<K, V>
	{
		@Override
		public V onEmpty(K k)
		{
			return null;
		}
	}
	
	public static class OnEmptySupply<K, V> implements EmptyPolicy<K, V>
	{
		private final Function<K, V> mCallback;
		
		public OnEmptySupply(Function<K, V> f)
		{
			mCallback = f;
		}
		
		@Override
		public V onEmpty(K k)
		{
			return mCallback.apply(k);
		}
	}

	public static class OnOverwriteReplace<K, V> implements OverwritePolicy<K, V>
	{
		@Override
		public V onOverwrite(K k, V o, V n)
		{
			return n;
		}
	}
	
	public static class OnOverwriteRetain<K, V> implements OverwritePolicy<K, V>
	{
		@Override
		public V onOverwrite(K k, V o, V n)
		{
			return o;
		}
	}
	
	public static class OnOverwriteThrowException<K, V> implements OverwritePolicy<K, V>
	{
		@Override
		public V onOverwrite(K k, V o, V n)
		{
			throw new UnsupportedOperationException(Objects.toString(k) + ": " + Objects.toString(o) + " -> " + Objects.toString(n));
		}
	}
}
