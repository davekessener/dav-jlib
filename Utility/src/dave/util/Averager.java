package dave.util;

public class Averager
{
	private double mValue = 0;
	private int mCount = 0;
	private double mMin = Double.MAX_VALUE, mMax = -Double.MAX_VALUE;
	
	public void add(double v)
	{
		mValue += v;
		++mCount;
		
		if(v < mMin) mMin = v;
		if(v > mMax) mMax = v;
	}
	
	public double min() { return mMin; }
	public double max() { return mMax; }
	
	public void add(int c, double v)
	{
		for(int i = 0 ; i < c ; ++i)
		{
			add(v);
		}
	}
	
	public double get()
	{
		if(mCount <= 0)
			throw new IllegalStateException();
		
		return mValue / mCount;
	}
	
	public int count()
	{
		return mCount;
	}
	
	public void reset()
	{
		mValue = 0;
		mCount = 0;
	}
}
