package dave.util.relay;

public class OverloadException extends RuntimeException
{
	private static final long serialVersionUID = -3179623679788725618L;
	
	public OverloadException(Throwable e) { super(e); }
	public OverloadException(String e) { super(e); }
}