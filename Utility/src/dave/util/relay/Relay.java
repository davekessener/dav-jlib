package dave.util.relay;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.function.Predicate;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Relay
{
	private final Object[] mRef;

	protected Relay( )
	{
		mRef = new Object[] { this };
	}

	public Relay(Object ... r)
	{
		if(r == null || r.length == 0)
			throw new NullPointerException();
		
		mRef = Arrays.copyOf(r, r.length);
	}

	@SuppressWarnings("unchecked")
	public <R> R call(Object ... a)
	{
		try
		{
			return (R) invoke(Stream.of(mRef)
				.flatMap(self -> Stream.of(self.getClass().getDeclaredMethods())
					.map(m -> new Container<>(self, m)))
				.filter(e -> e.deferred.isAnnotationPresent(Overloaded.class))
				.filter(e -> e.deferred.getParameterCount() == a.length)
				.filter(new TypeFilter(a))
				.map(e -> new Container<>(e.self, new WeightedOption(calculateFitness(e.deferred, a), e.deferred)))
				.filter(e -> e.deferred.valid())
				.sorted((x, y) -> Integer.compare(x.deferred.weight, y.deferred.weight))
				.findFirst().get(), a);
		}
		catch(Exception e)
		{
			throw new OverloadException(e);
		}
	}
	
	private Object invoke(Container<WeightedOption> c, Object[] args) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException
	{
		c.deferred.method.setAccessible(true);
		
		return c.deferred.method.invoke(c.self, args);
	}

	private static int calculateFitness(Method m, Object[] a)
	{
		Class<?>[] p = m.getParameterTypes();

		if(p.length != a.length)
			throw new IllegalStateException("" + p.length + " != " + a.length);
		
		return (int) IntStream.range(0, a.length)
			.map(i -> quantifyRelation(p[i], a[i].getClass()))
			.sum();
	}

	private static int quantifyRelation(Class<?> parent, Class<?> child)
	{
		if(!parent.isAssignableFrom(child))
			return -1;
		
		int r = 0;
		while(child != null)
		{
			if(parent.equals(child))
				break;
			
			++r;
			
			if(Stream.of(child.getInterfaces()).anyMatch(i -> parent.equals(i)))
				break;
			
			child = child.getSuperclass();
		}

		if(child == null)
			return -1;
		
		return r;
	}
	
	private static class Container<T>
	{
		public final Object self;
		public final T deferred;
		
		public Container(Object self, T method)
		{
			this.self = self;
			this.deferred = method;
		}
	}

	private static class WeightedOption
	{
		public final int weight;
		public final Method method;

		public WeightedOption(int w, Method m)
		{
			this.weight = w;
			this.method = m;
		}
		
		public boolean valid( ) { return weight >= 0; }
	}

	private static class TypeFilter implements Predicate<Container<Method>>
	{
		private final Object[] mArgs;

		public TypeFilter(Object[]  a)
		{
			mArgs = a;
		}

		@Override
		public boolean test(Container<Method> e)
		{
			Class<?>[] p = e.deferred.getParameterTypes();

			if(mArgs.length != p.length)
				return false;
			
			for(int i = 0 ; i < p.length ; ++i)
			{
				if(!p[i].isAssignableFrom(mArgs[i].getClass()))
					return false;
			}

			return true;
		}
	}
}
