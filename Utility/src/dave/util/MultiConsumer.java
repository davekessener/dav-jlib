package dave.util;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

public class MultiConsumer<T> implements Consumer<T>
{
	private final List<Consumer<T>> mConsumers;
	
	@SafeVarargs
	public MultiConsumer(Consumer<T> ... c)
	{
		mConsumers = Arrays.asList(c);
	}
	
	public MultiConsumer<T> add(Consumer<T> c)
	{
		mConsumers.add(c);
		
		return this;
	}

	@Override
	public void accept(T t)
	{
		for(Consumer<T> c : mConsumers)
		{
			c.accept(t);
		}
	}
}
