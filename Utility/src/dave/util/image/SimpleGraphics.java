package dave.util.image;

public class SimpleGraphics implements Window
{
	private final Canvas mCanvas;
	private final ColorBuffer mBufColor;
	private Color mFill, mStroke;
	
	public SimpleGraphics(Canvas c)
	{
		if(c == null)
			throw new NullPointerException();
		
		mCanvas = c;
		mFill = Colors.WHITE;
		mStroke = Colors.BLACK;
		
		mBufColor = (ColorBuffer) mCanvas.get(ColorBuffer.ID);
	}
	
	public void setFill(Color c) { mFill = c; }
	public void setStroke(Color c) { mStroke = c; }
	
	@Override
	public int width()
	{
		return mCanvas.width();
	}
	
	@Override
	public int height()
	{
		return mCanvas.height();
	}
	
	public void drawPixel(int x, int y, Color c)
	{
		mBufColor.set(x, y, c);
	}
	
	public void strokeRect(int x0, int y0, int x1, int y1)
	{
		for(int x = x0 ; x < x1 ; ++x)
		{
			drawPixel(x, y0, mStroke);
			drawPixel(x, y1 - 1, mStroke);
		}
		
		for(int y = y0 ; y < y1 ; ++y)
		{
			drawPixel(x0, y, mStroke);
			drawPixel(x0, y1 - 1, mStroke);
		}
	}
	
	public void fillRect(int x0, int y0, int x1, int y1)
	{
		for(int y = y0 ; y < y1 ; ++y)
		{
			for(int x = x0 ; x < x1 ; ++x)
			{
				drawPixel(x, y, mFill);
			}
		}
	}
	
	public void fillCircle(int x0, int y0, int r)
	{
		for(int y = y0 - r ; y < y0 + r ; ++y)
		{
			for(int x = x0 - r ; x < x0 + r ; ++x)
			{
				int dx = x - x0, dy = y - y0;
				
				if(dx * dx + dy * dy <= r * r)
				{
					drawPixel(x, y, mFill);
				}
			}
		}
	}
}
