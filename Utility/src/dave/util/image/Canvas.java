package dave.util.image;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

public class Canvas implements Window
{
	private final Map<String, Buffer> mBuffers;
	private final int mWidth, mHeight;
	
	private Canvas(int w, int h, List<Buffer> bufs)
	{
		mBuffers = new HashMap<>();
		
		mWidth = w;
		mHeight = h;
		
		bufs.forEach(b -> {
			if(mBuffers.put(b.getID(), b) != null)
				throw new IllegalStateException(b.getID());
		});
	}
	
	public Buffer get(String id)
	{
		Buffer b = mBuffers.get(id);
		
		if(b == null)
			throw new NoSuchElementException(id);
		
		return b;
	}
	
	public boolean has(String id)
	{
		return mBuffers.containsKey(id);
	}
	
	@Override
	public int width()
	{
		return mWidth;
	}

	@Override
	public int height()
	{
		return mHeight;
	}
	
	public static Canvas build(int w, int h)
	{
		return (new Builder(w, h)).addSimpleColorBuffer().build();
	}
	
	public static class Builder
	{
		private final List<Buffer> mBufs;
		private final int mWidth, mHeight;
		
		public Builder(int w, int h)
		{
			if(w <= 0 || h <= 0)
				throw new IllegalArgumentException();
		
			mBufs = new ArrayList<>();
			mWidth = w;
			mHeight = h;
		}
		
		public Builder add(Buffer b)
		{
			if(b.width() != mWidth || b.height() != mHeight)
				throw new IllegalArgumentException();
			
			mBufs.add(b);
			
			return this;
		}
		
		public Builder addSimpleColorBuffer()
		{
			mBufs.add(new ColorBuffer(mWidth, mHeight));
			
			return this;
		}
		
		public Canvas build()
		{
			return new Canvas(mWidth, mHeight, mBufs);
		}
	}
}
