package dave.util.image;

import java.util.function.Consumer;

public interface Converter
{
	public abstract int bpp( );
	public abstract void convert(Canvas c, Consumer<Byte> f);
	
	public default byte[] convert(Canvas c)
	{
		byte[] buffer = new byte[bpp() * c.width() * c.height()];
		int[] index = new int[] { 0 };
		
		convert(c, v -> buffer[index[0]++] = v);
		
		return buffer;
	}
}
