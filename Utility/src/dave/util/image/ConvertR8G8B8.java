package dave.util.image;

import java.util.function.Consumer;

public class ConvertR8G8B8 implements Converter
{
	@Override
	public int bpp()
	{
		return 3;
	}

	@Override
	public void convert(Canvas c, Consumer<Byte> f)
	{
		ColorBuffer color = (ColorBuffer) c.get(ColorBuffer.ID);
		
		for(int y = 0 ; y < c.height() ; ++y)
		{
			for(int x = 0 ; x < c.width() ; ++x)
			{
				Color v = color.get(x, y);

				f.accept((byte) (v.red * 0xFF));
				f.accept((byte) (v.green * 0xFF));
				f.accept((byte) (v.blue * 0xFF));
			}
		}
	}
}
