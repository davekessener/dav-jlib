package dave.util.image;

public class Color
{
	public final double red, green, blue;
	public final double alpha;

	public Color(double red, double green, double blue) { this(red, green, blue, 1); }
	public Color(double red, double green, double blue, double alpha)
	{
		if(red < 0 || red > 1)
			throw new IllegalArgumentException();
		
		if(green < 0 || green > 1)
			throw new IllegalArgumentException();
		
		if(blue < 0 || blue > 1)
			throw new IllegalArgumentException();
		
		if(alpha < 0 || alpha > 1)
			throw new IllegalArgumentException();
		
		this.red = red;
		this.green = green;
		this.blue = blue;
		this.alpha = alpha;
	}
	
	public static Color monochrome(double v) { return new Color(v, v, v); }
}
