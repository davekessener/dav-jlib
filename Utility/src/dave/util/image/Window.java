package dave.util.image;

public interface Window
{
	public abstract int width( );
	public abstract int height( );
}
