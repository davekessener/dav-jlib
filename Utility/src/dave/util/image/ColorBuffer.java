package dave.util.image;

public class ColorBuffer implements Buffer
{
	private final Color[] mBuffer;
	private final int mWidth, mHeight;
	
	public ColorBuffer(int w, int h) { this(w, h, Colors.WHITE); }
	public ColorBuffer(int w, int h, Color c)
	{
		if(w <= 0 || h <= 0)
			throw new IllegalArgumentException();
		
		mBuffer = new Color[w * h];
		mWidth = w;
		mHeight = h;
		
		for(int i = 0 ; i < w * h ; ++i)
		{
			mBuffer[i] = c;
		}
	}
	
	public Color get(int x, int y)
	{
		check(x, y);
		
		return mBuffer[x + y * mWidth];
	}
	
	public void set(int x, int y, Color c)
	{
		check(x, y);
		
		if(c == null)
			throw new NullPointerException();
		
		mBuffer[x + y * mWidth] = c;
	}
	
	private void check(int x, int y)
	{
		if(x < 0 || x >= mWidth || y < 0 || y >= mHeight)
			throw new IllegalArgumentException(String.format("%d, %d not in %d x %d", x, y, mWidth, mHeight));
	}

	@Override
	public String getID()
	{
		return ID;
	}

	@Override
	public int width()
	{
		return mWidth;
	}

	@Override
	public int height()
	{
		return mHeight;
	}
	
	public static final String ID = "COLOR";
}
