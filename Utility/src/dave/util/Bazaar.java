package dave.util;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Stream;

import dave.util.property.Property;
import dave.util.property.SimpleProperty;

public interface Bazaar<K>
{
	public abstract <T> void offer(K k, Function<Arguments<K>, T> f, Options o);
	public abstract <T> T acquire(K k, Object ... a);
	
	public default <T> void offer(K k, Function<Arguments<K>, T> f) { offer(k, f, options()); }
	
	public static Options options()
	{
		return new Options();
	}
	
	public static class Options
	{
		private int mPriority;
		private boolean mCanBeBusy;
		
		public Options()
		{
			mPriority = 0;
			mCanBeBusy = true;
		}
		
		public int priority() { return mPriority; }
		public boolean canBeBusy() { return mCanBeBusy; }
		
		public Options setPriority(int p) { mPriority = p; return this; }
		public Options setOccupiable(boolean f) { mCanBeBusy = f; return this; }
	}
	
	public static class Arguments<K>
	{
		private final Bazaar<K> mSelf;
		private final Object[] mArgs;
		private final K mRequest;
		
		protected Arguments(Bazaar<K> self, K request, Object ... o)
		{
			mSelf = self;
			mRequest = request;
			mArgs = o;
		}
		
		public int size() { return mArgs.length; }
		public K request() { return mRequest; }
		
		@SuppressWarnings("unchecked")
		public <T> T get(int i)
		{
			return (T) mArgs[i];
		}
		
		public Bazaar<K> bazaar()
		{
			return mSelf;
		}
	}
	
	public static class StickyWrapper<K, T> implements Function<Arguments<K>, T>
	{
		private final Property<T> mCache;
		private final Function<Arguments<K>, T> mCallback;
		private boolean mDone;
		
		public StickyWrapper(Function<Arguments<K>, T> f)
		{
			mCache = new SimpleProperty<>();
			mCallback = f;
			mDone = false;
		}
		
		@Override
		public T apply(Arguments<K> a)
		{
			if(!mDone)
			{
				mDone = true;
				
				mCache.set(mCallback.apply(a));
			}
			
			return mCache.get();
		}
	}
	
	public static class Failure extends RuntimeException
	{
		private static final long serialVersionUID = -1046587311374967555L;
		
		private final List<Refusal> mRefusals;
		
		protected Failure(List<Refusal> f)
		{
			mRefusals = f;
		}
		
		public int count() { return mRefusals.size(); }
		public Stream<Refusal> refusals() { return mRefusals.stream(); }
	}
	
	public static class Refusal extends RuntimeException
	{
		private static final long serialVersionUID = -3791746713374903741L;
		
		public Refusal(String msg)
		{
			super(msg);
		}
	}
}
