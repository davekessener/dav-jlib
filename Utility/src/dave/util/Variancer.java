package dave.util;

import java.util.ArrayList;
import java.util.List;

public class Variancer
{
	private final Averager mAvg;
	private final List<Double> mValues;
	
	public Variancer()
	{
		mAvg = new Averager();
		mValues = new ArrayList<>(100);
	}
	
	public void add(double v)
	{
		mAvg.add(v);
		mValues.add(v);
	}
	
	public double get()
	{
		double avg = mAvg.get();
		double v = 0;
		
		for(Double e : mValues)
		{
			double t = e - avg;
			
			v += t * t;
		}
		
		return v / mAvg.count();
	}
	
	public double mean() { return mAvg.get(); }
	public double variance() { return get(); }
	public double deviation() { return Math.sqrt(variance()); }
	
	public double min() { return mAvg.min(); }
	public double max() { return mAvg.max(); }
}
