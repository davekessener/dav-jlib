package dave.util;

public class BufferedProducer<T> implements Producer<T>
{
	private final Producer<T> mCallback;
	private final T[] mBuffer;
	private int mIdx;
	
	@SafeVarargs
	public BufferedProducer(Producer<T> f, T ... b)
	{
		mCallback = f;
		mBuffer = b;
		mIdx = 0;
	}

	@Override
	public T produce( )
	{
		if(mIdx < mBuffer.length)
		{
			return mBuffer[mIdx++];
		}
		else
		{
			return mCallback.produce();
		}
	}
}
