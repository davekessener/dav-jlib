package dave.util;

import dave.util.math.Matrix;
import dave.util.math.Vec2;

public final class MathUtils
{
	public static double lerp(double a, double b, double p)
	{
		if(p < 0 || p > 1)
			throw new IllegalArgumentException("" + p);
		
		return a + p * (b - a);
	}
	
	public static double clamp(double v) { return clamp(0, v, 1); }
	public static double clamp(double min, double v, double max) { return (v < min) ? min : ((v > max) ? max : v); }
	
	public static Matrix rotation2D(double a)
	{
		double s = Math.sin(-a), c = Math.cos(-a);
		
		return new Matrix(2, 2, new double[] { c, -s, s, c });
	}
	
	public static Vec2 rotate2D(Vec2 v, double a)
	{
		return rotation2D(a).mul(Matrix.from(v)).toVec2();
	}
	
	public static double angle(Vec2 f, Vec2 v)
	{
		double t1 = f.x * v.y - f.y * v.x;
		double t2 = f.x * v.x + f.y * v.y;
		
		return -Math.atan2(t1, t2);
	}
	
	public static double r2d(double r) { return r * 180 / Math.PI; }
	public static double d2r(double a) { return a * Math.PI / 180; }
	
	private MathUtils( ) { }
}
