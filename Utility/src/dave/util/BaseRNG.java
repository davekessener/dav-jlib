package dave.util;

public abstract class BaseRNG implements RNG
{
	private double mBuffer;
	private boolean mDone;
	
	protected BaseRNG()
	{
		mBuffer = 0;
		mDone = false;
	}
	
	@Override
	public double nextGauss( )
	{
		if(mDone)
		{
			mDone = false;
			
			return mBuffer;
		}
		else
		{
			double u0, u1, s;
			
			do
			{
				u0 = nextDouble() * 2 - 1;
				u1 = nextDouble() * 2 - 1;
				s = u0 * u0 + u1 * u1;
			}
			while(s >= 1);
			
			double t = Math.sqrt(-2 * Math.log(s) / s);
			
			mDone = true;
			mBuffer = u1 * t;
			
			return u0 * t;
		}
	}
}
