package dave.util.publish;

public interface Event
{
	public abstract Licence cause( );
}
