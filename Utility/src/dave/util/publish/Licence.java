package dave.util.publish;

public final class Licence
{
	public final long id;
	
	public Licence(Long id)
	{
		this.id = id;
	}
	
	@Override
	public int hashCode()
	{
		return Long.hashCode(id) ^ 0xa26763f2;
	}
	
	@Override
	public boolean equals(Object o)
	{
		if(o instanceof Licence)
		{
			return id == ((Licence) o).id;
		}
		
		return false;
	}
	
	@Override
	public String toString()
	{
		return "Licence-" + id;
	}
}
