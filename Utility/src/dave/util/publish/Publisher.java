package dave.util.publish;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

import dave.util.Actor;
import dave.util.SevereException;
import dave.util.publish.event.RejectEvent;

public class Publisher implements Actor
{
	private final Map<Licence, Subscriber> mSubscribers;
	private final Map<String, List<Subscription>> mSubscriptions;
	private final BlockingQueue<Runnable> mBacklog;
	private final Thread mThread;
	private long mNextID;
	private boolean mRunning;
	
	public Publisher()
	{
		mSubscribers = new HashMap<>();
		mSubscriptions = new HashMap<>();
		mBacklog = new LinkedBlockingQueue<>();
		mThread = new Thread(this::run);
		
		mNextID = 0;
		mRunning = false;
	}
	
	@Override
	public void start()
	{
		mRunning = true;
		
		mThread.start();
	}
	
	@Override
	public void stop()
	{
		mRunning = false;
		
		try
		{
			mThread.join(100);
			
			if(mThread.isAlive())
				mThread.interrupt();
			
			mThread.join();
		}
		catch(InterruptedException e)
		{
			throw new SevereException(e);
		}
	}
	
	public Licence register(Subscriber s)
	{
		Licence id = new Licence(mNextID++);
		
		mSubscribers.put(id, s);
		
		return id;
	}

	public void subscribe(Licence id, Consumer<PublishEvent> cb, String topic, Class<?>[] params, TagFilter f, int priority)
	{
		subscriber(id);
		
		Subscription s = new Subscription(id, cb, params, f, priority);
		List<Subscription> subs = subscriptions(topic);
		boolean inserted = false;
		
		for(ListIterator<Subscription> i = subs.listIterator() ; i.hasNext() ;)
		{
			Subscription e = i.next();
			
			if(e.priority < priority)
			{
				i.previous();
				i.add(s);
				
				inserted = true;
				
				break;
			}
		}
		
		if(!inserted)
		{
			subs.add(s);
		}
	}
	
	public void publish(Licence id, String topic, Object[] content, String ... tags)
	{
		subscriber(id);
		
		PublishEvent e = new CancalablePublishEvent(id, topic, content, tags);
		
		mBacklog.add(() -> send(e));
	}
	
	public void run()
	{
		try
		{
			while(mRunning)
			{
				Runnable callback = mBacklog.poll(50, TimeUnit.MILLISECONDS);
				
				if(callback == null)
					continue;
				
				callback.run();
			}
		}
		catch(InterruptedException e)
		{
			throw new SevereException(e);
		}
	}
	
	private void send(PublishEvent e)
	{
		List<Subscription> subs = subscriptions(e.topic());
		
		for(Subscription s : subs)
		{
			if(e.consumed())
				break;
			
			if(s.applies(e))
			{
				s.callback.accept(e);
			}
		}
	}
	
	private Subscriber subscriber(Licence id)
	{
		Subscriber s = mSubscribers.get(id);
		
		if(s == null)
			throw new IllegalArgumentException("No subscriber " + id);
		
		return s;
	}
	
	private List<Subscription> subscriptions(String topic)
	{
		List<Subscription> subs = mSubscriptions.get(topic);
		
		if(subs == null)
		{
			mSubscriptions.put(topic, subs = new ArrayList<>());
		}
		
		return subs;
	}
	
	private static class Subscription
	{
		public final Licence id;
		public final Consumer<PublishEvent> callback;
		public final Class<?>[] parameters;
		public final TagFilter filter;
		public final int priority;
		
		public Subscription(Licence id, Consumer<PublishEvent> callback, Class<?>[] parameters, TagFilter filter, int priority)
		{
			this.id = id;
			this.callback = callback;
			this.parameters = parameters;
			this.filter = filter;
			this.priority = priority;
		}
		
		public boolean applies(PublishEvent e)
		{
			if(id.equals(e.cause()))
				return false;
			
			if(parameters != null)
			{
				if(e.arguments().length != parameters.length)
					return false;
				
				for(int i = 0 ; i < parameters.length ; ++i)
				{
					if(!parameters[i].isAssignableFrom(e.arguments()[i].getClass()))
						return false;
				}
			}
			
			if(!filter.test(e.tags()))
				return false;
			
			return true;
		}
	}
	
	private class CancalablePublishEvent extends PublishEvent
	{
		public CancalablePublishEvent(Licence id, String t, Object[] a, String[] tags)
		{
			super(id, t, a, tags);
		}

		@Override
		public void reject(Licence id, Object note)
		{
			if(consumed())
			{
				throw new IllegalStateException("Can't reject a consumed event!");
			}
			else
			{
				consume();
				
				RejectEvent e = new RejectEvent(this, id, note);
				
				mBacklog.add(() -> subscriber(this.cause()).onRejection(e));
			}
		}
	}
}
