package dave.util.publish;

public abstract class PublishEvent implements Event
{
	private final Licence mPublisher;
	private final String mTopic;
	private final Object[] mArguments;
	private final String[] mTags;
	private boolean mConsumed;
	
	public PublishEvent(Licence id, String t, Object[] a, String[] tags)
	{
		mPublisher = id;
		mTopic = t;
		mArguments = a;
		mTags = tags;
		mConsumed = false;
	}
	
	@Override
	public Licence cause()
	{
		return mPublisher;
	}
	
	public String topic() { return mTopic; }
	public Object[] arguments() { return mArguments; }
	public String[] tags() { return mTags; }
	public boolean consumed() { return mConsumed; }
	
	public void reject(Licence id) { reject(id, null); }
	public abstract void reject(Licence id, Object note);
	
	@SuppressWarnings("unchecked")
	public <T> T argument(int i)
	{
		return (T) mArguments[i];
	}
	
	public String[] tags(String ... t)
	{
		String[] tags = new String[mTags.length + t.length];
		
		for(int i = 0 ; i < mTags.length ; ++i)
		{
			tags[i] = mTags[i];
		}
		
		for(int i = 0 ; i < t.length ; ++i)
		{
			tags[i + mTags.length] = t[i];
		}
		
		return tags;
	}
	
	public void consume()
	{
		mConsumed = true;
	}
}
