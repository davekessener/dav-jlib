package dave.util.publish.event;

import dave.util.publish.Licence;
import dave.util.publish.PublishEvent;

public class RejectEvent
{
	private final PublishEvent mEvent;
	private final Licence mCause;
	private final Object mNote;
	
	public RejectEvent(PublishEvent e, Licence id) { this(e, id, null); }
	public RejectEvent(PublishEvent e, Licence id, Object msg)
	{
		mEvent = e;
		mCause = id;
		mNote = msg;
	}
	
	public PublishEvent event() { return mEvent; }
	public Licence cause() { return mCause; }
	public boolean hasNoteAttached() { return mNote != null; }
	
	@SuppressWarnings("unchecked")
	public <T> T note()
	{
		return (T) mNote;
	}
}
