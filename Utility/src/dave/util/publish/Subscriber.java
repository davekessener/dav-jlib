package dave.util.publish;

import dave.util.publish.event.DieEvent;
import dave.util.publish.event.LiveEvent;
import dave.util.publish.event.RejectEvent;

public interface Subscriber
{
	public default void onLife(LiveEvent e) { }
	public default void onDeath(DieEvent e) { }
	public default void onRejection(RejectEvent e) { }
}
