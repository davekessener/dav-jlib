package dave.util.publish;

public interface TagFilter
{
	public abstract boolean test(String[] tags);
}
