package dave.util.publish;

public final class TagFilters
{
	public static TagFilter yes()
	{
		return t -> true;
	}
	
	public static TagFilter no()
	{
		return t -> t.length == 0;
	}
	
	public static TagFilter contains(String ... tags)
	{
		return t -> {
			for(int i = 0 ; i < tags.length ; ++i)
			{
				boolean f = false;
				
				for(int j = 0 ; j < t.length ; ++j)
				{
					if(tags[i].equals(t[j]))
					{
						f = true;
						
						break;
					}
				}
				
				if(!f)
					return false;
			}
			
			return true;
		};
	}
	
	public static TagFilter exact(String ... tags)
	{
		TagFilter p = contains(tags);
		
		return t -> (t.length == tags.length) && p.test(t);
	}
	
	public static TagFilter not(TagFilter f)
	{
		return t -> !f.test(t);
	}
	
	@SafeVarargs
	public static TagFilter and(TagFilter ... f)
	{
		return t -> {
			for(int i = 0 ; i < f.length ; ++i)
			{
				if(!f[i].test(t))
					return false;
			}
			
			return true;
		};
	}
	
	@SafeVarargs
	public static TagFilter or(TagFilter ... f)
	{
		return t -> {
			for(int i = 0 ; i < f.length ; ++i)
			{
				if(f[i].test(t))
					return true;
			}
			
			return false;
		};
	}
}
