package dave.util.actor;

import java.util.Map;
import java.util.function.Consumer;

public interface LookupAgent extends Agent
{
	public abstract Map<String, Consumer<Message>> getHandlers( );
	
	public default void registerHandler(String msg, Consumer<Message> f)
	{
		getHandlers().put(msg, f);
	}
	
	@Override
	public default void accept(Message msg) { handleMessage(msg); }
	
	public default void handleMessage(Message msg)
	{
		Consumer<Message> f = getHandlers().get(msg.message);
		
		if(f == null)
			throw new IllegalArgumentException("No handler for message: " + msg);
		
		f.accept(msg);
	}
}
