package dave.util.actor;

import java.util.function.Consumer;

import dave.util.Identifiable;

public interface Agent extends Identifiable, Consumer<Message>
{
}
