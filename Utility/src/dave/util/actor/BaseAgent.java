package dave.util.actor;

public abstract class BaseAgent implements Agent
{
	private final String mID;
	
	public BaseAgent(String id)
	{
		mID = id;
	}

	@Override
	public String getID()
	{
		return mID;
	}
}
