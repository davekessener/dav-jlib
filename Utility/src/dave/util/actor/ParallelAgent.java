package dave.util.actor;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import dave.util.Actor;
import dave.util.SevereException;
import dave.util.Utils;

public abstract class ParallelAgent extends BaseAgent implements Actor
{
	private final Thread mThread;
	private final BlockingQueue<Message> mBacklog;
	private boolean mRunning;
	
	public ParallelAgent(String id)
	{
		super(id);
		
		mThread = new Thread(this::run);
		mBacklog = new LinkedBlockingQueue<>();
		mRunning = false;
	}
	
	protected abstract void process(Message msg);

	@Override
	public void accept(Message t)
	{
		mBacklog.add(t);
	}
	
	@Override
	public void start()
	{
		mRunning = true;
		mThread.start();
	}
	
	@Override
	public void stop()
	{
		mRunning = false;
		Utils.run(() -> mThread.join());
	}
	
	private void run()
	{
		try
		{
			while(mRunning)
			{
				Message msg = mBacklog.poll(5, TimeUnit.MILLISECONDS);
				
				if(msg == null)
					continue;
				
				process(msg);
			}
		}
		catch(InterruptedException e)
		{
			throw new SevereException(e);
		}
	}
}
