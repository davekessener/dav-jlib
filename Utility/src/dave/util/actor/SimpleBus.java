package dave.util.actor;

import java.util.ArrayDeque;
import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;

public class SimpleBus implements Bus
{
	private final Map<String, Agent> mAgents;
	private final Queue<Message> mBacklog;
	
	public SimpleBus() { this(new HashMap<>(), new ArrayDeque<>()); }
	public SimpleBus(Map<String, Agent> a, Queue<Message> l)
	{
		mAgents = a;
		mBacklog = l;
	}
	
	@Override
	public void register(Agent a)
	{
		if(mAgents.put(a.getID(), a) != null)
			throw new IllegalArgumentException("Duplicate agent: " + a.getID());
	}
	
	@Override
	public void flush()
	{
		int c = mBacklog.size();
		
		for(int i = 0 ; i < c ; ++i)
		{
			Message msg = mBacklog.poll();
			Agent a = mAgents.get(msg.to);
			
			if(a == null)
				throw new IllegalStateException("Cannot deliver: " + msg);
			
			a.accept(msg);
		}
	}
	
	@Override
	public void send(Agent from, String to, String msg, Object payload)
	{
		mBacklog.add(new Message(from.getID(), to, msg, payload));
	}
	
	public static SimpleBus createThreadSafeBus()
	{
		return new SimpleBus(new ConcurrentHashMap<>(), new LinkedBlockingQueue<>());
	}
}
