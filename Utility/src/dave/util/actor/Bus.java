package dave.util.actor;

public interface Bus
{
	public abstract void register(Agent a);
	public abstract void flush( );
	public abstract void send(Agent from, String to, String msg, Object payload);
}
