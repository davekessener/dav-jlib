package dave.util.actor;

import java.util.Objects;

public class Message
{
	public final String from, to;
	public final String message;
	public final Object payload;
	
	public Message(String from, String to, String message, Object payload)
	{
		this.from = from;
		this.to = to;
		this.message = message;
		this.payload = payload;
	}
	
	@SuppressWarnings("unchecked")
	public <T> T payload()
	{
		return (T) this.payload;
	}
	
	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		
		sb.append("msg$")
		  .append(message)
		  .append("{from=")
		  .append(from)
		  .append(", to=")
		  .append(to);
		
		if(payload != null)
		{
			sb.append(", payload=").append(payload);
		}
		
		sb.append("}");
		
		return sb.toString();
	}
	
	@Override
	public int hashCode()
	{
		return (from.hashCode() * 3) ^ (to.hashCode() * 5) ^ (message.hashCode() * 7) ^ (Objects.hashCode(payload) * 11);
	}
	
	@Override
	public boolean equals(Object o)
	{
		if(o instanceof Message)
		{
			Message msg = (Message) o;
			
			return from.equals(msg.from) && to.equals(msg.to) && message.equals(msg.message) && Objects.deepEquals(payload, msg.payload);
		}
		
		return false;
	}
}
