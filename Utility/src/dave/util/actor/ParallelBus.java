package dave.util.actor;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;

import dave.util.Actor;
import dave.util.Utils;

public class ParallelBus extends SimpleBus implements Actor
{
	private final Thread mThread;
	private boolean mRunning;
	
	public ParallelBus()
	{
		super(new ConcurrentHashMap<>(), new LinkedBlockingQueue<>());
		
		mThread = new Thread(this::run);
		mRunning = false;
	}
	
	@Override
	public void start()
	{
		mRunning = true;
		mThread.start();
	}
	
	@Override
	public void stop()
	{
		mRunning = false;
		Utils.run(() -> mThread.join());
	}
	
	private void run()
	{
		while(mRunning)
		{
			this.flush();
			
			Utils.sleep(1);
		}
	}
}
