package dave.util.actor;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

public class SimpleLookupAgent extends BaseAgent implements LookupAgent
{
	private final Map<String, Consumer<Message>> mLUT;
	
	public SimpleLookupAgent(String id) { this(id, new HashMap<>()); }
	public SimpleLookupAgent(String id, Map<String, Consumer<Message>> m)
	{
		super(id);
		
		mLUT = m;
	}

	@Override
	public Map<String, Consumer<Message>> getHandlers()
	{
		return mLUT;
	}
}
