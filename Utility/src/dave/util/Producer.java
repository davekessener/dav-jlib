package dave.util;

public interface Producer<T>
{
	public abstract T produce( );
}
