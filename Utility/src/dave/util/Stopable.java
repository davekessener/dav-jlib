package dave.util;

public interface Stopable
{
	public default void stop( ) { }
}
