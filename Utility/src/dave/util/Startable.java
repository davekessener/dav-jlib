package dave.util;

public interface Startable
{
	public default void start( ) { }
}
