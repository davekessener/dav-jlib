package dave.util.service;

import dave.util.property.Property;
import dave.util.property.SimpleProperty;

public class PassivePromise<T> implements Promise<T>
{
	private final Property<T> mContent;
	private boolean mDone;
	
	public PassivePromise()
	{
		mContent = new SimpleProperty<>();
		mDone = false;
	}
	
	public void set(T v)
	{
		if(mDone)
		{
			throw new IllegalStateException();
		}
		
		mContent.set(v);
		
		mDone = true;
	}

	@Override
	public T get( )
	{
		if(!mDone)
		{
			throw new IllegalStateException();
		}
		
		return mContent.get();
	}

	@Override
	public boolean ready( )
	{
		return mDone;
	}
}
