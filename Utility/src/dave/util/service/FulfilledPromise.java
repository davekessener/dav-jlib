package dave.util.service;

public class FulfilledPromise<T> implements Promise<T>
{
	private final T mContent;
	
	public FulfilledPromise(T o)
	{
		mContent = o;
	}

	@Override
	public T get( )
	{
		return mContent;
	}

	@Override
	public boolean ready( )
	{
		return true;
	}
}
