package dave.util.service;

import java.util.function.Supplier;

import dave.util.property.Property;
import dave.util.property.SimpleProperty;

public class DeferredPromise<T> implements Promise<T>
{
	private final Property<T> mContent;
	private final Supplier<T> mProvider;
	private boolean mDone;
	
	public DeferredPromise(Supplier<T> f)
	{
		mContent = new SimpleProperty<>();
		mProvider = f;
		mDone = false;
	}

	@Override
	public T get( )
	{
		if(!mDone)
		{
			mContent.set(mProvider.get());
			
			mDone = true;
		}
		
		return mContent.get();
	}

	@Override
	public boolean ready( )
	{
		return true;
	}
}
