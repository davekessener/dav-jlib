package dave.util.service;

public interface Promise<T>
{
	public abstract T get( );
	public abstract boolean ready( );
}
