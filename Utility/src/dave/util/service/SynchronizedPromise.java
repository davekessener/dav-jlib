package dave.util.service;

import dave.util.SevereException;

public class SynchronizedPromise<T> extends PassivePromise<T>
{
	@Override
	public synchronized void set(T v)
	{
		super.set(v);
		
		this.notifyAll();
	}
	
	@Override
	public synchronized T get( )
	{
		while(!ready())
		{
			try
			{
				this.wait();
			}
			catch(InterruptedException e)
			{
				throw new SevereException(e);
			}
		}
		
		return super.get();
	}
}
