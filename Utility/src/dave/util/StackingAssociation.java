package dave.util;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.ListIterator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Stream;

import dave.util.math.IVec2;
import dave.util.math.IVec3;
import dave.util.property.NonNullProperty;
import dave.util.property.Property;

public class StackingAssociation<K1, K2, V> implements Association<K2, V>
{
	private final Association<K1, List<Entry<K2, V>>> mSuper;
	private final Function<K2, K1> mConvert;
	private final Comparator<K2> mCompare;
	private final EmptyPolicy<K2, V> mOnEmpty;
	
	protected StackingAssociation(Association<K1, List<Entry<K2, V>>> s, Function<K2, K1> cv, Comparator<K2> cmp, EmptyPolicy<K2, V> f)
	{
		mSuper = s;
		mConvert = cv;
		mCompare = cmp;
		mOnEmpty = f;
	}
	
	public Stream<V> all(K1 k)
	{
		return mSuper.get(k).stream().map(e -> e.value);
	}
	
	@Override
	public boolean contains(K2 k1)
	{
		K1 k2 = mConvert.apply(k1);
		
		return (mSuper.contains(k2)) && mSuper.get(k2).stream().anyMatch(e -> e.position.equals(k1));
	}
	
	@Override
	public void put(K2 k, V v)
	{
		List<Entry<K2, V>> l = mSuper.get(mConvert.apply(k));
		
		for(ListIterator<Entry<K2, V>> i = l.listIterator() ; i.hasNext() ;)
		{
			Entry<K2, V> e = i.next();
			
			if(mCompare.compare(e.position, k) > 0)
			{
				i.previous();
				
				i.add(new Entry<>(k, v));
				
				return;
			}
		}
		
		l.add(new Entry<>(k, v));
	}

	@Override
	public V get(K2 k)
	{
		List<Entry<K2, V>> l = mSuper.get(mConvert.apply(k));
		
		for(Entry<K2, V> e : l)
		{
			if(e.position.equals(k))
			{
				return e.value;
			}
		}
		
		V v = mOnEmpty.onEmpty(k);
		
		if(v != null)
		{
			put(k, v);
		}
		
		return v;
	}

	@Override
	public void remove(K2 k)
	{
		List<Entry<K2, V>> l = mSuper.get(mConvert.apply(k));
		
		l.removeIf(e -> e.position.equals(k));
	}

	@Override
	public Stream<V> stream()
	{
		return mSuper.stream().flatMap(e -> e.stream().map(x -> x.value));
	}
	
	private static class Entry<K, V>
	{
		public final K position;
		public final V value;
		
		public Entry(K position, V value)
		{
			this.position = position;
			this.value = value;
		}
	}
	
	public static class Builder<K1, K2, V>
	{
		private final Property<Association<K1, List<Entry<K2, V>>>> mSuper;
		private final Property<Function<K2, K1>> mConvert;
		private final Property<Comparator<K2>> mCompare;
		private final Property<EmptyPolicy<K2, V>> mOnEmpty;
		
		public Builder(Association<K1, List<Entry<K2, V>>> s, Function<K2, K1> cv, Comparator<K2> f)
		{
			mSuper    = new NonNullProperty<>(s);
			mConvert  = new NonNullProperty<>(cv);
			mCompare  = new NonNullProperty<>(f);
			mOnEmpty  = new NonNullProperty<>(new OnEmptyThrowException<>());
		}
		
		public Builder<K1, K2, V> setSuper(Association<K1, List<Entry<K2, V>>> s) { mSuper.set(s); return this; }
		public Builder<K1, K2, V> setConverter(Function<K2, K1> f) { mConvert.set(f); return this; }
		public Builder<K1, K2, V> setComparator(Comparator<K2> f) { mCompare.set(f); return this; }
		public Builder<K1, K2, V> setOnEmptyPolicy(EmptyPolicy<K2, V> f) { mOnEmpty.set(f); return this; }
		
		public StackingAssociation<K1, K2, V> build()
		{
			return new StackingAssociation<>(mSuper.get(), mConvert.get(), mCompare.get(), mOnEmpty.get());
		}
	}
	
	public static class QuadTreeBuilder<V> extends Builder<IVec2, IVec3, V>
	{
		private final Property<Integer> mSize;
		private final Property<Supplier<List<Entry<IVec3, V>>>> mSupplier;
		private final Property<OverwritePolicy<IVec2, List<Entry<IVec3, V>>>> mOnOverwrite;
		
		public QuadTreeBuilder()
		{
			super(makeQuad(10, defaultSupplier(), defaultOverwritePolicy()), defaultConverter(), defaultComparator());
			
			mSize = new NonNullProperty<>(10);
			mSupplier = new NonNullProperty<>(defaultSupplier());
			mOnOverwrite = new NonNullProperty<>(defaultOverwritePolicy());
		}
		
		public QuadTreeBuilder<V> setSize(int v)
		{
			mSize.set(v);
			setSuper(makeQuad(v, mSupplier.get(), mOnOverwrite.get()));
			
			return this;
		}
		
		public QuadTreeBuilder<V> setSupplier(Supplier<List<Entry<IVec3, V>>> f)
		{
			mSupplier.set(f);
			setSuper(makeQuad(mSize.get(), f, mOnOverwrite.get()));
			
			return this;
		}
		
		public QuadTreeBuilder<V> setOverwritePolicy(OverwritePolicy<IVec2, List<Entry<IVec3, V>>> f)
		{
			mOnOverwrite.set(f);
			setSuper(makeQuad(mSize.get(), mSupplier.get(), f));
			
			return this;
		}
		
		private static <V> QuadTree<List<Entry<IVec3, V>>> makeQuad(int s, Supplier<List<Entry<IVec3, V>>> f, OverwritePolicy<IVec2, List<Entry<IVec3, V>>> oo)
		{
			return (new QuadTree.Builder<List<Entry<IVec3, V>>>())
				.setSize(s)
				.setOnEmpty(new OnEmptySupply<>(p -> f.get()))
				.setOnOverwrite(oo)
				.build();
		}
		
		private static <V> Supplier<List<Entry<IVec3, V>>> defaultSupplier() { return () -> new ArrayList<>(); }
		private static <V> OverwritePolicy<IVec2, List<Entry<IVec3, V>>> defaultOverwritePolicy() { return new OnOverwriteReplace<>(); }
		private static Function<IVec3, IVec2> defaultConverter() { return v -> new IVec2(v.x, v.z); }
		private static Comparator<IVec3> defaultComparator() { return (v1, v2) -> Integer.compare(v1.y, v2.y); }
	}
}
