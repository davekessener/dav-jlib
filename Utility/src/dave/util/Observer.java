package dave.util;

public interface Observer
{
	public abstract void alert(BaseObservable source);
}
