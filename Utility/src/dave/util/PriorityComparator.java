package dave.util;

import java.util.Comparator;

public class PriorityComparator implements Comparator<Prioritizable>
{
	@Override
	public int compare(Prioritizable o0, Prioritizable o1)
	{
		return o0.priority() - o1.priority();
	}
}
