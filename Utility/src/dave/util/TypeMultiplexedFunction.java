package dave.util;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

public class TypeMultiplexedFunction<U, V> implements Function<U, V>
{
	private final Map<Class<? extends U>, Function<U, V>> mCallbacks;
	
	public TypeMultiplexedFunction(Map<Class<? extends U>, Function<U, V>> f)
	{
		mCallbacks = f;
	}
	
	@Override
	public V apply(U c)
	{
		Function<U, V> f = mCallbacks.get(c.getClass());
		
		if(f == null)
			throw new IllegalArgumentException("No callback for " + c.getClass().getName());
		
		return f.apply(c);
	}
	
	public static class Builder<U, V>
	{
		private final Map<Class<? extends U>, Function<U, V>> mCallbacks;
		
		public Builder()
		{
			mCallbacks = new HashMap<>();
		}

		@SuppressWarnings("unchecked")
		public <T extends U> Builder<U, V> register(Class<T> c, Function<T, V> f)
		{
			mCallbacks.put(c, (Function<U, V>) f);
			
			return this;
		}

		public TypeMultiplexedFunction<U, V> build()
		{
			return new TypeMultiplexedFunction<>(mCallbacks);
		}
	}
}
