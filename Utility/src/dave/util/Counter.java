package dave.util;

public class Counter
{
	private int mCounter = 0;
	
	public void inc( ) { ++mCounter; }
	public void dec( ) { --mCounter; }
	public int get( ) { return mCounter; }
	public void set(int v) { mCounter = v; }
}
