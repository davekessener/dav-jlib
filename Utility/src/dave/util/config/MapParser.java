package dave.util.config;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import dave.util.Utils;

public class MapParser<T> implements Function<String, Map<String, T>>
{
	private final Class<? extends T> mClass;
	private final TypeParser mParser;
	private final boolean mIsString;
	
	public MapParser(Class<? extends T> c)
	{
		mClass = c;
		mParser = new TypeParser();
		mIsString = c.equals(String.class);
	}
	
	@Override
	public Map<String, T> apply(String v)
	{
		if(v.charAt(0) != '{' || v.charAt(v.length() - 1) != '}')
			throw new IllegalArgumentException();
		
		List<String> strings = new ArrayList<>();
		
		if(mIsString)
		{
			Matcher m = PTRN_STRING.matcher(v);
			
			while(m.find())
			{
				strings.add(m.group(1));
				v = m.replaceFirst("" + (strings.size() - 1));
				m = PTRN_STRING.matcher(v);
			}
		}
		
		return Stream.of(v.substring(1, v.length() - 1).split(","))
			.map(e -> e.replaceAll("[ \t]+", ""))
			.filter(e -> !e.isEmpty())
			.map(e -> e.split(":"))
			.map(a -> Utils.pair(a[0], parse(a[1], strings)))
			.collect(Utils.toMap());
	}
	
	@SuppressWarnings("unchecked")
	private T parse(String v, List<String> strings)
	{
		if(mIsString)
		{
			return (T) strings.get(Integer.parseInt(v));
		}
		else
		{
			return mParser.convert(mClass, v);
		}
	}
	
	private static final Pattern PTRN_STRING = Pattern.compile("\"(([^\"]*|\\\")*)\"");
}
