package dave.util.config;

import java.lang.invoke.MethodHandles.Lookup;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.stream.Stream;

import dave.json.JSON;
import dave.json.JsonObject;
import dave.json.JsonValue;
import dave.json.Loadable;
import dave.json.Saveable;
import dave.util.ImmutableMapBuilder;

public class Configuration<T> implements Saveable, Loadable
{
	private final Class<T> mClass;
	private final Map<String, Container> mProperties;
	private final T mProxy;
	
	@SuppressWarnings("unchecked")
	public Configuration(Class<T> c)
	{
		if(c == null)
			throw new NullPointerException();
		
		if(!c.isInterface())
			throw new IllegalArgumentException();
		
		mClass = c;
		mProperties = new HashMap<>();
		
		mProxy = (T) Proxy.newProxyInstance(
			Configuration.class.getClassLoader(),
			new Class<?>[] { mClass },
			this::callback);
		
		TypeParser parser = new TypeParser();
		
		Stream.of(mClass.getMethods()).forEach(m -> {
			if(!m.isDefault())
			{
				if(m.getParameterCount() > 0)
					throw new IllegalArgumentException(m.getName());
				
				Container value = new Container(m.getReturnType());
				
				if(m.isAnnotationPresent(Default.class))
				{
					Default d = m.getAnnotation(Default.class);
					
					value.set(parser.convert(m.getReturnType(), d.value(), d.valueType()));
				}
				
				mProperties.put(m.getName(), value);
			}
		});
	}
	
	public T proxy( ) { return mProxy; }
	
	@SuppressWarnings("unchecked")
	public <TT> TT get(String id)
	{
		Container c = mProperties.get(id);
		
		if(c == null)
			throw new NoSuchElementException(id);
		
		return (TT) c.get();
	}
	
	public void set(String id, Object v)
	{
		Container c = mProperties.get(id);
		
		if(c == null)
			throw new NoSuchElementException(id);
		
		c.set(v);
	}
	
	@Override
	public JsonValue save( )
	{
		JsonObject json = new JsonObject();
		
		mProperties.entrySet().forEach(e -> {
			json.put(e.getKey(), JSON.serialize(e.getValue().get()));
		});
		
		return json;
	}
	
	@Override
	public void load(JsonValue json)
	{
		JsonObject o = (JsonObject) json;
		
		if(!o.keySet().stream().allMatch(k -> mProperties.keySet().contains(k)))
			throw new IllegalStateException();
		
		o.keySet().forEach(k -> {
			mProperties.get(k).set(JSON.deserialize(o.get(k)));
		});
	}
	
	private Object callback(Object p, Method m, Object[] a) throws IllegalArgumentException, Throwable
	{
		if(m.isDefault())
		{
			Constructor<Lookup> c = Lookup.class.getDeclaredConstructor(Class.class);
			
			c.setAccessible(true);
			
			return c.newInstance(mClass).in(mClass).unreflectSpecial(m, mClass).bindTo(p).invokeWithArguments(a);
		}
		else
		{
			return get(m.getName());
		}
	}
	
	private static class Container
	{
		private final Class<?> mClass;
		private Object mObject;
		
		public Container(Class<?> c)
		{
			if(c == null)
				throw new NullPointerException();
			
			if(c.equals(Void.class) || c.equals(void.class))
				throw new IllegalArgumentException();
			
			mClass = c;
		}
		
		public Object get( )
		{
			return mObject;
		}
		
		public void set(Object o)
		{
			if(o != null && !isValid(o.getClass()))
				throw new IllegalArgumentException(mClass.getName() + " - " + o.getClass().getName());
			
			mObject = o;
		}
		
		private boolean isValid(Class<?> c)
		{
			Class<?> p = (mClass.isPrimitive() ? PRIMITIVES.get(mClass) : mClass);
			
			return p.isAssignableFrom(c);
		}
	}
	
	private static final Map<Class<?>, Class<?>> PRIMITIVES = (new ImmutableMapBuilder<Class<?>, Class<?>>())
		.put(double.class, Double.class)
		.put(float.class, Float.class)
		.put(short.class, Short.class)
		.put(int.class, Integer.class)
		.put(long.class, Long.class)
		.put(byte.class, Byte.class)
		.put(boolean.class, Boolean.class)
		.build();
}
