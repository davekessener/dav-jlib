package dave.util.config;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

public class TypeParser
{
	private final Map<Class<?>, Function<String, ?>> mTransformers;
	
	public TypeParser()
	{
		mTransformers = new HashMap<>();
		
		registerConversion(byte.class,    s -> Byte.parseByte(s));
		registerConversion(Byte.class,    s -> Byte.parseByte(s));
		registerConversion(short.class,   s -> Short.parseShort(s));
		registerConversion(Short.class,   s -> Short.parseShort(s));
		registerConversion(int.class,     s -> Integer.parseInt(s));
		registerConversion(Integer.class, s -> Integer.parseInt(s));
		registerConversion(long.class,    s -> Long.parseLong(s));
		registerConversion(Long.class,    s -> Long.parseLong(s));
		registerConversion(float.class,   s -> Float.parseFloat(s));
		registerConversion(Float.class,   s -> Float.parseFloat(s));
		registerConversion(double.class,  s -> Double.parseDouble(s));
		registerConversion(Double.class,  s -> Double.parseDouble(s));
		registerConversion(boolean.class, s -> Boolean.parseBoolean(s));
		registerConversion(Boolean.class, s -> Boolean.parseBoolean(s));
		registerConversion(String.class,  s -> s);
	}
	
	public void clear() { mTransformers.clear(); }
	
	public <T> void registerConversion(Class<T> c, Function<String, T> f)
	{
		mTransformers.put(c, f);
	}
	
	public <T> T convert(Class<?> c, String v) { return convert(c, v, null); }
	
	@SuppressWarnings("unchecked")
	public <T> T convert(Class<?> c, String v, Class<?> d)
	{
		if(c.equals(Map.class))
		{
			return (T) (new MapParser<>(d)).apply(v);
		}
		else
		{
			Function<String, ?> f = mTransformers.get(c);
			
			if(f == null)
				throw new IllegalArgumentException("No converter for " + c.getName());
			
			return (T) f.apply(v);
		}
	}
}
