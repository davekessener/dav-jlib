package dave.util;

public interface Prioritizable
{
	public abstract int priority( );
}
