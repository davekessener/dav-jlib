package dave.util;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

public class Distributor<T> implements Consumer<T>, Actor
{
	private final BlockingQueue<T> mQueue;
	private final Consumer<T> mCallback;
	private final Thread mThread;
	private boolean mRunning;
	
	public Distributor(Consumer<T> cb)
	{
		mQueue = new LinkedBlockingQueue<>();
		mCallback = cb;
		mThread = new Thread(() -> run());
		mRunning = true;
	}
	
	@Override
	public void start( )
	{
		mThread.start();
	}
	
	@Override
	public void stop( )
	{
		try
		{
			mRunning = false;
			
			mThread.join();
		}
		catch(InterruptedException e)
		{
			e.printStackTrace();
		}
	}
	
	@Override
	public void accept(T o)
	{
		try
		{
			mQueue.put(o);
		}
		catch(InterruptedException e)
		{
			throw new RuntimeException(e);
		}
	}
	
	private void run( )
	{
		try
		{
			while(mRunning)
			{
				T o = mQueue.poll(10, TimeUnit.MILLISECONDS);
				
				if(o != null)
				{
					mCallback.accept(o);
				}
			}
		}
		catch(InterruptedException e)
		{
			e.printStackTrace();
		}
	}
}
