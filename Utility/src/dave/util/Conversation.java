package dave.util;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

import dave.util.Utils.ThrowingRunnable;
import dave.util.relay.Overloaded;
import dave.util.relay.Relay;

public abstract class Conversation<S, T> implements Actor
{
	private final Function<S, Transmitter<T>> mCreate;
	
	public Conversation(Function<S, Transmitter<T>> f)
	{
		mCreate = f;
	}
	
	public Sequence start(S s) { return start(s, DEFAULT_ERROR); }
	public Sequence start(S s, Consumer<? super T> e) { return join(mCreate.apply(s), e); }
	public Sequence join(Transmitter<T> c) { return join(c, DEFAULT_ERROR); }
	public Sequence join(Transmitter<T> c, Consumer<? super T> e) { return create(c, e); }
	
	@Override
	public void start()
	{
	}
	
	@Override
	public void stop()
	{
	}
	
	protected abstract void run(Runnable f);
	
	protected Sequence create(Transmitter<T> c, Consumer<? super T> e)
	{
		return new Sequence(c, e);
	}
	
	public class Sequence
	{
		private final Relay mRelay;
		private final Transmitter<T> mBase;
		private final Consumer<? super T> mOnError;
		private Cell mSeq;
		private ThrowingRunnable mEnsure;
		private int mStack;
		
		public Sequence(Transmitter<T> c, Consumer<? super T> e)
		{
			mRelay = new Relay(this);
			mBase = c;
			mOnError = e;
			mSeq = new Cell();
			mEnsure = null;
			mStack = 0;
		}
		
		public Sequence say(T v)
		{
			if(mSeq.value != null)
				throw new IllegalArgumentException();
			
			mSeq.value = v;
			
			return this;
		}
		
		public <TT> Sequence await(TT p, BiConsumer<Sequence, T> cb)
		{
			return mRelay.call(p, cb);
		}
		
		public Sequence await(BiConsumer<Sequence, T> cb) { return await(e -> true, cb); }
		
		@Overloaded
		public Sequence await(Predicate<T> f, BiConsumer<Sequence, T> cb)
		{
			mSeq.callbacks.add(new Entry(f, cb));

			return this;
		}
		
		public T receive( ) { return mBase.receive(); }
		
		public void run()
		{
			if(mStack++ == 0)
			{
				Conversation.this.run(this::impl);
			}
			else
			{
				impl();
			}
		}
		
		private void impl()
		{
			try
			{
				Cell c = mSeq;
				
				mSeq = new Cell();

				if(c.value != null)
				{
					mBase.send(c.value);
				}
				
				if(!c.callbacks.isEmpty())
				{
					T r = mBase.receive();
					
					c.callbacks.stream()
						.filter(e -> e.f.test(r))
						.map(e -> e.cb)
						.findFirst()
						.orElse((q, e) -> mOnError.accept(e))
						.accept(this, r);
				}
			}
			catch(Throwable e)
			{
				e.printStackTrace();
			}
			finally
			{
				if(--mStack == 0)
				{
					if(mEnsure != null)
					{
						try
						{
							mEnsure.run();
						}
						catch(Exception e)
						{
							e.printStackTrace();
						}
					}
					
					mBase.destroy();
				}
			}
		}
	}
	
	private class Entry
	{
		public Predicate<T> f;
		public BiConsumer<Sequence, T> cb;
		
		public Entry(Predicate<T> f, BiConsumer<Sequence, T> cb)
		{
			this.f = f;
			this.cb = cb;
		}
	}
	
	private class Cell
	{
		public T value;
		public final List<Entry> callbacks = new ArrayList<>();
		
		public Cell()
		{
			value = null;
		}
	}
	
	public static Consumer<Object> DEFAULT_ERROR = e -> { throw new IllegalArgumentException(e.toString()); };
}
