package dave.util;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Proxy;
import java.util.HashSet;
import java.util.Set;

public final class Proxify
{
	public static interface Callback<T> { Object call(T o, Method f, Object[] a) throws Exception; }
	
	@SuppressWarnings("unchecked")
	public static <B, T extends B> T wrap(Class<B> b, T o, Callback<T> f, Class<?> ... c)
	{
		Set<Method> methods = new HashSet<>();
		
		for(int i = 0 ; i < c.length ; ++i)
		{
			Method[] m = c[i].getDeclaredMethods();
			
			for(int j = 0 ; j < m.length ; ++j)
			{
				if((m[j].getModifiers() & Modifier.PUBLIC) != 0)
				{
					methods.add(m[j]);
				}
			}
		}
		
		return (T) Proxy.newProxyInstance(
				Proxify.class.getClassLoader(),
				new Class<?>[] { b },
				new Wrapper<>(o, methods, f));
	}
	
	@SuppressWarnings("unchecked")
	public static <B, T extends B> T synchronize(Class<B> b, T o)
	{
		return (T) Proxy.newProxyInstance(
				Proxify.class.getClassLoader(),
				new Class<?>[] { b },
		(proxy, method, args) -> {
			synchronized(o)
			{
				return method.invoke(o, args);
			}
		});
	}
	
	private static class Wrapper<T> implements InvocationHandler
	{
		private final T mObj;
		private final Set<Method> mMethods;
		private final Callback<T> mCallback;
		
		public Wrapper(T o, Set<Method> m, Callback<T> cb)
		{
			mObj = o;
			mMethods = m;
			mCallback = cb;
		}

		@Override
		public Object invoke(Object proxy, Method m, Object[] a) throws Throwable
		{
			if(mMethods.contains(m))
			{
				return mCallback.call(mObj, m, a);
			}
			else
			{
				return m.invoke(mObj, a);
			}
		}
	}
	
	private Proxify( ) { }
}
