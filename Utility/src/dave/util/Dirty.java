package dave.util;

public interface Dirty
{
	public abstract boolean dirty( );
	public abstract void clean( );
}
