package dave.util;

import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public interface Streamable<T> extends Iterable<T>
{
	public default Stream<T> stream( )
	{
		return StreamSupport.stream(this.spliterator(), false);
	}
}
