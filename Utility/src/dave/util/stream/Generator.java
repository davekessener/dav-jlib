package dave.util.stream;

public interface Generator<T, S>
{
	public abstract void accept(S e);
	public abstract int size( );
	public abstract T pop( );
}
