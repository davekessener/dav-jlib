package dave.util.stream;

public class InvalidElementException extends RuntimeException
{
	private static final long serialVersionUID = -5257603902214084551L;
	
	public InvalidElementException(String s) { super(s); }
}
