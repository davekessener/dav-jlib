package dave.util.stream;

import java.util.ArrayDeque;
import java.util.Queue;

public abstract class BaseGenerator<T, S> implements Generator<T, S>
{
	private final Queue<T> mBuf = new ArrayDeque<>();
	
	protected void push(T e) { mBuf.add(e); }
	
	@Override
	public int size()
	{
		return mBuf.size();
	}

	@Override
	public T pop()
	{
		return mBuf.remove();
	}
}
