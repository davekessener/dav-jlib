package dave.util.stream;

import java.util.function.Function;

public class FunctionWrapper<T, S> extends BaseGenerator<T, S>
{
	private final Function<S, T> mCallback;
	
	public FunctionWrapper(Function<S, T> f)
	{
		mCallback = f;
	}

	@Override
	public void accept(S e)
	{
		push(mCallback.apply(e));
	}
}
