package dave.util.stream;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class StatefulGenerator<T, S> extends BaseGenerator<T, S>
{
	public static class Result<T>
	{
		public final T result;
		public final boolean done;
		
		public Result(T result, boolean done)
		{
			this.result = result;
			this.done = done;
		}
	}
	
	public static interface State<T, S>
	{
		boolean matches(S e);
		Result<T> process(S e);
	}
	
	private final List<State<T, S>> mStates;
	private State<T, S> mCurrent;
	
	public StatefulGenerator( )
	{
		mStates = new ArrayList<>();
		mCurrent = null;
	}
	
	public StatefulGenerator<T, S> add(State<T, S> s)
	{
		mStates.add(s);
		
		return this;
	}
	
	@Override
	public void accept(S e)
	{
		if(mCurrent == null)
		{
			mCurrent = mStates.stream().filter(s -> s.matches(e)).findFirst().orElse(null);
		}
		
		if(mCurrent == null)
		{
			throw new InvalidElementException(Objects.toString(e));
		}
		
		Result<T> r = mCurrent.process(e);
		
		if(r.done)
		{
			push(r.result);
			
			mCurrent = null;
		}
	}
}
