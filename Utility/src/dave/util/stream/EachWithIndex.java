package dave.util.stream;

import java.util.function.Function;

public class EachWithIndex<T> implements Function<T, EachWithIndex.IndexedElement<T>>
{
	private int mIdx = 0;
	
	@Override
	public IndexedElement<T> apply(T e)
	{
		return new IndexedElement<>(mIdx++, e);
	}
	
	public static class IndexedElement<T>
	{
		public final T element;
		public final int index;
		
		public IndexedElement(int i, T e)
		{
			this.element = e;
			this.index = i;
		}
	}
}
