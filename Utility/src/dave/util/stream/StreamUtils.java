package dave.util.stream;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import java.util.stream.Stream.Builder;
import java.util.stream.StreamSupport;

import dave.util.ImmutableMapBuilder;
import dave.util.RNG;
import dave.util.SevereException;
import dave.util.Utils;
import dave.util.Utils.Pair;

public final class StreamUtils
{
	public static <T> boolean isUnique(Stream<T> s)
	{
		Set<T> old = new HashSet<>();
		
		return s.anyMatch(e -> !old.add(e));
	}
	
	public static <T> Stream<T> join(Stream<? extends T> a, Stream<? extends T> b)
	{
		return Stream.concat(a, b);
	}
	
	public static <T> Stream<T> stream(T[][] a)
	{
		return Arrays.stream(a).flatMap(e -> Arrays.stream(e));
	}

	public static IntStream stream(int[][] a)
	{
		return Arrays.stream(a).flatMapToInt(e -> Arrays.stream(e));
	}

	public static DoubleStream stream(double[][] a)
	{
		return Arrays.stream(a).flatMapToDouble(e -> Arrays.stream(e));
	}
	
	public static DoubleStream stream(float[] a)
	{
		return IntStream.range(0, a.length).mapToDouble(i -> a[i]);
	}
	
	public static <T> Stream<T> stream(Iterator<T> i)
	{
		Iterable<T> s = () -> i;
		
		return StreamSupport.stream(s.spliterator(), false);
	}
	
	public static <T> Stream<T> reverse(Stream<T> s)
	{
		return StreamSupport.stream(Utils.reverse(s.collect(Collectors.toList())).spliterator(), false);
	}
	
	public static <T> Optional<T> last(Stream<T> s) { return s.reduce((a, b) -> b); }
	
	public static <T> Collector<T, List<T>, Stream<T>> shuffle(RNG rng)
	{
		return Collector.of(
			(Supplier<List<T>>) () -> new ArrayList<>(),
			(BiConsumer<List<T>, T>) (l, v) -> l.add(v),
			(BinaryOperator<List<T>>) (l0, l1) -> { l0.addAll(l1); return l0; },
			(Function<List<T>, Stream<T>>) l -> { Utils.shuffle(l, rng); return l.stream(); });
	}
	
	public static <T> Stream<T> buffer(Stream<T> s)
	{
		Builder<T> b = Stream.builder();
		
		s.forEach(b::accept);
		
		return b.build();
	}
	
	public static class Element<T>
	{
		public final int index;
		public final T value;
		
		public Element(int index, T value)
		{
			this.index = index;
			this.value = value;
		}
	}
	
	public static <T> Function<T, Element<T>> stream_with_index()
	{
		return new Function<T, Element<T>>() {
			private int mIndex = 0;

			@Override
			public Element<T> apply(T e)
			{
				return new Element<>(mIndex++, e);
			}
		};
	}
	
	public static <T1, T2> Stream<Pair<T1, T2>> zip(Stream<T1> s1, Stream<T2> s2)
	{
		return stream(new Iterator<Pair<T1, T2>>() {
			private final Iterator<T1> i1 = s1.iterator();
			private final Iterator<T2> i2 = s2.iterator();

			@Override
			public boolean hasNext()
			{
				return i1.hasNext() || i2.hasNext();
			}

			@Override
			public Pair<T1, T2> next()
			{
				return new Pair<>(i1.next(), i2.next());
			}
		});
	}
	
	public static <T1, T2> Stream<Pair<T1, T2>> product(Stream<T1> s1, Stream<T2> s2)
	{
		List<T2> a2 = s2.collect(Collectors.toList());
		
		return s1.flatMap(e1 -> a2.stream().map(e2 -> new Pair<>(e1, e2)));
	}
	
	@SuppressWarnings("unchecked")
	public static <T1, C1 extends Collection<T1>, T2, C2 extends Collection<T2>> C2 map(C1 a, Function<T1, T2> f)
	{
		return a.stream().map(f).collect(Collectors.toCollection(() -> (C2) createCollectionBasedOn(a.getClass())));
	}
	
	public static <T, C extends Collection<T>> C createCollectionBasedOn(Class<C> c)
	{
		return createCollectionBasedOn(c, c);
	}
	
	@SuppressWarnings("unchecked")
	private static <T, C extends Collection<T>> C createCollectionBasedOn(Class<C> base, Class<? super C> c)
	{
		if(c == null)
		{
			try
			{
				return base.getDeclaredConstructor().newInstance();
			}
			catch(InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e)
			{
				throw new SevereException(e);
			}
		}
		
		if(sBaseCollections.containsKey(c))
		{
			return (C) sBaseCollections.get(c).get();
		}
		
		return Stream.of(c.getInterfaces())
			.map(cc -> sBaseCollections.get(cc))
			.filter(s -> s != null)
			.map(s -> (C) s.get())
			.findFirst().orElseGet(() -> createCollectionBasedOn(base, c.getSuperclass()));
	}
	
	private static final Map<Class<?>, Supplier<Collection<?>>> sBaseCollections = (new ImmutableMapBuilder<Class<?>, Supplier<Collection<?>>>())
		.put(List.class, () -> new ArrayList<>())
		.put(Set.class, () -> new HashSet<>())
		.build();
	
	private StreamUtils( ) { }
}
