package dave.util.stream;

import java.util.Iterator;

public class ConvertingIterator<S, T> implements Iterator<T>
{
	private final Generator<T, S> mGen;
	private final Iterator<S> mSource;
	
	public ConvertingIterator(Generator<T, S> f, Iterator<S> i)
	{
		mGen = f;
		mSource = i;
		
		step();
	}

	@Override
	public boolean hasNext()
	{
		return mGen.size() > 0;
	}

	@Override
	public T next()
	{
		T e = mGen.pop();
		
		if(mGen.size() == 0)
		{
			step();
		}
		
		return e;
	}
	
	private void step( )
	{
		if(!mSource.hasNext())
			return;
		
		while(mGen.size() == 0)
		{
			if(!mSource.hasNext())
				throw new UnterminatedStreamException();
			
			mGen.accept(mSource.next());
		}
	}
}