package dave.util.stream;

import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class MultiStream<T>
{
	private final Stream<T> mSource;
	
	public MultiStream(Stream<T> s)
	{
		if(s == null)
			throw new NullPointerException();
		
		mSource = s;
	}
	
	public Stream<T> get( )
	{
		return mSource;
	}
	
	public <TT> MultiStream<TT> map(Generator<TT, T> f)
	{
		return new MultiStream<>(
			StreamSupport.stream(
				Spliterators.spliteratorUnknownSize(
					new ConvertingIterator<>(f, mSource.iterator()),
					Spliterator.ORDERED),
				false));
	}
	
	public static <T> MultiStream<T> of(Stream<T> in)
	{
		return new MultiStream<>(in);
	}
}
