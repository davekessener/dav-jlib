package dave.util.stream;

import java.util.Iterator;

public class ArrayIterator<T> implements Iterator<T>
{
	private final T[] mArr;
	private final int mEnd;
	private int mPos;
	
	public ArrayIterator(T[] a) { this(a, 0, a.length); }
	
	public ArrayIterator(T[] a, int i1, int i2)
	{
		if(a == null)
			throw new NullPointerException();
		if(i1 < 0 || i1 > i2 || i2 > a.length)
			throw new ArrayIndexOutOfBoundsException();
		
		mArr = a;
		mPos = i1;
		mEnd = i2;
	}
	
	@Override
	public boolean hasNext()
	{
		return mPos < mEnd;
	}

	@Override
	public T next()
	{
		return mArr[mPos++];
	}
}
