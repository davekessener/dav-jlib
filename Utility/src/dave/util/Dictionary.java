package dave.util;

public interface Dictionary<K, V> extends Dirty
{
	public abstract V put(K key, V value);
	public abstract V get(K key);
	public abstract V remove(K key);
	public abstract boolean containsKey(K key);
}
