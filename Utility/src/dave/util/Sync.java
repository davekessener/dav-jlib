package dave.util;

import java.util.function.LongConsumer;
import java.util.function.LongSupplier;

public class Sync
{
	private final LongSupplier mNow;
	private final LongConsumer mSleep;
	private final long mDelta;
	private long mLast;
	
	public Sync(long d, LongSupplier f1, LongConsumer f2)
	{
		mNow = f1;
		mSleep = f2;
		mDelta = d;
		mLast = mNow.getAsLong();
	}
	
	public long delta() { return mDelta; }
	
	public long sync()
	{
		long t = mNow.getAsLong();
		long d = t - mLast;
		
		if(d < mDelta)
		{
			mSleep.accept(mDelta - d);
			
			t = mNow.getAsLong();
			d = t - mLast;
		}
		
		mLast = t;
		
		return d;
	}
	
	public static Sync build(long d) { return (new Builder()).build(d); }
	
	public static class Builder
	{
		private LongSupplier mNow;
		private LongConsumer mSleep;
		
		public Builder()
		{
			mNow = () -> System.currentTimeMillis();
			mSleep = d -> Utils.sleep(d);
		}
		
		public Builder setNow(LongSupplier f) { mNow = f; return this; }
		public Builder setSleep(LongConsumer f) { mSleep = f; return this; }
		
		public Sync build(long d)
		{
			return new Sync(d, mNow, mSleep);
		}
	}
}
