package dave.util;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Function;

public class SimpleCache<K, T> implements Cache<K, T>
{
	private final int mCapacity;
	private final Function<K, T> mSupplier;
	private final Consumer<T> mUpdater;
	private final Map<K, T> mCache;
	
	public SimpleCache(int cap, Function<K, T> f) { this(cap, f, null); }
	public SimpleCache(int cap, Function<K, T> f1, Consumer<T> f2)
	{
		mCapacity = cap;
		mSupplier = f1;
		mUpdater = f2;
		mCache = new LinkedHashMap<K, T>(mCapacity + 1, 0.75f, true) {
			private static final long serialVersionUID = 569531895491180473L;

			@Override
			public boolean removeEldestEntry(Map.Entry<K, T> e)
			{
				return size() > mCapacity;
			}
		};
	}
	
	@Override
	public int size()
	{
		return mCache.size();
	}
	
	@Override
	public int capacity()
	{
		return mCapacity;
	}
	
	@Override
	public boolean loaded(K id)
	{
		return mCache.containsKey(id);
	}
	
	@Override
	public T load(K id)
	{
		T e = mCache.get(id);
		
		if(e == null)
		{
			mCache.put(id, e = mSupplier.apply(id));
		}
		else if(mUpdater != null)
		{
			mUpdater.accept(e);
		}
		
		return e;
	}
	
	@Override
	public void clear()
	{
		mCache.clear();
	}
}
