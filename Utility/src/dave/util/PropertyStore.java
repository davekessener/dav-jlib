package dave.util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;

import dave.json.JSON;
import dave.json.JsonObject;
import dave.json.JsonUtils;
import dave.json.JsonValue;
import dave.json.Loadable;
import dave.json.Loader;
import dave.json.Saveable;
import dave.json.Saver;

public class PropertyStore implements Saveable, Loadable
{
	private final Map<String, Object> mProperties;
	
	public PropertyStore( )
	{
		mProperties = new HashMap<>();
	}
	
	public void put(String id, Object o)
	{
		if(o == null)
			throw new NullPointerException();
		
		if(mProperties.containsKey(id))
			throw new IllegalArgumentException("Duplicate entry " + id);
		
		mProperties.put(id, o);
	}
	
	public Object get(String id)
	{
		Object r = mProperties.get(id);
		
		if(r == null)
			throw new NoSuchElementException(id);
		
		return r;
	}

	@Override
	public JsonValue save()
	{
		JsonObject json = new JsonObject();
		
		mProperties.forEach((id, o) -> {
			try
			{
				JsonValue v = null;
				
				if(o instanceof Saveable)
				{
					v = ((Saveable) o).save();
				}
				else
				{
					Class<?> c = o.getClass();
					Method[] m = c.getMethods();
					
					for(int i = 0 ; v == null && i < m.length ; ++i)
					{
						if(m[i].isAnnotationPresent(Saver.class))
						{
							v = (JsonValue) m[i].invoke(o);
						}
					}
				}
				
				if(v == null)
				{
					v = JSON.serialize(o);
				}
				
				json.put(id, v);
			}
			catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e)
			{
				throw new SevereException(e);
			}
		});
		
		return json;
	}

	@Override
	public void load(JsonValue json)
	{
		JsonObject o = (JsonObject) json;
		
		for(String key : o.keySet())
		{
			if(!mProperties.containsKey(key))
				throw new IllegalArgumentException("Unknown property " + key);
		}
		
		o = (JsonObject) JsonUtils.merge(save(), o);
		
		o.forEach((key, v) -> {
			try
			{
				Object oo = mProperties.get(key);
				
				if(oo instanceof Loadable)
				{
					((Loadable) oo).load(v);
				}
				else
				{
					Class<?> c = oo.getClass();
					Method[] m = c.getMethods();
					Object no = null;
					
					for(int i = 0 ; no == null && i < m.length ; ++i)
					{
						if(m[i].isAnnotationPresent(Loader.class))
						{
							no = m[i].invoke(null, v);
						}
					}
					
					if(no == null)
					{
						no = JSON.deserialize(v);
					}
					
					mProperties.put(key, no);
				}
			}
			catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e)
			{
				throw new SevereException(e);
			}
		});
	}
}
