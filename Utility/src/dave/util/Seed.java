package dave.util;

public class Seed
{
	private final long a, b;
	
	public Seed()
	{
		this((long) ((Math.random() - 0.5) * 0x10000000000000L)
            ^ (long) (((Math.random() - 0.5) * 2.0) * 0x8000000000000000L),
            (long) ((Math.random() - 0.5) * 0x10000000000000L)
                    ^ (long) (((Math.random() - 0.5) * 2.0) * 0x8000000000000000L));
	}
	
	public Seed(long a, long b)
	{
		this.a = a;
		this.b = b;
	}
	
	public Seed(String s)
	{
		if(s == null || s.length() != 32)
			throw new IllegalArgumentException();
		
		a = Long.parseUnsignedLong(s.substring(0, s.length() / 2), 16);
		b = Long.parseUnsignedLong(s.substring(s.length() / 2), 16);
	}
	
	public long getPartA( ) { return a; }
	public long getPartB( ) { return b; }
	
	private static String i2s(int i) { return String.format("%08x", i); }
	
	@Override
	public String toString( )
	{
		StringBuilder sb = new StringBuilder();
		
		sb.append(i2s((int) ((a >> 32) & 0xFFFFFFFF)));
		sb.append(i2s((int) ( a        & 0xFFFFFFFF)));
		sb.append(i2s((int) ((b >> 32) & 0xFFFFFFFF)));
		sb.append(i2s((int) ( b        & 0xFFFFFFFF)));
		
		return sb.toString();
	}
	
	@Override
	public int hashCode( )
	{
		return ((int) a) ^ ((int) (a >> 32)) ^ ((int) b) ^ ((int) (b >> 32));
	}
	
	@Override
	public boolean equals(Object o)
	{
		if(o instanceof Seed)
		{
			Seed s = (Seed) o;
			
			return a == s.a && b == s.b;
		}
		
		return false;
	}
}
