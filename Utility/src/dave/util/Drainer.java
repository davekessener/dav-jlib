package dave.util;

import java.io.IOException;
import java.util.function.Consumer;

public class Drainer<T>
{
	public static interface Source<T> { T retrieve( ) throws IOException; }
	
	private final Source<T> mSource;
	private final Consumer<T> mDestination;
	private final Thread mThread;
	
	public Drainer(Source<T> in, Consumer<T> out)
	{
		mSource = in;
		mDestination = out;
		mThread = new Thread(() -> run());
	}
	
	public void start( )
	{
		mThread.start();
	}
	
	public void stop( )
	{
		try
		{
			mThread.join();
		}
		catch(InterruptedException e)
		{
			throw new RuntimeException(e);
		}
	}
	
	private void run( )
	{
		try
		{
			while(true)
			{
				mDestination.accept(mSource.retrieve());
			}
		}
		catch(IOException e)
		{
		}
	}
}
