package dave.util.persistence;

import dave.json.JsonValue;
import dave.util.Streamable;

public interface Storage extends Streamable<String>
{
	public abstract void store(String id, JsonValue data);
	public abstract JsonValue retrieve(String id);
	public abstract void remove(String id);
	public abstract boolean has(String id);
}
