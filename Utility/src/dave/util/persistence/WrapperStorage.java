package dave.util.persistence;

import java.util.Iterator;
import java.util.function.Consumer;

import dave.json.JsonValue;

public class WrapperStorage implements Storage, Consumer<Storage>
{
	private Storage mInternal = null;
	
	@Override
	public void accept(Storage io)
	{
		mInternal = io;
	}
	
	public boolean isValid( )
	{
		return mInternal != null;
	}

	@Override
	public Iterator<String> iterator( )
	{
		return mInternal.iterator();
	}

	@Override
	public void store(String id, JsonValue data)
	{
		mInternal.store(id, data);
	}

	@Override
	public JsonValue retrieve(String id)
	{
		return mInternal.retrieve(id);
	}

	@Override
	public void remove(String id)
	{
		mInternal.remove(id);
	}

	@Override
	public boolean has(String id)
	{
		return mInternal.has(id);
	}
}
