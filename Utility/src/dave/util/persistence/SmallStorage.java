package dave.util.persistence;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import dave.json.JsonValue;

public class SmallStorage implements Storage
{
	private final Map<String, JsonValue> mData = new HashMap<>();

	@Override
	public void store(String id, JsonValue data)
	{
		mData.put(id, data);
	}

	@Override
	public JsonValue retrieve(String id)
	{
		JsonValue data = mData.get(id);
		
		if(data == null)
			throw new IllegalArgumentException(id);
		
		return data;
	}
	
	@Override
	public void remove(String id)
	{
		if(mData.remove(id) == null)
			throw new IllegalArgumentException(id);
	}

	@Override
	public boolean has(String id)
	{
		return mData.containsKey(id);
	}

	@Override
	public Iterator<String> iterator( )
	{
		return mData.keySet().iterator();
	}
}
