package dave.util.persistence;

import java.util.Iterator;

import dave.json.JsonValue;

public class StorageAdapter implements Storage
{
	private final Storage mBackend;
	private final String mPrefix;
	
	public StorageAdapter(Storage s, String pre)
	{
		mBackend = s;
		mPrefix = pre + ".";
	}

	@Override
	public void store(String id, JsonValue data)
	{
		mBackend.store(mPrefix + id, data);
	}

	@Override
	public JsonValue retrieve(String id)
	{
		return mBackend.retrieve(mPrefix + id);
	}

	@Override
	public void remove(String id)
	{
		mBackend.remove(mPrefix + id);
	}

	@Override
	public boolean has(String id)
	{
		return mBackend.has(mPrefix + id);
	}

	@Override
	public Iterator<String> iterator( )
	{
		return new Iterator<String>() {
			private final Iterator<String> mSuper = mBackend.iterator();
			
			@Override
			public String next( )
			{
				return mPrefix + mSuper.next();
			}
			
			@Override
			public boolean hasNext( )
			{
				return mSuper.hasNext();
			}
		};
	}
}
