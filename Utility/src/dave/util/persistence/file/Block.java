package dave.util.persistence.file;

public class Block
{
	public final int address;
	public final int length;
	
	public Block(int a, int l)
	{
		address = a;
		length = l;
	}
	
	@Override
	public int hashCode( )
	{
		return Integer.hashCode(address) ^ Integer.hashCode(length);
	}
	
	@Override
	public boolean equals(Object o)
	{
		if(o instanceof Block)
		{
			return address == ((Block) o).address && length == ((Block) o).length;
		}
		
		return false;
	}
	
	@Override
	public String toString( )
	{
		return String.format("{@$%08x: %dB}", address, length);
	}
	
	public static final int HEADER_SIZE = 8;
}
