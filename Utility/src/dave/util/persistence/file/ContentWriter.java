package dave.util.persistence.file;

import java.util.List;

import dave.json.JsonValue;
import dave.util.BufferedProducer;
import dave.util.Producer;
import dave.util.Utils;
import dave.util.io.CountingOutput;
import dave.util.io.OutputPrinter;
import dave.util.io.SeekableOutput;
import dave.util.io.SegmentedOutput;

public class ContentWriter
{
	private final SeekableOutput mIO;
	private final MemoryManager mMemory;
	
	public ContentWriter(SeekableOutput io, MemoryManager mem)
	{
		mIO = io;
		mMemory = mem;
	}
	
	public Record write(Name name, JsonValue json)
	{
		Chunk c = mMemory.allocate(2 + name.getByteCount() + 4 + Block.HEADER_SIZE + 1);
		int address = c.getStartAddress();
		
		mIO.seek(address);
		name.write(mIO);
		mIO.writeInt(0);
		
		Producer<Chunk> p = new BufferedProducer<>(() -> mMemory.allocate(Block.HEADER_SIZE + 1), c.clone());
		SegmentProducer segments = new SegmentProducer(mIO, p);
		CountingOutput out = new CountingOutput(new SegmentedOutput(mIO, segments));
		
		json.write(new OutputPrinter(out, Utils.CHARSET));
		
		List<Block> b = segments.finish();
		int dl = out.getByteCount();
		
		mIO.seek(address + 2 + name.getByteCount());
		mIO.writeInt(dl);
		
		return new Record(address, dl, name, b);
	}
}
