package dave.util.persistence.file;

import dave.util.Utils;
import dave.util.io.Input;
import dave.util.io.Output;

public class Name
{
	private final String mName;
	private Integer mHash;
	private byte[] mData;
	
	public Name(String n)
	{
		mName = n;
		mHash = null;
		mData = null;
	}

	public String asString( )
	{
		return mName;
	}

	public int computeHash( )
	{
		if(mHash == null)
		{
			mHash = Utils.HASHER.compute(mName);
		}
		
		return mHash;
	}
	
	public int getByteCount( )
	{
		if(mData == null)
		{
			mData = mName.getBytes(Utils.CHARSET);
		}

		return mData.length;
	}
	
	public void write(Output io)
	{
		io.writeShort(getByteCount());
		io.write(mData);
	}
	
	public static Name read(Input io)
	{
		byte[] d = io.read(io.readShort());
		
		return new Name(new String(d, Utils.CHARSET));
	}
	
	@Override
	public int hashCode( )
	{
		return mName.hashCode() ^ computeHash();
	}
	
	@Override
	public boolean equals(Object o)
	{
		if(o instanceof Name)
		{
			return mName.equals(((Name) o).mName);
		}
		
		return false;
	}
	
	@Override
	public String toString( )
	{
		return String.format("'%s' [%08x]", mName, computeHash());
	}
}
