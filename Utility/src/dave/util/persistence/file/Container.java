package dave.util.persistence.file;

import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import dave.util.Dirty;

public interface Container<T> extends Iterable<T>, Dirty
{
	public abstract void insert(T o);
	public abstract void remove(T o);
	
	public default Stream<T> stream( )
	{
		return StreamSupport.stream(this.spliterator(), false);
	}
}
