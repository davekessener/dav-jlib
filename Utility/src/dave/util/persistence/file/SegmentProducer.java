package dave.util.persistence.file;

import java.util.ArrayList;
import java.util.List;

import dave.util.Producer;
import dave.util.io.SeekableOutput;

public class SegmentProducer implements Producer<Integer>
{
	private final SeekableOutput mIO;
	private final Producer<Chunk> mCallback;
	private final List<Block> mBlocks;
	private Chunk mCurrent;
	
	public SegmentProducer(SeekableOutput io, Producer<Chunk> f)
	{
		mIO = io;
		mCallback = f;
		mBlocks = new ArrayList<>();
		mCurrent = null;
	}
	
	public List<Block> finish( )
	{
		mBlocks.add(mCurrent.toBlock());
		
		mIO.seek(mBlocks.get(0).address + 4);
		
		mBlocks.stream().forEach(b -> {
			mIO.writeInt(b.address);
			mIO.seek(b.address);
			mIO.writeInt(b.length);
		});
		
		mIO.writeInt(0);
		
		return mBlocks;
	}
	
	@Override
	public Integer produce( )
	{
		if(mCurrent != null)
		{
			mBlocks.add(mCurrent.toBlock());
		}
		
		mCurrent = mCallback.produce();
		mIO.seek(mCurrent.getStartAddress());
		mIO.writeInt(0);
		mIO.writeInt(0);
		
		return mCurrent.getEffectiveSize() - Block.HEADER_SIZE;
	}
}
