package dave.util.persistence.file;

import java.util.ArrayList;
import java.util.List;

import dave.util.io.SeekableInput;

public class RecordReader
{
	private final SeekableInput mIO;
	
	public RecordReader(SeekableInput io)
	{
		mIO = io;
	}
	
	public Record read(int a)
	{
		final int address = a;
		
		mIO.seek(a);
		
		final Name name = Name.read(mIO);
		final int l = mIO.readInt();
		final List<Block> data = new ArrayList<>();
		int tmp = 0;
		
		while(mIO.seek() != 0)
		{
			Block b = readBlock();
			
			data.add(b);
			tmp += b.length;
		}
		
		if(tmp < l)
			throw new IllegalStateException(String.format("Record @$%08x contains %d block totaling %dB; expected min %dB!",
					address, data.size(), tmp, l));
		
		return new Record(address, l, name, data);
	}
	
	private Block readBlock( )
	{
		final int address = mIO.seek();
		final int length = mIO.readInt();
		
		mIO.seek(mIO.readInt());
		
		return new Block(address, length);
	}
}
