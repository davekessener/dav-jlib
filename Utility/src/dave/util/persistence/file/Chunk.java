package dave.util.persistence.file;

public interface Chunk
{
	public abstract int getStartAddress();
	public abstract int getEffectiveSize( );
	public abstract Block toBlock( );
	public abstract Chunk clone( );
}
