package dave.util.persistence.file;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import dave.json.JsonValue;
import dave.util.BoundedDictionary;
import dave.util.Dictionary;
import dave.util.DirtyObject;
import dave.util.log.Logger;
import dave.util.Producer;
import dave.util.Proxify;
import dave.util.Utils;
import dave.util.io.IO;
import dave.util.io.Input;
import dave.util.io.SegmentedInput;
import dave.util.io.BaseOutput;
import dave.util.persistence.Storage;

public class IOStorage implements Storage
{
	private final Producer<IO> mCallback;
	private final Container<Block> mFreeMemory;
	private final Container<RecordTable> mRecords;
	private final Dictionary<String, Record> mCache;
	
	public IOStorage(Producer<IO> f)
	{
		mCallback = f;
		mFreeMemory = new LinkedBlockContainer();
		mRecords = new ListContainer<>(new ArrayList<>());
		mCache = new BoundedDictionary<>(100);
		
		try(Impl o = new Impl())
		{
			o.load();
		}
	}
	
	public void copy(IO io)
	{
		try(Impl o = new Impl())
		{
			o.copy(io);
		}
	}
	
	@Override
	public void store(String id, JsonValue data)
	{
		try(Impl o = new Impl())
		{
			o.store(new Name(id), data);
		}
	}

	@Override
	public JsonValue retrieve(String id)
	{
		try(Impl o = new Impl())
		{
			return o.retrieve(new Name(id));
		}
	}

	@Override
	public void remove(String id)
	{
		try(Impl o = new Impl())
		{
			o.remove(new Name(id));
		}
	}
	
	@Override
	public Iterator<String> iterator( )
	{
		try(Impl o = new Impl())
		{
			return o.keySet().iterator();
		}
	}

	@Override
	public boolean has(String id)
	{
		return has(Utils.HASHER.compute(id));
	}
	
	private boolean has(int hash)
	{
		return mRecords.stream().anyMatch(t -> t.stream().anyMatch(e -> e.hash == hash));
	}
	
	private Stream<RecordTable> findTableContaining(int hash)
	{
		return mRecords.stream().filter(t -> t.stream().anyMatch(e -> e.hash == hash));
	}

	private IntStream getAddressOf(int hash)
	{
		return mRecords.stream().flatMap(t -> t.stream().filter(e -> e.hash == hash)).mapToInt(e -> e.file);
	}
	
// # --------------------------------------------------------------------------
	
	private static class IOIndicator extends DirtyObject implements Proxify.Callback<IO>
	{
		@Override
		public Object call(IO o, Method f, Object[] a) throws Exception
		{
			if(!dirty())
			{
				int current = o.seek();
				
				o.seek(Addresses.IN_USE);
				o.writeInt(MAGIC_IN_USE);
				o.seek(current);
				
				use();
			}
			
			return f.invoke(o, a);
		}
	}
	
	private class Impl implements AutoCloseable
	{
		private final IO mmIO;
		private final MemoryManager mmMemory;
		private final IOIndicator mmIndicator;
		
		public Impl( )
		{
			mmIndicator = new IOIndicator();
			mmIO = Proxify.wrap(IO.class, mCallback.produce(), mmIndicator, BaseOutput.class);
			mmMemory = new SimpleMemoryManager(mmIO, mFreeMemory);
		}
		
		public void load( )
		{
			int t = 0;
			
			if(mmIO.size() == 0)
			{
				writeLeadIn();
			}
			else if((t = mmIO.readShort()) != SIGNATURE)
			{
				throw new IllegalStateException(String.format("Corrupted signature $%04x", t));
			}
			else if((t = mmIO.readShort()) != VERSION)
			{
				throw new IllegalStateException(String.format("Incompatible version %d!", t));
			}
			else if((t = mmIO.readInt()) == MAGIC_IN_USE)
			{
				throw new IllegalStateException("Corrupted save (marked IN_USE)");
			}
			else if(t != MAGIC_DONE)
			{
				throw new IllegalStateException(String.format("Corrupted save (unknown mark $%08x)", t));
			}
			else
			{
				doLoad();
			}
		}
		
		private void writeLeadIn( )
		{
			mmIO.writeShort(SIGNATURE);
			mmIO.writeShort(VERSION);
			mmIO.writeInt(MAGIC_DONE);
			mmIO.writeInt(0);
			mmIO.writeInt(0);
		}
		
		private void doLoad( )
		{
			int a = mmIO.readInt();
			
			while(a != 0)
			{
				mmIO.seek(a);
				mFreeMemory.insert(new Block(a, mmIO.readInt()));
				a = mmIO.readInt();
			}
			
			mmIO.seek(Addresses.FIRST_TABLE);
			a = mmIO.readInt();
			
			while(a != 0)
			{
				RecordTable t = new RecordTable(a, NO_OF_RECORDS);
				
				mmIO.seek(a);
				a = mmIO.readInt();
				t.read(mmIO);
				
				mRecords.insert(t);
			}
		}
		
		@Override
		public void close( )
		{
			if(mFreeMemory.dirty())
			{
				mmIO.seek(Addresses.FREE_MEM);
				
				mFreeMemory.stream().forEach(b -> {
					mmIO.writeInt(b.address);
					mmIO.seek(b.address);
					mmIO.writeInt(b.length);
				});
				
				mmIO.writeInt(0);
				
				mFreeMemory.clean();
			}
			
			mRecords.stream().filter(t -> t.dirty()).forEach(t -> {
				t.write(mmIO);
				t.clean();
			});
			
			if(mRecords.dirty())
			{
				mmIO.seek(Addresses.FIRST_TABLE);
				
				mRecords.stream().forEach(t -> {
					mmIO.writeInt(t.getAddress());
					mmIO.seek(t.getAddress());
				});
				
				mmIO.writeInt(0);
				
				mRecords.clean();
			}
			
			if(mmIndicator.dirty())
			{
				mmIO.seek(Addresses.IN_USE);
				mmIO.writeInt(MAGIC_DONE);
				mmIndicator.clean();
			}
			
			mmIO.close();
		}
		
		public void copy(IO io)
		{
			if(io.size() != 0 || io.seek() != 0)
				throw new IllegalArgumentException();
			
			LOG.log("Copying db to new file [...]");
			
			io.writeShort(SIGNATURE);
			io.writeShort(VERSION);
			io.writeInt(MAGIC_IN_USE);
			io.writeInt(0);
			io.writeInt(0);
			
			int rc = (int) mRecords.stream().mapToLong(t -> t.stream().filter(e -> e.file != 0).count()).sum();
			int tc = (rc + NO_OF_RECORDS - 1) / NO_OF_RECORDS;
			List<RecordTable> ts = new ArrayList<>();
			
			LOG.log("Found %d records stored in %d tables.", rc, tc);

			io.seek(Addresses.FIRST_TABLE);
			
			while(tc-- > 0)
			{
				RecordTable t = new RecordTable(io.size(), NO_OF_RECORDS);
				
				ts.add(t);
				io.writeInt(t.getAddress());
				io.seek(t.getAddress());
				io.writeInt(0);
				t.stream().forEach(e -> io.writeLong(0));
				io.seek(t.getAddress());
			}
			
			io.writeInt(0);
			
			if(!ts.isEmpty())
			{
				LOG.log("Writing files [...]");
				
				Consumer<Record> cb = new Consumer<Record>() {
					private final Iterator<RecordTable> ri = ts.iterator();
					private RecordTable current = ri.next();
					
					@Override
					public void accept(Record r)
					{
						if(current.isFull())
						{
							LOG.log("Filled table @$%08x", current.getAddress());
							
							current = ri.next();
						}
						
						current.add(r);
					}
				};
				
				mRecords.stream().flatMap(t -> t.stream().filter(e -> e.file != 0)).forEach(e -> {
					final Record r = (new RecordReader(mmIO)).read(e.file);
					final int a = io.size();
					final int l = r.getDataLength();
					final Iterator<Block> i = r.iterator();
					final Input in = new SegmentedInput(mmIO, () -> {
						Block b = i.next();
						
						mmIO.seek(b.address + Block.HEADER_SIZE);
						
						return b.length - Block.HEADER_SIZE;
					});
					
					io.seek(a);
					r.getName().write(io);
					io.writeInt(l);
					io.writeInt(l + Block.HEADER_SIZE);
					io.writeInt(0);
					
					for(int j = 0 ; j < l ; ++j)
					{
						io.write(in.read());
					}
					
					cb.accept(new Record(a, r));
				});
	
				io.seek(Addresses.FIRST_TABLE);
				ts.stream().forEach(t -> {
					io.writeInt(t.getAddress());
					io.seek(t.getAddress() + 4);
					t.stream().forEach(e -> {
						io.writeInt(e.hash);
						io.writeInt(e.file);
					});
					io.seek(t.getAddress());
				});
			}
			
			io.seek(Addresses.IN_USE);
			io.writeInt(MAGIC_DONE);
			
			LOG.log("Copy [DONE]!");
		}
		
		public Set<String> keySet( )
		{
			return mRecords.stream().flatMap(t -> t.stream().map(e -> (new RecordReader(mmIO)).read(e.file).getName().asString())).collect(Collectors.toSet());
		}
		
		public void store(Name id, JsonValue data)
		{
			remove(id);
			
			putRecord((new ContentWriter(mmIO, mmMemory)).write(id, data));
		}
		
		public JsonValue retrieve(Name id)
		{
			return (new ContentReader(mmIO)).read(getRecord(id));
		}
		
		public void remove(Name id)
		{
			int hash = id.computeHash();
			Record r = getRecord(id);
			
			findTableContaining(hash)
				.filter(t -> t.stream().anyMatch(e -> e.file == r.getAddress()))
				.findFirst().ifPresent(
			t -> {
				mmMemory.free(new Block(r.getAddress(), 2 + id.getByteCount() + 4));
				r.stream().forEach(b -> mmMemory.free(b));
				t.remove(r);
			});
		}
		
		private void putRecord(Record r)
		{
			mCache.put(r.getName().asString(), r);
			
			findAnyTable().add(r);
		}

		private Record getRecord(Name name)
		{
			String id = name.asString();
			int hash = name.computeHash();
			Record r = mCache.get(id);
			
			if(r == null)
			{
				r = getAddressOf(hash)
					.mapToObj(a -> (new RecordReader(mmIO)).read(a))
					.filter(o -> o.getName().asString().equals(id))
					.findFirst().orElse(null);
				
				if(r != null)
				{
					mCache.put(id, r);
				}
			}
			
			return r;
		}

		private RecordTable createTable( )
		{
			RecordTable t = new RecordTable(mmIO.size(), NO_OF_RECORDS);
			
			mmIO.seek(mmIO.size());
			mmIO.writeInt(0);
			
			mRecords.insert(t);
			
			return t;
		}
		
		private RecordTable findAnyTable( )
		{
			return mRecords.stream().filter(t -> !t.isFull()).findAny().orElseGet(() -> createTable());
		}
	}
	
	private static class Addresses
	{
//		public static final int MAGIC = 0;
//		public static final int VERSION = 2;
		public static final int IN_USE = 4;
		public static final int FREE_MEM = 8;
		public static final int FIRST_TABLE = 12;
		
		private Addresses( ) { }
	}
	
	private static final int SIGNATURE = 0x7eda;
	private static final int MAGIC_IN_USE = 0x7a3d91b0;
	private static final int MAGIC_DONE = 0x538f30c9;
	private static final int NO_OF_RECORDS = 64;
	private static final int VERSION = 1;
	
	private static final Logger LOG = Logger.get("io-storage");
}
