package dave.util.persistence.file;

import dave.util.io.IO;

public class SimpleMemoryManager implements MemoryManager
{
	private final IO mIO;
	private final Container<Block> mBuffer;
	
	public SimpleMemoryManager(IO io, Container<Block> b)
	{
		mIO = io;
		mBuffer = b;
	}

	public Chunk allocate(int min)
	{
		for(Block b : mBuffer)
		{
			if(b.length >= min)
			{
				mBuffer.remove(b);
				
				if(b.address + b.length == mIO.size())
				{
					return new LastChunk(b);
				}
				else
				{
					return new RegularChunk(b);
				}
			}
		}
		
		return new EOFChunk();
	}
	
	public void free(Block b)
	{
		mBuffer.insert(b);
	}
	
// # --------------------------------------------------------------------------
	
	private class RegularChunk implements Chunk
	{
		private final Block mBlock;
		
		public RegularChunk(Block b)
		{
			mBlock = b;
		}
		
		protected Block getRawBlock( ) { return mBlock; }
		
		@Override
		public int getStartAddress( )
		{
			return mBlock.address;
		}
		
		@Override
		public int getEffectiveSize( )
		{
			return mBlock.length;
		}
		
		@Override
		public Block toBlock( )
		{
			Block b = mBlock;
			int a = mIO.seek();
			int l = a - b.address;
			int leftover = b.length - l;
			
			if(leftover > Block.HEADER_SIZE)
			{
				mBuffer.insert(new Block(a, leftover));
				b = new Block(b.address, l);
			}
			
			return b;
		}
		
		@Override
		public Chunk clone( )
		{
			int a = mIO.seek();
			Block b = new Block(a, mBlock.length - (a - mBlock.address));
			
			return new RegularChunk(b);
		}
	}
	
	private class LastChunk extends RegularChunk
	{
		public LastChunk(Block b)
		{
			super(b);
		}
		
		@Override
		public int getEffectiveSize( )
		{
			return Integer.MAX_VALUE;
		}
		
		@Override
		public Block toBlock( )
		{
			Block b = super.toBlock();
			int a = mIO.seek();
			
			if(a > b.address + b.length)
			{
				b = new Block(b.address, a - b.address);
			}
			
			return b;
		}
		
		@Override
		public Chunk clone( )
		{
			int a = mIO.seek();
			Block b = super.getRawBlock();
			
			b = new Block(a, b.length - (a - b.address));
			
			return new LastChunk(b);
		}
	}
	
	private class EOFChunk implements Chunk
	{
		private final int mAddr = mIO.size();
		
		@Override
		public int getStartAddress( )
		{
			return mAddr;
		}
		
		@Override
		public int getEffectiveSize( )
		{
			return Integer.MAX_VALUE;
		}
		
		@Override
		public Block toBlock( )
		{
			return new Block(mAddr, mIO.seek() - mAddr);
		}
		
		@Override
		public Chunk clone( )
		{
			return new EOFChunk();
		}
	}
}
