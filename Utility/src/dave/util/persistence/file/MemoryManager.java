package dave.util.persistence.file;

public interface MemoryManager
{
	public abstract Chunk allocate(int min);
	public abstract void free(Block b);
}
