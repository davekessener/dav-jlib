package dave.util.persistence.file;

import java.util.Iterator;

import dave.util.DirtyObject;

public class LinkedBlockContainer extends DirtyObject implements Container<Block>
{
	private final Cell mRoot;
	
	public LinkedBlockContainer( )
	{
		mRoot = new Cell();
		
		mRoot.address = Integer.MAX_VALUE;
		mRoot.length = 0;
		mRoot.next = mRoot.previous = mRoot;
	}
	
	@Override
	public void insert(Block b)
	{
		use();
		
		int a = b.address;
		int l = b.length;
		
		for(Cell c = mRoot.next ; true ; c = c.next)
		{
			if(a < c.address)
			{
				if(c.previous.address + c.previous.length == a)
				{
					c = c.previous;
					c.length += l;
					
					if(c.address + c.length == c.next.address)
					{
						c.length += c.next.length;
						c.next = c.next.next;
						c.next.previous = c;
					}
				}
				else if(a + l == c.address)
				{
					c.address = a;
					c.length += l;
				}
				else
				{
					Cell e = new Cell();
					
					e.address = b.address;
					e.length = b.length;
					e.previous = c.previous;
					e.next = c;
					e.previous.next = e;
					e.next.previous = e;
				}
				
				return;
			}
		}
	}
	
	@Override
	public void remove(Block b)
	{
		use();
		
		for(Cell c = mRoot.next ; c != mRoot ; c = c.next)
		{
			if(c.address == b.address)
			{
				c.previous.next = c.next;
				c.next.previous = c.previous;
				
				return;
			}
		}
		
		throw new IllegalArgumentException(b.toString());
	}

	@Override
	public Iterator<Block> iterator( )
	{
		return new Iterator<Block>() {
			private Cell mCell = mRoot;
			
			@Override
			public Block next( )
			{
				mCell = mCell.next;
				
				return new Block(mCell.address, mCell.length);
			}
			
			@Override
			public boolean hasNext( )
			{
				return mCell.next != mRoot;
			}
		};
	}
	
	private static class Cell
	{
		public int address, length;
		public Cell next, previous;
	}
}
