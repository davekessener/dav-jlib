package dave.util.persistence.file;

import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class Record implements Iterable<Block>
{
	private final int mAddr;
	private final int mLength;
	private final Name mName;
	private final List<Block> mData;
	
	public Record(int a, Record old)
	{
		this(a, old.mLength, old.mName, old.mData);
	}
	
	public Record(int a, int dl, Name n, List<Block> d)
	{
		mAddr = a;
		mLength = dl;
		mName = n;
		mData = d;
		
		if(mAddr <= 0)
			throw new IllegalArgumentException();
		
		if(mLength <= 0)
			throw new IllegalArgumentException();
		
		if(mData.size() <= 0)
			throw new IllegalArgumentException();
		
		if(mName == null)
			throw new NullPointerException();
	}
	
	public int getAddress( ) { return mAddr; }
	public int getDataLength( ) { return mLength; }
	public Name getName( ) { return mName; }
	
	public Stream<Block> stream( )
	{
		return StreamSupport.stream(this.spliterator(), false);
	}

	@Override
	public Iterator<Block> iterator( )
	{
		return mData.iterator();
	}
	
	@Override
	public int hashCode( )
	{
		int c = Integer.hashCode(mAddr) ^ Integer.hashCode(mLength) ^ mName.hashCode();
		
		for(Block b : mData)
		{
			c ^= b.hashCode();
		}
		
		return c;
	}
	
	@Override
	public boolean equals(Object o)
	{
		if(o instanceof Record)
		{
			Record r = (Record) o;
			
			return mAddr == r.mAddr && mName.equals(r.mName) && mData.equals(r.mData);
		}
		
		return false;
	}
	
	@Override
	public String toString( )
	{
		return String.format("Record '%s' @$%08x %dB (%d blocks)", mName, mAddr, mLength, mData.size());
	}
}
