package dave.util.persistence.file;

import java.util.Iterator;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import dave.util.DirtyObject;
import dave.util.io.SeekableInput;
import dave.util.io.SeekableOutput;

public class RecordTable extends DirtyObject implements Iterable<RecordTable.Entry>
{
	private final int mAddress;
	private final Entry[] mEntries;
	private int mSize;
	
	public RecordTable(int a, int s)
	{
		mAddress = a;
		mEntries = new Entry[s];
		mSize = 0;
		
		use();
	}
	
	public int getAddress( )
	{
		return mAddress;
	}
	
	public boolean isFull( )
	{
		return mSize == mEntries.length;
	}
	
	public void add(Record r)
	{
		if(isFull())
			throw new IllegalArgumentException(r.toString());
		
		use();
		
		for(int i = 0 ; i < mEntries.length ; ++i)
		{
			if(mEntries[i] == null)
			{
				mEntries[i] = new Entry(r.getName().computeHash(), r.getAddress());
				++mSize;
				break;
			}
		}
	}
	
	public void remove(Record r)
	{
		int hash = r.getName().computeHash();
		
		for(int i = 0 ; i < mEntries.length ; ++i)
		{
			if(mEntries[i] != null && mEntries[i].hash == hash)
			{
				mEntries[i] = null;
				--mSize;
				
				use();
				
				return;
			}
		}
		
		throw new IllegalArgumentException(r.toString());
	}
	
	public void write(SeekableOutput out)
	{
		out.seek(mAddress + 4);

		stream().forEach(e -> {
			out.writeInt(e.hash);
			out.writeInt(e.file);
		});
	}
	
	public void read(SeekableInput in)
	{
		in.seek(mAddress + 4);
		
		for(int i = 0 ; i < mEntries.length ; ++i)
		{
			int hash = in.readInt();
			int file = in.readInt();
			
			if(file != 0)
			{
				mEntries[i] = new Entry(hash, file);
			}
		}
	}
	
	public Stream<Entry> stream( )
	{
		return StreamSupport.stream(this.spliterator(), false);
	}
	
	@Override
	public Iterator<Entry> iterator( )
	{
		return new Iterator<Entry>() {
			private int mIdx = 0;
			
			@Override
			public Entry next( )
			{
				Entry e = mEntries[mIdx];
				
				if(e == null)
				{
					e = new Entry(0, 0);
				}
				
				++mIdx;
				
				return e;
			}
			
			@Override
			public boolean hasNext( )
			{
				return mIdx < mEntries.length;
			}
		};
	}

	public static class Entry
	{
		public final int hash;
		public final int file;
		
		public Entry(int h, int f)
		{
			hash = h;
			file = f;
		}
	}
}
