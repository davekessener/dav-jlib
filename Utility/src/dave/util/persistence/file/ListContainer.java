package dave.util.persistence.file;

import java.util.Iterator;
import java.util.List;

import dave.util.DirtyObject;

public class ListContainer<T> extends DirtyObject implements Container<T>
{
	private final List<T> mBase;
	
	public ListContainer(List<T> base)
	{
		mBase = base;
	}

	@Override
	public Iterator<T> iterator( )
	{
		return mBase.iterator();
	}

	@Override
	public void insert(T o)
	{
		mBase.add(o);
		
		use();
	}

	@Override
	public void remove(T o)
	{
		mBase.remove(o);
		
		use();
	}
}
