package dave.util.persistence.file;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.function.Consumer;

import dave.json.JsonValue;
import dave.util.Producer;
import dave.util.Utils;
import dave.util.io.OutputPrinter;
import dave.json.SevereIOException;

public class AlternatingFileWriter implements Consumer<JsonValue>
{
	private final Producer<File> mProd;
	private File mOld, mNew;
	
	public AlternatingFileWriter(Producer<File> p)
	{
		mProd = p;
	}
	
	private void update( )
	{
		if(mOld != null)
		{
			mOld.delete();
		}
		
		mOld = mNew;
		mNew = mProd.produce();
	}

	@Override
	public void accept(JsonValue data)
	{
		update();
		
		try(OutputStream out = new FileOutputStream(mNew))
		{
			data.write(new OutputPrinter((b, o, l) -> Utils.wrap(() -> out.write(b, o, l)).run(), Utils.CHARSET));
		}
		catch(IOException e)
		{
			throw new SevereIOException(e);
		}
	}
}
