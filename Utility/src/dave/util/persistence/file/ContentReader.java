package dave.util.persistence.file;

import java.util.Iterator;

import dave.json.JsonValue;
import dave.util.Utils;
import dave.util.io.Input;
import dave.util.io.InputBuffer;
import dave.util.io.SeekableInput;
import dave.util.io.SegmentedInput;

public class ContentReader
{
	private final SeekableInput mIO;
	
	public ContentReader(SeekableInput io)
	{
		mIO = io;
	}
	
	public JsonValue read(Record r)
	{
		if(r == null) return null;
		
		final Iterator<Block> i = r.iterator();
		final Input in = new SegmentedInput(mIO, () -> {
			Block b = i.next();
			
			mIO.seek(b.address + Block.HEADER_SIZE);
			
			return b.length - Block.HEADER_SIZE;
		});
		
		return JsonValue.read(new InputBuffer(r.getDataLength(), in, Utils.CHARSET));
	}
}
