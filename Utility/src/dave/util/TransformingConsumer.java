package dave.util;

import java.util.function.Consumer;

public class TransformingConsumer<S, T> implements Consumer<S>
{
	private final Consumer<T> mCallback;
	private final Transformer<S, T> mTrans;
	
	public TransformingConsumer(Consumer<T> cb, Transformer<S, T> t)
	{
		mCallback = cb;
		mTrans = t;
	}
	
	@Override
	public void accept(S o)
	{
		mCallback.accept(mTrans.create(o));
	}
}
