package dave.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.concurrent.Callable;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.DoubleUnaryOperator;
import java.util.function.Function;
import java.util.function.IntConsumer;
import java.util.function.Supplier;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import dave.json.SevereIOException;
import dave.util.ShutdownService.Priority;
import dave.util.io.FileIO;
import dave.util.log.Logger;
import dave.util.log.Severity;
import dave.util.math.Vec3;
import dave.util.persistence.file.IOStorage;

public final class Utils
{
	private static final List<Thread> sCleanupThreads = new ArrayList<>();
	private static boolean sThreadsRunning = true;
	
	public static String join(String[] s, String p)
	{
		return Stream.of(s).collect(Collectors.joining(p));
	}
	
	public static Vec3 lerp(Vec3 a, Vec3 b, double x) { return a.add(b.sub(a).scale(x)); }
	
	public static void runLater(int ms, ThrowingRunnable f)
	{
		long start = System.currentTimeMillis();
		
		Thread t = new Thread(wrap(() -> {
			while(sThreadsRunning && (System.currentTimeMillis() - start) < ms)
			{
				sleep(1);
			}
			
			if(sThreadsRunning)
			{
				f.run();
			}
		}));
		
		if(sCleanupThreads.isEmpty())
		{
			ShutdownService.INSTANCE.register(Priority.LAST, Utils::cleanupThreads);
		}
		
		sCleanupThreads.add(t);
		
		t.start();
	}
	
	private static void cleanupThreads()
	{
		sThreadsRunning = false;
		sCleanupThreads.forEach(t -> run(() -> t.join(5)));
	}
	
	public static boolean supportsANSI() { return System.console() != null && System.getenv("TERM") != null; }
	
	public static float[] to_float(double[] v)
	{
		float[] r = new float[v.length];
		
		for(int i = 0 ; i < r.length ; ++i)
		{
			r[i] = (float) v[i];
		}
		
		return r;
	}
	
	public static byte[] to_byte(int[] v)
	{
		byte[] r = new byte[v.length];
		
		for(int i = 0 ; i < v.length ; ++i)
		{
			r[i] = (byte) v[i];
		}
		
		return r;
	}
	
	public static void times(int l, IntConsumer f)
	{
		for(int i = 0 ; i < l ; ++i)
		{
			f.accept(i);
		}
	}
	
	public static String basename_without_extension(File f)
	{
		String fn = f.getName();
		int i = fn.lastIndexOf('.');
		
		return (i == -1 ? fn : fn.substring(0, i));
	}
	
	public static double[] join(double[] ... a)
	{
		return Stream.of(a).flatMapToDouble(e -> DoubleStream.of(e)).toArray();
	}
	
	public static void pointwiseTransform(double[] a, DoubleUnaryOperator f)
	{
		for(int i = 0 ; i < a.length ; ++i)
		{
			a[i] = f.applyAsDouble(a[i]);
		}
	}
	
	public static <V, T> Optional<T> tryToGet(Function<V, T> f, V v) { return tryToGet(() -> f.apply(v)); }
	public static <V1, V2, T> Optional<T> tryToGet(BiFunction<V1, V2, T> f, V1 v1, V2 v2) { return tryToGet(() -> f.apply(v1, v2)); }
	public static <T> Optional<T> tryToGet(Supplier<T> f)
	{
		try
		{
			return Optional.ofNullable(f.get());
		}
		catch(Throwable e)
		{
			return Optional.empty();
		}
	}
	
	public static <T> T any(T[] v) { return any(v, new XoRoRNG()); }
	public static <T> T any(T[] v, RNG rng) { return v[(int) (rng.nextDouble() * v.length)]; }
	
	public static <T> T tap(T o, ThrowingConsumer<T> f) { return tap(o, f, e -> { throw new SevereException(e); }); }
	public static <T> T tap(T o, ThrowingConsumer<T> f, Consumer<Exception> cb)
	{
		try
		{
			f.accept(o);
		}
		catch(Exception e)
		{
			cb.accept(e);
		}
		
		return o;
	}
	
	public static <T> String array_to_s(T[] a)
	{
		return "[" + Stream.of(a).map(Object::toString).collect(Collectors.joining(", ")) + "]";
	}
	
	public static String array_to_s(double[] v)
	{
		return "[" + DoubleStream.of(v).mapToObj(e -> "" + e).collect(Collectors.joining(", ")) + "]";
	}
	
	public static <T> void shuffle(List<T> l, RNG rng)
	{
		for(int i = l.size() - 1 ; i > 0 ; --i)
		{
			int p = Math.floorMod(rng.nextInt(), i);
			
			T t = l.get(i);
			l.set(i, l.get(p));
			l.set(p, t);
		}
	}
	
	public static void serialize(String path, Serializable s)
	{
		try (FileOutputStream out = new FileOutputStream(path))
		{
			ObjectOutputStream io = new ObjectOutputStream(out);
			
			io.writeObject(s);
		}
		catch(IOException e)
		{
			throw new SevereIOException(e);
		}
	}
	
	@SuppressWarnings("unchecked")
	public static <T> T deserialize(String path)
	{
		try (FileInputStream in = new FileInputStream(path))
		{
			ObjectInputStream io = new ObjectInputStream(in);
			
			return (T) io.readObject();
		}
		catch(IOException e)
		{
			throw new SevereIOException(e);
		}
		catch (ClassNotFoundException e)
		{
			throw new SevereException(e);
		}
	}
	
	public static void sleep(long ms)
	{
		try
		{
			Thread.sleep(ms);
		}
		catch(InterruptedException e)
		{
			throw new SevereException(e);
		}
	}
	
	public static String hash(byte[] data) { return hash(data, "MD5"); }
	public static String hash(byte[] data, String type)
	{
		return Utils.run(() -> {
			MessageDigest digest = MessageDigest.getInstance(type);
			
			digest.update(data);
			
			byte[] hash = digest.digest();
			
			return printHexBinary(hash);
		});
	}
	
	public static String printHexBinary(byte[] a)
	{
		StringBuilder sb = new StringBuilder();
		
		for(int i = 0 ; i < a.length ; ++i)
		{
			sb.append(String.format("%02x", a[i]));
		}
		
		return sb.toString();
	}
	
	public static <T> Iterable<T> reverse(List<T> c)
	{
		return () -> new Iterator<T>() {
			private int idx = c.size() - 1;
			
			@Override
			public boolean hasNext()
			{
				return idx >= 0;
			}

			@Override
			public T next()
			{
				return c.get(idx--);
			}
		};
	}
	
	public static <K, V> Collector<Pair<K, V>, ?, Map<K, V>> toMap()
	{
		return Collectors.toMap(e -> e.first, e -> e.second);
	}
	
	public static File getLastFile(String pre)
	{
		final Pattern ptrn = Pattern.compile(pre + "-([0-9])+\\.bin");
		
		File root = new File(".");
		File r = null;
		int ts = -1;
		File[] fs = root.listFiles();
		
		for(int i = 0 ; i < fs.length ; ++i)
		{
			final File f = fs[i];
			Matcher m = ptrn.matcher(f.getName());
			
			if(!m.find()) continue;
			
			int t = Integer.parseInt(m.group(1));
			
			if(t > ts) try
			{
				new IOStorage(() -> new FileIO(f));
				
				if(r != null)
				{
					Logger.DEFAULT.log(Severity.INFO, "Deleting old file '%s'!", r.getName());
					
					r.delete();
				}
				
				r = f;
				ts = t;
			}
			catch(IllegalStateException e)
			{
				Logger.DEFAULT.log(Severity.ERROR, "Invalid file '%s'!", f.getName());
			}
		}
		
		return r;
	}
	
	public static String repeat(String s, int n)
	{
		StringBuilder sb = new StringBuilder();
		
		while(n-- > 0) sb.append(s);
		
		return sb.toString();
	}

	public static class Pair<U, V>
	{
		public U first;
		public V second;
		
		public Pair( ) { this(null, null); }
		public Pair(U f, V s) { first = f; second = s; }
		
		@Override
		public String toString()
		{
			return String.format("(%s, %s)", first, second);
		}
		
		@Override
		public int hashCode()
		{
			return (Objects.hashCode(first) * 3) ^ (Objects.hashCode(second) * 7);
		}
		
		@SuppressWarnings("unchecked")
		@Override
		public boolean equals(Object o)
		{
			if(o instanceof Pair)
			{
				Pair<U, V> p = (Pair<U, V>) o;
				
				return Objects.deepEquals(first, p.first) && Objects.deepEquals(second, p.second);
			}
			
			return false;
		}
	}

	public static class Triple<U, V, W>
	{
		public U first;
		public V second;
		public W third;
		
		public Triple( ) { this(null, null, null); }
		public Triple(U f, V s, W t) { first = f; second = s; third = t; }
		
		@Override
		public String toString()
		{
			return String.format("(%s, %s, %s)", first, second, third);
		}
		
		@Override
		public int hashCode()
		{
			return Objects.hash(first, second, third);
		}
		
		@SuppressWarnings("unchecked")
		@Override
		public boolean equals(Object o)
		{
			if(o instanceof Triple)
			{
				Triple<U, V, W> p = (Triple<U, V, W>) o;
				
				return Objects.deepEquals(first, p.first)
					&& Objects.deepEquals(second, p.second)
					&& Objects.deepEquals(third, p.third);
			}
			
			return false;
		}
	}
	
	public static <U, V> Pair<U, V> pair(U u, V v)
	{
		return new Pair<>(u, v);
	}
	
	public static <U, V, W> Triple<U, V, W> triple(U u, V v, W w)
	{
		return new Triple<>(u, v, w);
	}
	
	public static <U, V> Pair<U, V> pair(Map.Entry<U, V> e)
	{
		return pair(e.getKey(), e.getValue());
	}
	
	public static <T> Stream<Pair<T, Integer>> stream_with_index(T[] a)
	{
		return StreamSupport.stream(new Spliterators.AbstractSpliterator<Pair<T, Integer>>(Long.MAX_VALUE, Spliterator.ORDERED) {
			private int mIdx = 0;

			@Override
			public boolean tryAdvance(Consumer<? super Pair<T, Integer>> action)
			{
				boolean f = mIdx < a.length;
				
				if(f)
				{
					action.accept(new Pair<>(a[mIdx], mIdx));
					
					++mIdx;
				}
				
				return f;
			}
		}, false);
	}
	
	public static <T> Stream<T> stream(Enumeration<T> e)
	{
		return StreamSupport.stream((new Spliterators.AbstractSpliterator<T>(Long.MAX_VALUE, Spliterator.ORDERED) {
			private final Enumeration<T> mEnum = e;
			
			@Override
			public boolean tryAdvance(Consumer<? super T> action)
			{
				boolean r = mEnum.hasMoreElements();
				
				if(r)
				{
					action.accept(mEnum.nextElement());
				}
				
				return r;
			}
			
			@Override
			public void forEachRemaining(Consumer<? super T> action)
			{
				while(mEnum.hasMoreElements())
				{
					action.accept(mEnum.nextElement());
				}
			}
		}), false);
	}
	
	public static <T> Stream<T> stream(Iterable<T> e)
	{
		return StreamSupport.stream(e.spliterator(), false);
	}
	
	public static interface ThrowingConsumer<T> { void accept(T o) throws Exception; }
	public static interface ThrowingFunction<S, T> { T apply(S s) throws Exception; }
	public static interface ThrowingRunnable { void run( ) throws Exception; }
	
	public static <S, T> Function<S, T> wrap(ThrowingFunction<S, T> f)
	{
		return s -> {
			try
			{
				return f.apply(s);
			}
			catch(Exception e)
			{
				throw new SevereException(e);
			}
		};
	}
	
	public static <T> T run(Callable<T> f)
	{
		try
		{
			return f.call();
		}
		catch(Exception e)
		{
			throw new SevereException(e);
		}
	}
	
	public static void run(ThrowingRunnable f) { run(f, e -> { throw new SevereException(e); }); }
	public static void run(ThrowingRunnable f, Consumer<Exception> cb)
	{
		try
		{
			f.run();
		}
		catch(Exception e)
		{
			cb.accept(e);
		}
	}
	
	public static Runnable wrap(ThrowingRunnable f) { return wrap(f, e -> { throw new SevereException(e); }); }
	public static Runnable wrap(ThrowingRunnable f, Consumer<Exception> on_e)
	{
		return () -> {
			try
			{
				f.run();
			}
			catch(Exception e)
			{
				on_e.accept(e);
			}
		};
	}
	
	public static <T> T wrap(Callable<T> f)
	{
		try
		{
			return f.call();
		}
		catch(Exception e)
		{
			throw new SevereException(e);
		}
	}
	
	public static <T> Consumer<T> wrap(ThrowingConsumer<T> f)
	{
		return v -> wrap(() -> f.accept(v)).run();
	}
	
	public static <T> Consumer<T> wrapConsumer(ThrowingConsumer<T> f)
	{
		return wrap(f);
	}

	public static <T> T[] append(T[] a, T o)
	{
		T[] r = Arrays.copyOf(a, a.length + 1);
		
		r[r.length - 1]  = o;
		
		return r;
	}
	
	@SuppressWarnings("unchecked")
	public static <T> T[] prepend(T o, T[] a)
	{
		Object[] r = new Object[a.length + 1];
		
		r[0] = o;
		for(int i = 1 ; i < r.length ; ++i)
		{
			r[i] = a[i - 1];
		}
		
		return (T[]) r;
	}
	
	public static void writeToFile(String path, String content)
	{
		try
		{
			Files.write(Paths.get(path), content.getBytes(CHARSET));
		}
		catch (IOException e)
		{
			throw new IllegalStateException(e);
		}
	}
	
	public static String readFromFile(String ... path) { return readFromFile(CHARSET, path); }
	public static String readFromFile(String path) { return readFromFile(path, CHARSET); }
	public static String readFromFile(Charset cs, String ... path) { return readFromFile(Stream.of(path).collect(Collectors.joining(File.separator)), cs); }
	public static String readFromFile(String path, Charset cs)
	{
		try
		{
			return new String(Files.readAllBytes(Paths.get(path)), cs);
		}
		catch (IOException e)
		{
			throw new SevereIOException(e);
		}
	}
	
	public static interface Consumer_2D<T> { public abstract void consume(int x, int y, T v); }
	
	public static <T> void forEach(T[][] a, Consumer_2D<T> c)
	{
		for(int y = 0 ; y < a.length ; ++y)
		{
			for(int x = 0 ; x < a[0].length ; ++x)
			{
				c.consume(x, y, a[y][x]);
			}
		}
	}
	
	public static void forEach(byte[][] a, Consumer_2D<Byte> c)
	{
		for(int y = 0 ; y < a.length ; ++y)
		{
			for(int x = 0 ; x < a[0].length ; ++x)
			{
				c.consume(x, y, a[y][x]);
			}
		}
	}
	
	public static int i_sqrt(int n)
	{
		int l = 1, u = n;
		
		while(l != u)
		{
			int t = (l + u) / 2;
			int tt = t * t;
			
			int ol = l, ou = u;
			
			if(tt == n)
			{
				return t;
			}
			else if(tt < n)
			{
				l = t;
			}
			else
			{
				u = t;
			}
			
			if(l == ol && u == ou)
				throw new IllegalArgumentException();
		}
		
		throw new IllegalArgumentException();
	}

	public static int min(int a, int b) { return a < b ? a : b; }
	public static int max(int a, int b) { return a > b ? a : b; }
	
	public static byte[][] makeMap(String[] s)
	{
		int l = s[0].length();
		byte[][] r = new byte[s.length][l];
		
		for(int i = 0 ; i < s.length ; ++i)
		{
			if(s[i].length() != l)
				throw new IllegalArgumentException();
			
			for(int j = 0 ; j < l ; ++j)
			{
				r[i][j] = (byte) (s[i].charAt(j) - '0');
			}
		}
		
		return r;
	}

	public static final Charset CHARSET = Charset.forName("UTF-8");
	public static final Hasher<String> HASHER = new SimpleStringHasher();
	
	private Utils( ) { }
}
