package dave.util;

public class SimpleStringHasher implements Hasher<String>
{
	@Override
	public int compute(String o)
	{
		int v = 0;
		
		for(int i = 0 ; i < o.length() ; ++i)
		{
			v = (v * 13) + o.charAt(i);
		}
		
		return v ^ PRIME;
	}
	
	private static final int PRIME = 2147483647;
}
