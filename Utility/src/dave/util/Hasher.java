package dave.util;

public interface Hasher<T>
{
	public default int compute(T o)
	{
		return o.hashCode();
	}
}
