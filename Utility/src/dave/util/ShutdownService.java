package dave.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ShutdownService
{
	public static ShutdownService INSTANCE = new ShutdownService();
	
	public static enum Priority
	{
		FIRST,
		EARLIER,
		MEDIUM,
		LATER,
		LAST
	}
	
	private final Map<Priority, List<Runnable>> mCallbacks;
	private boolean mIsShutdown;
	
	private ShutdownService( )
	{
		mCallbacks = new HashMap<>();
		mIsShutdown = false;
	}
	
	public void register(Runnable f) { register(Priority.MEDIUM, f); }
	public void register(Priority p, Runnable f)
	{
		if(mIsShutdown)
			throw new IllegalStateException();
		
		getCallbacks(p).add(f);
	}
	
	public void shutdown( )
	{
		if(mIsShutdown)
			throw new IllegalStateException();
		
		for(int i = 0 ; i < Priority.values().length ; ++i)
		{
			List<Runnable> cb = mCallbacks.get(Priority.values()[i]);
			
			if(cb != null)
			{
				cb.stream().forEachOrdered(Runnable::run);
			}
		}
		
		mIsShutdown = true;
	}
	
	private List<Runnable> getCallbacks(Priority p)
	{
		List<Runnable> cb = mCallbacks.get(p);
		
		if(cb == null)
		{
			mCallbacks.put(p, cb = new ArrayList<>());
		}
		
		return cb;
	}
}
