package dave.util;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import dave.util.math.IVec2;
import dave.util.property.NonNullProperty;
import dave.util.property.Property;
import dave.util.property.SimpleProperty;

public class QuadTree<T> implements Association<IVec2, T>
{
	private final Property<Node> mRoot;
	private final EmptyPolicy<IVec2, T> mOnEmpty;
	private final OverwritePolicy<IVec2, T> mOnOverwrite;
	
	protected QuadTree(int s, EmptyPolicy<IVec2, T> f1, OverwritePolicy<IVec2, T> f2)
	{
		mRoot = new SimpleProperty<>();
		mOnEmpty = f1;
		mOnOverwrite = f2;
		
		mRoot.set(new Node(AABB.fromPoints(new IVec2(-s, -s), new IVec2(s, s))));
	}
	
	@Override
	public Stream<T> stream()
	{
		return mRoot.get().stream();
	}
	
	@Override
	public void forEach(Consumer<T> f)
	{
		mRoot.get().forEach(f);
	}
	
	@Override
	public boolean contains(IVec2 k)
	{
		return mRoot.get().contains(k);
	}
	
	@Override
	public void put(IVec2 k, T v)
	{
		AABB bb = mRoot.get().boundingBox();
		
		if(bb.inside(k))
		{
			mRoot.get().insert(new Entry<>(k, v));
		}
		else
		{
			int x0 = bb.min().x, x1 = bb.max().x;
			int y0 = bb.min().y, y1 = bb.max().y;
			int l = bb.width();

			AABB[] aabb = new AABB[] {
				AABB.fromPoints(new IVec2(x0 + 0, y0 + 0), new IVec2(x1 + l, y1 + l)),
				AABB.fromPoints(new IVec2(x0 + 0, y0 - l), new IVec2(x1 + l, y1 + 0)),
				AABB.fromPoints(new IVec2(x0 - l, y0 + 0), new IVec2(x1 + 0, y1 + l)),
				AABB.fromPoints(new IVec2(x0 - l, y0 - l), new IVec2(x1 + 0, y1 + 0))
			};
			
			int i = (k.x < bb.min().x ? 2 : 0) | (k.y < bb.min().y ? 1 : 0);
			Node n = new Node(aabb[i]);
			
			n.split();
			n.substitute(mRoot.get());
			
			mRoot.set(n);
			
			put(k, v);
		}
	}

	@Override
	public T get(IVec2 k)
	{
		return mRoot.get().get(k);
	}
	
	@Override
	public void remove(IVec2 k)
	{
		mRoot.get().remove(k);
	}
	
	@Override
	public String toString()
	{
		return inspect();
	}
	
	public String inspect()
	{
		return mRoot.get().inspect(0);
	}

	private static final int LIMIT = 5;
	private static final int SIZE = 10;
	
	public static class Builder<V>
	{
		private final Property<Integer> mSize;
		private final Property<EmptyPolicy<IVec2, V>> mOnEmpty;
		private final Property<OverwritePolicy<IVec2, V>> mOnOverwrite;
		
		public Builder()
		{
			mSize = new NonNullProperty<>(SIZE);
			mOnEmpty = new NonNullProperty<>(new OnEmptyThrowException<>());
			mOnOverwrite = new NonNullProperty<>(new OnOverwriteReplace<>());
		}
		
		public Builder<V> setSize(int v) { mSize.set(v); return this; }
		public Builder<V> setOnEmpty(EmptyPolicy<IVec2, V> f) { mOnEmpty.set(f); return this; }
		public Builder<V> setOnOverwrite(OverwritePolicy<IVec2, V> f) { mOnOverwrite.set(f); return this; }
		
		public QuadTree<V> build()
		{
			return new QuadTree<>(mSize.get(), mOnEmpty.get(), mOnOverwrite.get());
		}
	}
	
	public static <V> QuadTree<V> build() { return (new Builder<V>()).build(); }
	
	private static class AABB
	{
		private final IVec2 mMin, mMax;
		
		private AABB(IVec2 a, IVec2 b)
		{
			mMin = new IVec2(Math.min(a.x, b.x), Math.min(a.y, b.y));
			mMax = new IVec2(Math.max(a.x, b.x), Math.max(a.y, b.y));
		}
		
		public IVec2 min() { return mMin; }
		public IVec2 max() { return mMax; }
		
		public boolean inside(IVec2 p)
		{
			return mMin.x <= p.x && p.x < mMax.x && mMin.y <= p.y && p.y < mMax.y;
		}
		
		public int width() { return mMax.x - mMin.x; }
		public int height() { return mMax.y - mMin.y; }
		
		public IVec2 center() { return mMin.add(width() / 2, height() / 2); }
		
		@Override
		public boolean equals(Object o)
		{
			if(o instanceof AABB)
			{
				AABB bb = (AABB) o;
				
				return mMin.equals(bb.mMin) && mMax.equals(bb.mMax);
			}
			
			return false;
		}
		
		@Override
		public String toString()
		{
			return mMin.toString() + " -> " + mMax.toString();
		}
		
		public static AABB fromPoints(IVec2 a, IVec2 b)
		{
			return new AABB(a, b);
		}
		
//		public static AABB fromSize(IVec3 a, IVec3 b)
//		{
//			return new AABB(a, a.add(b));
//		}
	}
	
	private class Node
	{
		private final AABB mBB;
		private final List<Entry<T>> mElements;
		private final Node[] mChildren;
		private final IVec2 mCenter;
		private final boolean mFinal;
		
		@SuppressWarnings("unchecked")
		public Node(AABB bb)
		{
			mBB = bb;
			mElements = new ArrayList<>();
			mChildren = (Node[]) Array.newInstance(Node.class, 4);
			mCenter = mBB.center();
			mFinal = (bb.width() == 1 || bb.height() == 1);
		}
		
		public AABB boundingBox() { return mBB; }
		public boolean isLeaf() { return mChildren[0] == null; }
		public int unique() { return (int) mElements.stream().map(e -> e.position).distinct().count(); }
		
		public Stream<T> stream()
		{
			if(isLeaf())
			{
				return mElements.stream().map(e -> e.element);
			}
			else
			{
				return Stream.of(mChildren).flatMap(c -> c.stream());
			}
		}
		
		public void forEach(Consumer<T> f)
		{
			if(isLeaf())
			{
				mElements.forEach(e -> f.accept(e.element));
			}
			else
			{
				Stream.of(mChildren).forEach(n -> n.forEach(f));
			}
		}
		
		public boolean contains(IVec2 k)
		{
			if(isLeaf())
			{
				return mElements.stream().anyMatch(e -> e.position.equals(k));
			}
			else
			{
				return mChildren[index(k)].contains(k);
			}
		}
		
		public void insert(Entry<T> e)
		{
			if(isLeaf())
			{
				boolean r = false;
				
				for(ListIterator<Entry<T>> i = mElements.listIterator() ; i.hasNext() ;)
				{
					Entry<T> x = i.next();
					
					if(x.position.equals(e.position))
					{
						i.set(new Entry<>(e.position, QuadTree.this.mOnOverwrite.onOverwrite(e.position, x.element, e.element)));
						
						r = true;
						break;
					}
				}
				
				if(!r)
				{
					mElements.add(e);
				}
				
				if(!mFinal && unique() > LIMIT)
				{
					split();
				}
			}
			else
			{
				mChildren[index(e.position)].insert(e);
			}
		}
		
		public void remove(IVec2 k)
		{
			if(isLeaf())
			{
				mElements.removeIf(e -> e.position.equals(k));
			}
			else
			{
				mChildren[index(k)].remove(k);
			}
		}
		
		public T get(IVec2 p)
		{
			if(isLeaf())
			{
				return mElements.stream()
					.filter(e -> e.position.x == p.x && e.position.y == p.y)
					.map(e -> e.element)
					.findFirst().orElseGet(() -> {
						T e = QuadTree.this.mOnEmpty.onEmpty(p);
						
						if(e != null)
						{
							insert(new Entry<>(p, e));
						}
						
						return e;
					});
			}
			else
			{
				return mChildren[index(p)].get(p);
			}
		}
		
		public void substitute(Node n)
		{
			for(int i = 0 ; i < 4 ; ++i)
			{
				if(mChildren[i].mBB.equals(n.mBB))
				{
					mChildren[i] = n;
					
					return;
				}
			}
			
			throw new IllegalStateException();
		}
		
		public void split()
		{
			int x0 = mBB.min().x, x1 = mBB.max().x;
			int y0 = mBB.min().y, y1 = mBB.max().y;
			IVec2 c = new IVec2(mCenter.x, mCenter.y);
			
			mChildren[0] = new Node(AABB.fromPoints(new IVec2(x0, y0), c));
			mChildren[1] = new Node(AABB.fromPoints(new IVec2(x1, y0), c));
			mChildren[2] = new Node(AABB.fromPoints(new IVec2(x0, y1), c));
			mChildren[3] = new Node(AABB.fromPoints(new IVec2(x1, y1), c));
			
			mElements.forEach(e -> insert(e));
			mElements.clear();
		}
		
		@Override
		public String toString()
		{
			return inspect(0);
		}
		
		public String inspect(int ind)
		{
			StringBuilder sb = new StringBuilder();
			
			sb.append("{[");
			sb.append(mBB.toString());
			sb.append("]: ");
			
			if(isLeaf())
			{
				sb.append(mElements.stream().map(e -> "@" + e.position.toString() + " " + e.element).collect(Collectors.joining(", ")));
			}
			else
			{
				String indent = indent(ind);
				String nl = "\n" + indent + "    ";
				
				sb.append(nl);
				sb.append(Stream.of(mChildren).map(c -> c.inspect(ind + 1)).collect(Collectors.joining("," + nl)));
				sb.append("\n").append(indent);
			}
			
			sb.append("}");
			
			return sb.toString();
		}
		
		private int index(IVec2 p) { return (p.x < mCenter.x ? 0 : 1) | (p.y < mCenter.y ? 0 : 2); }
	}
	
	private static String indent(int i) { String ind = ""; while(--i >= 0) ind += "    "; return ind; }
	
	private static class Entry<T>
	{
		public final IVec2 position;
		public final T element;
		
		public Entry(IVec2 p, T e)
		{
			position = p;
			element = e;
		}
	}
}
