package dave.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.function.Function;

import dave.util.property.Property;
import dave.util.property.SimpleProperty;

public class SimpleBazaar<K> implements Bazaar<K>
{
	private final Map<K, List<Container>> mContents;
	private int mDepth;
	
	public SimpleBazaar(int d)
	{
		mContents = new HashMap<>();
		mDepth = d;
	}
	
	@Override
	public <T> void offer(K k, Function<Arguments<K>, T> f, Options o)
	{
		Container c = new Container(f, o);
		List<Container> l = list(k);
		
		for(ListIterator<Container> i = l.listIterator() ; i.hasNext() ;)
		{
			Container e = i.next();
			
			if(e.options.priority() <= c.options.priority())
			{
				i.previous();
				i.add(c);
				
				return;
			}
		}
		
		l.add(c);
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public <T> T acquire(K k, Object ... a)
	{
		if(mDepth == 0)
			throw new IllegalStateException("Stack overflow!");
		
		Arguments<K> args = new Arguments<>(this, k, a);
		List<Refusal> refusals = new ArrayList<>();
		
		for(Container f : list(k))
		{
			if(f.busy && f.options.canBeBusy())
			{
				refusals.add(new Refusal("busy!"));
				
				continue;
			}
			
			f.busy = true;
			--mDepth;
			
			try
			{
				return (T) f.callback.apply(args);
			}
			catch(Refusal x)
			{
				refusals.add(x);
			}
			finally
			{
				++mDepth;
				f.busy = false;
			}
		}
		
		throw new Failure(refusals);
	}
	
	private List<Container> list(K k)
	{
		List<Container> l = mContents.get(k);
		
		if(l == null)
		{
			mContents.put(k, l = new ArrayList<>());
		}
		
		return l;
	}
	
	private class Container
	{
		public final Function<Arguments<K>, ?> callback;
		public final Options options;
		public boolean busy;
		
		public Container(Function<Arguments<K>, ?> f, Options o)
		{
			this.callback = f;
			this.options = o;
			this.busy = false;
			
			if(f == null)
				throw new NullPointerException();
			if(o == null)
				throw new NullPointerException();
		}
	}
	
	public static class Buffer<K, T> implements Function<Arguments<K>, T>
	{
		private final Property<T> mBuffer;
		private boolean mDone;
		
		public Buffer()
		{
			mBuffer = new SimpleProperty<>();
			mDone = false;
		}
		
		@Override
		public T apply(Arguments<K> a)
		{
			if(!mDone)
			{
				mDone = true;
				
				mBuffer.set(a.bazaar().acquire(a.request()));
			}
			
			return mBuffer.get();
		}
	}
}
