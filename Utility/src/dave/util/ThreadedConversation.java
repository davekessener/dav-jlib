package dave.util;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Function;

public class ThreadedConversation<S, T> extends Conversation<S, T>
{
	private final ExecutorService mAsync;

	public ThreadedConversation(Function<S, Transmitter<T>> f)
	{
		super(f);
		
		mAsync = Executors.newCachedThreadPool();
	}

	@Override
	protected void run(Runnable f)
	{
		mAsync.submit(f);
	}
	
	@Override
	public void stop()
	{
		mAsync.shutdown();
		
		super.stop();
	}
}
