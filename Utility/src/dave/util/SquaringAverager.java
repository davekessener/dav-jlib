package dave.util;

public class SquaringAverager extends Averager
{
	@Override
	public void add(double v)
	{
		super.add(v * v);
	}
}
