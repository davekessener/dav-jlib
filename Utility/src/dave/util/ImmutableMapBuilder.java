package dave.util;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class ImmutableMapBuilder<U, V>
{
	private final Map<U, V> mMap;
	
	public ImmutableMapBuilder( ) { this(new HashMap<>()); }
	public ImmutableMapBuilder(Map<U, V> m)
	{
		mMap = m;
	}
	
	public ImmutableMapBuilder<U, V> put(U key, V value)
	{
		mMap.put(key, value);
		
		return this;
	}
	
	public Map<U, V> build( )
	{
		return Collections.unmodifiableMap(mMap);
	}
}
