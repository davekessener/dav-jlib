package dave.util.test;

import static org.junit.Assert.*;

import org.junit.Test;

import dave.util.MathUtils;
import dave.util.math.Matrix;
import dave.util.math.Vec2;

public class MathUT
{
	@Test
	public void testAngle()
	{
		assertEquals( 45, MathUtils.r2d(MathUtils.angle(new Vec2(0, 1), new Vec2( 1,  1))), 0.001);
		assertEquals(-45, MathUtils.r2d(MathUtils.angle(new Vec2(0, 1), new Vec2(-1,  1))), 0.001);
		assertEquals( 90, MathUtils.r2d(MathUtils.angle(new Vec2(0, 1), new Vec2( 1,  0))), 0.001);
		assertEquals(  0, MathUtils.r2d(MathUtils.angle(new Vec2(0, 1), new Vec2( 0,  1))), 0.001);
		assertEquals(180, MathUtils.r2d(MathUtils.angle(new Vec2(0, 1), new Vec2( 0, -1))), 0.001);
	}
	
	@Test
	public void testRotate()
	{
		assertSoftEquals(new Vec2(0, 0), MathUtils.rotation2D(MathUtils.d2r(33)).mul(Matrix.from(new Vec2(0, 0))).toVec2());
		assertSoftEquals(new Vec2(1, 0), MathUtils.rotation2D(MathUtils.d2r(90)).mul(Matrix.from(new Vec2(0, 1))).toVec2());
		assertSoftEquals(new Vec2(-1, 0), MathUtils.rotation2D(MathUtils.d2r(-90)).mul(Matrix.from(new Vec2(0, 1))).toVec2());
		assertSoftEquals(new Vec2(-1, 0), MathUtils.rotation2D(MathUtils.d2r(270)).mul(Matrix.from(new Vec2(0, 1))).toVec2());
		assertSoftEquals(new Vec2(1, 1), MathUtils.rotation2D(MathUtils.d2r(45)).mul(Matrix.from(new Vec2(0, Math.sqrt(2)))).toVec2());
		assertSoftEquals(new Vec2(1, -1), MathUtils.rotation2D(MathUtils.d2r(90)).mul(Matrix.from(new Vec2(1, 1))).toVec2());
	}
	
	private static void assertSoftEquals(Vec2 a, Vec2 b)
	{
		assertEquals(a.x, b.x, 0.001);
		assertEquals(a.y, b.y, 0.001);
	}
}
