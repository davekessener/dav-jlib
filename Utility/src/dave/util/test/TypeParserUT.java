package dave.util.test;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import dave.util.config.TypeParser;

public class TypeParserUT
{
	@Test
	public void test()
	{
		TypeParser p = new TypeParser();
		
		Map<String, Double> m1 = new HashMap<>();
		Map<String, String> m2 = new HashMap<>();
		Map<String, Integer> m3 = new HashMap<>();
		
		m1.put("pi", 3.141);
		m1.put("e", 2.67);

		m2.put("never", "gonna");
		m2.put("give", "you");
		m2.put("up", "never");
		m2.put("gonna", "let");
		m2.put("you", "down");
		
		assertEquals(Integer.valueOf(7), p.convert(Integer.class, "7"));
		assertEquals(Double.valueOf(3.141), p.convert(Double.class, "3.141"));
		assertEquals(Double.valueOf(-1.5), p.convert(Double.class, "-1.5"));
		assertEquals("", p.convert(String.class, ""));
		assertEquals("abc123", p.convert(String.class, "abc123"));
		assertEquals(m1, p.convert(Map.class, "{pi:3.141,e:2.67}", Double.class));
		assertEquals(m1, p.convert(Map.class, "{pi : 3.141, e : 2.67}", Double.class));
		assertEquals(m2, p.convert(Map.class, "{never:\"gonna\",give:\"you\",up:\"never\",gonna:\"let\",you:\"down\"}", String.class));
		assertEquals(m3, p.convert(Map.class, "{}", Integer.class));
	}
}
