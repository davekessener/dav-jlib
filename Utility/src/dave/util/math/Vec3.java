package dave.util.math;

import dave.json.Container;
import dave.json.JsonObject;
import dave.json.JsonValue;
import dave.json.Loader;
import dave.json.Saveable;
import dave.json.Saver;

@Container
public class Vec3 implements Saveable
{
	public static final Vec3 ORIGIN = new Vec3(0, 0, 0);
	public static final Vec3 UNIT = new Vec3(1, 1, 1);
	
	public final double x, y, z;
	
	public Vec3(double x, double y, double z)
	{
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public Vec3 add(Vec3 v) { return new Vec3(x + v.x, y + v.y, z + v.z); }
	public Vec3 scale(double f) { return new Vec3(f * x, f * y, f * z); }
	public Vec3 sub(Vec3 v) { return add(v.scale(-1)); }
	public double mul(Vec3 v) { return x * v.x + y * v.y + z * v.z; }
	public double lengthSquared() { return mul(this); }
	public double length() { return Math.sqrt(lengthSquared()); }
	
	public Vec3 normalize() { return scale(1.0 / length()); }
	
	public Vec3 cross(Vec3 v)
	{
		return new Vec3(
			y * v.z - z * v.y,
			z * v.x - x * v.z,
			x * v.y - y * v.x
		);
	}

	@Override
	public int hashCode()
	{
		return (Double.hashCode(x) * 3) ^ (Double.hashCode(y) * 7) ^ (Double.hashCode(z) * 13);
	}
	
	@Override
	public boolean equals(Object o)
	{
		if(o instanceof Vec3)
		{
			Vec3 v = (Vec3) o;
			
			return x == v.x && y == v.y && z == v.z;
		}
		
		return false;
	}
	
	@Override
	public String toString()
	{
		return String.format("(%.3f, %.3f, %.3f)", x, y, z);
	}
	
	@Saver
	@Override
	public JsonValue save()
	{
		JsonObject json = new JsonObject();

		json.putNumber("x", x);
		json.putNumber("y", y);
		json.putNumber("z", z);
		
		return json;
	}
	
	@Loader
	public static Vec3 load(JsonValue json)
	{
		JsonObject o = (JsonObject) json;

		double x = o.getDouble("x");
		double y = o.getDouble("y");
		double z = o.getDouble("z");
		
		return new Vec3(x, y, z);
	}
}
