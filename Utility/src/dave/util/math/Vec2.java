package dave.util.math;

import dave.json.Container;
import dave.json.JsonObject;
import dave.json.JsonValue;
import dave.json.Loader;
import dave.json.Saveable;
import dave.json.Saver;

@Container
public class Vec2 implements Saveable
{
	public static final Vec2 ORIGIN = new Vec2(0, 0);
	public static final Vec2 UNIT = new Vec2(1, 1);
	
	public final double x, y;
	
	public Vec2(IVec2 p) { this(p.x, p.y); }
	public Vec2(double x, double y)
	{
		this.x = x;
		this.y = y;
	}
	
	public Vec2 add(Vec2 v) { return new Vec2(x + v.x, y + v.y); }
	public Vec2 scale(double f) { return new Vec2(f * x, f * y); }
	public Vec2 sub(Vec2 v) { return add(v.scale(-1)); }
	public double mul(Vec2 v) { return x * v.x + y * v.y; }
	public double lengthSquared() { return mul(this); }
	public double length() { return Math.sqrt(lengthSquared()); }
	
	public Vec2 normalized() { return scale(1.0 / length()); }
	
	public double get(int i)
	{
		if(i < 0 || i > 1)
			throw new IllegalArgumentException();
		
		return (i == 0 ? x : y);
	}

	@Override
	public int hashCode()
	{
		return (Double.hashCode(x) * 3) ^ (Double.hashCode(y) * 7);
	}
	
	@Override
	public boolean equals(Object o)
	{
		if(o instanceof Vec2)
		{
			Vec2 v = (Vec2) o;
			
			return x == v.x && y == v.y;
		}
		
		return false;
	}
	
	@Override
	public String toString()
	{
		return String.format("(%.3f, %.3f)", x, y);
	}
	
	@Saver
	@Override
	public JsonValue save()
	{
		JsonObject json = new JsonObject();

		json.putNumber("x", x);
		json.putNumber("y", y);
		
		return json;
	}
	
	@Loader
	public static Vec2 load(JsonValue json)
	{
		JsonObject o = (JsonObject) json;

		double x = o.getDouble("x");
		double y = o.getDouble("y");
		
		return new Vec2(x, y);
	}
}
