package dave.util.math;

import java.awt.Rectangle;

public class IRect
{
	public final int x, y, w, h;
	
	private IRect(int x, int y, int w, int h)
	{
		if(w < 0 || h < 0)
			throw new IllegalArgumentException("w=" + w + ", h=" + h);
		
		this.x = x;
		this.y = y;
		this.w = w;
		this.h = h;
	}
	
	public Rectangle toRectangle() { return new Rectangle(x, y, w, h); }
	
	public static IRect fromCoords(int x0, int y0, int x1, int y1)
	{
		return new IRect(x0, y0, x1 - x0, y1 - y0);
	}
	
	public static IRect fromSize(int x, int y, int w, int h)
	{
		return new IRect(x, y, w, h);
	}
	
	public static IRect fromRectangle(Rectangle r)
	{
		return new IRect(r.x, r.y, r.width, r.height);
	}
}
