package dave.util.math;

public class OctaveNoise implements Noise
{
	private final Noise mSource;
	private final int mCount;
	private final double mPersistance;
	
	public OctaveNoise(Noise s, int c, double p)
	{
		mSource = s;
		mCount = c;
		mPersistance = p;
		
		if(mSource == null)
			throw new NullPointerException();
		
		if(mCount < 1)
			throw new IllegalArgumentException();
		
		if(mPersistance <= 0 || mPersistance >= 1)
			throw new IllegalArgumentException();
	}

	@Override
	public double at(Vec3 p)
	{
		double f = 1;
		double a = 1;
		double m = 0;
		double t = 0;
		
		for(int i = 0 ; i < mCount ; ++i)
		{
			t += a * mSource.at(p.scale(f));
			
			m += a;
			f *= 2;
			a *= mPersistance;
		}
		
		return t / m;
	}
}
