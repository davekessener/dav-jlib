package dave.util.math;

import java.nio.FloatBuffer;

public class Mat4
{
	private final float[] mContent;
	
	public Mat4(float[] v)
	{
		if(v.length != 4 * 4)
			throw new IllegalArgumentException("" + v.length);
		
		mContent = v;
	}
	
	public float get(int x, int y)
	{
		check(x, y);
		
		return mContent[x + y * 4];
	}
	
	public Mat4 add(Mat4 m)
	{
		float[] r = new float[4 * 4];
		
		for(int i = 0 ; i < r.length ; ++i)
		{
			r[i] = mContent[i] + m.mContent[i];
		}
		
		return new Mat4(r);
	}
	
	public Mat4 mul(Mat4 m)
	{
		float[] r = new float[4 * 4];
		
		for(int y = 0 ; y < 4 ; ++y)
		{
			for(int x = 0 ; x < 4 ; ++x)
			{
				for(int i = 0 ; i < 4 ; ++i)
				{
					r[x + y * 4] += mContent[i + y * 4] * m.mContent[x + i * 4];
				}
			}
		}
		
		return new Mat4(r);
	}
	
	public Vec3 mul(Vec3 v)
	{
		float[] w = new float[] { (float) v.x, (float) v.y, (float) v.z, 1 };
		float[] r = new float[4];
		
		for(int y = 0 ; y < 3 ; ++y)
		{
			for(int x = 0 ; x < 4 ; ++x)
			{
				r[y] += mContent[x + y * 4] * w[x];
			}
		}
		
		return new Vec3(r[0], r[1], r[2]);
	}
	
	public FloatBuffer fill(FloatBuffer fb)
	{
		fb.put(mContent).flip();
		
		return fb;
	}
	
	private void check(int x, int y)
	{
		if(x < 0 || x >= 4 || y < 0 || y >= 4)
			throw new IllegalArgumentException("x=" + x + ", y=" + y);
	}
	
	public static Mat4 empty()
	{
		return new Mat4(new float[4 * 4]);
	}

	public static Mat4 identity()
	{
		return new Mat4(IDENTITY);
	}

	public static Mat4 translate(Vec3 v) { return translate(v.x, v.y, v.z); }
	public static Mat4 translate(double dx, double dy, double dz)
	{
		float fx = (float) dx;
		float fy = (float) dy;
		float fz = (float) dz;
		
		return new Mat4(new float[] {
			1, 0, 0, fx,
			0, 1, 0, fy,
			0, 0, 1, fz,
			0, 0, 0, 1
		});
	}

	public static Mat4 rotate_x(double d)
	{
		float cx = (float) Math.cos(d);
		float sx = (float) Math.sin(d);
		
		return new Mat4(new float[] {
			1,  0,  0,  0,
			0, cx, -sx, 0,
			0, sx, cx,  0,
			0,  0,  0,  1
		});
	}

	public static Mat4 rotate_y(double d)
	{
		float cy = (float) Math.cos(d);
		float sy = (float) Math.sin(d);
		
		return new Mat4(new float[] {
		 	 cy, 0, sy, 0,
			  0, 1,  0, 0,
			-sy, 0, cy, 0,
			  0, 0,  0, 1
		});
	}

	public static Mat4 rotate_z(double d)
	{
		float cz = (float) Math.cos(d);
		float sz = (float) Math.sin(d);
		
		return new Mat4(new float[] {
			cz, -sz, 0, 0,
			sz,  cz, 0, 0,
			 0,   0, 1, 0,
			 0,   0, 0, 1
		});
	}

	public static Mat4 scale(double sx, double sy, double sz)
	{
		float fx = (float) sx;
		float fy = (float) sy;
		float fz = (float) sz;
		
		return new Mat4(new float[] {
			fx,  0,  0, 0,
			 0, fy,  0, 0,
			 0,  0, fz, 0,
			 0,  0,  0, 1
		});
	}
	
	public static Mat4 perspective(double a, double fov, double z_near, double z_far)
	{
		float[] m = new float[4 * 4];
		double f1 = (1.0 / Math.tan(fov / 2));
		double zm = (z_far - z_near);
		double zp = (z_far + z_near);
		
		m[0 + 0 * 4] = (float) (f1 / a);
		m[1 + 1 * 4] = (float) f1;
		m[2 + 2 * 4] = (float) (-zp / zm);
		m[2 + 3 * 4] = -1;
		m[3 + 2 * 4] = (float) (-2 * z_far * z_near / zm);
		
		return (new Mat4(m)).mul(new Mat4(INVERT_Z));
	}
	
	public static Mat4 orthographic(double w, double h, double d)
	{
		return translate(-1, 1, 0).mul(scale(2.0 / w, 2.0 / h, -1.0 / d));
	}
	
	private static final float[] INVERT_Z = new float[] {
		1, 0, 0, 0,
		0, 1, 0, 0,
		0, 0,-1, 0,
		0, 0, 0, 1
	};
	
	private static final float[] IDENTITY = new float[] {
		1, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1
	};
}
