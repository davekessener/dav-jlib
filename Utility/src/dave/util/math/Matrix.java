package dave.util.math;

import java.util.Arrays;

import dave.json.Container;
import dave.json.JsonArray;
import dave.json.JsonObject;
import dave.json.JsonValue;
import dave.json.Loader;
import dave.json.Saveable;
import dave.json.Saver;

@Container
public class Matrix implements Saveable
{
	private final double[] mContent;
	private final int mWidth, mHeight;
	
	public Matrix(double[][] c)
	{
		if(c == null)
			throw new NullPointerException();
		
		mWidth = c[0].length;
		mHeight = c.length;
		mContent = new double[mWidth * mHeight];
		
		for(int y = 0 ; y < mHeight ; ++y)
		{
			for(int x = 0 ; x < mWidth ; ++x)
			{
				mContent[x + y * mWidth] = c[y][x];
			}
		}
	}
	
	public Matrix(int w, int h, double[] c)
	{
		if(w * h != c.length)
			throw new IllegalArgumentException();
		
		mContent = Arrays.copyOf(c, c.length);
		mWidth = w;
		mHeight = h;
	}
	
	public int width() { return mWidth; }
	public int height() { return mHeight; }
	public double get(int x, int y)
	{
		if(x < 0 || y < 0 || x >= mWidth || y >= mHeight)
			throw new IllegalArgumentException();
		
		return mContent[x + y * mWidth];
	}
	
	public static Matrix from(Vec2 v) { return new Matrix(1, 2, new double[] { v.x, v.y }); }
	public Vec2 toVec2( )
	{
		if(mWidth != 1 && mHeight != 2)
			throw new IllegalStateException();
		
		return new Vec2(mContent[0], mContent[1]);
	}
	
	public Matrix mul(Matrix w)
	{
		if(mWidth != w.mHeight)
			throw new IllegalArgumentException();
		
		double[] r = new double[w.mWidth * mHeight];
		
		for(int y = 0 ; y < mHeight ; ++y)
		{
			for(int x = 0 ; x < w.mWidth ; ++x)
			{
				double v = 0;
				
				for(int i = 0 ; i < mWidth ; ++i)
				{
					v += get(i, y) * w.get(x, i);
				}
				
				r[x + y * w.mWidth] = v;
			}
		}
		
		return new Matrix(w.mWidth, mHeight, r);
	}
	
	public static Matrix rotation2D(double a)
	{
		double s = Math.sin(a);
		double c = Math.cos(a);
		
		return new Matrix(2, 2, new double[] {
			c, -s,
			s,  c
		});
	}
	
	@Override
	@Saver
	public JsonValue save()
	{
		JsonObject json = new JsonObject();
		
		json.putInt("width", mWidth);
		json.putInt("height", mHeight);
		json.put("content", new JsonArray(mContent));
		
		return json;
	}
	
	@Loader
	public static Matrix load(JsonValue json)
	{
		JsonObject o = (JsonObject) json;
		
		int w = o.getInt("width");
		int h = o.getInt("height");
		double[] m = o.getArray("content").asDoubles();
		
		return new Matrix(w, h, m);
	}
}
