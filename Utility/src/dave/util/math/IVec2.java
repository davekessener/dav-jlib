package dave.util.math;

import dave.json.Container;
import dave.json.JsonObject;
import dave.json.JsonValue;
import dave.json.Loader;
import dave.json.Saveable;
import dave.json.Saver;

@Container
public class IVec2 implements Cloneable, Saveable
{
	public static final IVec2 ORIGIN = new IVec2(0, 0);
	public static final IVec2 LEFT = new IVec2(-1, 0);
	public static final IVec2 RIGHT = new IVec2(1, 0);
	public static final IVec2 UP = new IVec2(0, 1);
	public static final IVec2 DOWN = new IVec2(0, -1);
	public static final IVec2[] SIDES = new IVec2[] { LEFT, RIGHT, UP, DOWN };
	
	public final int x, y;
	
	public IVec2(int x, int y)
	{
		this.x = x;
		this.y = y;
	}
	
	@Override
	public IVec2 clone()
	{
		return new IVec2(x, y);
	}
	
	@Override
	public String toString()
	{
		return "(" + x + ", " + y + ")";
	}
	
	public IVec2 add(IVec2 v) { return new IVec2(x + v.x, y + v.y); }
	public IVec2 add(int dx, int dy) { return new IVec2(x + dx, y + dy); }
	public IVec2 sub(IVec2 v) { return new IVec2(x - v.x, y - v.y); }
	public IVec2 sub(int dx, int dy) { return new IVec2(x - dx, y - dy); }
	public IVec2 scale(int f) { return new IVec2(f * x, f * y); }
	public int mul(IVec2 v) { return x * v.x + y * v.y; }
	public int lengthSquared() { return mul(this); }
	public double length() { return Math.sqrt(lengthSquared()); }
	
	@Override
	public int hashCode()
	{
		return (Integer.hashCode(x) * 7) ^ (Integer.hashCode(y) * 17) ^ 0xfa732b6c;
	}
	
	@Override
	public boolean equals(Object o)
	{
		if(o instanceof IVec2)
		{
			IVec2 v = (IVec2) o;
			
			return x == v.x && y == v.y;
		}
		
		return false;
	}
	
	@Override
	@Saver
	public JsonValue save()
	{
		JsonObject json = new JsonObject();

		json.putInt("x", x);
		json.putInt("y", y);
		
		return json;
	}
	
	@Loader
	public static IVec2 load(JsonValue json)
	{
		JsonObject o = (JsonObject) json;
		
		int x = o.getInt("x");
		int y = o.getInt("y");
		
		return new IVec2(x, y);
	}
}
