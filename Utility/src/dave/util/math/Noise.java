package dave.util.math;

public interface Noise
{
	public abstract double at(Vec3 p);
}
