package dave.util.math;

public class TransformedNoise implements Noise
{
	private final Noise mSuper;
	private final Vec3 mScale, mOffset;
	
	public TransformedNoise(Noise n, double s, double o) { this(n, new Vec3(s, s, s), new Vec3(o, o, o)); }
	public TransformedNoise(Noise n, Vec3 s, Vec3 o)
	{
		mSuper = n;
		mScale = s;
		mOffset = o;
	}

	@Override
	public double at(Vec3 p)
	{
		return mSuper.at(transform(p));
	}

	private Vec3 transform(Vec3 p)
	{
		return (new Vec3(p.x * mScale.x, p.y * mScale.y, p.z * mScale.z)).add(mOffset);
	}
}
