package dave.util.math;

public class Mat3
{
	private final float[] mContent;
	
	public Mat3(float[] v)
	{
		if(v.length != 3 * 3)
			throw new IllegalArgumentException("" + v.length);
		
		mContent = v;
	}
	
	public float get(int x, int y)
	{
		if(x < 0 || x >= 3 || y < 0 || y >= 3)
			throw new IllegalArgumentException("x=" + x + ", y=" + y);
		
		return mContent[x + y * 3];
	}
	
	public Mat3 mul(Mat3 m)
	{
		float[] r = new float[3 * 3];
		
		for(int y = 0 ; y < 3 ; ++y)
		{
			for(int x = 0 ; x < 3 ; ++x)
			{
				for(int i = 0 ; i < 3 ; ++i)
				{
					r[x + y * 3] += mContent[i + y * 3] * m.mContent[x + i * 3];
				}
			}
		}
		
		return new Mat3(r);
	}
	
	public static Mat3 empty()
	{
		return new Mat3(new float[3 * 3]);
	}
	
	public static Mat3 identity()
	{
		return new Mat3(new float[] {
			1, 0, 0,
			0, 1, 0,
			0, 0, 1
		});
	}
	
	public static Mat3 translate(Vec2 v) { return translate(v.x, v.y); }
	public static Mat3 translate(double ddx, double ddy)
	{
		float dx = (float) ddx;
		float dy = (float) ddy;
		
		return new Mat3(new float[] {
			1, 0, dx,
			0, 1, dy,
			0, 0, 1
		});
	}
	
	public static Mat3 scale(double dsx, double dsy)
	{
		float sx = (float) dsx;
		float sy = (float) dsy;
		
		return new Mat3(new float[] {
			sx,  0, 0,
			 0, sy, 0,
			 0,  0, 1
		});
	}
	
	public static Mat3 orthographic(int w, int h)
	{
		return scale(2.0 / w, 2.0 / h).mul(translate(-w / 2.0, -h / 2.0));
	}
}
