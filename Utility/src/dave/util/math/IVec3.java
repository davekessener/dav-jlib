package dave.util.math;

import java.io.Serializable;

import dave.json.Container;
import dave.json.JsonObject;
import dave.json.JsonValue;
import dave.json.Loader;
import dave.json.Saveable;
import dave.json.Saver;

@Container
public class IVec3 implements Cloneable, Saveable, Serializable
{
	private static final long serialVersionUID = -4719759847095434122L;
	
	public static final IVec3 ORIGIN = new IVec3(0, 0, 0);
	public static final IVec3 UNIT = new IVec3(1, 1, 1);
	public static final IVec3 LEFT = new IVec3(-1, 0, 0);
	public static final IVec3 RIGHT = new IVec3(1, 0, 0);
	public static final IVec3 UP = new IVec3(0, 1, 0);
	public static final IVec3 DOWN = new IVec3(0, -1, 0);
	public static final IVec3 FORWARD = new IVec3(0, 0, 1);
	public static final IVec3 BACKWARD = new IVec3(0, 0, -1);
	
	public final int x, y, z;
	
	public IVec3(int x, int y, int z)
	{
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public IVec3 add(IVec3 v) { return new IVec3(x + v.x, y + v.y, z + v.z); }
	public IVec3 negate( ) { return new IVec3(-x, -y, -z); }
	public IVec3 sub(IVec3 v) { return add(v.negate()); }
	public IVec3 scale(int f) { return new IVec3(x * f, y * f, z * f); }
	public int mul(IVec3 v) { return x * v.x + y * v.y + z * v.z; }
	public double lengthSquared() { return mul(this); }
	public double length() { return Math.sqrt(lengthSquared()); }
	public int volume( ) { return x * y * z; }
	
	@Override
	public String toString( )
	{
		return "(" + x + ", " + y + ", " + z + ")";
	}
	
	@Override
	public IVec3 clone( )
	{
		return new IVec3(x, y, z);
	}

	@Override
	public int hashCode()
	{
		return (Integer.hashCode(x) * 3) ^ (Integer.hashCode(y) * 7) ^ (Integer.hashCode(z) * 13);
	}
	
	@Override
	public boolean equals(Object o)
	{
		if(o instanceof IVec3)
		{
			IVec3 v = (IVec3) o;
			
			return x == v.x && y == v.y && z == v.z;
		}
		
		return false;
	}
	
	@Saver
	@Override
	public JsonObject save( )
	{
		JsonObject obj = new JsonObject();
		
		obj.putInt("x", x);
		obj.putInt("y", y);
		obj.putInt("z", z);
		
		return obj;
	}
	
	@Loader
	public static IVec3 load(JsonValue json)
	{
		JsonObject o = (JsonObject) json;
		
		int x = o.getInt("x");
		int y = o.getInt("y");
		int z = o.getInt("z");
		
		return new IVec3(x, y, z);
	}

	public static IVec3 getDirection(double dx, double dy, double dz, double t)
	{
		IVec3 d = ORIGIN;
		
		if(dx < -t)
		{
			d = d.add(LEFT);
		}
		else if(dx > t)
		{
			d = d.add(RIGHT);
		}
		
		if(dy < -t)
		{
			d = d.add(DOWN);
		}
		else if(dy > t)
		{
			d = d.add(UP);
		}
		
		if(dz < -t)
		{
			d = d.add(BACKWARD);
		}
		else if(dz > t)
		{
			d = d.add(FORWARD);
		}
		
		return d;
	}
}
