package dave.dynj.test;

import static org.junit.Assert.*;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Test;

import dave.dynj.JLibrary;
import dave.dynj.JProgram;
import dave.dynj.model.BlockStatement;
import dave.dynj.model.ConstantExpression;
import dave.dynj.model.ExpressionStatement;
import dave.dynj.model.FunctionCallExpression;
import dave.dynj.model.Generatable;
import dave.dynj.model.Generator;
import dave.dynj.model.JAnnotation;
import dave.dynj.model.JExpression;
import dave.dynj.model.JMethod;
import dave.dynj.model.JPackage;
import dave.dynj.model.JReference;
import dave.dynj.model.JTranslationUnit;
import dave.dynj.model.JType;
import dave.dynj.model.ReturnStatement;
import dave.dynj.model.Visibility;
import dave.util.Identifiable;

public class UT
{
	@Test
	public void testPackage()
	{
		JPackage root = JPackage.generateRoot();
		
		assertEquals(root.path(), "");
		
		JPackage dave_utils_misc = root.create("dave").create("utils").create("misc");
		
		assertEquals(dave_utils_misc.path(), "dave.utils.misc");
	}
	
	@Test
	public void testRef()
	{
		JPackage p = JPackage.generateRoot().create("dave").create("test");
		JReference r1 = new JReference("SomeClass", p, true, true, true, Visibility.PUBLIC, true, Collections.emptyList());
		
		assertEquals("public static abstract final SomeClass[]", generate(r1));
		assertEquals("dave.test.SomeClass", r1.fullyQualifiedPath());
		
		JReference r2 = new JReference("AnotherClass", JPackage.generateRoot(), false, false, false, Visibility.PACKAGE, false, Collections.emptyList());
		
		assertEquals("AnotherClass", generate(r2));
		assertEquals("AnotherClass", r2.fullyQualifiedPath());
		
		JReference r3 = new JReference(String[].class, true, true, true, Visibility.PRIVATE, Collections.emptyList());
		
		assertEquals("private static abstract final String[]", generate(r3));
		assertEquals("java.lang.String", r3.fullyQualifiedPath());
		
		JReference r4 = new JReference(String.class, false, true, false, Visibility.PROTECTED, Collections.emptyList());
		
		assertEquals("protected final String", generate(r4));
		assertEquals("java.lang.String", r4.fullyQualifiedPath());

		JReference r5 = new JReference(int.class, false, false, false, Visibility.PACKAGE, Collections.emptyList());
		
		assertEquals("int", generate(r5));
		assertEquals("int", r5.fullyQualifiedPath());
		
		JReference r6 = new JReference(int[].class, false, false, false, Visibility.PACKAGE, Collections.emptyList());
		
		assertEquals("int[]", generate(r6));
		assertEquals("int", r6.fullyQualifiedPath());
	}
	
	@Test
	public void testMethod()
	{
		JPackage p = JPackage.generateRoot().create("dave").create("test");
		JReference ret = new JReference("MyClass", p, false, false, true, Visibility.PUBLIC, true, Collections.emptyList());
		BlockStatement body = new BlockStatement(Arrays.asList(
			new ReturnStatement(ConstantExpression.NULL)
		));
		JReference arg1 = new JReference(String.class, false, false, false, Visibility.PACKAGE, Collections.emptyList());
		JMethod m = new JMethod("testf", ret, new JMethod.Parameter[] {
			new JMethod.Parameter("a1", arg1)
		}, body);
		
		m.annotate(new JAnnotation(JReference.builder(Override.class).build(), null));
		m.annotate(new JAnnotation(JReference.builder(String.class).build(), new ConstantExpression("\"test string for testing\"")));
		
		assertEquals("@Override\n@String(\"test string for testing\")\npublic abstract MyClass[] testf(String a1)\n{\n\treturn null;\n}", generate(m));
	}
	
	@Test
	public void testFile()
	{
		JPackage p = JPackage.generateRoot().create("dave").create("test");
		JReference self = JReference.builder("MyClass", p).setVisibility(Visibility.PUBLIC).build();
		BlockStatement body = new BlockStatement(Arrays.asList(
			new ExpressionStatement(new FunctionCallExpression("System.out.println", new JExpression[] {
				ConstantExpression.of("Hello, World!")
			}))
		));
		List<JType.JVariable> vars = Arrays.asList(
			new JType.JVariable("mVersion", JReference.builder(String.class).setVisibility(Visibility.PRIVATE).setFinal(true).build(), null),
			new JType.JVariable("VERSION", JReference.builder(int.class).setStatic(true).setFinal(true).setVisibility(Visibility.PUBLIC).build(), ConstantExpression.of(420))
		);
		List<JMethod> methods = Arrays.asList(
			new JMethod(null, self, new JMethod.Parameter[] { }, new BlockStatement(Arrays.asList()))
		);
		List<JReference> impl = Arrays.asList(
			JReference.builder(Cloneable.class).build(),
			JReference.builder(Serializable.class).build()
		);
		JReference ext = JReference.builder(String.class).build();
		JType c = new JType(self, "class", impl, Arrays.asList(ext), vars, methods, null, body);
		JTranslationUnit u = new JTranslationUnit(c);
		
		u.addImport(JReference.builder(String.class).build());
		
		String g = u.generate();
		
		assertEquals("package dave.test;\n\n"
				+ "import java.lang.String;\n\n"
				+ "public class MyClass extends String implements Cloneable, Serializable\n{\n"
				+ "\tprivate final String mVersion;\n"
				+ "\tpublic static final int VERSION = 420;\n\n"
				+ "\tpublic MyClass()\n\t{\n\t}\n\n"
				+ "\tstatic\n\t{\n\t\tSystem.out.println(\"Hello, World!\");\n\t}\n"
				+ "}", g);
	}
	
	@Test
	public void testRun()
	{
		JPackage p = JPackage.generateRoot().create("dynj").create("ut");
		JReference self = JReference.builder("DynamicTest", p).setVisibility(Visibility.PUBLIC).build();
		BlockStatement getIDBody = new BlockStatement(Arrays.asList(new ReturnStatement(ConstantExpression.of("Hello, World!"))));
		JMethod getID = new JMethod("getID", JReference.builder(String.class).setVisibility(Visibility.PUBLIC).build(), new JMethod.Parameter[] { }, getIDBody);
		
		getID.annotate(new JAnnotation(JReference.builder(Override.class).build(), null));
		
		List<JMethod> methods = Arrays.asList(getID);
		JReference riden = JReference.builder(Identifiable.class).build();
		List<JReference> impl = Arrays.asList(riden);
		JType c = new JType(self, "class", impl, null, null, methods, null, null);
		JTranslationUnit u = new JTranslationUnit(c);
		
		u.addImport(riden);
		
		System.out.println(u.generate());
		
		JProgram prgm = new JProgram("test-prog");
		
		prgm.addFile(u);
		
		JLibrary lib = prgm.build();
		
		Identifiable o = lib.newInstance(self.fullyQualifiedPath());
		
		assertEquals("Hello, World!", o.getID());
	}
	
	private static String generate(Generatable e)
	{
		Generator g = new Generator();
		
		e.generate(g);
		
		return g.toString();
	}
}
