package dave.dynj;

import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.URLClassLoader;

import dave.dynj.model.JType;
import dave.util.SevereException;

public class JLibrary
{
	private final URLClassLoader mLoader;
	
	public JLibrary(URI uri)
	{
		try
		{
			mLoader = URLClassLoader.newInstance(new URL[] { uri.toURL() });
		}
		catch(MalformedURLException e)
		{
			throw new SevereException(e);
		}
	}
	
	public Class<?> getClass(JType t) { return getClass(t.reference().fullyQualifiedPath()); }
	public Class<?> getClass(String path)
	{
		try
		{
			return Class.forName(path, true, mLoader);
		}
		catch(ClassNotFoundException e)
		{
			throw new SevereException(e);
		}
	}
	
	@SuppressWarnings("unchecked")
	public <T> T newInstance(String path, Object ... args)
	{
		Class<?>[] types = new Class[args.length];
		
		for(int i = 0 ; i < args.length ; ++i)
		{
			types[i] = args[i].getClass();
		}
		
		try
		{
			return (T) getClass(path).getConstructor(types).newInstance(args);
		}
		catch(InstantiationException
			| IllegalAccessException
			| IllegalArgumentException
			| InvocationTargetException
			| NoSuchMethodException
			| SecurityException e)
		{
			throw new SevereException(e);
		}
	}
}
