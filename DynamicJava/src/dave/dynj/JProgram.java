package dave.dynj;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import javax.tools.JavaCompiler;
import javax.tools.ToolProvider;

import dave.dynj.model.JPackage;
import dave.dynj.model.JTranslationUnit;
import dave.util.SevereException;

public class JProgram
{
	private final String mID;
	private final JPackage mRoot;
	private final List<JTranslationUnit> mClasses;
	
	public JProgram(String id)
	{
		mID = id;
		mRoot = JPackage.generateRoot();
		mClasses = new ArrayList<>();
	}
	
	public JPackage rootPackage() { return mRoot; }
	
	public Stream<JTranslationUnit> files() { return mClasses.stream(); }
	
	public void addFile(JTranslationUnit t)
	{
		mClasses.add(t);
	}
	
	public JLibrary build()
	{
		try
		{
			Charset charset = StandardCharsets.UTF_8;
			JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
			Path root = Paths.get("C:\\Users\\davek\\tmp\\mxsim\\dyn\\dynj-" + mID);
//			Path root = Files.createTempDirectory("dynj-" + mID);
			List<String> files = new ArrayList<>();
			
			for(JTranslationUnit t : mClasses)
			{
				String src = t.generate();
				File srcfile = new File(root.toFile(), t.filePath());
				
				srcfile.getParentFile().mkdirs();
				
				Files.write(srcfile.toPath(), src.getBytes(charset));
				
				files.add(srcfile.getPath());
			}
			
			ByteArrayOutputStream log = new ByteArrayOutputStream();
			
			int r = compiler.run(null, log, log, files.toArray(new String[files.size()]));
			
			if(r != 0)
			{
				throw new RuntimeException("Compilation error:\n" + new String(log.toByteArray()));
			}
			
			return new JLibrary(root.toUri());
		}
		catch(IOException e)
		{
			throw new SevereException(e);
		}
	}
}
