package dave.dynj.model;

public class AssignmentExpression implements JExpression
{
	private final String mName;
	private final JExpression mValue;
	
	public AssignmentExpression(String n, JExpression e)
	{
		mName = n;
		mValue = e;
	}

	@Override
	public void generate(Generator g)
	{
		g.add(mName).add(" = ").add(mValue);
	}
}
