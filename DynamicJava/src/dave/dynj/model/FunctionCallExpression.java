package dave.dynj.model;

import java.util.List;

public class FunctionCallExpression implements JExpression
{
	private final String mName;
	private final JExpression[] mArgs;
	
	public FunctionCallExpression(String name, List<JExpression> args) { this(name, args.toArray(new JExpression[args.size()])); }
	public FunctionCallExpression(String name, JExpression[] args)
	{
		mName = name;
		mArgs = args;
	}
	
	public FunctionCallExpression(JExpression e, List<JExpression> args) { this(e, args.toArray(new JExpression[args.size()])); }
	public FunctionCallExpression(JExpression e, JExpression[] args)
	{
		Generator g = new Generator();
		
		e.generate(g);
		
		mName = g.toString();
		mArgs = args;
	}
	
	@Override
	public void generate(Generator g)
	{
		g.add(mName).add("(");
		
		for(int i = 0 ; i < mArgs.length ; ++i)
		{
			if(i != 0) g.add(", ");
			g.add(mArgs[i]);
		}
		
		g.add(")");
	}
}
