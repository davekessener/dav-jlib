package dave.dynj.model;

public class ParenthesesExpression implements JExpression
{
	private final JExpression mExpr;
	
	public ParenthesesExpression(JExpression e)
	{
		mExpr = e;
	}

	@Override
	public void generate(Generator g)
	{
		g.add("(").add(mExpr).add(")");
	}
}
