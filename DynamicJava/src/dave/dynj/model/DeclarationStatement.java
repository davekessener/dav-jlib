package dave.dynj.model;

public class DeclarationStatement extends Annotatable implements JStatement
{
	private final JReference mType;
	private final AssignmentExpression mBody;
	
	public DeclarationStatement(JReference r, AssignmentExpression b)
	{
		mType = r;
		mBody = b;
	}

	@Override
	public void doGenerate(Generator g)
	{
		g.add(mType).add(" ").add(mBody).add(";");
	}
}
