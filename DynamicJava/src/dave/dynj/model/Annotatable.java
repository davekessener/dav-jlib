package dave.dynj.model;

import java.util.ArrayList;
import java.util.List;

public abstract class Annotatable implements Generatable
{
	private final List<JAnnotation> mAnnotations;
	
	public Annotatable()
	{
		mAnnotations = new ArrayList<>();
	}
	
	public Annotatable annotate(JAnnotation a)
	{
		mAnnotations.add(a);
		
		return this;
	}
	
	protected abstract void doGenerate(Generator g);

	@Override
	public final void generate(Generator g)
	{
		boolean nl = g.isNewLine();
		
		for(JAnnotation a : mAnnotations)
		{
			a.generate(g);
			if(nl) g.lineBreak();
			else g.add(" ");
		}
		
		doGenerate(g);
	}
}
