package dave.dynj.model;

import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MemberAccessExpression implements JExpression
{
	private final JExpression mBase;
	private final String mName;
	
	public MemberAccessExpression(JExpression base, String name)
	{
		mBase = base;
		mName = name;
	}
	
	public MemberAccessExpression(String ... s)
	{
		mBase = null;
		mName = Stream.of(s).collect(Collectors.joining("."));
	}

	@Override
	public void generate(Generator g)
	{
		if(mBase != null) g.add(mBase).add(".");
		g.add(mName);
	}
}
