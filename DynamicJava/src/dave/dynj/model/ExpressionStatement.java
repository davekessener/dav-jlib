package dave.dynj.model;

public class ExpressionStatement implements JStatement
{
	private final JExpression mExpr;
	
	public ExpressionStatement(JExpression e)
	{
		mExpr = e;
	}

	@Override
	public void generate(Generator g)
	{
		g.add(mExpr).add(";");
	}
}
