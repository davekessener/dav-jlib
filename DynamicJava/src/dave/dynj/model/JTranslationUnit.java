package dave.dynj.model;

import java.util.ArrayList;
import java.util.List;

public class JTranslationUnit
{
	private final JType mType;
	private final List<JReference> mImports;
	
	public JTranslationUnit(JType v)
	{
		mType = v;
		mImports = new ArrayList<>();
	}
	
	public JType getType() { return mType; }
	
	public JTranslationUnit addImport(JReference r)
	{
		mImports.add(r);
		
		return this;
	}
	
	public String filePath()
	{
		return mType.reference().fullyQualifiedPath().replaceAll("\\.", "/") + ".java";
	}
	
	public String generate()
	{
		Generator g = new Generator();
		String pack = mType.reference().packagePath();
		
		if(!pack.isEmpty())
		{
			g.add("package ").add(pack).add(";").newLine().newLine();
		}
		
		if(!mImports.isEmpty())
		{
			for(JReference r : mImports)
			{
				g.add("import ").add(r.fullyQualifiedPath()).add(";").lineBreak();
			}
			
			g.newLine().newLine();
		}
		
		mType.generate(g);
		
		return g.toString();
	}
}
