package dave.dynj.model;

import java.util.ArrayList;
import java.util.List;

public class JProject
{
	private final List<JPackage> mPackages;
	
	public JProject()
	{
		mPackages = new ArrayList<>();
	}
	
	public void add(JPackage p)
	{
		mPackages.add(p);
	}
	
	public void build()
	{
	}
}
