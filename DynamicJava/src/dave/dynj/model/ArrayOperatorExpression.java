package dave.dynj.model;

public class ArrayOperatorExpression extends OperatorExpression
{
	public ArrayOperatorExpression(JExpression v, JExpression i)
	{
		super(new JExpression[] { v, i });
	}

	@Override
	public void generate(Generator g)
	{
		JExpression value = operands()[0];
		JExpression index = operands()[1];
		
		g.add(value).add("[").add(index).add("]");
	}
}
