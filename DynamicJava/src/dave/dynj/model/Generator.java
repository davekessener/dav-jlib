package dave.dynj.model;

import dave.util.Holder;

public class Generator
{
	private final Generator mSuper;
	private final StringBuilder mGen;
	private final String mIndent;
	private final int mLevel;
	private final Holder<Boolean> mNewLine;
	private final Holder<Boolean> mAfterNewline;
	
	public Generator() { this(Generatable.IDENT); }
	public Generator(String i)
	{
		mSuper = null;
		mGen = new StringBuilder();
		mIndent = i;
		mLevel = 0;
		mNewLine = new Holder<>(false);
		mAfterNewline = new Holder<>(true);
	}
	
	private Generator(Generator s, StringBuilder g, String i, int l, Holder<Boolean> nl, Holder<Boolean> anl)
	{
		mSuper = s;
		mGen = g;
		mIndent = i;
		mLevel = l;
		mNewLine = nl;
		mAfterNewline = anl;
	}
	
	public boolean isNewLine() { return mAfterNewline.value; }
	
	public Generator add(Generatable e)
	{
		e.generate(this);
		
		return this;
	}

	public <T> Generator add(T e)
	{
		if(e instanceof Generatable)
			throw new IllegalStateException();
		
		if(e.equals("\n"))
			throw new IllegalArgumentException("USE NEWLINE!");
		
		if(mNewLine.value)
		{
			mGen.append("\n");
			mNewLine.value = false;
			mAfterNewline.value = true;
		}
		
		if(mAfterNewline.value)
		{
			for(int i = 0 ; i < mLevel ; ++i) mGen.append(mIndent);
			mAfterNewline.value = false;
		}
		
		mGen.append(e);
		
		return this;
	}
	
	public Generator lineBreak()
	{
		mNewLine.value = true;
		mAfterNewline.value = true;
		
		return this;
	}
	
	public Generator newLine()
	{
		mNewLine.value = false;
		mAfterNewline.value = true;
		
		mGen.append("\n");
		
		return this;
	}
	
	public Generator enter()
	{
		return new Generator(this, mGen, mIndent, mLevel + 1, mNewLine, mAfterNewline);
	}
	
	public Generator leave()
	{
		return mSuper;
	}
	
	@Override
	public String toString()
	{
		return mGen.toString();
	}
}
