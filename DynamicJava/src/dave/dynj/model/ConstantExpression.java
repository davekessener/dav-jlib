package dave.dynj.model;

public class ConstantExpression implements JExpression
{
	private final String mValue;
	
	public ConstantExpression(String v)
	{
		mValue = v;
	}

	@Override
	public void generate(Generator g)
	{
		g.add(mValue);
	}
	
	public static ConstantExpression of(int i) { return new ConstantExpression("" + i); }
	public static ConstantExpression of(double v) { return new ConstantExpression(Double.toString(v)); }
	public static ConstantExpression of(String s) { return new ConstantExpression("\"" + s + "\""); }
	
	public static final ConstantExpression NULL = new ConstantExpression("null");
}
