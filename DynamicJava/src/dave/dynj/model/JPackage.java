package dave.dynj.model;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class JPackage
{
	private final String mName;
	private final JPackage mSuper;
	private final Map<String, JPackage> mSubs;
	
	private JPackage(JPackage s, String name)
	{
		mName = name;
		mSuper = s;
		mSubs = new HashMap<>();
	}
	
	public String path()
	{
		if(mSuper == null) return "";
		
		StringBuilder sb = new StringBuilder();
		
		path(sb);
		
		return sb.substring(1);
	}
	
	private void path(StringBuilder sb)
	{
		if(mSuper != null) mSuper.path(sb);
		if(mName != null) sb.append(".").append(mName);
	}
	
	public JPackage create(String name)
	{
		JPackage r = mSubs.get(name);
		
		if(r == null)
		{
			mSubs.put(name, r = new JPackage(this, name));
		}
		
		return r;
	}
	
	public JPackage get(String name)
	{
		return Optional.of(mSubs.get(name)).get();
	}
	
	public JPackage mkdir_p(String path)
	{
		String[] p = path.split("\\.");
		
		JPackage r = this;
		
		for(int i = 0 ; i < p.length ; ++i)
		{
			r = r.create(p[i]);
		}
		
		return r;
	}
	
	public static JPackage generateRoot()
	{
		return new JPackage(null, null);
	}
}
