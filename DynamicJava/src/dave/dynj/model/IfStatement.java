package dave.dynj.model;

public class IfStatement implements JStatement
{
	private final JExpression mCond;
	private final JStatement mIf, mElse;
	
	public IfStatement(JExpression c, JStatement i, JStatement e)
	{
		mCond = c;
		mIf = i;
		mElse = e;
	}
	
	@Override
	public void generate(Generator g)
	{
		g.add("if(").add(mCond).add(")").lineBreak().add(mIf);
		
		if(mElse != null)
		{
			g.add("else").lineBreak().add(mElse);
		}
	}
}
