package dave.dynj.model;

public class ReturnStatement implements JStatement
{
	private final JExpression mExpr;
	
	public ReturnStatement(JExpression e)
	{
		mExpr = e;
	}

	@Override
	public void generate(Generator g)
	{
		g.add("return ").add(mExpr).add(";");
	}
}
