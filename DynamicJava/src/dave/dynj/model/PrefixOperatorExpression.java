package dave.dynj.model;

public class PrefixOperatorExpression extends OperatorExpression
{
	private final String mOp;
	
	public PrefixOperatorExpression(String op, JExpression ops)
	{
		super(new JExpression[] { ops });
		
		mOp = op;
	}

	@Override
	public void generate(Generator g)
	{
		g.add(mOp).add(operands()[0]);
	}
}
