package dave.dynj.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class JReference implements Generatable
{
	private final String mPackage;
	private final String mName;
	private final boolean mStatic, mFinal, mAbstract;
	private final Visibility mVis;
	private final boolean mArray;
	private final List<JReference> mParams;
	
	public JReference(String n, JPackage p, boolean s, boolean f, boolean a, Visibility v, boolean r, List<JReference> ps)
	{
		mPackage = p.path();
		mName = n;
		mStatic = s;
		mFinal = f;
		mAbstract = a;
		mVis = v;
		mArray = r;
		mParams = Collections.unmodifiableList(new ArrayList<>(ps));
	}
	
	public JReference(Class<?> c, boolean s, boolean f, boolean a, Visibility v, List<JReference> ps)
	{
		String path = c.getCanonicalName();
		int i = path.lastIndexOf('.');
		
		if(i == -1) i = 0;
		
		mPackage = path.substring(0, i);
		mName = (c.isArray() ? c.getComponentType().getSimpleName() : c.getSimpleName());
		mStatic = s;
		mFinal = f;
		mAbstract = a;
		mVis = v;
		mArray = c.isArray();
		mParams = Collections.unmodifiableList(new ArrayList<>(ps));
	}
	
	private JReference(String n, String p, boolean s, boolean f, boolean a, Visibility v, boolean r, List<JReference> ps)
	{
		mName = n;
		mPackage = p;
		mStatic = s;
		mFinal = f;
		mAbstract = a;
		mVis = v;
		mArray = r;
		mParams = Collections.unmodifiableList(new ArrayList<>(ps));
	}
	
	public String fullyQualifiedPath()
	{
		String p = mPackage;
		
		if(!p.isEmpty()) p += ".";
		
		return p + mName;
	}
	
	public String packagePath() { return mPackage; }
	
	public String typeName() { return mName + (mParams.isEmpty() ? "" : "<" + mParams.stream().map(JReference::typeName).collect(Collectors.joining(", ")) + ">"); }
	public boolean isStatic() { return mStatic; }
	public boolean isFinal() { return mFinal; }
	public Visibility visibility() { return mVis; }
	public boolean isArray() { return mArray; }
	
	public Generator generateQualifiers(Generator g)
	{
		if(mVis.mod != null) g.add(mVis.mod).add(" ");
		if(mStatic) g.add("static ");
		if(mAbstract) g.add("abstract ");
		if(mFinal) g.add("final ");

		return g;
	}
	
	public String variableType()
	{
		return (mFinal ? "final " : "") + typeName();
	}
	
	@Override
	public void generate(Generator g)
	{
		generateQualifiers(g).add(typeName());
		
		if(mArray) g.add("[]");
	}
	
	public static Builder builder(String n, JPackage p) { return new Builder(n, p); }
	public static Builder builder(Class<?> c) { return new Builder(c); }
	public static Builder builder(JReference r) { return new Builder(r); }
	
	public static JReference of(Class<?> c) { return builder(c).build(); }
	
	public static JReference of(String path)
	{
		String[] s = path.split("\\.");
		JPackage p = JPackage.generateRoot();
		
		for(int i = 0 ; i < s.length - 1 ; ++i)
		{
			p = p.create(s[i]);
		}
		
		return builder(s[s.length - 1], p).build();
	}
	
	public static class Builder
	{
		private final String mName, mPackage;
		private boolean mStatic, mFinal, mAbstract, mArray;
		private Visibility mVis;
		private final List<JReference> mParams;
		
		public Builder(String n, JPackage p)
		{
			mName = n;
			mPackage = p.path();
			
			mStatic = mFinal = mAbstract = mArray = false;
			mVis = Visibility.PACKAGE;
			mParams = new ArrayList<>();
		}
		
		public Builder(Class<?> c)
		{
			String path = c.getCanonicalName();
			int i = path.lastIndexOf('.');
			
			if(i == -1) i = 0;
			
			mName = (c.isArray() ? c.getComponentType().getSimpleName() : c.getSimpleName());
			mPackage = path.substring(0, i);
			mArray = c.isArray();
			
			mStatic = mFinal = mAbstract = false;
			mVis = Visibility.PACKAGE;
			mParams = new ArrayList<>();
		}
		
		private Builder(JReference r)
		{
			mName = r.mName;
			mPackage = r.mPackage;
			mStatic = r.mStatic;
			mFinal = r.mFinal;
			mAbstract = r.mAbstract;
			mArray = r.mArray;
			mVis = r.mVis;
			mParams = new ArrayList<>(r.mParams);
		}
		
		public Builder setStatic(boolean s) { mStatic = s; return this; }
		public Builder setFinal(boolean f) { mFinal = f; return this; }
		public Builder setAbstract(boolean a) { mAbstract = a; return this; }
		public Builder setArray(boolean a) { mArray = a; return this; }
		public Builder setVisibility(Visibility v) { mVis = v; return this; }
		public Builder addParam(JReference p) { mParams.add(p); return this; }
		
		public JReference build()
		{
			return new JReference(mName, mPackage, mStatic, mFinal, mAbstract, mVis, mArray, mParams);
		}
	}
}
