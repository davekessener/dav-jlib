package dave.dynj.model;

public abstract class OperatorExpression implements JExpression
{
	private final JExpression[] mOperands;
	
	public OperatorExpression(JExpression[] ops)
	{
		mOperands = ops;
	}
	
	protected JExpression[] operands() { return mOperands; }
}
