package dave.dynj.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

public class BlockStatement implements JStatement
{
	private final List<JStatement> mStats;
	
	public BlockStatement(List<JStatement> s)
	{
		mStats = Collections.unmodifiableList(new ArrayList<>(s));
	}
	
	public Stream<JStatement> statements() { return mStats.stream(); }

	@Override
	public void generate(Generator g)
	{
		Generator gg = g.enter();
		
		g.add("{").lineBreak();
		
		mStats.forEach(s -> gg.add(s).lineBreak());
		
		g.add("}");
	}
}
