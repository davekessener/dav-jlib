package dave.dynj.model;

import java.util.List;

public class InfixOperatorExpression extends OperatorExpression
{
	private final String mOp;
	
	public InfixOperatorExpression(String op, List<JExpression> ops) { this(op, ops.toArray(new JExpression[ops.size()])); }
	public InfixOperatorExpression(String op, JExpression[] ops)
	{
		super(ops);
		
		mOp = op;
	}

	@Override
	public void generate(Generator g)
	{
		JExpression[] ops = operands();
		
		for(int i = 0 ; i < ops.length ; ++i)
		{
			if(i != 0) g.add(" ").add(mOp).add(" ");
			g.add(ops[i]);
		}
	}
}
