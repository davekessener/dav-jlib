package dave.dynj.model;

import java.util.Arrays;
import java.util.List;

public class JMethod extends Annotatable implements Generatable
{
	private final String mName;
	private final JReference mReturn;
	private final Parameter[] mParams;
	private final BlockStatement mBody;
	
	public JMethod(String n, JReference r, Parameter[] p, BlockStatement b) { this(n, r, Arrays.asList(p), b); }
	public JMethod(String n, JReference r, List<Parameter> p, BlockStatement b)
	{
		mName = n;
		mReturn = r;
		mParams = p.toArray(new Parameter[p.size()]);
		mBody = b;
	}
	
	@Override
	public void doGenerate(Generator g)
	{
		g.add(mReturn);
		
		if(mName != null) g.add(" ").add(mName);
		
		g.add("(");
		
		for(int i = 0 ; i < mParams.length ; ++i)
		{
			if(i != 0) g.add(", ");
			
			if(mParams[i].type.isFinal()) g.add("final ");
			
			g.add(mParams[i].type.typeName()).add(" ").add(mParams[i].name);
		}
		
		g.add(")");
		
		if(mBody != null)
		{
			g.lineBreak().add(mBody);
		}
		else
		{
			g.add(" { }");
		}
	}
	
	public static class Parameter
	{
		public final String name;
		public final JReference type;
		
		public Parameter(String name, JReference type)
		{
			this.name = name;
			this.type = type;
		}
	}
}
