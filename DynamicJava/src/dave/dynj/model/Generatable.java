package dave.dynj.model;

public interface Generatable
{
	public abstract void generate(Generator g);
	
	public default String generate( ) { Generator g = new Generator(); generate(g); return g.toString(); }
	
	public static final String IDENT = "\t";
}
