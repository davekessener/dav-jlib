package dave.dynj.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class JType extends Annotatable implements Generatable
{
	private final JReference mSelf;
	private final List<JReference> mImplements;
	private final List<JReference> mExtends;
	private final String mType;
	private final List<JVariable> mVars;
	private final List<JMethod> mMethods;
	private final List<JType> mSubTypes;
	private final BlockStatement mStatic;
	
	public JType(
		JReference self,
		String type,
		List<JReference> impl,
		List<JReference> ext,
		List<JVariable> vars,
		List<JMethod> meth,
		List<JType> subs,
		BlockStatement stat)
	{
		mSelf = self;
		mImplements = new ArrayList<>(impl == null ? Collections.emptyList() : impl);
		mExtends = new ArrayList<>(ext == null ? Collections.emptyList() : ext);
		mType = type;
		mVars = new ArrayList<>(vars == null ? Collections.emptyList() : vars);
		mMethods = new ArrayList<>(meth == null ? Collections.emptyList() : meth);
		mSubTypes = new ArrayList<>(subs == null ? Collections.emptyList() : subs);
		mStatic = stat;
	}
	
	public JReference reference() { return mSelf; }
	
	@Override
	public void doGenerate(Generator g)
	{
		mSelf.generateQualifiers(g).add(mType).add(" ").add(mSelf.typeName());
		
		if(!mExtends.isEmpty())
		{
			g.add(" extends ");
			
			boolean first = true;
			for(JReference r : mExtends)
			{
				if(!first) g.add(", ");
				g.add(r.typeName());
				first = false;
			}
		}
		
		if(!mImplements.isEmpty())
		{
			g.add(" implements ");
			
			boolean first = true;
			for(JReference r : mImplements)
			{
				if(!first) g.add(", ");
				g.add(r.typeName());
				first = false;
			}
		}
		
		g.lineBreak().add("{").lineBreak();
		
		Generator gg = g.enter();
		
		mVars.forEach(e -> {
			gg.add(e.type).add(" ").add(e.name);
			
			if(e.value != null)
			{
				gg.add(" = ").add(e.value);
			}
			
			gg.add(";").lineBreak();
		});
		
		boolean first = mVars.isEmpty();
		
		for(JMethod m : mMethods)
		{
			if(!first) gg.newLine().newLine();
			gg.add(m).lineBreak();
			first = false;
		}
		
		for(JType t : mSubTypes)
		{
			if(!first) gg.newLine().newLine();
			gg.add(t).lineBreak();
		}
		
		if(mStatic != null)
		{
			if(!first) gg.newLine().newLine();
			gg.add("static").lineBreak().add(mStatic).lineBreak();
		}
		
		g.add("}");
	}
	
	public static class JVariable
	{
		public final String name;
		public final JReference type;
		public final JExpression value;
		
		public JVariable(String name, JReference type, JExpression value)
		{
			this.name = name;
			this.type = type;
			this.value = value;
		}
	}
}
