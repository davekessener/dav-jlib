package dave.dynj.model;

public enum Visibility
{
	PUBLIC("public"),
	PRIVATE("private"),
	PROTECTED("protected"),
	PACKAGE(null);
	
	public final String mod;
	
	private Visibility(String mod)
	{
		this.mod = mod;
	}
}
