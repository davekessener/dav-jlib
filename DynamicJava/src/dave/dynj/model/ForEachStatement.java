package dave.dynj.model;

public class ForEachStatement implements JStatement
{
	private final JReference mType;
	private final String mVar;
	private final JExpression mScope;
	private final JStatement mBody;
	
	public ForEachStatement(JReference t, String v, JExpression s, JStatement b)
	{
		mType = t;
		mVar = v;
		mScope = s;
		mBody = b;
	}

	@Override
	public void generate(Generator g)
	{
		g.add("for(").add(mType.variableType()).add(" ")
		 .add(mVar).add(" : ").add(mScope).add(")").newLine()
		 .add(mBody);
	}
}
