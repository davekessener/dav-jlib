package dave.dynj.model;

import java.util.HashMap;
import java.util.Map;

import dave.util.Holder;

public class JAnnotation implements Generatable
{
	private final JReference mAnnotation;
	private final Map<String, JExpression> mContent;
	private final JExpression mValue;
	
	public JAnnotation(JReference a, JExpression v)
	{
		mAnnotation = a;
		mValue = v;
		mContent = new HashMap<>();
	}
	
	public JAnnotation add(String id, JExpression v) { mContent.put(id, v); return this; }
	
	@Override
	public void generate(Generator g)
	{
		g.add("@").add(mAnnotation.typeName());
		
		if(mValue != null)
		{
			g.add("(").add(mValue).add(")");
		}
		else if(!mContent.isEmpty())
		{
			g.add("(");
			
			Holder<Boolean> first = new Holder<>(true);
			mContent.forEach((id, v) -> {
				if(!first.value) g.add(", ");
				g.add(id).add(" = ").add(v);
				first.value = false;
			});
			
			g.add(")");
		}
	}
}
