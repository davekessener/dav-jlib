package dave.dynj.model;

public class EmptyStatement implements JStatement
{
	@Override
	public void generate(Generator g)
	{
		g.add(";");
	}
}
