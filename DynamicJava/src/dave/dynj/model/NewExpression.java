package dave.dynj.model;

import java.util.List;

public class NewExpression implements JExpression
{
	private final JReference mType;
	private final List<JExpression> mParams;
	
	public NewExpression(JReference t, List<JExpression> p)
	{
		mType = t;
		mParams = p;
	}

	@Override
	public void generate(Generator g)
	{
		g.add("new ").add(mType.typeName()).add("(");
		
		boolean f = true;
		for(JExpression e : mParams)
		{
			if(!f) g.add(", ");
			g.add(e);
			f = false;
		}
		
		g.add(")");
	}
}
