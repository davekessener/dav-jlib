package dave.security;

import java.security.GeneralSecurityException;

public class SevereSecurityException extends RuntimeException
{
	private static final long serialVersionUID = -6957088280568713252L;

	public SevereSecurityException(GeneralSecurityException e)
	{
		super(e);
	}
}
