package dave.security;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.util.Arrays;
import java.util.Base64;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.TrustManagerFactory;

import dave.util.SevereException;

public final class SecurityUtils
{
	public static KeyStore loadKeystore(File f, String pw)
	{
		return loadKeystore(f, pw, KeyStore.getDefaultType());
	}
	
	public static KeyStore loadKeystore(File f, String pw, String t)
	{
		try
		{
			KeyStore store = KeyStore.getInstance(t);
			
			try
			{
				if(f.exists()) try(FileInputStream is = new FileInputStream(f))
				{
					store.load(is, pw.toCharArray());
				}
				else try(FileOutputStream os = new FileOutputStream(f))
				{
					store.store(os, pw.toCharArray());
				}
			}
			catch(IOException e)
			{
				throw new SevereException(e);
			}
			
			return store;
		}
		catch(GeneralSecurityException e)
		{
			throw new SevereSecurityException(e);
		}
	}
	
	public static SSLContext generateContext(KeyStore auth, String pw) { return generateContext(auth, auth, pw); }
	public static SSLContext generateContext(KeyStore auth, KeyStore trust, String pw)
	{
		try
		{
			KeyManagerFactory keyfac = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
			TrustManagerFactory trustfac = TrustManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
			SSLContext ssl = SSLContext.getInstance(TLS);
			
			keyfac.init(auth, pw.toCharArray());
			trustfac.init(auth);
			
			ssl.init(keyfac.getKeyManagers(), trustfac.getTrustManagers(), null);
			
			return ssl;
		}
		catch(GeneralSecurityException e)
		{
			throw new SevereSecurityException(e);
		}
	}
	
	public static SSLServerSocket listen(SSLContext ssl) { return listen(ssl, 0); }
	public static SSLServerSocket listen(SSLContext ssl, int port)
	{
		try
		{
			return (SSLServerSocket) ssl.getServerSocketFactory().createServerSocket(port);
		}
		catch(IOException e)
		{
			throw new SevereException(e);
		}
	}
	
	public static SSLSocket connect(SSLContext ssl, String host, int port)
	{
		try
		{
			return (SSLSocket) ssl.getSocketFactory().createSocket(host, port);
		}
		catch(IOException e)
		{
			throw new SevereException(e);
		}
	}
	
	private static void transform(int i, byte[] input, byte[] output, byte[] cipher)
	{
		int l = input.length;
		int j = (3 + 7 * i * (i + 1)) % cipher.length;
		int k = (i + l - 1) % l;
		
		output[i] = (byte) (input[i] ^ cipher[j] ^ output[k]);
	}

	public static String simpleEncrypt(String v) { return simpleEncrypt(v, PASSWORD); }
	public static String simpleEncrypt(String v, String pw)
	{
		byte[] input = v.getBytes(CHARSET);
		byte[] output = Arrays.copyOf(input, input.length);
		byte[] cipher = pw.getBytes(CHARSET);
		
		for(int i = 0 ; i < input.length ; ++i)
		{
			transform(i, input, output, cipher);
		}
		
		return Base64.getEncoder().encodeToString(output);
	}

	public static String simpleDecrypt(String v) { return simpleDecrypt(v, PASSWORD); }
	public static String simpleDecrypt(String v, String pw)
	{
		byte[] input = Base64.getDecoder().decode(v);
		byte[] output = Arrays.copyOf(input, input.length);
		byte[] cipher = pw.getBytes(CHARSET);
		
		for(int i = input.length - 1 ; i >= 0 ; --i)
		{
			transform(i, input, output, cipher);
		}
		
		return new String(output, CHARSET);
	}
	
	private static final String TLS = "TLSv1.2";
	
	private static final String PASSWORD = "^B$gta3Cq$RXU#M7#yPYE?ck";
	
	private static final Charset CHARSET = Charset.forName("UTF-8");
	
	private SecurityUtils( ) { }
}
