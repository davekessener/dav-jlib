package dave.security.test;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.security.KeyStore;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLSocket;

import org.junit.Test;

import dave.security.SecurityUtils;

public class StaticUT
{
	private static final String PATH_SERVER_KS = "run/server.jks";
	private static final String PATH_CLIENT_AUTH_KS = "run/client.jks";
	private static final String PATH_CLIENT_TRUST_KS = "run/client.jks";
	private static final String PATH_FRAUD_KS = "run/fraud.jks";
	
	@Test
	public void testSimpleCrypt()
	{
		String encrypted = SecurityUtils.simpleEncrypt(PAYLOAD);
		String other = SecurityUtils.simpleEncrypt(PAYLOAD, PASSWORD);
		String decrypted = SecurityUtils.simpleDecrypt(encrypted);
		String another = SecurityUtils.simpleDecrypt(other, PASSWORD);
		
		assertEquals(decrypted, PAYLOAD);
		assertEquals(another, PAYLOAD);
		assertNotEquals(encrypted, PAYLOAD);
		assertNotEquals(other, PAYLOAD);
		assertNotEquals(encrypted, other);
	}
	
	@Test
	public void testExisting( ) throws IOException, InterruptedException, ExecutionException
	{
		File f_server = new File(PATH_SERVER_KS);
		File f_client_auth = new File(PATH_CLIENT_AUTH_KS);
		File f_client_trust = new File(PATH_CLIENT_TRUST_KS);
		
		runTest(f_server, f_client_auth, f_client_trust);
	}

	@Test(expected = javax.net.ssl.SSLHandshakeException.class)
	public void testFailedClientAuth( ) throws IOException, InterruptedException, ExecutionException
	{
		File f_fraud = new File(PATH_FRAUD_KS);
		File f_server = new File(PATH_SERVER_KS);
		File f_client = new File(PATH_CLIENT_TRUST_KS);
		
		runTest(f_server, f_fraud, f_client);
	}

	@Test(expected = javax.net.ssl.SSLHandshakeException.class)
	public void testFailedServerAuth( ) throws IOException, InterruptedException, ExecutionException
	{
		File f_fraud = new File(PATH_FRAUD_KS);
		File f_auth = new File(PATH_CLIENT_AUTH_KS);
		File f_trust = new File(PATH_CLIENT_TRUST_KS);
		
		runTest(f_fraud, f_auth, f_trust);
	}
	
	private void runTest(File f_server, File f_client_auth, File f_client_trust) throws IOException, InterruptedException, ExecutionException
	{
		KeyStore ks_server = SecurityUtils.loadKeystore(f_server, PASSWORD);
		KeyStore ks_client_auth = SecurityUtils.loadKeystore(f_client_auth, PASSWORD);
		KeyStore ks_client_trust = SecurityUtils.loadKeystore(f_client_trust, PASSWORD);
		
		SSLContext ssl_server = SecurityUtils.generateContext(ks_server, PASSWORD);
		SSLContext ssl_client = SecurityUtils.generateContext(ks_client_auth, ks_client_trust, PASSWORD);
		
		byte[] payload = PAYLOAD.getBytes();
		
		ExecutorService async = Executors.newSingleThreadExecutor();
		
		SSLServerSocket server = SecurityUtils.listen(ssl_server);
		
		server.setNeedClientAuth(true);
		
		Future<String> await = async.submit(() -> {
			byte[] buf = new byte[payload.length];
			SSLSocket client = (SSLSocket) server.accept();
			
			client.getInputStream().read(buf);
			client.close();
			
			return new String(buf);
		});
		
		SSLSocket client = SecurityUtils.connect(ssl_client, "localhost", server.getLocalPort());
		
		client.getOutputStream().write(payload);
		
		String r = await.get();
		
		client.close();
		server.close();
		
		assertEquals(PAYLOAD, r);
	}
	
	private static final String PASSWORD = "123456";
	private static final String PAYLOAD = "Hello, World!";
}
