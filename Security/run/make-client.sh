#!/bin/bash

server_domain=server.$1
server_ks=server.jks

client_domain=client.$1
client_ks=client.jks

stty -echo
read -p "Server keystore password: " server_pw; echo
read -p "Client keystore password: " client_pw; echo
stty echo

keytool -importkeystore -srckeystore $server_ks -destkeystore server.p12 -deststoretype PKCS12 -srcalias $server_domain -deststorepass 123456 -destkeypass 123456 -srcstorepass "$server_pw" -srckeypass "$server_pw"
openssl pkcs12 -in server.p12 -nokeys -out server-cert.pem -passin pass:123456
openssl pkcs12 -in server.p12 -nodes -nocerts -out server-key.pem -passin pass:123456
keytool -keystore $client_ks -storepass "$client_pw" -keypass "$client_pw" -genkey -alias $client_domain -dname "CN=Unknown"
keytool -keystore $client_ks -storepass "$client_pw" -certreq -alias $client_domain -keyalg RSA -file client.csr
openssl x509 -req -CA server-cert.pem -CAkey server-key.pem -in client.csr -out client.cer -days 3650 -CAcreateserial
keytool -import -keystore $client_ks -storepass "$client_pw" -file server-cert.pem -alias $server_domain -noprompt
keytool -import -keystore $client_ks -storepass "$client_pw" -file client.cer -alias $client_domain

rm client.csr client.cer server-cert.pem server-cert.srl server-key.pem server.p12

