#!/bin/bash
stty -echo 
read -p "Keystore password: " passwd; echo
stty echo 

keytool -keystore server.jks -genkey -alias server.$1 -keyalg RSA -validity 3650 -dname "CN=Unknown" -storepass "$passwd" -keypass "$passwd"

