package dave.util.tools;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.security.KeyStoreException;
import java.security.PublicKey;
import java.util.stream.Collectors;

import javax.xml.bind.DatatypeConverter;

import dave.arguments.Arguments;
import dave.arguments.Option;
import dave.arguments.Option.OptionBuilder;
import dave.arguments.ParseException;
import dave.arguments.Parser;
import dave.json.JsonObject;
import dave.json.JsonValue;
import dave.json.StringBuffer;
import dave.security.SecurityUtils;
import dave.util.Utils;

public class TrustTool
{
	private static final String KEYSTORE = "auth.jks";
	
	private static final Option o_keystore = (new OptionBuilder("store")).setDefault(KEYSTORE).build();
	private static final Option o_lib = (new OptionBuilder("library")).hasValue(true).setForced(true).build();
	private static final Option o_s_pass = (new OptionBuilder("server-password")).hasValue(true).setForced(true).build();
	private static final Option o_c_pass = (new OptionBuilder("client-password")).hasValue(true).build();
	private static final Option o_alias = (new OptionBuilder("alias")).hasValue(true).build();
	private static final Option o_manifest = (new OptionBuilder("manifest")).hasValue(true).build();
	private static final Option o_list = (new OptionBuilder("list")).build();
	private static final Option o_add = (new OptionBuilder("add")).build();
	private static final Option o_extract = (new OptionBuilder("extract")).build();
	private static final Option o_pubkey = (new OptionBuilder("key")).hasValue(true).build();
	private static final Option o_verbose = (new OptionBuilder("verbose")).setShortcut("v").build();
	private static final Parser PARSER = new Parser(o_keystore, o_lib, o_s_pass, o_c_pass, o_alias, o_manifest, o_extract, o_pubkey, o_list, o_add, o_verbose);

	private static final int FLAG_LIST = 1;
	private static final int FLAG_EXTRACT = 2;
	private static final int FLAG_ADD = 4;
	
	private static boolean VERBOSE = false;
	
	public static void main(String[] args) throws KeyStoreException, IOException
	{
		try
		{
			Arguments a = PARSER.parse(args);
			int mode =
				(a.hasArgument(o_list) ? FLAG_LIST : 0) |
				(a.hasArgument(o_extract) ? FLAG_EXTRACT : 0) |
				(a.hasArgument(o_add) ? FLAG_ADD : 0);
			
			VERBOSE = a.hasArgument(o_verbose);
			
			switch(mode)
			{
				case FLAG_ADD:
					runAdd(a);
					break;
					
				case FLAG_EXTRACT:
					runExtract(a);
					break;
					
				case FLAG_LIST:
					runList(a);
					break;
					
				default:
					throw new IllegalStateException("Must select one of --add, --list, or --extract!");
			}
		}
		catch(ParseException e)
		{
			e.printStackTrace();
			System.err.println("-----");
			System.err.println("usage:");
			System.err.println(PARSER.toString());
		}
	}
	
	private static void runAdd(Arguments a) throws KeyStoreException, IOException
	{
		String spw = a.getArgument(o_s_pass);
		String cpw = a.getArgument(o_c_pass);
		String alias = "client." + a.getArgument(o_alias);
		File ksf = new File(a.getArgument(o_keystore));
		File libf = new File(a.getArgument(o_lib));
		File manifestf = new File(a.getArgument(o_manifest));
		
		if(VERBOSE)
		{
			p("#=======================================================================");
			p("| ADD ENTRY");
			p("+-----------------------------------------------------------------------");
			p("| Server password  " + spw);
			p("| Client password  " + cpw);
			p("| Alias            " + alias);
			p("| Keystore         " + ksf.getName());
			p("| Library          " + libf.getName());
			p("| Manifest         " + manifestf.getName());
			p("+-----------------------------------------------------------------------");
		}
		
		addEntry(spw, cpw, alias, ksf, libf, manifestf);
		
		if(VERBOSE)
		{
			p("#=======================================================================");
		}
	}
	
	private static void runExtract(Arguments a) throws IOException, KeyStoreException
	{
		String spw = a.getArgument(o_s_pass);
		String cpw = a.getArgument(o_c_pass);
		File libf = new File(a.getArgument(o_lib));
		JsonObject lib = loadLibrary(libf, spw);
		String key = null;
		
		if(a.hasArgument(o_pubkey))
		{
			key = a.getArgument(o_pubkey);
		}
		else
		{
			File ksf = new File(a.getArgument(o_keystore));
			String alias = "client." + a.getArgument(o_alias);
			
			key = Utils.hash(loadPubKey(ksf, spw, alias).getEncoded());
		}
		
		if(!lib.contains(key))
		{
			throw new IllegalArgumentException("No entry for key " + key);
		}
		
		if(VERBOSE)
		{
			p("#=======================================================================");
			p("| EXTRACT MANIFEST");
			p("+-----------------------------------------------------------------------");
			p("| Server password  " + spw);
			p("| Client password  " + cpw);
			p("| Public Key       " + key);
			p("| Library          " + libf.getName());
			p("+-----------------------------------------------------------------------");
		}
		
		String manifest = decrypt(lib.getObject(key).getString("manifest"), spw, cpw);
		
		if(VERBOSE)
		{
			p("#=======================================================================");
		}
		
		p("Manifest of " + key + ":");
		p(">>> BEGIN MANIFEST");
		p(manifest);
		p(">>> END MANIFEST");
	}
	
	private static void runList(Arguments a) throws IOException
	{
		String spw = a.getArgument(o_s_pass);
		File libf = new File(a.getArgument(o_lib));
		JsonObject lib = loadLibrary(libf, spw);
		
		if(!libf.exists())
			throw new IllegalArgumentException("There is no library " + libf.getName() + "!");

		if(VERBOSE)
		{
			p("#=======================================================================");
			p("| LIST ENTRIES");
			p("+-----------------------------------------------------------------------");
			p("| Server password  " + spw);
			p("| Library          " + libf.getName());
			p("#=======================================================================");
		}
		
		lib.keySet().forEach(System.out::println);
	}
	
	private static void addEntry(String spw, String cpw, String alias, File ksf, File libf, File manifestf) throws KeyStoreException, IOException
	{
		String manifest = Files.readAllLines(manifestf.toPath()).stream().collect(Collectors.joining("\n"));
		JsonObject library = loadLibrary(libf, spw);
		PublicKey key = loadPubKey(ksf, cpw, alias);
		String hash = Utils.hash(key.getEncoded());
		
		if(library.contains(hash))
			throw new IllegalStateException("An entry for client '" + hash + "' already exists!");
		
		p("| Creating new entry for client " + hash + " in " + libf.getName() + " of manifest " + manifestf.getName());
		
		JsonObject entry = new JsonObject();
		
		entry.putString("pubkey", DatatypeConverter.printHexBinary(key.getEncoded()));
		entry.putString("manifest", encrypt(manifest, spw, cpw));
		entry.putString("hash", Utils.hash(manifest.getBytes(CHARSET)));
		
		library.put(hash, entry);
		
		saveLibrary(libf, library, spw);
	}

	private static String encrypt(String v, String spw, String cpw)
	{
		String key_s = Utils.hash(spw.getBytes(CHARSET));
		String key_c = Utils.hash(cpw.getBytes(CHARSET));

		if(VERBOSE)
		{
			p("| Server PW hash   " + key_s);
			p("| Client PW hash   " + key_c);
		}
		
		return SecurityUtils.simpleEncrypt(SecurityUtils.simpleEncrypt(v, key_s), key_c);
	}

	private static String decrypt(String v, String spw, String cpw)
	{
		String key_s = Utils.hash(spw.getBytes(CHARSET));
		String key_c = Utils.hash(cpw.getBytes(CHARSET));

		if(VERBOSE)
		{
			p("| Server PW hash   " + key_s);
			p("| Client PW hash   " + key_c);
		}
		
		return SecurityUtils.simpleDecrypt(SecurityUtils.simpleDecrypt(v, key_c), key_s);
	}
	
	private static PublicKey loadPubKey(File ks, String pw, String alias) throws KeyStoreException
	{
		return SecurityUtils.loadKeystore(ks, pw).getCertificate(alias).getPublicKey();
	}
	
	private static JsonObject loadLibrary(File f, String pw) throws IOException
	{
		if(f.exists())
		{
			String encrypted = Files.readAllLines(f.toPath()).stream().collect(Collectors.joining(""));
			String decrypted = SecurityUtils.simpleDecrypt(encrypted, pw);
			
			return (JsonObject) JsonValue.read(new StringBuffer(decrypted));
		}
		else
		{
			return new JsonObject();
		}
	}
	
	private static void saveLibrary(File f, JsonValue v, String pw) throws IOException
	{
		String raw = v.toString();
		String encrypted = SecurityUtils.simpleEncrypt(raw, pw);
		
		Files.write(f.toPath(), encrypted.getBytes(CHARSET));
	}
	
	private static final void p(String s, Object ... f) { System.out.println(String.format(s, f)); }
	
	private static final Charset CHARSET = Charset.forName("UTF-8");
}
