package dave.ut.io;

import static org.junit.Assert.*;

import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import dave.util.Proxify;

public class ProxyUT
{
	public static interface A { void a( ); }
	public static interface B extends A { void b( ); }
	public static interface C { void c( ); }
	public static interface D extends A, B, C { }
	
	private static class Impl implements D
	{
		public final List<String> calls = new ArrayList<>();
		
		@Override
		public void c( )
		{
			calls.add("c");
		}

		@Override
		public void b( )
		{
			calls.add("b");
		}

		@Override
		public void a( )
		{
			calls.add("a");
		}
	}
	
	private static class CB implements Proxify.Callback<D>
	{
		public final List<String> calls = new ArrayList<>();
		
		@Override
		public Object call(D o, Method f, Object[] a) throws Exception
		{
			calls.add(f.getName());
			
			return null;
		}
	}
	
	@Test
	public void testBasics( )
	{
		final Impl base = new Impl();
		D proxy = (D) Proxy.newProxyInstance(
			ProxyUT.class.getClassLoader(),
			new Class<?>[] { D.class },
			(p, m, a) -> m.invoke(base, a));
		
		proxy.a();
		proxy.b();
		proxy.c();
		
		assertEquals(3, base.calls.size());
	}
	
	@Test
	public void testProxy( )
	{
		Impl base = new Impl();
		CB cb = new CB();
		D proxy = Proxify.wrap(D.class, base, cb, B.class);
		
		proxy.a();
		proxy.b();
		proxy.c();
		
		assertEquals(2, base.calls.size());
		assertEquals(1, cb.calls.size());
		
		assertEquals("a", base.calls.get(0));
		assertEquals("b", cb.calls.get(0));
		assertEquals("c", base.calls.get(1));
	}
}
