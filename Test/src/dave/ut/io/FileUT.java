package dave.ut.io;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

import dave.json.JsonObject;
import dave.json.JsonValue;
import dave.ut.json.TestClass;
import dave.util.Utils;
import dave.util.io.DummyIO;
import dave.util.io.FileIO;
import dave.util.io.IO;
import dave.util.io.InputBuffer;
import dave.util.io.OutputPrinter;
import dave.util.persistence.Storage;
import dave.util.persistence.file.IOStorage;

public class FileUT
{
	@Test
	public void testFileReadWrite( ) throws IOException
	{
		IO io = new DummyIO();
		JsonValue json = generateJSON();
		
		json.write(new OutputPrinter(io, Utils.CHARSET));
		
		io.seek(0);
		
		JsonValue o = JsonValue.read(new InputBuffer(io.size(), io, Utils.CHARSET));
		
		assertEquals(o, json);
	}
	
	@Test
	public void testCreationOfEmptyDB( )
	{
		DummyIO io = new DummyIO();
		
		create(io);
		io.reopen();
		
		assertEquals(2 + 2 + 4 + 4 + 4, io.size());
		
		io.seek(0);
		
		assertEquals(0x7eda, io.readShort());
		assertEquals(1, io.readShort());
		assertEquals(0x538f30c9, io.readInt());
		assertEquals(0, io.readInt());
		assertEquals(0, io.readInt());
	}
	
	@Test
	public void testWriteAndRead( )
	{
		DummyIO io = new DummyIO();
		Storage db = create(io);
		
		String id = generateName();
		JsonValue json = generateJSON();
		
		db.store(id, json);
		
		db = create(io);
		
		JsonValue cmp = db.retrieve(id);
		
		assertEquals(json, cmp);
	}
	
	@Test
	public void testHardcore( )
	{
		File f = new File("test.db");
		
		f.delete();
		
		IOStorage db = new IOStorage(() -> new FileIO(f));
		Set<String> ids = new HashSet<>();
		
		for(int i = 0 ; i < 100000 ; ++i)
		{
			String id = generateName() + "-" + (int)(9 * Math.random());
			
			db.store(id, generateJSON());
			ids.add(id);
		}
		
		File copy = new File("copy.db");
		
		copy.delete();
		
		IO io = new FileIO(copy);
		
		db.copy(io);
		
		io.close();
		
		Storage other = new IOStorage(() -> new FileIO(copy));
		
		for(String id : ids)
		{
			assertTrue(other.has(id));
			assertEquals(other.retrieve(id), db.retrieve(id));
		}
	}
	
	@Test
	public void listContent( )
	{
		File f = new File("test_miss.db");
		Storage s = new IOStorage(() -> new FileIO(f));
		
		assertTrue(s.has("6a4cb539f0984ca1811b7e2b67d71cdb"));
		
		System.out.println(s.retrieve("6a4cb539f0984ca1811b7e2b67d71cdb").toString());
	}
	
	private Storage create(DummyIO io)
	{
		return new IOStorage(() -> {
			io.reopen();
			return io;
		});
	}
	
	private static JsonValue generateJSON( )
	{
		JsonObject json = (JsonObject) (new TestClass()).save();
		
		if(Math.random() < 0.3)
		{
			json.putLong("exp", (long) (Math.random() * 0x1000000));
		}
		
		if(Math.random() < 0.1)
		{
			json.put("child", (new TestClass()).save());
		}
		
		return json;
	}
	
	private static String generateName( )
	{
		return NAMES[(int) Math.round(Math.random() * (NAMES.length - 1))];
	}
	
	private static final String[] NAMES = { "bob", "dante", "siegward", "solaire", "alfred", "jean", "alexander", "ornstein", "smough", "gwyn", "artorias", "gough", "elfriede", "gael", "mikolash", "rom", "amygdala", "ebrietas", "gehrman", "lady-maria", "laurence", "ludwig", "eileen", "lothric", "lorian", "sage-freke", "vallafax", "biorr", "rydell", "allant", "ostrava", "amelia", "seath", "guyra", "shudom", "oliviera", "orladin", "vallad"};
}
