package dave.ut.io;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import org.junit.Test;

import dave.util.io.DummyIO;
import dave.util.io.FileIO;
import dave.util.io.IO;

public class IOUT
{
	public void testIO(IO io)
	{
		byte[] data = "Hello, World!".getBytes();
		byte[] part = "World".getBytes();
		int o = "Hello, ".getBytes().length;
		
		assertEquals(0, io.size());
		assertEquals(0, io.seek());
		
		io.write(data);
		
		assertEquals(data.length, io.size());
		assertEquals(data.length, io.seek());
		
		io.seek(o);
		
		assertEquals(data.length, io.size());
		assertEquals(o, io.seek());
		
		byte[] cmp = io.read(part.length);
		
		assertEquals(o + part.length, io.seek());
		assertTrue(Arrays.equals(part, cmp));
	}
	
	@Test
	public void testDummyIO( )
	{
		testIO(new DummyIO());
	}
	
	@Test
	public void testFileIO( ) throws IOException
	{
		File f = File.createTempFile("" + System.currentTimeMillis(), "");
		
		f.deleteOnExit();
		
		testIO(new FileIO(f));
	}
	
	@Test
	public void testFormattedIO( )
	{
		IO io = new DummyIO();
		
		byte v1 = 0x7f;
		short v2 = 0x1234;
		int v3 = 0x44332211;
		long v4 = 0x0817263544536271l;
		byte[] v = { 0x7f, 0x34, 0x12, 0x11, 0x22, 0x33, 0x44, 0x71, 0x62, 0x53, 0x44, 0x35, 0x26, 0x17, 0x08};
		
		io.writeByte(v1);
		io.writeShort(v2);
		io.writeInt(v3);
		io.writeLong(v4);
		
		assertEquals(v.length, io.size());
		
		io.seek(0);
		
		byte[] cmp = io.read(v.length);
		
		assertTrue(Arrays.equals(v, cmp));
		
		io.seek(0);
		
		assertEquals(v1, io.readByte());
		assertEquals(v2, io.readShort());
		assertEquals(v3, io.readInt());
		assertEquals(v4, io.readLong());
		
		int t = io.seek();
		
		float pi = (float) Math.PI;
		double e = Math.E;
		
		io.writeFloat(pi);
		io.writeDouble(e);
		
		assertEquals(io.seek() - t, 4 + 8);
		
		io.seek(t);
		
		assertEquals(pi, io.readFloat(), 0.0f);
		assertEquals(e, io.readDouble(), 0.0f);
	}
}
