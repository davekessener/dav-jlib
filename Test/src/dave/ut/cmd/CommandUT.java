package dave.ut.cmd;

import static org.junit.Assert.*;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.junit.Test;

import dave.util.command.Argument.Type;
import dave.util.command.CommandBuilder;
import dave.util.command.Engine;
import dave.util.command.ParseException;
import dave.util.command.Tokenizer;

public class CommandUT
{
	private static interface HelpCommand { void execute( ); }
	private static interface AdderCommand { void execute(int a, int b); }
	
	public static List<String> process(String line) throws ParseException
	{
		return (new Tokenizer(line)).stream().collect(Collectors.toList());
	}
	
	@Test
	public void testTokenizer( ) throws ParseException
	{
		List<String> l;
		
		l = process("");
		assertEquals(0, l.size());
		
		l = process("  \t \n");
		assertEquals(0, l.size());
		
		l = process("help");
		assertEquals(1, l.size());
		assertEquals("help", l.get(0));
		
		l = process(" HeLp\n");
		assertEquals(1, l.size());
		assertEquals("HeLp", l.get(0));
		
		l = process("add 7 3.141");
		assertEquals(3, l.size());
		assertEquals("add", l.get(0));
		assertEquals("7", l.get(1));
		assertEquals("3.141", l.get(2));
		
		l = process("   help  \t\"Hello, World!\\n\" -9.1e-9\n");
		assertEquals(3, l.size());
		assertEquals("help", l.get(0));
		assertEquals("\"Hello, World!\n\"", l.get(1));
		assertEquals("-9.1e-9", l.get(2));
	}
	
	@Test
	public void testCreation( )
	{
		Engine e = new Engine();
		HelpCommand cmd_1 = () -> {};
		
		e.add((new CommandBuilder<>("help", "description", cmd_1)).build());
	}
	
	@Test
	public void testExecution( ) throws ParseException
	{
		final int[] a = new int[] { 0 };
		Engine e = new Engine();
		HelpCommand cmd_1 = () -> ++a[0];
		
		e.add((new CommandBuilder<>("help", "description", cmd_1)).build());
		
		e.run("help");
		
		assertEquals(1, a[0]);
	}
	
	@Test
	public void testArguments( ) throws ParseException
	{
		final int[] r = new int[] { 0 };
		
		Engine e = new Engine();
		AdderCommand cmd = (a, b) -> r[0] = a + b;
		
		e.add((new CommandBuilder<>("add", "adds two numbers", cmd))
				.add("first", Type.INT)
				.add("second", Type.INT)
				.build());
		
		e.run("add 7 11");
		
		assertEquals(18, r[0]);
	}
}
