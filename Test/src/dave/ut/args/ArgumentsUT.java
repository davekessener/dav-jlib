package dave.ut.args;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

import org.junit.Test;

import dave.arguments.Arguments;
import dave.arguments.Option;
import dave.arguments.ParseException;
import dave.arguments.Parser;
import dave.arguments.Option.OptionBuilder;

public class ArgumentsUT
{
	@Test
	public void testEmpty( ) throws ParseException
	{
		Parser p = new Parser(asSet());
		Arguments args = p.parse(new String[] { "a", "b", "c" });
		
		assertTrue(args.hasMainArgument());
		assertEquals(args.getMainArgument(), "a b c");
		
		args = p.parse(new String[] {});
		
		assertFalse(args.hasMainArgument());
	}
	
	@Test
	public void testOne( ) throws ParseException
	{
		Option a = (new OptionBuilder("test")).build();
		Parser p = new Parser(asSet(a));
		Arguments args = p.parse(new String[] {});
		
		assertFalse(args.hasArgument(a));
		
		args = p.parse(new String[] { "--test" });
		
		assertTrue(args.hasArgument(a));
	}
	
	@Test
	public void testShort( ) throws ParseException
	{
		Option a = (new OptionBuilder("test")).setShortcut("t").build();
		Parser p = new Parser(asSet(a));
		Arguments args = p.parse(new String[] { "-t" });
		
		assertTrue(args.hasArgument(a));
	}
	
	@Test
	public void withArgument( ) throws ParseException
	{
		Option a = (new OptionBuilder("test")).setShortcut("t").hasValue(true).build();
		Parser p = new Parser(asSet(a));
		Arguments args = p.parse(new String[] { "--test", "abc" });
		
		assertTrue(args.hasArgument(a));
		assertEquals(args.getArgument(a), "abc");
		
		args = p.parse(new String[] { "--test=def" });
		
		assertTrue(args.hasArgument(a));
		assertEquals(args.getArgument(a), "def");
		
		args = p.parse(new String[] { "-t", "ghi" });
		
		assertTrue(args.hasArgument(a));
		assertEquals(args.getArgument(a), "ghi");
	}
	
	@Test(expected = ParseException.class)
	public void failsUnknown( ) throws ParseException
	{
		Parser p = new Parser(asSet());
		
		p.parse(new String[] { "--test" });
	}

	@Test(expected = ParseException.class)
	public void failsRequired( ) throws ParseException
	{
		Option a = (new OptionBuilder("test")).setForced(true).build();
		Parser p = new Parser(asSet(a));
		
		p.parse(new String[] { "what" });
	}
	
	@Test
	public void suppliesDefault( ) throws ParseException
	{
		Option a = (new OptionBuilder("test")).setDefault("abc").build();
		Parser p = new Parser(asSet(a));
		Arguments args = p.parse(new String[] { });
		
		assertTrue(args.hasArgument(a));
		assertEquals(args.getArgument(a), "abc");
	}
	
	private static Set<Option> asSet(Option ... o)
	{
		return Arrays.stream(o).collect(Collectors.toSet());
	}
}
