package dave.ut.json;

import static org.junit.Assert.*;

import org.junit.Test;

import dave.json.StringBuffer;
import dave.json.JSON;
import dave.json.JsonArray;
import dave.json.JsonConstant;
import dave.json.JsonNumber;
import dave.json.JsonObject;
import dave.json.JsonString;
import dave.json.JsonValue;
import dave.json.PrettyPrinter;
import dave.json.Printer;

public class JsonUT
{
	@Test
	public void testSerialiazion( )
	{
		TestClass test = new TestClass("dave", -7, 13, 69, .89f);
		
		JsonValue json = JSON.serialize(test);
		
//		System.out.println(json.toString());
		
		Object o = JSON.deserialize(json);
		
		assertEquals(test, o);
	}
	
	@Test
	public void testNumbers( )
	{
		int t = 0;
		Number o = t;
		JsonNumber n = new JsonNumber(o.longValue());
		Object oo = 0;
		
		assertEquals(n.toString(), "0");
		assertEquals(JSON.serialize(oo), n);
		
		Number nn = (Number) JSON.deserialize(JSON.serialize(oo));
		
		assertTrue(0 == nn.intValue());
	}
	
	@Test
	public void test( )
	{
		JsonObject coords = new JsonObject();
		
		coords.putNumber("x", 3.141);
		coords.putNumber("y", 42.0);
		coords.putNumber("z", 6.9);
		
		JsonArray vert = new JsonArray();
		
		for(int i = 0 ; i < 10 ; ++i)
		{
			vert.add(new JsonNumber(i * i / 10.0f));
		}
		
		JsonArray props = new JsonArray();
		
		props.add(new JsonString("frequent-flyer"));
		props.add(new JsonString("whatever"));
		props.add(new JsonString("some-long-string"));
		props.add(new JsonString("abcdefg"));
		props.add(new JsonString("finger lickin' good"));
		
		JsonArray oarr = new JsonArray();
		JsonObject dummy = new JsonObject();
		
		dummy.putInt("the answer", 42);
		
		oarr.add(dummy);
		oarr.add(new JsonObject());
		oarr.add(dummy);
		oarr.add(new JsonObject());
		oarr.add(dummy);
		
		JsonObject o = new JsonObject();
		
		o.putString("name", "Orphan of Kos");
		o.putInt("HP", 300000);
		o.put("vertices", vert);
		o.put("pos", coords);
		o.put("objarr", oarr);
		o.put("attributes", props);
		o.put("weakness", JsonConstant.NULL);
		o.putString("a \"difficult\" string", "\nWhatever,\t\"matey\"!");
		
		Printer t = new PrettyPrinter();
		
		o.write(t);
		
		String saved = t.toString();
		
		JsonValue v = JsonValue.read(new StringBuffer(saved));
		
		t = new PrettyPrinter();
		
		v.write(t);
		
		String s = t.toString();
		
		assertEquals(s, saved);
		assertEquals(v, o);
	}
}
