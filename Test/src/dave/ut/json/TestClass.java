package dave.ut.json;

import java.util.Objects;

import dave.json.Container;
import dave.json.JsonObject;
import dave.json.JsonValue;
import dave.json.Loader;
import dave.json.Saver;

@Container
public class TestClass
{
	private final String mName;
	private final int[] mPos;
	private final float mHealth;
	
	public TestClass( )
	{
		this("dave",
			(int) (1000 * Math.random()),
			(int) (1000 * Math.random()),
			(int) (1000 * Math.random()),
			(float) Math.random());
	}
	
	public TestClass(String name, int x, int y, int z, float f)
	{
		mName = name;
		mPos = new int[] { x, y, z };
		mHealth = f;
	}
	
	@Override
	public boolean equals(Object o)
	{
		if(o instanceof TestClass)
		{
			TestClass t = (TestClass) o;
			
			return t.mName.equals(mName) && Objects.deepEquals(t.mPos, mPos);
		}
		
		return false;
	}
	
	@Saver
	public JsonValue save( )
	{
		JsonObject json = new JsonObject();
		JsonObject pos = new JsonObject();
		
		pos.putInt("x", mPos[0]);
		pos.putInt("y", mPos[1]);
		pos.putInt("z", mPos[2]);

		json.putString("name", mName);
		json.put("pos", pos);
		json.putNumber("health", mHealth);
		
		return json;
	}
	
	@Loader
	public static TestClass load(JsonValue json)
	{
		JsonObject o = (JsonObject) json;
		JsonObject pos = o.getObject("pos");
		
		String name = o.getString("name");
		int x = pos.getInt("x");
		int y = pos.getInt("y");
		int z = pos.getInt("z");
		float h = (float) o.getDouble("health");
		
		return new TestClass(name, x, y, z, h);
	}
}
