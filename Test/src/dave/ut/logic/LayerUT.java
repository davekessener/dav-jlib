package dave.ut.logic;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Test;

import dave.nn.conv.ImmediateBuffer;
import dave.nn.conv.Layer;
import dave.nn.conv.MaxPool;
import dave.nn.conv.PoolingLayer;
import dave.util.math.Vec3;

public class LayerUT
{
	@Test
	public void pooling_layer( )
	{
		Layer l = new PoolingLayer(new Vec3(6, 4, 2), new MaxPool(2));
		
		float[] in = {
			2, 4, 1, 3, 6, 4,
			1, 1, 5, 0, 1, 1,
			0, 0, 9, 8, 0, 1,
			0, 1, 7, 6, 2, 0,
			
			0,-1, 1, 3, 3, 7,
			0,-2,15, 0, 0, 0,
			0, 0,-9,-8, 6, 4,
			0, 0,-7,-6, 8, 0
		};
		
		float[] out = new float[3 * 2 * 2];
		float[] r = {
			4, 5, 6,
			1, 9, 2,
			0,15, 7,
			0,-6, 8
		};
		
		Vec3 d = l.dimension();
		
		assertEquals(3, d.getX());
		assertEquals(2, d.getY());
		assertEquals(2, d.getZ());
		
		l.calc(new ImmediateBuffer(in), new ImmediateBuffer(out));
		
		Assert.assertArrayEquals(r, out, 0.0f);
	}
}
