package dave.ut.logic;

import static org.junit.Assert.*;

import org.junit.Test;

import dave.nn.conv.Allocator;
import dave.nn.conv.BasicAllocator;
import dave.nn.conv.Buffer;

public class AllocatorUT
{
	@Test
	public void can_allocate( )
	{
		Allocator a = new BasicAllocator();
		
		Buffer b1 = a.allocate(10);
		
		assertEquals(10, b1.size());
		
		for(int i = 0 ; i < b1.size() ; ++i)
		{
			int v = i * i;
			
			b1.set(i, v);
			
			assertEquals(v, b1.get(i), 0.0f);
		}
		
		Buffer b2 = a.allocate(5);

		assertEquals(10, b1.size());
		assertEquals(5, b2.size());
		
		for(int i = 0 ; i < b2.size() ; ++i)
		{
			int v = i * i - 1;
			
			b2.set(i, v);
			
			assertEquals(v, b2.get(i), 0.0f);
		}
		
		for(int i = 0 ; i < b1.size() ; ++i)
		{
			assertEquals(i * i, b1.get(i), 0.0f);
		}
		
		Allocator a_c = a.clone();
		
		Buffer c2 = a_c.clone(b2);
		Buffer c1 = a_c.clone(b1);
		
		for(int i = 0 ; i < b1.size() ; ++i)
		{
			assertEquals(b1.get(i), c1.get(i), 0.0f);
		}
		
		for(int i = 0 ; i < b2.size() ; ++i)
		{
			b2.set(i, 7 * i);
			assertEquals(7 * i, b2.get(i), 0.0f);
			assertEquals(i * i - 1, c2.get(i), 0.0f);
		}
	}
}
