package dave.ut.logic;

import static org.junit.Assert.*;

import org.junit.Test;

import dave.nn.conv.AveragePool;
import dave.nn.conv.FeedForwardFilter;
import dave.nn.conv.Filter;
import dave.nn.conv.ImmediateBuffer;
import dave.nn.conv.Matrix;
import dave.nn.conv.MaxPool;

public class FilterUT
{
	@Test
	public void max_pool( )
	{
		float[] data = { 4, 2, 7, 2, 9, 1, 0, 4, 7 };
		Filter f = new MaxPool(3);
		
		assertEquals(9.0, f.calc(new ImmediateBuffer(data)), 0.0f);
	}
	
	@Test
	public void avg_pool( )
	{
		float[] data = { 4, 1, 0, 7 };
		Filter f = new AveragePool(2);
		
		assertEquals(3.0, f.calc(new ImmediateBuffer(data)), 0.0f);
	}
	
	@Test
	public void ff_filter( )
	{
		float[] fun = { 1.0f, 3.0f, 2.0f, 0.0f };
		float[] data = { 5.0f, 2.0f, -0.5f, 666.0f };
		Filter f = new FeedForwardFilter(new Matrix(new ImmediateBuffer(fun), 4, 1));
		
		// (1 * 5 + 2 * 3 + 2 * -0.5 + 0 * 666) / (2 * 2)
		assertEquals(2.5, f.calc(new ImmediateBuffer(data)), 0.0f);
	}
}
