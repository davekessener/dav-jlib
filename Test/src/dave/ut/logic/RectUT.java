package dave.ut.logic;

import static org.junit.Assert.*;

import org.junit.Test;

import dave.nn.conv.MaxRect;
import dave.nn.conv.Rectifier;
import dave.nn.conv.SigmoidRect;

public class RectUT
{
	@Test
	public void sigrect( )
	{
		Rectifier f = new SigmoidRect();

		assertEquals(0.0f, f.apply(0.0f), 0.0f);
		assertEquals(0.5f, f.apply(0.5f), 0.05);
		assertEquals(-0.5f, f.apply(-0.5f), 0.05);
		
		for(int i = 0 ; i < 150 ; ++i)
		{
			float v = f.apply(i / 10.0f);
			
			assertTrue(-1.0 <= v && v <= 1.0);
			
			v = f.apply(-i / 10.0f);
			
			assertTrue(-1.0 <= v && v <= 1.0);
		}
	}
	
	@Test
	public void relu( )
	{
		Rectifier f = new MaxRect();
		
		for(int i = 0 ; i < 150 ; ++i)
		{
			float v = f.apply(i / 10.0f);
			
			assertEquals(i / 10.0f, v, 0.0f);
			
			v = f.apply(-i / 10.0f);
			
			assertEquals(0.0f, v, 0.0f);
		}
	}
}
