package dave.ut.system;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.NetworkInterface;
import java.util.Scanner;

import org.junit.Test;

import dave.net.NetUtils;
import dave.system.BaseSystem;
import dave.system.DistributedAISystem;
import dave.system.SystemUtils;
import dave.util.TransformingConsumer;
import dave.util.Utils;
import dave.util.log.LogBase;
import dave.util.log.SimpleFormatter;
import dave.util.log.Spacer;

public class WatcherUT
{
	@Test
	public void runWatcher( ) throws IOException
	{
		LogBase.INSTANCE.registerSink(e -> true, new TransformingConsumer<>(new Spacer(s -> System.out.println(s), 1500, Utils.repeat("=", 200)), new SimpleFormatter()));
		
		LogBase.INSTANCE.start();
		
		final NetworkInterface nic = SystemUtils.findActiveNetwork();
		final InetSocketAddress mca = new InetSocketAddress(NetUtils.getAnyAddress(nic), SystemUtils.MULTICAST_PORT);
		final BaseSystem sys = DistributedAISystem.buildManager(mca, 5);
		
		sys.start();

		try(Scanner in = new Scanner(System.in))
		{
			for(String line ; true ;)
			{
				line = in.nextLine();
				
				if(line.startsWith("q")) break;
			}
		}
		
		sys.stop();
		
		LogBase.INSTANCE.stop();
	}
}
