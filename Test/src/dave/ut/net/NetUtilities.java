package dave.ut.net;

import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;

import org.junit.Before;
import org.junit.Test;

import dave.json.JsonObject;
import dave.json.JsonValue;
import dave.net.NetUtils;
import dave.net.server.PacketedServer;
import dave.net.server.Server;
import dave.util.log.Logger;

public class NetUtilities
{
	private JsonObject json;
	
	@Before
	public void setup( )
	{
		json = new JsonObject();
		JsonObject pos = new JsonObject();
		
		pos.putInt("x", 42);
		pos.putInt("y", 69);
		pos.putInt("z", -1337);
		json.put("pos", pos);
		json.putString("name", "dave");
	}
	
//	private static String hwAddr(byte[] a)
//	{
//		if(a == null)
//			return "-n/a-";
//		
//		StringBuilder sb = new StringBuilder();
//		
//		for(int i = 0 ; i < a.length ; ++i)
//		{
//			if(i > 0) sb.append(':');
//			sb.append(String.format("%02x", a[i]));
//		}
//		
//		return sb.toString();
//	}
//	
//	@Test
//	public void listInterfaces( ) throws SocketException
//	{
//		Enumeration<NetworkInterface> nifs = NetworkInterface.getNetworkInterfaces();
//		Map<String, String> valid = new HashMap<>();
//		
//		for(int i = 0 ; nifs.hasMoreElements() ; ++i)
//		{
//			NetworkInterface nif = nifs.nextElement();
//			String hwa = hwAddr(nif.getHardwareAddress());
//			
//			String id = String.format("%2d: %s %s [%s]", i, nif.getName(), hwa, nif.getDisplayName());
//			
//			if(nif.getHardwareAddress() != null 
//				&& nif.getHardwareAddress().length == 6
//				&& nif.getHardwareAddress()[0] != 0
//				&& !valid.containsKey(hwa))
//			{
//				valid.put(hwa, id);
//			}
//		}
//		
//		for(String nif : valid.values())
//		{
//			LOG.log("%s", nif);
//		}
//	}
	
	@Test
	public void serialization( )
	{
		byte[] data = NetUtils.serialize(json);
		JsonValue o = NetUtils.deserialize(data);
		
		assertEquals(o, json);
	}
	
	@Test
	public void streaming( ) throws IOException
	{
		testStream(false);
	}
	
	@Test
	public void zippedStream( ) throws IOException
	{
		testStream(true);
	}
	
	private PacketedServer server;
	
	@Test
	public void udpServer( ) throws IOException
	{
		final int server_port = 38000;
		final int client_port = 38001;
		
		server = Server.createUDPServer(server_port, datagram -> {
			LOG.log("Server received JSON datagram sized %dB, answering ...", 
					NetUtils.serialize(datagram.payload).length);
			
			server.send(datagram.source, json);
		});
		
		server.start();
		
		sleep(50);
		
		byte[] data = NetUtils.serialize(json);
		
//		System.out.println(PrettyPrinter.convert(packet.save()));
		
		DatagramSocket s = new DatagramSocket(client_port);
		DatagramPacket p = new DatagramPacket(data, data.length);
		
		p.setAddress(InetAddress.getByName(null));
		p.setPort(server_port);
		
		LOG.log("Client sends JSON datagram sized %dB", data.length);
		
		s.send(p);
		
		data = new byte[64 * 1024];
		p = new DatagramPacket(data, data.length);
		s.receive(p);
		
		LOG.log("Client received JSON datagram sized %dB", p.getLength());
		
		JsonValue o = NetUtils.deserialize(data, 0, p.getLength());
		
		s.close();
		server.stop();
		
		assertEquals(o, json);
	}
	
	@Test
	public void tcpServer( ) throws IOException
	{
		Server tcp = Server.createTCPServer(32000, c -> {
			JsonValue json = c.receive();
			
			LOG.log("Server received JSON packet sized %dB, answering ...", NetUtils.serialize(json).length);
			
			c.send(json);
			c.close();
		});
		
		tcp.start();
		
		sleep(100);
		
		Socket c = new Socket(InetAddress.getByName(null), 32000);
		byte[] data = NetUtils.serialize(json);
		
		LOG.log("Client send JSON packet of size %dB", data.length);
		
		NetUtils.send(data, c.getOutputStream(), true);
		
		data = NetUtils.recv(c.getInputStream());
		
		JsonValue o = NetUtils.deserialize(data);
		
		LOG.log("Client received JSON packet of size %dB", data.length);
		
		c.close();
		tcp.stop();
		
		assertEquals(o, json);
	}
	
	private static void sleep(int ms)
	{
		long ts = System.currentTimeMillis();
		
		for(long now = ts ; (now - ts) < ms ; now = System.currentTimeMillis())
		{
			try { Thread.sleep(ms - (now - ts)); } catch(InterruptedException e) { }
		}
	}
	
	private void testStream(boolean zipped) throws IOException
	{
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		byte[] data = NetUtils.serialize(json);

		NetUtils.send(data, out, zipped);
		
		ByteArrayInputStream in = new ByteArrayInputStream(out.toByteArray());
		
		JsonValue o = NetUtils.deserialize(NetUtils.recv(in));
		
		assertEquals(o, json);
	}
	
	private static final Logger LOG = Logger.get("ut-netutils");
}
