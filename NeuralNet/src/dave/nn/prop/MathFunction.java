package dave.nn.prop;

public interface MathFunction
{
	public abstract float apply(float v);
	public abstract float derivative(float v);
	
	public default double apply(double v) { return apply((float) v); }
	public default double derivative(double v) { return derivative((float) v); }
}
