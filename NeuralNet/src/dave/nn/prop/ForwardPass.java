package dave.nn.prop;

import java.util.List;
import java.util.stream.Stream;

import dave.nn.conv.Allocator;
import dave.nn.conv.Buffer;

public class ForwardPass
{
	private final Allocator mAlloc;
	private final List<Buffer> mResults;
	
	public ForwardPass(Allocator a, List<Buffer> r)
	{
		mAlloc = a;
		mResults = r;
	}
	
	public int size() { return mResults.size(); }
	public Buffer get(int i) { return mResults.get(i); }
	public Stream<Buffer> buffers() { return mResults.stream(); }
	public Allocator allocator() { return mAlloc; }
	public float[] result() { return mResults.get(mResults.size() - 1).get(); }
}
