package dave.nn.prop;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import dave.nn.conv.Allocator;
import dave.nn.conv.Buffer;
import dave.util.stream.StreamUtils;

public class WeightGradients
{
	private final Allocator mAlloc;
	private final List<Buffer> mGradients;
	
	public WeightGradients(Allocator a, List<Buffer> g)
	{
		mAlloc = a;
		mGradients = g;
	}
	
	private WeightGradients(WeightGradients d)
	{
		mAlloc = d.mAlloc.clone();
		mGradients = d.mGradients.stream()
			.map(e -> e == null ? null : mAlloc.clone(e))
			.collect(Collectors.toList());
	}
	
	public int size() { return mGradients.size(); }
	public Optional<Buffer> get(int i) { return Optional.ofNullable(mGradients.get(i)); }
	public Stream<Buffer> gradients() { return mGradients.stream(); }
	public Allocator allocator() { return mAlloc; }
	
	public void scale(float rate)
	{
		gradients().filter(b -> b != null).forEach(b -> b.apply(v -> v * rate));
	}
	
	public void add(WeightGradients w)
	{
		if(size() != w.size())
			throw new IllegalArgumentException();
		
		StreamUtils.zip(gradients(), w.gradients())
			.filter(e -> e.first != null || e.second != null)
			.forEach(e -> e.first.add(e.second));
	}
	
	@Override
	public WeightGradients clone()
	{
		return new WeightGradients(this);
	}
	
	public static Collector<WeightGradients, CollectImpl, WeightGradients> collect()
	{
		return Collector.of(
			() -> new CollectImpl(),
			(c, o) -> c.add(o),
			(c1, c2) -> { c1.add(c2.root); c1.count += c2.count - 1; return c1; },
			c -> c.finish());
	}
	
	private static class CollectImpl
	{
		private WeightGradients root = null;
		private int count = 0;
		
		public WeightGradients finish()
		{
			if(count == 0)
				throw new IllegalStateException();
			
			root.scale(1.0f / count);
			
			return root;
		}
		
		public void add(WeightGradients d)
		{
			++count;
			
			if(root == null)
			{
				root = d.clone();
			}
			else
			{
				root.add(d);
			}
		}
	}
}
