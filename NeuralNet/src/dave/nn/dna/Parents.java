package dave.nn.dna;

import dave.json.Container;
import dave.json.JsonArray;
import dave.json.JsonObject;
import dave.json.JsonValue;
import dave.json.Loader;
import dave.json.Saver;
import dave.util.RNG;
import dave.util.Seed;

@Container
public class Parents extends BasicHeritage
{
	private final String[] mParents;
	private final double mChance;
	
	public Parents(Seed s, double c, Prototype proto, String ... p)
	{
		super(s, proto);

		mChance = c;
		mParents = p;
	}
	
	@Override
	public String toString( )
	{
		return "Offspring{" + getID() + "}";
	}
	
	private Parents(JsonValue json)
	{
		super(json);
		
		JsonObject o = (JsonObject) json;
		
		mChance = o.getDouble("chance");
		mParents = o.getArray("parents").asStrings();
	}
	
	@Override
	public String[] getParents( )
	{
		return mParents;
	}

	@Override
	public DNA recreate(RNG random, IStorage<Seed, DNA> db)
	{
		DNA[] p = new DNA[mParents.length];
		int l = getPrototype().getSpace();
		
		for(int i = 0 ; i < p.length ; ++i)
		{
			p[i] = db.get(new Seed(mParents[i]));
			
			if(p[i].getBase().length != l)
				throw new IllegalArgumentException();
		}
		
		float[] d = new float[l];
		Range r = getPrototype().getRange();
		
		random.setSeed(getSeed());
		
		for(int i = 0 ; i < d.length ; ++i)
		{
			double v = random.nextDouble();
			
			if(v <= mChance)
			{
				d[i] = r.scale((float) random.nextDouble());
			}
			else for(int j = 0 ; j < p.length ; ++j)
			{
				if(v <= mChance + (1.0 - mChance) * (j + 1) / p.length)
				{
					d[i] = p[j].getBase()[i];
					break;
				}
			}
		}
		
		return new DNA(this, d);
	}
	
	@Override
	@Saver
	public JsonValue save( )
	{
		JsonObject json = new JsonObject();
		
		saveBase(json);
		
		json.putNumber("chance", mChance);
		json.put("parents", new JsonArray(mParents));
		
		return json;
	}
	
	@Loader
	public static Parents load(JsonValue json)
	{
		return new Parents(json);
	}
}
