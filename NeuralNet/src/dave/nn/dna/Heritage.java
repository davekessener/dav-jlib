package dave.nn.dna;

import dave.json.Saveable;
import dave.util.Identifiable;
import dave.util.RNG;
import dave.util.Seed;

public interface Heritage extends Saveable, Identifiable
{
	public abstract Seed getSeed( );
	public abstract Prototype getPrototype( );
	public abstract String[] getParents( );
	public abstract DNA recreate(RNG random, IStorage<Seed, DNA> db);
}
