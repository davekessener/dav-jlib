package dave.nn.dna;

import java.util.HashMap;
import java.util.Map;

import dave.util.Seed;

public class ImmediateStorage implements IStorage<Seed, DNA>
{
	private final Map<String, DNA> mMap;
	
	public ImmediateStorage(DNA ... dna)
	{
		mMap = new HashMap<>();
		
		for(int i = 0 ; i < dna.length ; ++i)
		{
			mMap.put(dna[i].getID(), dna[i]);
		}
	}

	@Override
	public DNA get(Seed key)
	{
		DNA dna = mMap.get(key.toString());
		
		if(dna == null)
			throw new IllegalArgumentException();
		
		return dna;
	}

	@Override
	public void put(Seed key, DNA value)
	{
		throw new IllegalStateException();
	}
}
