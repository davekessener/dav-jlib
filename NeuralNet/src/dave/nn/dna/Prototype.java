package dave.nn.dna;

import dave.json.Container;
import dave.json.JsonArray;
import dave.json.JsonObject;
import dave.json.JsonValue;
import dave.json.Loader;
import dave.json.Saveable;
import dave.json.Saver;

@Container
public class Prototype implements Saveable
{
	private final int mSize;
	private final Range mRange;
	
	public Prototype(int s, Range r)
	{
		mSize = s;
		mRange = r;
	}
	
	public int getSpace( ) { return mSize; }
	public Range getRange( ) { return mRange; }
	
	@Override
	public int hashCode( )
	{
		return Integer.hashCode(mSize) ^ mRange.hashCode() ^ 0x18362593;
	}
	
	@Override
	public boolean equals(Object o)
	{
		if(o instanceof Prototype)
		{
			Prototype p = (Prototype) o;
			
			return mSize == p.mSize && mRange.equals(p.mRange);
		}
		
		return false;
	}
	
	@Override
	public String toString( )
	{
		return "{DNA-Prototype: " + mSize + ", " + mRange + "}";
	}
	
	@Saver
	@Override
	public JsonValue save( )
	{
		JsonObject json = new JsonObject();
		
		json.putInt("size", mSize);
		json.put("range", new JsonArray(new float[] { mRange.min, mRange.max }));
		
		return json;
	}
	
	@Loader
	public static Prototype load(JsonValue json)
	{
		JsonObject o = (JsonObject) json;
		
		int s = o.getInt("size");
		float[] r = o.getArray("range").asFloats();
		
		return new Prototype(s, new Range(r[0], r[1]));
	}
	
	public static Prototype join(Prototype ... ps)
	{
		Prototype p = ps[0];
		
		for(int i = 1 ; i < ps.length ; ++i)
		{
			if(!p.equals(ps[i]))
				throw new IllegalArgumentException(p.toString());
		}
		
		return p;
	}
}
