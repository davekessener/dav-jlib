package dave.nn.dna;

import dave.json.Container;
import dave.json.JsonObject;
import dave.json.JsonValue;
import dave.json.Loader;
import dave.json.Saver;
import dave.util.RNG;
import dave.util.Seed;

@Container
public class Primordial extends BasicHeritage
{
	public Primordial(Seed s, Prototype p)
	{
		super(s, p);
	}
	
	private Primordial(JsonValue json)
	{
		super(json);
	}
	
	@Override
	public String toString( )
	{
		return "Primordial{" + getID() + "}";
	}

	@Override
	public DNA recreate(RNG random, IStorage<Seed, DNA> db)
	{
		random.setSeed(getSeed());
		
		return new DNA(getPrototype(), random);
	}
	
	@Override
	public String[] getParents( )
	{
		return new String[0];
	}
	
	@Override
	@Saver
	public JsonValue save( )
	{
		JsonObject o = new JsonObject();
		
		saveBase(o);
		
		return o;
	}
	
	@Loader
	public static Primordial load(JsonValue json)
	{
		return new Primordial(json);
	}
}
