package dave.nn.dna;

public class Range
{
	public final float min, max;
	
	public Range(float min, float max)
	{
		this.min = min;
		this.max = max;
	}
	
	public float scale(float v)
	{
		return min + (max - min) * v;
	}
	
	@Override
	public int hashCode( )
	{
		return Double.valueOf(min).hashCode() ^ Double.valueOf(max).hashCode();
	}
	
	@Override
	public boolean equals(Object o)
	{
		if(o instanceof Range)
		{
			return min == ((Range) o).min && max == ((Range) o).max;
		}
		
		return false;
	}
	
	@Override
	public String toString( )
	{
		return String.format("[%g, %g]", min, max);
	}
}
