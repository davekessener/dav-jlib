package dave.nn.dna;

public interface IStorage<K, V>
{
	public abstract V get(K key);
	public abstract void put(K key, V value);
}
