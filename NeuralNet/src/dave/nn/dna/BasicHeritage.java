package dave.nn.dna;

import dave.json.JsonObject;
import dave.json.JsonValue;
import dave.util.Seed;

public abstract class BasicHeritage implements Heritage
{
	private final Seed mSeed;
	private final Prototype mPrototype;

	public BasicHeritage(Seed s, Prototype p)
	{
		mSeed = s;
		mPrototype = p;
	}

	protected BasicHeritage(JsonValue json)
	{
		JsonObject o = (JsonObject) json;
		
		mSeed = new Seed(o.getString("seed"));
		mPrototype = Prototype.load(o.get("prototype"));
	}
	
	@Override
	public Prototype getPrototype( )
	{
		return mPrototype;
	}
	
	@Override
	public String getID( )
	{
		return mSeed.toString();
	}
	
	@Override
	public Seed getSeed( )
	{
		return mSeed;
	}

	protected void saveBase(JsonObject json)
	{
		json.putString("seed", mSeed.toString());
		json.put("prototype", mPrototype.save());
	}
}
