package dave.nn.dna;

import dave.util.Identifiable;
import dave.util.RNG;
import dave.json.Container;
import dave.json.JSON;
import dave.json.JsonArray;
import dave.json.JsonObject;
import dave.json.JsonValue;
import dave.json.Loader;
import dave.json.Saveable;
import dave.json.Saver;

@Container
public class DNA implements Saveable, Identifiable
{
	private final float[] mData;
	private final Heritage mHeritage;
	
	public DNA(Prototype p, RNG random)
	{
		int l = p.getSpace();
		Range r = p.getRange();
		
		mData = new float[l];
		mHeritage = new Primordial(random.getSeed(), p);
		
		for(int i = 0 ; i < mData.length ; ++i)
		{
			mData[i] = r.scale((float) random.nextDouble());
		}
	}
	
	public DNA(Heritage p, float[] d)
	{
		mData = d;
		mHeritage = p;
	}
	
	public static DNA procreate(DNA dad_0, DNA dad_1, RNG random, float c)
	{
		Prototype prototype = Prototype.join(dad_0.mHeritage.getPrototype(), dad_1.mHeritage.getPrototype());
		Heritage ancestry = new Parents(random.getSeed(), c, prototype, dad_0.getID(), dad_1.getID());
		
		return ancestry.recreate(random, new ImmediateStorage(dad_0, dad_1));
	}
	
	@Override
	public String getID( )
	{
		return mHeritage.getSeed().toString();
	}
	
	public Heritage getHeritage( ) { return mHeritage; }
	public float[] getBase( ) { return mData; }
	
	@Override
	@Saver
	public JsonValue save( )
	{
		JsonObject json = new JsonObject();
		
		json.put("heritage", JSON.serialize(mHeritage));
		json.put("data", new JsonArray(mData));
		
		return json;
	}
	
	@Loader
	public static DNA load(JsonValue json)
	{
		JsonObject o = (JsonObject) json;
		
		Heritage h = (Heritage) JSON.deserialize(o.get("heritage"));
		float[] d = o.getArray("data").asFloats();
		
		return new DNA(h, d);
	}
	
	@Override
	public int hashCode( )
	{
		return getID().hashCode() ^ 0x83264931;
	}
	
	@Override
	public boolean equals(Object o)
	{
		if(o instanceof DNA)
		{
			return getID().equals(((DNA) o).getID());
		}
		
		return false;
	}
	
	@Override
	public String toString( )
	{
		return "DNA-" + getID();
	}
}
