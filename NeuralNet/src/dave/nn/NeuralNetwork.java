package dave.nn;

import dave.nn.prop.ForwardPass;
import dave.nn.prop.MathFunction;
import dave.nn.prop.WeightGradients;

public interface NeuralNetwork
{
	public default float[] calculate(float[] a) { return forward(a).result(); }
	
	public abstract ForwardPass forward(float[] a);
	public abstract WeightGradients backward(ForwardPass f, float[] l);
	public abstract void adjust(WeightGradients w);
}
