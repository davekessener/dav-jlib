package dave.nn.test;

import java.util.stream.Collectors;

import org.junit.Assert;
import org.junit.Test;

import dave.nn.builder.ConvolutionLayerTemplate;
import dave.nn.builder.FeedForwardLayerTemplate;
import dave.nn.builder.LayerTemplate;
import dave.nn.builder.NetworkTemplate;
import dave.nn.builder.RectifyLayerTemplate;
import dave.nn.conv.Allocator;
import dave.nn.conv.BasicAllocator;
import dave.nn.conv.Buffer;
import dave.nn.conv.ConvolutionLayer;
import dave.nn.conv.FeedForwardFilter;
import dave.nn.conv.FeedForwardLayer;
import dave.nn.conv.Filter;
import dave.nn.conv.ImmediateBuffer;
import dave.nn.conv.Layer;
import dave.nn.conv.Matrix;
import dave.nn.conv.Network;
import dave.nn.conv.SigmoidRect;
import dave.nn.conv.TanhRect;
import dave.nn.prop.ForwardPass;
import dave.nn.prop.WeightGradients;
import dave.util.math.IVec3;
import dave.util.stream.StreamUtils;

public class BackpropUT
{
	@Test
	public void testFeedForward()
	{
		for(int i = 0 ; i < 10000 ; ++i)
		{
			testLayer(10, new FeedForwardLayer(new IVec3(10, 1, 1), new Matrix(getRandomChunk(100), 10, 10), new ImmediateBuffer(new float[10])));
		}
	}
	
	@Test
	public void testConvolution()
	{
		for(int i = 0 ; i < 10000 ; ++i)
		{
			Filter ff = new FeedForwardFilter(getRandomChunk(5 * 5 + 1));
			
			testLayer(100, new ConvolutionLayer(new IVec3(10, 10, 1), new Filter[] { ff }, 1));
		}
	}

	private void testLayer(int cin, Layer l)
	{
		Buffer in = new ImmediateBuffer(new float[cin]);
		Buffer out = new ImmediateBuffer(new float[l.dimension().volume()]);
		Buffer target = new ImmediateBuffer(new float[out.size()]);
		Buffer tmp = new ImmediateBuffer(new float[in.size()]);
		
		randomize(in);
		randomize(target);
		
		l.forward(in, out);
		
		Loss run_0 = new Loss(target, out);

		float[] delta = l.backward(in, run_0.loss, tmp);
		
		for(int i = 0 ; i < delta.length ; ++i)
		{
			delta[i] *= -0.0001f;
		}
		
		l.adjust(new ImmediateBuffer(delta));

		l.forward(in, out);
		
		Loss run_1 = new Loss(target, out);
		
		Assert.assertTrue(run_0.total > run_1.total);
	}
	
	public static final NetworkTemplate TEMPLATE = new NetworkTemplate(
		new IVec3(15, 15, 1),
		new LayerTemplate[] {
			new ConvolutionLayerTemplate(2, 5),
			new RectifyLayerTemplate(new SigmoidRect()),
//
			new ConvolutionLayerTemplate(2, 3),
			new RectifyLayerTemplate(new SigmoidRect()),

			new ConvolutionLayerTemplate(1, 2),
			new RectifyLayerTemplate(new SigmoidRect()),

//			new FeedForwardLayerTemplate(10),
//			new RectifyLayerTemplate(new TanhRect()),
//
//			new FeedForwardLayerTemplate(10),
//			new RectifyLayerTemplate(new TanhRect()),

			new FeedForwardLayerTemplate(10),
			new RectifyLayerTemplate(new SigmoidRect())
	});
	
	@Test
	public void testNetwork()
	{
		for(int i = 0 ; i < 100 ; ++i)
		{
			BasicAllocator a = new BasicAllocator();
			Network nn = TEMPLATE.generate(a);
			
			for(int j = 0 ; j < a.size() ; ++j)
			{
				a.set(j, (float) (Math.random() * 2 - 1) * 0.25f);
			}
			
			int c = testNN(TEMPLATE.getInputDimension().volume(), 10, nn);
			
			p("%d: %d", i+1, c);
		}
	}
	
	private int testNN(int cin, int cout, Network nn)
	{
		Buffer in = new ImmediateBuffer(new float[cin]);
		Buffer target = new ImmediateBuffer(new float[cout]);
		
		randomize(in);
		
		for(int i = 0 ; i < target.size() ; ++i)
		{
			target.set(i, 0.1f);
		}
		
		target.set((int) (Math.random() * (target.size() - 1)), 0.9f);
		
		Loss run_0 = null, run_1 = null;
		
		int c = 0;
		
		do
		{
			ForwardPass forward = nn.forward(in.get());
			
			run_1 = new Loss(target, new ImmediateBuffer(forward.result()));
			
			WeightGradients delta = nn.backward(forward, run_1.loss.get());
			
			delta.scale(-0.01f);
			
			nn.adjust(delta);
			
			if(c > 100000)
			{
				p("%.5f: %.9f", run_1.total, delta.gradients()
					.filter(b -> b != null)
					.mapToDouble(b -> StreamUtils.stream(b.get())
						.average().getAsDouble())
					.average().getAsDouble());
				return c;
			}
//			
//			p(forward.result());
//			p(target.get());
//			p(run_1.loss.get());
			
			if(run_0 != null && run_1 != null)
			{
//				Assert.assertTrue(run_0.total > run_1.total);
			}
			
			run_0 = run_1;
			++c;
		}
		while(run_1.total > 0.1);
		
		return c;
	}
	
	private static void p(float[] v)
	{
		p("[%s]", StreamUtils.stream(v).mapToObj(e -> String.format("%.6f", e)).collect(Collectors.joining(", ")));
	}
	
	@SuppressWarnings("unused")
	private static void p(String f, Object ... o)
	{
		System.out.println(String.format(f, o));
	}
	
	private static class Loss
	{
		public final float total;
		public final Buffer loss;
		
		public Loss(Buffer expected, Buffer result)
		{
			int l = result.size();
			loss = new ImmediateBuffer(new float[l]);
			
			float t = 0;
			for(int i = 0 ; i < l ; ++i)
			{
				float d = result.get(i) - expected.get(i);
				
				t += d * d;
				loss.set(i, d);
			}
			t *= 0.5;
			
			total = t;
		}
	}
	
	private static Buffer getRandomChunk(int l)
	{
		Allocator a = new BasicAllocator();
		Buffer b = a.allocate(l);
		
		randomize(b);
		
		return b;
	}
	
	private static void randomize(Buffer b)
	{
		b.apply(x -> (Math.random() * 2 - 1));
	}
}
