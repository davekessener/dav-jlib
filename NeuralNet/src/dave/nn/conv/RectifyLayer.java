package dave.nn.conv;

import dave.json.JsonObject;
import dave.json.JsonValue;
import dave.util.math.IVec3;

public class RectifyLayer extends Layer
{
	private final IVec3 mDim;
	private final Rectifier mF;
	
	public RectifyLayer(IVec3 in, Rectifier f)
	{
		mDim = in;
		mF = f;
		
		if(in == null || f == null)
			throw new NullPointerException();
	}

	@Override
	public IVec3 dimension()
	{
		return mDim;
	}

	@Override
	public Layer.Type type()
	{
		return Layer.Type.RECT;
	}

	@Override
	public void forward(Buffer in, Buffer out)
	{
		int l = in.size();
		
		if(l != out.size() || l != mDim.volume() || l != out.size())
			throw new IllegalArgumentException();
		
		for(int i = 0 ; i < l ; ++i)
		{
			out.set(i, mF.apply(in.get(i)));
		}
	}

	@Override
	public float[] backward(Buffer result, Buffer in, Buffer out)
	{
		int l = result.size();
		
		if(l != in.size() || l != out.size())
			throw new IllegalArgumentException();
		
		for(int i = 0 ; i < l ; ++i)
		{
			out.set(i, in.get(i) * mF.derivative(result.get(i)));
		}
		
		return null;
	}
	
	@Override
	public void adjust(Buffer w)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public Layer clone(Allocator a)
	{
		return new RectifyLayer(mDim.clone(), mF);
	}

	@Override
	protected void doSave(JsonObject json)
	{
		json.put("input", mDim.save());
		json.putString("fun", mF.type().toString());
	}
	
	protected static RectifyLayer doLoad(JsonValue json, Allocator a)
	{
		JsonObject o = (JsonObject) json;
		
		IVec3 dim = IVec3.load(o.get("input"));
		Rectifier f = Rectifier.generate(Rectifier.Type.valueOf(o.getString("fun")));
		
		return new RectifyLayer(dim, f);
	}
}
