package dave.nn.conv;

import dave.nn.prop.MathFunction;

public interface Rectifier extends MathFunction
{
	public abstract float apply(float v);
	public abstract Type type( );
	
	public static Rectifier generate(Type t)
	{
		switch(t)
		{
			case SIGMOID:
				return new SigmoidRect();
				
			case TANH:
				return new TanhRect();
				
			case RELU:
				return new MaxRect();
		}
		
		throw new IllegalArgumentException();
	}
	
	public static enum Type
	{
		SIGMOID,
		TANH,
		RELU
	}
}
