package dave.nn.conv;

import dave.util.math.IVec3;

public class PoolingLayer extends ConvolutionLayer
{
	public PoolingLayer(IVec3 in, Pool f)
	{
		super(in, new Filter[] { f }, f.dimension());
	}
	
	@Override
	public float[] backward(Buffer results, Buffer in , Buffer out)
	{
		throw new UnsupportedOperationException();
	}
	
	@Override
	public Type type( )
	{
		return Type.POOL;
	}
}
