package dave.nn.conv;

import dave.json.JsonArray;
import dave.json.JsonValue;

public class Matrix implements Node
{
	private final Buffer mData;
	private final int mW, mH;
	
	public Matrix(Buffer b, int w, int h)
	{
		mData = b;
		mW = w;
		mH = h;
		
		if(b.size() != w * h)
			throw new IllegalArgumentException();
	}
	
	public int getWidth( ) { return mW; }
	public int getHeight( ) { return mH; }
	
	public float get(int x, int y)
	{
		return mData.get(x + y * mW);
	}
	
	public void set(int x, int y, float v)
	{
		mData.set(x + y * mW, v);
	}
	
	public void calc(Buffer in, Buffer out)
	{
		if(in.size() != mW || out.size() != mH)
			throw new IllegalArgumentException();
		
		for(int y = 0 ; y < mH ; ++y)
		{
			float r = 0.0f;
			
			for(int x = 0 ; x < mW ; ++x)
			{
				r += mData.get(x + y * mW) * in.get(x);
			}
			
			out.set(y, r);
		}
	}

	@Override
	public Matrix clone(Allocator a)
	{
		return new Matrix(a.clone(mData), mW, mH);
	}

	@Override
	public JsonValue save()
	{
		JsonArray v = new JsonArray();
		
		for(int y = 0 ; y < mH ; ++y)
		{
			JsonArray row = new JsonArray();
			
			for(int x = 0 ; x < mW ; ++x)
			{
				row.addNumber(mData.get(x + y * mW));
			}
			
			v.add(row);
		}
		
		return v;
	}
	
	public static Matrix load(JsonValue json, Allocator a)
	{
		JsonArray rows = (JsonArray) json;
		Buffer b = null;
		int w = 0, h = rows.size(), y = 0;
		
		for(JsonValue v : rows)
		{
			float[] t = ((JsonArray) v).asFloats();
			
			if(b == null)
			{
				w = t.length;
				b = a.allocate(w * h);
			}
			
			if(t.length != w)
				throw new IllegalArgumentException();
			
			for(int x = 0 ; x < w ; ++x)
			{
				b.set(x + y * w, t[x]);
			}
			
			++y;
		}
		
		return new Matrix(b, w, h);
	}
}
