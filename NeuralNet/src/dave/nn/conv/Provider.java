package dave.nn.conv;

public interface Provider
{
	public abstract float[] get( );
}
