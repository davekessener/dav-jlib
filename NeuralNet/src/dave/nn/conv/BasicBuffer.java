package dave.nn.conv;

import java.util.Arrays;

public class BasicBuffer implements Buffer
{
	private final int mIdx, mLen;
	private final Provider mData;
	
	public BasicBuffer(Provider p, int i, int l)
	{
		mIdx = i;
		mLen = l;
		mData = p;
	}
	
	public BasicBuffer section(int o, int l)
	{
		if(o < 0 || l <= 0 || o + l > mLen)
			throw new IllegalArgumentException();
		
		return new BasicBuffer(mData, mIdx + o, l);
	}
	
	@Override
	public BasicBuffer clone(Provider p)
	{
		return new BasicBuffer(p, mIdx, mLen);
	}

	@Override
	public float get(int idx)
	{
		if(idx >= mLen)
			throw new ArrayIndexOutOfBoundsException(String.format("Element %d exceeds buffer sized %d!", idx, mLen));
		
		return mData.get()[mIdx + idx];
	}

	@Override
	public void set(int idx, float v)
	{
		if(idx < 0 || idx >= mLen)
			throw new ArrayIndexOutOfBoundsException();
		
		if(!Float.isFinite(v))
			throw new IllegalArgumentException();
		
		mData.get()[mIdx + idx] = v;
	}

	@Override
	public int size()
	{
		return mLen;
	}

	@Override
	public float[] get()
	{
		return Arrays.copyOfRange(mData.get(), mIdx, mIdx + mLen);
	}
}
