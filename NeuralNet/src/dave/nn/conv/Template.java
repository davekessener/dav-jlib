package dave.nn.conv;

public interface Template<T>
{
	public abstract int inputs( );
	public abstract T generate(Buffer in);
}
