package dave.nn.conv;

public class SigmoidRect implements Rectifier
{
	@Override
	public float apply(float x)
	{
		return (float) (1 / (1 + Math.exp(-x)));
	}

	@Override
	public float derivative(float x)
	{
		float t = apply(x);
		
		return t * (1 - t);
	}

	@Override
	public Type type()
	{
		return Type.SIGMOID;
	}
}
