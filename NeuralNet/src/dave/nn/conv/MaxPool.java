package dave.nn.conv;

import dave.json.JsonObject;
import dave.json.JsonValue;

public class MaxPool extends Pool
{
	public MaxPool(int l) { super(l); }
	
	@Override
	public float forward(Buffer in)
	{
		if(in.size() != dimension() * dimension())
			throw new IllegalArgumentException();
		
		float r = Float.NEGATIVE_INFINITY;
		
		for(int i = 0 ; i < in.size() ; ++i)
		{
			r = Math.max(r, in.get(i));
		}
		
		return r;
	}

	@Override
	public Type type()
	{
		return Type.POOL_MAX;
	}

	@Override
	public MaxPool clone(Allocator a)
	{
		return new MaxPool(dimension());
	}

	@Override
	protected void doSave(JsonObject json)
	{
		json.putInt("dim", dimension());
	}
	
	protected static MaxPool doLoad(JsonValue json, Allocator a)
	{
		JsonObject o = (JsonObject) json;
		
		return new MaxPool(o.getInt("dim"));
	}
}
