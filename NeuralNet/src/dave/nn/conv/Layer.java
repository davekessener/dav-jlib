package dave.nn.conv;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import dave.json.JsonObject;
import dave.json.JsonValue;
import dave.util.math.IVec3;

public abstract class Layer implements Node
{
	public abstract IVec3 dimension( );
	public abstract void forward(Buffer in, Buffer out);
	public abstract float[] backward(Buffer result, Buffer in, Buffer out);
	public abstract void adjust(Buffer w);
	
	public abstract Type type( );
	public abstract Layer clone(Allocator a);
	protected abstract void doSave(JsonObject json);

	@Override
	public final JsonValue save( )
	{
		JsonObject json = new JsonObject();
		
		doSave(json);
		json.putString("type", type().toString());
		
		return json;
	}
	
	public static Layer load(JsonValue json, Allocator a)
	{
		JsonObject o = (JsonObject) json;
		Class<? extends Layer> c = null;
		
		switch(Type.valueOf(o.getString("type")))
		{
			case CONV: c = ConvolutionLayer.class; break;
			case REDUCE: c = ReductionLayer.class; break;
			case POOL: c = ConvolutionLayer.class; break;
			case RECT: c = RectifyLayer.class; break;
			case FORWARD: c = FeedForwardLayer.class; break;
		}
		
		try
		{
			Method m = c.getDeclaredMethod("doLoad", JsonValue.class, Allocator.class);
			
			m.setAccessible(true);
			
			return (Layer) m.invoke(null, json, a);
		}
		catch ( IllegalAccessException
			  | IllegalArgumentException
			  | InvocationTargetException
			  | NoSuchMethodException
			  | SecurityException e)
		{
			throw new IllegalArgumentException(e);
		}
	}
	
	public static enum Type
	{
		CONV,
		REDUCE,
		POOL,
		RECT,
		FORWARD
	}
}
