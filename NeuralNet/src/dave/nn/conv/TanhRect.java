package dave.nn.conv;

public class TanhRect implements Rectifier
{
	@Override
	public float apply(float v)
	{
		return (float) Math.tanh(v);
	}

	@Override
	public float derivative(float v)
	{
		float t = apply(v);
		
		return 1 - t * t;
	}

	@Override
	public Type type()
	{
		return Type.SIGMOID;
	}
}
