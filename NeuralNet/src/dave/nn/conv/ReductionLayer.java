package dave.nn.conv;

import dave.json.JsonObject;
import dave.json.JsonValue;
import dave.util.math.IVec3;

public class ReductionLayer extends Layer
{
	private final IVec3 mDim;
	private final Matrix mF;
	
	public ReductionLayer(IVec3 in, Matrix f)
	{
		if(in.z != f.getWidth())
			throw new IllegalArgumentException();
		
		mDim = new IVec3(in.x, in.y, f.getHeight());
		mF = f;
	}
	
	private ReductionLayer(Matrix f, IVec3 dim)
	{
		mDim = dim;
		mF = f;
	}

	@Override
	public void forward(Buffer in, Buffer out)
	{
		float[] t_in = new float[mF.getWidth()];
		float[] t_out = new float[mF.getHeight()];
		
		for(int y = 0 ; y < mDim.y ; ++y)
		{
			for(int x = 0 ; x < mDim.x ; ++x)
			{
				for(int z = 0 ; z < t_in.length ; ++z)
				{
					t_in[z] = in.get(x + (y + z * mDim.y) * mDim.x);
				}
				
				mF.calc(new ImmediateBuffer(t_in), new ImmediateBuffer(t_out));
				
				for(int z = 0 ; z < t_out.length ; ++z)
				{
					out.set(x + (y + z * mDim.y) * mDim.x, t_out[z]);
				}
			}
		}
	}

	@Override
	public float[] backward(Buffer result, Buffer in, Buffer out)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public void adjust(Buffer w)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public IVec3 dimension()
	{
		return mDim;
	}
	
	@Override
	public Layer.Type type( )
	{
		return Layer.Type.REDUCE;
	}

	@Override
	public Layer clone(Allocator a)
	{
		return new ReductionLayer(mDim.clone(), mF.clone(a));
	}

	@Override
	protected void doSave(JsonObject json)
	{
		json.put("input", mDim.save());
		json.put("weights", mF.save());
	}
	
	protected static ReductionLayer doLoad(JsonValue json, Allocator a)
	{
		JsonObject o = (JsonObject) json;
		
		IVec3 dim = IVec3.load(o.get("input"));
		Matrix f = Matrix.load(o.get("weights"), a);
		
		return new ReductionLayer(f, dim);
	}
}
