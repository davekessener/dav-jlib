package dave.nn.conv;

import java.util.Arrays;

public class ImmediateBuffer implements Buffer
{
	private final float[] mBuf;
	
	public ImmediateBuffer(float[] b)
	{
		mBuf = b;
	}

	@Override
	public float get(int idx)
	{
		return mBuf[idx];
	}

	@Override
	public void set(int idx, float v)
	{
		if(!Float.isFinite(v))
			throw new IllegalArgumentException();
		
		mBuf[idx] = v;
	}

	@Override
	public int size()
	{
		return mBuf.length;
	}

	@Override
	public ImmediateBuffer clone(Provider p)
	{
		return new ImmediateBuffer(mBuf);
	}

	@Override
	public float[] get()
	{
		return Arrays.copyOf(mBuf, mBuf.length);
	}
}
