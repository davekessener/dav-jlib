package dave.nn.conv;

import dave.nn.NeuralNetwork;
import dave.nn.prop.ForwardPass;
import dave.nn.prop.WeightGradients;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.IntStream;

import dave.json.JsonArray;
import dave.json.JsonObject;
import dave.json.JsonValue;
import dave.json.Saveable;
import dave.util.math.IVec3;

public class Network implements NeuralNetwork, Saveable
{
	private final Layer[] mLayers;
	private final IVec3 mInputDim;
	
	public Network(IVec3 dim, Layer[] l)
	{
		mLayers = l;
		mInputDim = dim;
	}
	
	@Override
	public ForwardPass forward(float[] data)
	{
		if(data.length != mInputDim.volume())
			throw new IllegalArgumentException("Data sized " + data.length + ", expected " + mInputDim);
		
		Allocator a = new BasicAllocator();
		List<Buffer> results = new ArrayList<>(mLayers.length);
		
		Buffer in = a.allocate(data);
		
		results.add(in);
		for(int i = 0 ; i < mLayers.length ; ++i)
		{
			Buffer out = a.allocate(mLayers[i].dimension().volume());
			
			mLayers[i].forward(in, out);
			
			results.add(in = out);
		}
		
		return new ForwardPass(a, results);
	}
	
	@Override
	public WeightGradients backward(ForwardPass forward, float[] loss)
	{
		Allocator g_a = new BasicAllocator();
		Allocator b_a = new BasicAllocator();
		List<Buffer> g = new LinkedList<>();
		Buffer in = b_a.allocate(loss);
		
		for(int i = mLayers.length - 1 ; i >= 0 ; --i)
		{
			Buffer results = forward.get(i);
			Buffer out = b_a.allocate(results.size());
			float[] t = mLayers[i].backward(results, in, out);
			
			g.add(0, t == null ? null : g_a.allocate(t));
			
			in = out;
		}
		
		return new WeightGradients(g_a, new ArrayList<>(g));
	}
	
	@Override
	public void adjust(WeightGradients w)
	{
		IntStream.range(0, mLayers.length)
			.forEach(i -> w.get(i).ifPresent(b -> mLayers[i].adjust(b)));
	}
	
	@Override
	public JsonValue save()
	{
		JsonObject json = new JsonObject();
		JsonArray l = new JsonArray();
		
		for(int i = 0 ; i < mLayers.length ; ++i)
		{
			l.add(mLayers[i].save());
		}
		
		json.put("input", mInputDim.save());
		json.put("layers", l);
		
		return json;
	}
	
	public static Network load(JsonValue json, Allocator a)
	{
		JsonObject o = (JsonObject) json;
		JsonArray l = o.getArray("layers");
		Layer[] ls = new Layer[l.size()];
		
		for(int i = 0 ; i < ls.length ; ++i)
		{
			ls[i] = Layer.load(l.get(i), a);
		}
		
		IVec3 in = IVec3.load(o.get("input"));
		
		return new Network(in, ls);
	}
}
