package dave.nn.conv;

import java.util.function.DoubleUnaryOperator;

public interface Buffer
{
	public abstract float get(int idx);
	public abstract void set(int idx, float v);
	public abstract int size( );
	public abstract Buffer clone(Provider p);
	public abstract float[] get( );
	
	public default void apply(DoubleUnaryOperator f)
	{
		for(int i = 0 ; i < size() ; ++i)
		{
			set(i, (float) f.applyAsDouble(get(i)));
		}
	}
	
	public default void add(Buffer b)
	{
		if(size() != b.size())
			throw new IllegalArgumentException("Expected buffer sized " + size() + ", got " + b.size());
		
		for(int i = 0 ; i < size() ; ++i)
		{
			set(i, get(i) + b.get(i));
		}
	}
}
