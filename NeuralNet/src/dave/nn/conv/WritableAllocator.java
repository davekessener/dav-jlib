package dave.nn.conv;

public interface WritableAllocator extends Allocator
{
	public abstract void setContent(float[] buf);
}
