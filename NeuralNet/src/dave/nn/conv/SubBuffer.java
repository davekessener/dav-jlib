package dave.nn.conv;

public class SubBuffer implements Buffer
{
	private final Buffer mSuper;
	private final int mOffset, mSize;
	
	public SubBuffer(Buffer s, int i0, int i1)
	{
		mSuper = s;
		mOffset = i0;
		mSize = i1 - i0;
	}

	@Override
	public float get(int idx)
	{
		if(idx < 0 || idx >= mSize)
			throw new ArrayIndexOutOfBoundsException();
		
		return mSuper.get(mOffset + idx);
	}

	@Override
	public void set(int idx, float v)
	{
		if(idx < 0 || idx >= mSize)
			throw new ArrayIndexOutOfBoundsException();
		
		mSuper.set(mOffset + idx, v);
	}

	@Override
	public int size()
	{
		return mSize;
	}

	@Override
	public Buffer clone(Provider p)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public float[] get()
	{
		float[] r = new float[mSize];
		
		for(int i = 0 ; i < mSize ; ++i)
		{
			r[i] = mSuper.get(mOffset + i);
		}
		
		return r;
	}
}
