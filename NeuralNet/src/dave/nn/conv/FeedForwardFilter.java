package dave.nn.conv;

import dave.util.Utils;
import dave.json.JsonObject;
import dave.json.JsonValue;

public class FeedForwardFilter extends Filter
{
	private final Matrix mF;
	private final Buffer mBuffer;
	private final int mSize;
	
	public FeedForwardFilter(Buffer b)
	{
		mBuffer = new SubBuffer(b, b.size() - 1, b.size());
		mF = new Matrix(new SubBuffer(b, 0, b.size() - 1), b.size() - 1, 1);
		mSize = Utils.i_sqrt(mF.getWidth());
		
		if(mSize * mSize + 1 != b.size())
			throw new IllegalArgumentException();
	}
	
	private FeedForwardFilter(Matrix m, Buffer b)
	{
		mBuffer = b;
		mF = m;
		mSize = Utils.i_sqrt(mF.getWidth());
	}
	
	@Override
	public int dimension()
	{
		return mSize;
	}

	@Override
	public float forward(Buffer in)
	{
		if(in.size() != mF.getWidth())
			throw new IllegalArgumentException();
		
		float[] r = new float[1];
		
		mF.calc(in, new ImmediateBuffer(r));
		
		return r[0] + mBuffer.get(0);
	}
	
	@Override
	public float[] backward(float hidden)
	{
		float[] r = new float[mSize * mSize];
		
		for(int i = 0 ; i < mSize * mSize ; ++i)
		{
			r[i] = mF.get(i, 0) * hidden;
		}
		
		return r;
	}
	
	@Override
	public void adjust(Buffer w)
	{
		for(int i = 0 ; i < mSize * mSize ; ++i)
		{
			mF.set(i, 0, mF.get(i, 0) + w.get(i));
		}
		
		mBuffer.set(0, mBuffer.get(0) + w.get(mSize * mSize));
	}

	@Override
	protected void doSave(JsonObject json)
	{
		json.put("weights", mF.save());
		json.putNumber("bias", mBuffer.get(0));
	}

	@Override
	public Type type()
	{
		return Type.FORWARD;
	}

	@Override
	public FeedForwardFilter clone(Allocator a)
	{
		return new FeedForwardFilter(mF.clone(a), a.clone(mBuffer));
	}

	protected static FeedForwardFilter doLoad(JsonValue json, Allocator a)
	{
		JsonObject o = (JsonObject) json;
		
		float b = o.getFloat("bias");
		
		return new FeedForwardFilter(Matrix.load(o.get("weights"), a), a.allocate(new float[] { b }));
	}
}
