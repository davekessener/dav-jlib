package dave.nn.conv;

import java.lang.reflect.InvocationTargetException;

import dave.json.JsonObject;
import dave.json.JsonValue;

public abstract class Filter implements Node
{
	public abstract float forward(Buffer in);
	public abstract float[] backward(float hidden);
	public abstract void adjust(Buffer w);
	public abstract int dimension( );
	
	public abstract Filter clone(Allocator a);
	public abstract Type type( );
	protected abstract void doSave(JsonObject json);
	
	@Override
	public final JsonValue save( )
	{
		JsonObject json = new JsonObject();
		
		doSave(json);
		json.putString("type", type().toString());
		
		return json;
	}
	
	public static Filter load(JsonValue v, Allocator a)
	{
		JsonObject o = (JsonObject) v;
		Class<? extends Filter> c = null;
		
		switch(Type.valueOf(o.getString("type")))
		{
			case FORWARD: c = FeedForwardFilter.class; break;
			case POOL_AVG: c = AveragePool.class; break;
			case POOL_MAX: c = MaxPool.class; break;
		}
		
		try
		{
			return (Filter) c.getDeclaredMethod("doLoad", JsonValue.class, Allocator.class).invoke(null, o, a);
		}
		catch ( IllegalAccessException
			  | IllegalArgumentException
			  | InvocationTargetException
			  | NoSuchMethodException
			  | SecurityException e)
		{
			throw new IllegalArgumentException(e);
		}
	}
	
	public static enum Type
	{
		FORWARD,
		POOL_AVG,
		POOL_MAX
	}
}
