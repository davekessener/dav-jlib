package dave.nn.conv;

import dave.json.Saveable;

public interface Node extends Saveable
{
	public abstract Node clone(Allocator a);
}
