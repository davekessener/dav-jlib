package dave.nn.conv;

public class MaxRect implements Rectifier
{
	@Override
	public float apply(float v)
	{
		return Math.max(0.0f, v);
	}

	@Override
	public float derivative(float v)
	{
		return v > 0 ? 1 : 0;
	}

	@Override
	public Type type()
	{
		return Type.RELU;
	}
}
