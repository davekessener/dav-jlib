package dave.nn.conv;

public interface Allocator extends Cloneable
{
	public abstract Buffer allocate(int l);
	public abstract Allocator clone( );
	public abstract Buffer clone(Buffer b);
	
	public default Buffer allocate(float[] a)
	{
		Buffer b = allocate(a.length);
		
		for(int i = 0 ; i < a.length ; ++i)
		{
			b.set(i, a[i]);
		}
		
		return b;
	}
}
