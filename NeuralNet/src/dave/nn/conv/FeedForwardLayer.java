package dave.nn.conv;

import dave.json.JsonArray;
import dave.json.JsonObject;
import dave.json.JsonValue;
import dave.util.math.IVec3;

public class FeedForwardLayer extends Layer
{
	private final IVec3 mDim;
	private final Matrix mWeights;
	private final Buffer mBias;
	
	public FeedForwardLayer(IVec3 in, Matrix w, Buffer bias)
	{
		mDim = new IVec3(w.getHeight(), 1, 1);
		mWeights = w;
		mBias = bias;
		
		if(bias.size() != w.getHeight())
			throw new IllegalArgumentException();
		
		if(in.volume() != w.getWidth())
			throw new IllegalArgumentException();
	}
	
	private FeedForwardLayer(Matrix w, Buffer bias)
	{
		mDim = new IVec3(w.getHeight(), 1, 1);
		mWeights = w;
		mBias = bias;
	}

	@Override
	public IVec3 dimension()
	{
		return mDim;
	}

	@Override
	public Type type()
	{
		return Layer.Type.FORWARD;
	}

	@Override
	public void forward(Buffer in, Buffer out)
	{
		if(out.size() != mBias.size())
			throw new IllegalStateException();
		
		mWeights.calc(in, out);
		
		for(int i = 0 ; i < out.size() ; ++i)
		{
			out.set(i, out.get(i) + mBias.get(i));
		}
	}
	
	@Override
	public float[] backward(Buffer result, Buffer in, Buffer out)
	{
		int w = mWeights.getWidth(), h = mWeights.getHeight();
		float[] r = new float[(w+1) * h];
		
		for(int x = 0 ; x < w ; ++x)
		{
			float e = 0;
			
			for(int y = 0 ; y < h ; ++y)
			{
				e += mWeights.get(x, y) * in.get(y);

				r[x + y * (w+1)] = in.get(y) * result.get(x);
			}
			
			out.set(x, e);
		}
		
		for(int y = 0 ; y < h ; ++y)
		{
			r[w + y * (w+1)] = in.get(y);
		}
		
		return r;
	}
	
	@Override
	public void adjust(Buffer g)
	{
		int w = mWeights.getWidth(), h = mWeights.getHeight();
		
		for(int y = 0 ; y < h ; ++y)
		{
			for(int x = 0 ; x < w ; ++x)
			{
				mWeights.set(x, y, mWeights.get(x, y) + g.get(x + y * (w+1)));
			}
			
			mBias.set(y, mBias.get(y) + g.get(w + y * (w+1)));
		}
	}

	@Override
	public Layer clone(Allocator a)
	{
		return new FeedForwardLayer(mWeights.clone(a), a.clone(mBias));
	}

	@Override
	protected void doSave(JsonObject json)
	{
		json.put("weights", mWeights.save());
		json.put("biases", new JsonArray(mBias.get()));
	}
	
	protected static FeedForwardLayer doLoad(JsonValue json, Allocator a)
	{
		JsonObject o = (JsonObject) json;
		
		Matrix w = Matrix.load(o.get("weights"), a);
		float[] b = o.getArray("biases").asFloats();
		
		return new FeedForwardLayer(w, a.allocate(b));
	}
}
