package dave.nn.conv;

import dave.json.JsonObject;
import dave.json.JsonValue;

public class AveragePool extends Pool
{
	public AveragePool(int l) { super(l); }
	
	@Override
	public float forward(Buffer in)
	{
		if(in.size() != dimension() * dimension())
			throw new IllegalArgumentException();
		
		float r = 0.0f;
		
		for(int i = 0 ; i < in.size() ; ++i)
		{
			r += in.get(i);
		}
		
		r /= in.size();
		
		return r;
	}

	@Override
	public Type type()
	{
		return Type.POOL_AVG;
	}

	@Override
	public AveragePool clone(Allocator a)
	{
		return new AveragePool(dimension());
	}

	@Override
	protected void doSave(JsonObject json)
	{
		json.putInt("dim", dimension());
	}
	
	protected static AveragePool doLoad(JsonValue json, Allocator a)
	{
		JsonObject o = (JsonObject) json;
		
		return new AveragePool(o.getInt("dim"));
	}
}
