package dave.nn.conv;

import dave.json.JsonArray;
import dave.json.JsonObject;
import dave.json.JsonValue;
import dave.util.math.IVec3;

public class ConvolutionLayer extends Layer
{
	private final Filter[] mFilters;
	private final IVec3 mInDim, mOutDim;
	private final int mStride, mSize;
	
	public ConvolutionLayer(IVec3 in, Filter[] f, int stride)
	{
		mFilters = f;
		mStride = stride;
		
		int s = f[0].dimension();
		
		for(int i = 0 ; i < f.length ; ++i)
		{
			if(s != f[i].dimension())
				throw new IllegalArgumentException();
		}
		
		mSize = s;
		
		if(mStride <= 0 || mStride > mSize)
			throw new IllegalArgumentException();

		int tx = in.x - mSize;
		int ty = in.y - mSize;
		int tz = in.z * mFilters.length;
		
		if(tx % mStride != 0 || ty % mStride != 0)
			throw new IllegalArgumentException();
		
		tx /= mStride;
		ty /= mStride;
		
		mInDim = in;
		mOutDim = new IVec3(tx + 1, ty + 1, tz);
	}
	
	private ConvolutionLayer(Filter[] f, IVec3 in, IVec3 out, int size, int stride)
	{
		mFilters = f;
		mInDim = in;
		mOutDim = out;
		mSize = size;
		mStride = stride;
	}
	
	@Override
	public Layer clone(Allocator a)
	{
		Filter[] fs = new Filter[mFilters.length];
		
		for(int i = 0 ; i < fs.length ; ++i)
		{
			fs[i] = mFilters[i].clone(a);
		}
		
		return new ConvolutionLayer(fs, mInDim.clone(), mOutDim.clone(), mSize, mStride);
	}

	@Override
	protected void doSave(JsonObject json)
	{
		JsonArray filters = new JsonArray();
		
		filters.addArray(mFilters);

		json.put("input", mInDim.save());
		json.put("output", mOutDim.save());
		json.putInt("size", mSize);
		json.putInt("stride", mStride);
		json.put("filters", filters);
	}
	
	protected static ConvolutionLayer doLoad(JsonValue json, Allocator a)
	{
		JsonObject o = (JsonObject) json;
		
		IVec3 in = IVec3.load(o.get("input"));
		IVec3 out = IVec3.load(o.get("output"));
		int size = o.getInt("size");
		int stride = o.getInt("stride");
		
		JsonArray fs = o.getArray("filters");
		Filter[] f = new Filter[fs.size()];
		
		for(int i = 0 ; i < f.length ; ++i)
		{
			f[i] = Filter.load(fs.get(i), a);
		}
		
		return new ConvolutionLayer(f, in, out, size, stride);
	}

	@Override
	public Layer.Type type()
	{
		return Layer.Type.CONV;
	}
	
	@Override
	public IVec3 dimension( )
	{
		return mOutDim;
	}
	
	@Override
	public void forward(Buffer in, Buffer out)
	{
		if(in.size() != mInDim.volume() || out.size() != mOutDim.volume())
			throw new IllegalArgumentException();
		
		float[] tmp = new float[mSize * mSize];
		int i = 0;
		
		for(int fi = 0 ; fi < mFilters.length ; ++fi)
		{
			for(int z = 0 ; z < mInDim.z ; ++z)
			{
				for(int y = 0 ; y < mInDim.y - (mSize - 1) ; y += mStride)
				{
					for(int x = 0 ; x < mInDim.x - (mSize - 1) ; x += mStride)
					{
						for(int dy = 0 ; dy < mSize ; ++dy)
						{
							for(int dx = 0 ; dx < mSize ; ++dx)
							{
								tmp[dx + dy * mSize] = in.get((x + dx) + ((y + dy) + z * mInDim.y) * mInDim.x);
							}
						}
						
						out.set(i++, mFilters[fi].forward(new ImmediateBuffer(tmp)));
					}
				}
			}
		}
	}

	@Override
	public float[] backward(Buffer result, Buffer in, Buffer out)
	{
		float[] r = new float[mFilters.length * (mSize * mSize + 1)];
		
		int i = 0;
		for(int fi = 0 ; fi < mFilters.length ; ++fi)
		{
			for(int z = 0 ; z < mInDim.z ; ++z)
			{
				for(int y = 0 ; y < mInDim.y - (mSize - 1) ; y += mStride)
				{
					for(int x = 0 ; x < mInDim.x - (mSize - 1) ; x += mStride)
					{
						float h = in.get(i++);
						float[] t = mFilters[fi].backward(h);
						
						for(int dy = 0 ; dy < mSize ; ++dy)
						{
							for(int dx = 0 ; dx < mSize ; ++dx)
							{
								int j = (x + dx) + ((y + dy) + z * mInDim.y) * mInDim.x;
								
								out.set(j, out.get(j) + t[dx + dy * mSize]);
								r[dx + dy * mSize + fi * (mSize * mSize + 1)] += h * result.get(j);
							}
						}
						
						r[mSize * mSize + fi * (mSize * mSize + 1)] += h;
					}
				}
			}
		}
		
		return r;
	}
	
	@Override
	public void adjust(Buffer w)
	{
		int l = mSize * mSize + 1;
		
		for(int fi = 0 ; fi < mFilters.length ; ++fi)
		{
			mFilters[fi].adjust(new SubBuffer(w, fi * l, (fi + 1) * l));
		}
	}
}
