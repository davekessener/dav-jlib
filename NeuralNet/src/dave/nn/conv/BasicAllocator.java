package dave.nn.conv;

import java.util.Arrays;

import dave.util.log.Logger;
import dave.util.log.Severity;

public class BasicAllocator implements WritableAllocator
{
	private final MyProvider mProv;
	private int mSize;
	
	public BasicAllocator( ) { this(10); }
	public BasicAllocator(int l)
	{
		mProv = new MyProvider();
		mProv.buffer = new float[l];
		mSize = 0;
	}
	
	private BasicAllocator(float[] b, int l)
	{
		mProv = new MyProvider();
		mProv.buffer = Arrays.copyOf(b, l);
		mSize = l;
	}
	
	public float get(int i) { return mProv.buffer[i]; }
	public void set(int i, float v) { mProv.buffer[i] = v; }
	
	public void copy(BasicAllocator a)
	{
		this.mProv.buffer = Arrays.copyOf(a.mProv.buffer, a.mSize);
		this.mSize = a.mSize;
	}
	
	@Override
	public void setContent(float[] d)
	{
		if(d.length != mSize)
		{
			Logger.DEFAULT.log(Severity.WARNING, "Allocator buffer sized %d overwritten with new buffer sized %d!", mSize, d.length);
		}
		
		mProv.buffer = d;
		mSize = d.length;
	}
	
	public int size( )
	{
		return mSize;
	}
	
	@Override
	public Buffer allocate(int l)
	{
		mSize += l;
		
		if(mSize > mProv.buffer.length)
		{
			mProv.buffer = Arrays.copyOf(mProv.buffer, Math.max(mSize, mProv.buffer.length * 2 + 1));
		}
		
		return new BasicBuffer(mProv, mSize - l, l);
	}
	
	@Override
	public BasicAllocator clone( )
	{
		return new BasicAllocator(mProv.buffer, mSize);
	}
	
	@Override
	public Buffer clone(Buffer b)
	{
		return b.clone(mProv);
	}
	
	private static class MyProvider implements Provider
	{
		public float[] buffer;
		
		@Override
		public float[] get( )
		{
			return buffer;
		}
	}
}
