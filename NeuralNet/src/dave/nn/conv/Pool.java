package dave.nn.conv;

import dave.json.JsonValue;

public abstract class Pool extends Filter
{
	private final int mSize;
	
	public Pool(int l)
	{
		mSize = l;
	}

	@Override
	public int dimension()
	{
		return mSize;
	}

	@Override
	public float[] backward(float hidden)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public void adjust(Buffer w)
	{
		throw new UnsupportedOperationException();
	}
	
	public static Pool load(JsonValue json)
	{
		return (Pool) Filter.load(json, null);
	}
}
