package dave.nn.builder;

import dave.nn.conv.Allocator;
import dave.nn.conv.Filter;
import dave.json.JsonObject;
import dave.json.JsonValue;
import dave.json.Saveable;

public abstract class FilterTemplate implements Saveable
{
	public abstract Filter generate(Allocator a);
	public abstract Type type( );
	protected abstract void doSave(JsonObject json);
	
	@Override
	public final JsonValue save( )
	{
		JsonObject json = new JsonObject();
		
		doSave(json);
		json.putString("type", type().toString());
		
		return json;
	}
	
	public static FilterTemplate load(JsonValue json)
	{
		JsonObject o = (JsonObject) json;
		
		switch(Type.valueOf(o.getString("type")))
		{
			case FORWARD:
				return FeedForwardFilterTemplate.doLoad(json);
		}
		
		throw new IllegalArgumentException();
	}
	
	public static enum Type
	{
		FORWARD
	}
}
