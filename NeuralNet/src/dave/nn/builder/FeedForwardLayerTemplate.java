package dave.nn.builder;

import dave.nn.conv.Allocator;
import dave.nn.conv.FeedForwardLayer;
import dave.nn.conv.Layer;
import dave.nn.conv.Matrix;
import dave.json.JsonObject;
import dave.json.JsonValue;
import dave.util.math.IVec3;

public class FeedForwardLayerTemplate extends LayerTemplate
{
	private final int mOut;
	
	public FeedForwardLayerTemplate(int n)
	{
		mOut = n;
	}

	@Override
	public Layer build(IVec3 in, Allocator a)
	{
		return new FeedForwardLayer(in, new Matrix(a.allocate(mOut * in.volume()), in.volume(), mOut), a.allocate(mOut));
	}

	@Override
	public Type type()
	{
		return Type.FORWARD;
	}

	@Override
	protected void doSave(JsonObject json)
	{
		json.putInt("size", mOut);
	}
	
	protected static FeedForwardLayerTemplate doLoad(JsonValue json)
	{
		JsonObject o = (JsonObject) json;
		
		int s = o.getInt("size");
		
		return new FeedForwardLayerTemplate(s);
	}
}
