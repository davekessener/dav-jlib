package dave.nn.builder;

import dave.nn.conv.Allocator;
import dave.nn.conv.BasicAllocator;
import dave.nn.conv.Layer;
import dave.nn.conv.Network;
import dave.json.JsonArray;
import dave.json.JsonObject;
import dave.json.JsonValue;
import dave.json.Saveable;
import dave.util.math.IVec3;

public class NetworkTemplate implements Saveable
{
	private final LayerTemplate[] mLayers;
	private final IVec3 mInputDim;
	
	public NetworkTemplate(IVec3 in, LayerTemplate[] l)
	{
		mLayers = l;
		mInputDim = in;
	}
	
	public IVec3 getInputDimension( ) { return mInputDim; }
	
	public int calculateSpaceRequirements( )
	{
		BasicAllocator a = new BasicAllocator();
		
		generate(a);
		
		return a.size();
	}
	
	public Network generate(Allocator a)
	{
		Layer[] l = new Layer[mLayers.length];
		IVec3 in = mInputDim;
		
		for(int i = 0 ; i < l.length ; ++i)
		{
			l[i] = mLayers[i].build(in, a);
			in = l[i].dimension();
		}
		
		return new Network(mInputDim, l);
	}

	@Override
	public JsonValue save()
	{
		JsonObject json = new JsonObject();
		JsonArray l = new JsonArray();
		
		l.addArray(mLayers);
		
		json.put("input", mInputDim.save());
		json.put("layers", l);
		
		return json;
	}
	
	public static NetworkTemplate load(JsonValue json)
	{
		JsonObject o = (JsonObject) json;
		JsonArray ls = o.getArray("layers");
		IVec3 in = IVec3.load(o.get("input"));
		LayerTemplate[] l = new LayerTemplate[ls.size()];
		
		for(int i = 0 ; i < l.length ; ++i)
		{
			l[i] = LayerTemplate.load(ls.get(i));
		}
		
		return new NetworkTemplate(in, l);
	}
}
