package dave.nn.builder;

import dave.nn.conv.Allocator;
import dave.nn.conv.Layer;
import dave.json.JsonObject;
import dave.json.JsonValue;
import dave.json.Saveable;
import dave.util.math.IVec3;

public abstract class LayerTemplate implements Saveable
{
	public abstract Layer build(IVec3 in, Allocator a);
	public abstract Type type( );
	protected abstract void doSave(JsonObject json);
	
	@Override
	public final JsonValue save( )
	{
		JsonObject json = new JsonObject();
		
		doSave(json);
		json.putString("type", type().toString());
		
		return json;
	}
	
	public static LayerTemplate load(JsonValue json)
	{
		JsonObject o = (JsonObject) json;
		
		switch(Type.valueOf(o.getString("type")))
		{
			case CONV: return ConvolutionLayerTemplate.doLoad(json);
			case FORWARD: return FeedForwardLayerTemplate.doLoad(json);
			case POOL: return PoolingLayerTemplate.doLoad(json);
			case RECT: return RectifyLayerTemplate.doLoad(json);
			case REDUCE: return ReductionLayerTemplate.doLoad(json);
		}
		
		throw new IllegalArgumentException();
	}
	
	public static enum Type
	{
		CONV,
		FORWARD,
		POOL,
		RECT,
		REDUCE
	}
}
