package dave.nn.builder;

import dave.nn.conv.Allocator;
import dave.nn.conv.FeedForwardFilter;
import dave.nn.conv.Filter;
import dave.nn.conv.Matrix;
import dave.json.JsonObject;
import dave.json.JsonValue;

public class FeedForwardFilterTemplate extends FilterTemplate
{
	private final int mSize;
	
	public FeedForwardFilterTemplate(int s)
	{
		mSize = s;
	}

	@Override
	public Filter generate(Allocator a)
	{
		return new FeedForwardFilter(a.allocate(mSize * mSize + 1));
	}

	@Override
	public Type type()
	{
		return Type.FORWARD;
	}

	@Override
	protected void doSave(JsonObject json)
	{
		json.putInt("size", mSize);
	}
	
	protected static FeedForwardFilterTemplate doLoad(JsonValue json)
	{
		JsonObject o = (JsonObject) json;
		
		int s = o.getInt("size");
		
		return new FeedForwardFilterTemplate(s);
	}
}
