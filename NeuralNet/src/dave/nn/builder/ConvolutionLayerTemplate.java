package dave.nn.builder;

import dave.nn.conv.Allocator;
import dave.nn.conv.ConvolutionLayer;
import dave.nn.conv.Filter;
import dave.nn.conv.Layer;
import dave.json.JsonArray;
import dave.json.JsonObject;
import dave.json.JsonValue;
import dave.util.math.IVec3;

public class ConvolutionLayerTemplate extends LayerTemplate
{
	private final FilterTemplate[] mFilters;
	private final int mStride;
	
	public ConvolutionLayerTemplate(int n, int w) { this(n, w, 1); }
	public ConvolutionLayerTemplate(int n, int w, int s)
	{
		mFilters = new FilterTemplate[n];
		mStride = s;
		
		for(int i = 0 ; i < n ; ++i)
		{
			mFilters[i] = new FeedForwardFilterTemplate(w);
		}
	}
	
	public ConvolutionLayerTemplate(FilterTemplate[] f) { this(f, 1); }
	public ConvolutionLayerTemplate(FilterTemplate[] f, int s)
	{
		mFilters = f;
		mStride = s;
	}

	@Override
	public Type type()
	{
		return Type.CONV;
	}

	@Override
	public Layer build(IVec3 in, Allocator a)
	{
		Filter[] f = new Filter[mFilters.length];
		
		for(int i = 0 ; i < f.length ; ++i)
		{
			f[i] = mFilters[i].generate(a);
		}
		
		return new ConvolutionLayer(in, f, mStride);
	}
	
	@Override
	public void doSave(JsonObject json)
	{
		JsonArray f = new JsonArray();
		
		f.addArray(mFilters);
		
		json.putInt("stride", mStride);
		json.put("filters", f);
	}
	
	protected static ConvolutionLayerTemplate doLoad(JsonValue json)
	{
		JsonObject o = (JsonObject) json;
		JsonArray fs = o.getArray("filters");
		int s = o.getInt("stride");
		
		FilterTemplate[] f = new FilterTemplate[fs.size()];
		
		for(int i = 0 ; i < f.length ; ++i)
		{
			f[i] = FilterTemplate.load(fs.get(i));
		}
		
		return new ConvolutionLayerTemplate(f, s);
	}
}
