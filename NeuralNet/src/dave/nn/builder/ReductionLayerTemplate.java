package dave.nn.builder;

import dave.nn.conv.Allocator;
import dave.nn.conv.Layer;
import dave.nn.conv.Matrix;
import dave.nn.conv.ReductionLayer;
import dave.json.JsonObject;
import dave.json.JsonValue;
import dave.util.math.IVec3;

public class ReductionLayerTemplate extends LayerTemplate
{
	private final int mOut;
	
	public ReductionLayerTemplate(int n)
	{
		mOut = n;
	}

	@Override
	public Layer build(IVec3 in, Allocator a)
	{
		return new ReductionLayer(in, new Matrix(a.allocate(mOut * in.z), in.z, mOut));
	}

	@Override
	public Type type()
	{
		return Type.REDUCE;
	}

	@Override
	protected void doSave(JsonObject json)
	{
		json.putInt("out", mOut);
	}
	
	protected static ReductionLayerTemplate doLoad(JsonValue json)
	{
		JsonObject o = (JsonObject) json;
		
		int out = o.getInt("out");
		
		return new ReductionLayerTemplate(out);
	}
}
