package dave.nn.builder;

import dave.nn.conv.Allocator;
import dave.nn.conv.Layer;
import dave.nn.conv.Rectifier;
import dave.nn.conv.RectifyLayer;
import dave.json.JsonObject;
import dave.json.JsonValue;
import dave.util.math.IVec3;

public class RectifyLayerTemplate extends LayerTemplate
{
	private final Rectifier mF;
	
	public RectifyLayerTemplate(Rectifier f)
	{
		mF = f;
	}

	@Override
	public Layer build(IVec3 in, Allocator a)
	{
		return new RectifyLayer(in, mF);
	}

	@Override
	public Type type()
	{
		return Type.RECT;
	}

	@Override
	protected void doSave(JsonObject json)
	{
		json.putString("fun", mF.type().toString());
	}
	
	protected static RectifyLayerTemplate doLoad(JsonValue json)
	{
		JsonObject o = (JsonObject) json;
		
		Rectifier f = Rectifier.generate(Rectifier.Type.valueOf(o.getString("fun")));
		
		return new RectifyLayerTemplate(f);
	}
}
