package dave.nn.builder;

import dave.nn.conv.Allocator;
import dave.nn.conv.Pool;
import dave.nn.conv.PoolingLayer;
import dave.json.JsonObject;
import dave.json.JsonValue;
import dave.util.math.IVec3;

public class PoolingLayerTemplate extends LayerTemplate
{
	private final Pool mF;
	
	public PoolingLayerTemplate(Pool f)
	{
		mF = f;
	}

	@Override
	public PoolingLayer build(IVec3 in, Allocator a)
	{
		return new PoolingLayer(in ,mF);
	}

	@Override
	public Type type()
	{
		return Type.POOL;
	}

	@Override
	protected void doSave(JsonObject json)
	{
		json.put("fun", mF.save());
	}
	
	protected static PoolingLayerTemplate doLoad(JsonValue json)
	{
		JsonObject o = (JsonObject) json;
		
		Pool p = Pool.load(o.get("fun"));
		
		return new PoolingLayerTemplate(p);
	}
}
