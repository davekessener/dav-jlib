package dave.net.server;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import dave.json.JsonValue;

public interface Transceiver
{
	public abstract void send(OutputStream out, JsonValue json) throws IOException;
	public abstract JsonValue receive(InputStream in) throws IOException;
}
