package dave.net.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import javax.net.ssl.SSLSocket;

public class SSLServer extends TCPServer
{
	public SSLServer(TypedSocketProxy<ServerSocket> p, Transceiver com, Handler<Connection> f) throws IOException
	{
		super(p, com, f);
	}

	@Override
	protected Connection create(Socket s, Transceiver f) throws IOException
	{
		SSLSocket ssl = (SSLSocket) s;
		
		ssl.setNeedClientAuth(true);
		ssl.startHandshake();
		
		return super.create(ssl, f);
	}
}
