package dave.net.server;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;

import dave.json.JsonValue;
import dave.json.SevereIOException;
import dave.json.StreamBuffer;
import dave.json.StreamPrinter;
import dave.util.Utils;

public class StreamingTransceiver implements Transceiver
{
	private final Charset mCharset;
	
	public StreamingTransceiver( ) { this(Utils.CHARSET); }
	public StreamingTransceiver(Charset c)
	{
		mCharset = c;
	}
	
	@Override
	public void send(OutputStream out, JsonValue json) throws IOException
	{
		json.write(new StreamPrinter(out, mCharset));
	}

	@Override
	public JsonValue receive(InputStream in) throws IOException
	{
		try
		{
			return JsonValue.read(new StreamBuffer(in));
		}
		catch(SevereIOException e)
		{
			throw (IOException) e.getCause();
		}
	}
}
