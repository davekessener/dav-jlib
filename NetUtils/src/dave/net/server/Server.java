package dave.net.server;

import java.io.IOException;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.MulticastSocket;
import java.net.Socket;
import java.net.SocketAddress;

import dave.json.JsonValue;
import dave.util.Actor;

public interface Server extends Actor
{
	public abstract SocketAddress getAddress( );
	public abstract Socket getSocket( );
	
	public static interface Handler<T>
	{
		public abstract void handle(T obj) throws IOException;
	}
	
	public static class Datagram
	{
		public final InetSocketAddress source;
		public final JsonValue payload;
		
		public Datagram(InetSocketAddress source, JsonValue payload)
		{
			this.source = source;
			this.payload = payload;
		}
	}

	public static TCPServer createTCPServer(int port, Handler<Connection> f) throws IOException
		{ return createTCPServer(port, new SimpleTransceiver(), f); }
	public static TCPServer createTCPServer(int port, Transceiver t, Handler<Connection> f) throws IOException
		{ return createTCPServer(new InetSocketAddress(DEFAULT_HOST, port), t, f); }
	public static TCPServer createTCPServer(String host, Handler<Connection> f) throws IOException
		{ return createTCPServer(host, new SimpleTransceiver(), f); }
	public static TCPServer createTCPServer(String host, Transceiver t, Handler<Connection> f) throws IOException
	{ return createTCPServer(new InetSocketAddress(host, 0), t, f); }
	public static TCPServer createTCPServer(InetSocketAddress a, Handler<Connection> f) throws IOException
		{ return createTCPServer(a, new SimpleTransceiver(), f); }
	public static TCPServer createTCPServer(InetSocketAddress a, Transceiver t, Handler<Connection> f) throws IOException
		{ return new TCPServer(new TCPSocketProxy(a), t, f); }
	
	public static DatagramServer<DatagramSocket> createUDPServer(int port, Handler<Datagram> f) throws IOException { return createUDPServer(port, new SimpleTransceiver(), f); }
	public static DatagramServer<DatagramSocket> createUDPServer(int port, Transceiver t, Handler<Datagram> f) throws IOException
	{
		return new DatagramServer<>(new UDPSocketProxy(new InetSocketAddress(port)), t, f);
	}
	
	public static DatagramServer<MulticastSocket> createMulticastServer(InetAddress group, InetSocketAddress a, Handler<Datagram> f) throws IOException { return createMulticastServer(group, a, new SimpleTransceiver(), f); }
	public static DatagramServer<MulticastSocket> createMulticastServer(InetAddress group, InetSocketAddress a, Transceiver t, Handler<Datagram> f) throws IOException
	{
		return new DatagramServer<>(new MulticastSocketProxy(group, a),t, f);
	}
	
	public static final String DEFAULT_HOST = "0.0.0.0";
}
