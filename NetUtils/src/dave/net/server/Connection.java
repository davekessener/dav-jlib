package dave.net.server;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;

import dave.json.JsonValue;

public interface Connection
{
	public abstract void send(JsonValue json) throws IOException;
	public abstract JsonValue receive( ) throws IOException;
	public abstract void close( ) throws IOException;
	
	public default Socket socket( ) { throw new UnsupportedOperationException(); }
	public default InetSocketAddress[] participants( ) { throw new UnsupportedOperationException(); }
}
