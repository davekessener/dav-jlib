package dave.net.server;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import dave.json.JsonValue;
import dave.net.NetUtils;

public class SimpleTransceiver implements Transceiver
{
	@Override
	public void send(OutputStream out, JsonValue json) throws IOException
	{
		byte[] data = NetUtils.serialize(json);
		
		NetUtils.send(data, out, data.length > 512);
	}

	@Override
	public JsonValue receive(InputStream in) throws IOException
	{
		return NetUtils.deserialize(NetUtils.recv(in));
	}
}
