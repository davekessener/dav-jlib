package dave.net.server;

import java.io.Closeable;
import java.io.IOException;

import dave.json.SevereIOException;

public abstract class TypedSocketProxy<S extends Closeable> implements SocketProxy
{
	private final S mSocket;
	private final String mType;
	
	public TypedSocketProxy(String t, S s)
	{
		mSocket = s;
		mType = t;
	}

	@Override
	public S getSocket( )
	{
		return mSocket;
	}
	
	@Override
	public String getSocketType( )
	{
		return mType;
	}
	
	@Override
	public void close( )
	{
		try
		{
			mSocket.close();
		}
		catch(IOException e)
		{
			throw new SevereIOException(e);
		}
	}
}
