package dave.net.server;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.MulticastSocket;
import java.net.SocketAddress;

import dave.json.SevereIOException;

public class MulticastSocketProxy extends TypedSocketProxy<MulticastSocket>
{
	private final InetAddress mGroup;
	private final InetAddress mGateway;
	
	public MulticastSocketProxy(InetAddress g, InetSocketAddress a) throws IOException
	{
		super("IPv4-MC", new MulticastSocket(a.getPort()));
		
		mGroup = g;
		mGateway = a.getAddress();
		
		getSocket().setInterface(mGateway);
		getSocket().joinGroup(mGroup);
		getSocket().setLoopbackMode(true);
	}
	
	public InetAddress getGroup( ) { return mGroup; }

	@Override
	public SocketAddress getSocketAddress( )
	{
		return new InetSocketAddress(mGateway, getSocket().getLocalPort());
	}
	
	@Override
	public SocketAddress getDefaultRecipient( )
	{
		return new InetSocketAddress(mGroup, getSocket().getLocalPort());
	}
	
	@Override
	public void close( )
	{
		try
		{
			getSocket().leaveGroup(mGroup);
		}
		catch(IOException e)
		{
			throw new SevereIOException(e);
		}
		
		super.close();
	}
}
