package dave.net.server;

import java.io.IOException;
import java.net.SocketAddress;

import dave.json.JsonValue;

public interface PacketedServer extends Server
{
	public abstract void send(SocketAddress a, JsonValue packet) throws IOException;
}
