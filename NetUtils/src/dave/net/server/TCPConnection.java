package dave.net.server;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;

import dave.json.JsonValue;
import dave.net.NetUtils;
import dave.util.log.Logger;

public class TCPConnection implements Connection
{
	private final Socket mSock;
	private final Transceiver mCom;
	private final String mRemote;
	private final InetSocketAddress[] mParticipants;
	
	public TCPConnection(Socket s, Transceiver com)
	{
		if(s == null)
			throw new NullPointerException();

		mSock = s;
		mCom = com;

		InetSocketAddress a = (InetSocketAddress) s.getLocalSocketAddress();
		InetSocketAddress b = (InetSocketAddress) mSock.getRemoteSocketAddress();
		
		mRemote = "[" + b.getHostString() + "]:" + b.getPort();
		
		NetUtils.LOG.log("Established TCP connection with %s", mRemote);
		
		mParticipants = new InetSocketAddress[] { a, b };
	}

	@Override
	public void send(JsonValue json) throws IOException
	{
		LOG.log("Sending to %s | %s", mRemote, json);
		
		mCom.send(mSock.getOutputStream(), json);
	}

	@Override
	public JsonValue receive() throws IOException
	{
		JsonValue v = mCom.receive(mSock.getInputStream());
		
		LOG.log("Received from %s | %s", mRemote, v);
		
		return v;
	}

	@Override
	public void close() throws IOException
	{
		NetUtils.LOG.log("Closing TCP connection to %s", mRemote);
		
		mSock.close();
	}
	
	@Override
	public Socket socket()
	{
		return mSock;
	}
	
	@Override
	public InetSocketAddress[] participants()
	{
		return mParticipants;
	}
	
	private static final Logger LOG = Logger.get("tcp");
}
