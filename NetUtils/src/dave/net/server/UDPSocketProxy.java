package dave.net.server;

import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.net.SocketException;

public class UDPSocketProxy extends TypedSocketProxy<DatagramSocket>
{
	public UDPSocketProxy(InetSocketAddress a) throws SocketException
	{
		super("UDP", new DatagramSocket());
		
		getSocket().bind(a);
	}

	@Override
	public SocketAddress getSocketAddress( )
	{
		return getSocket().getLocalSocketAddress();
	}
}
