package dave.net.server;

import java.io.IOException;

import dave.json.JSON;

public class TypedConnection<T>
{
	private final Connection mCon;
	
	public TypedConnection(Connection c)
	{
		mCon = c;
	}
	
	public void send(T o) throws IOException
	{
		mCon.send(JSON.serialize(o));
	}
	
	@SuppressWarnings("unchecked")
	public T receive( ) throws IOException
	{
		return (T) JSON.deserialize(mCon.receive());
	}
	
	public void close( ) throws IOException
	{
		mCon.close();
	}
}
