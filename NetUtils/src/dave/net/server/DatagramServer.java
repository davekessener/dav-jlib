package dave.net.server;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.SocketAddress;

import dave.json.JsonValue;
import dave.util.log.Logger;

public class DatagramServer<S extends DatagramSocket> extends SocketServer<TypedSocketProxy<S>> implements PacketedServer
{
	private final Handler<Datagram> mCallback;
	private final Transceiver mCom;
	private final byte[] mBuf;
	
	public DatagramServer(TypedSocketProxy<S> s, Transceiver com, Handler<Datagram> f)
	{
		super(s);
		
		mCallback = f;
		mCom = com;
		mBuf = new byte[64 * 1024];
	}
	
	@Override
	public void send(SocketAddress a, JsonValue p) throws IOException
	{
		ByteArrayOutputStream buf = new ByteArrayOutputStream();
		
		mCom.send(buf, p);
		
		byte[] data = buf.toByteArray();
		DatagramPacket datagram = new DatagramPacket(data, data.length);
		InetSocketAddress recipient = (InetSocketAddress) (a != null ? a : proxy().getDefaultRecipient());
		
		datagram.setSocketAddress(recipient);
		
		LOG.log("Sending datagram (%dB) to %s | %s", data.length, recipient, p);
		
		proxy().getSocket().send(datagram);
	}

	@Override
	protected void tick() throws IOException
	{
		DatagramPacket p = new DatagramPacket(mBuf, mBuf.length);
		
		proxy().getSocket().receive(p);
		
		InetSocketAddress source = (InetSocketAddress) p.getSocketAddress();
		JsonValue payload = mCom.receive(new ByteArrayInputStream(p.getData(), 0, p.getLength()));
		
		LOG.log("Received datagram (%dB) from %s | %s", p.getLength(), source, payload);
		
		try
		{
			mCallback.handle(new Datagram(source, payload));
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}
	
	private static final Logger LOG = Logger.get("dg-svr");
}
