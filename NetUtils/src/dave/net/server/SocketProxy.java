package dave.net.server;

import java.io.Closeable;
import java.net.SocketAddress;

public interface SocketProxy extends Closeable
{
	public abstract SocketAddress getSocketAddress( );
	public abstract Object getSocket( );
	public abstract String getSocketType( );
	public abstract void close( );
	
	public default SocketAddress getDefaultRecipient( ) { return getSocketAddress(); }
}
