package dave.net.server;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketException;

import dave.net.NetUtils;
import dave.util.log.Severity;

public abstract class SocketServer<S extends SocketProxy> implements Server
{
	private final S mSock;
	private final String mAddr;
	private final Thread mThread;
	private boolean mRunning;
	
	protected SocketServer(S s)
	{
		mSock = s;
		
		InetSocketAddress a = (InetSocketAddress) mSock.getSocketAddress();
		
		mAddr = a.getHostString() + ":" + a.getPort();
		mThread = new Thread(() -> run());
		mRunning = true;
	}
	
	protected abstract void tick( ) throws IOException;
	protected S proxy( ) { return mSock; }
	
	@Override
	public Socket getSocket( )
	{
		return (Socket) mSock.getSocket();
	}
	
	@Override
	public SocketAddress getAddress( )
	{
		return mSock.getSocketAddress();
	}

	@Override
	public void start( )
	{
		NetUtils.LOG.log(Severity.INFO, "Starting %s server %s", mSock.getSocketType(), mAddr);
		
		mThread.start();
	}

	private void run( )
	{
		try
		{
			while(mRunning)
			{
				tick();
			}
		}
		catch(IOException e)
		{
			if(mRunning || !(e instanceof SocketException))
			{
				e.printStackTrace();
			}
		}
	}

	@Override
	public void stop()
	{
		NetUtils.LOG.log(Severity.INFO, "Stopping %s server %s", mSock.getSocketType(), mAddr);
		
		mRunning = false;
		
		mSock.close();
		
		try { mThread.join(); }
			catch(InterruptedException e)
				{ throw new RuntimeException(e); }
	}
}
