package dave.net.server;

import java.net.ServerSocket;
import java.net.SocketAddress;

import javax.net.ssl.SSLServerSocket;

public class SSLSocketProxy extends TypedSocketProxy<ServerSocket>
{
	public SSLSocketProxy(SSLServerSocket s)
	{
		super("SSL", s);
	}

	@Override
	public SocketAddress getSocketAddress( )
	{
		return getSocket().getLocalSocketAddress();
	}
}
