package dave.net.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class TCPServer extends SocketServer<TypedSocketProxy<ServerSocket>>
{
	private final Handler<Connection> mCallback;
	private final Transceiver mCom;
	
	public TCPServer(TypedSocketProxy<ServerSocket> p, Transceiver com, Handler<Connection> f) throws IOException
	{
		super(p);
		
		mCallback = f;
		mCom = com;
	}
	
	@Override
	protected void tick( ) throws IOException
	{
		Socket client = proxy().getSocket().accept();
		
		try
		{
			mCallback.handle(create(client, mCom));
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}
	
	protected Connection create(Socket s, Transceiver f) throws IOException
	{
		return new TCPConnection(s, f);
	}
}
