package dave.net.server;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.SocketAddress;

public class TCPSocketProxy extends TypedSocketProxy<ServerSocket>
{
	public TCPSocketProxy(InetSocketAddress a) throws IOException
	{
		super("TCP", new ServerSocket());
		
		getSocket().bind(a);
	}

	@Override
	public SocketAddress getSocketAddress( )
	{
		return getSocket().getLocalSocketAddress();
	}
}
