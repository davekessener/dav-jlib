package dave.net;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.NetworkInterface;
import java.net.SocketAddress;
import java.util.Enumeration;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import dave.json.BasicPrinter;
import dave.json.JsonValue;
import dave.json.Printer;
import dave.json.StringBuffer;
import dave.net.server.PacketedServer;
import dave.net.server.Server;
import dave.util.Counter;
import dave.util.log.Logger;
import dave.util.log.Severity;
import dave.util.Utils;

public final class NetUtils
{
	public static InetSocketAddress addressOf(String host, int port)
	{
		return Utils.wrap(() -> new InetSocketAddress(InetAddress.getByName(host), port));
	}
	
	public static InetAddress getAnyAddress(NetworkInterface nic) throws IOException
	{
		Enumeration<InetAddress> a = nic.getInetAddresses();
		
		while(a.hasMoreElements())
		{
			InetAddress r = a.nextElement();
			
			if(r.isLoopbackAddress() || r.isLinkLocalAddress() || !(r instanceof Inet4Address))
				continue;
			
			return r;
		}
		
		throw new IOException();
	}

	public static InetAddress getMulticastAddress(String nif) throws NumberFormatException, IOException
	{
		NetworkInterface nic = Utils.stream(NetworkInterface.getNetworkInterfaces())
				.filter(i -> nif.equals(i.getName()))
				.findFirst().get();
		
		Logger.DEFAULT.log(Severity.INFO, "Selected NIC %s (%s) for multicast.", nic.getName(), nic.getDisplayName());
		
		return NetUtils.getAnyAddress(nic);
	}

	private static final ExecutorService ASYNC = Executors.newCachedThreadPool();
	public static final Logger LOG = Logger.get("network");
	
	public static Future<Integer> queryNIC(NetworkInterface nic, InetAddress group, int port, JsonValue packet)
	{
		return ASYNC.submit(() -> {
			final Counter count = new Counter();
			final InetAddress a = getAnyAddress(nic);
			final SocketAddress target = new InetSocketAddress(group, port);
			final PacketedServer s = Server.createMulticastServer(group, new InetSocketAddress(a, 0), p -> {
				LOG.log(Severity.INFO, "on NIC %s received response from %s", nic.getName(), p.source);
				count.inc();
			});
			
			s.start();
			s.send(target, packet);
			
			sleep(2000);
			
			s.stop();
			
			return count.get();
		});
	}

	public static void sleep(int ms)
	{
		long ts = System.currentTimeMillis();
		long c = ts;
		
		while((c - ts) < ms) try
		{
			Thread.sleep(ms - (c - ts));
			
			return;
		}
		catch(InterruptedException e)
		{
			c = System.currentTimeMillis();
		}
	}

	public static byte[] serialize(JsonValue json)
	{
		Printer p = new BasicPrinter();
		
		json.write(p);
		
		return p.toString().getBytes(Utils.CHARSET);
	}
	
	public static JsonValue deserialize(byte[] json) { return deserialize(json, 0, json.length); }
	public static JsonValue deserialize(byte[] json, int o, int l)
	{
		return JsonValue.read(new StringBuffer(new String(json, o, l, Utils.CHARSET)));
	}
	
	public static void send(byte[] data, OutputStream out, boolean zip) throws IOException
	{
		out.write(zip ? MAGIC_ZIP : MAGIC_FLAT);
		
		int l = data.length;
		
		out.write((l >> 24) & 0xFF);
		out.write((l >> 16) & 0xFF);
		out.write((l >>  8) & 0xFF);
		out.write( l        & 0xFF);
		
		if(zip)
		{
			out = new GZIPOutputStream(out);
		}
		
		out.write(data);
		out.flush();
		
		if(zip)
		{
			((GZIPOutputStream) out).finish();
		}
	}
	
	public static byte[] recv(InputStream in) throws IOException
	{
		final int type = in.read();
		int l = 0;
		
		for(int i = 0 ; i < 4 ; ++i)
		{
			int t = in.read();
			
			if(t < 0 || t >= 0x100)
				throw new IOException();
			
			l = (l << 8) | t;
		}
		
		switch(type)
		{
		case MAGIC_ZIP:
			in = new GZIPInputStream(in);
			break;
			
		case MAGIC_FLAT:
			break;
			
		default:
			throw new IOException(String.format("Unexpected type %02x in buffer sized %d!", type, l));
		}
		
		byte[] data = new byte[l];
		
		for(int t = 0 ; t < l ;)
		{
			int r = in.read(data, t, l - t);
			
			if(r < 0)
				throw new IOException();
			
			t += r;
		}
		
		return data;
	}
		
	public static final byte MAGIC_ZIP = 0x55, MAGIC_FLAT = 0x7F;
	
	private NetUtils( ) { }
}
