package dave.email;

import javax.mail.MessagingException;

public class EmailSendError extends RuntimeException
{
	private static final long serialVersionUID = -8427902920441833746L;

	public final Email email;
	
	public EmailSendError(Email email, MessagingException e)
	{
		super(e);
		
		this.email = email;
	}
}
