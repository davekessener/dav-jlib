package dave.email;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class Email
{
	private String mSubject;
	private final List<String> mRecipients;
	private String mText;
	
	public Email()
	{
		mSubject = null;
		mRecipients = new ArrayList<>();
		mText = null;
	}
	
	public void subject(String s) { mSubject = s; }
	public void text(String t) { mText = t; }
	
	public void addRecipient(String a) { mRecipients.add(a); }
	public void removeRecipient(String a) { mRecipients.remove(a); }
	
	public String subject()
	{
		return "" + mSubject;
	}
	
	public Stream<String> recipients()
	{
		return mRecipients.stream();
	}
	
	public String text()
	{
		return "" + mText;
	}
}
