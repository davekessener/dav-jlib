package dave.email;

import java.util.Properties;
import java.util.stream.Collectors;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

// https://www.google.com/settings/security/lesssecureapps

public class EmailAccount
{
	private final EmailConfiguration mConfig;
	private final Properties mProps;
	
	public EmailAccount(EmailConfiguration cfg)
	{
		mConfig = cfg;
		
		mProps = new Properties();

		mProps.put("mail.smtp.timeout", "5000");
		mProps.put("mail.smtp.connectiontimeout", "5000");
		mProps.put("mail.smtp.starttls.enable", "true");
		mProps.put("mail.smtp.auth", "true");
		mProps.put("mail.smtp.host", mConfig.smtp_address);
		mProps.put("mail.smtp.port", "" + mConfig.smtp_port);
	}
	
	public void send(Email email)
	{
		Session s = Session.getInstance(mProps, new Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication()
			{
				return new PasswordAuthentication(mConfig.username, mConfig.password);
			}
		});
		
		try
		{
			Message msg = new MimeMessage(s);
			
			msg.setFrom(new InternetAddress(mConfig.username));
			msg.setRecipients(RecipientType.TO, InternetAddress.parse(email.recipients().collect(Collectors.joining(";"))));
			msg.setSubject(email.subject());
			msg.setText(email.text());
			
			Transport.send(msg);
		}
		catch(MessagingException e)
		{
			throw new EmailSendError(email, e);
		}
	}
}
