package dave.email;

import dave.json.Container;
import dave.json.JsonObject;
import dave.json.JsonValue;
import dave.json.Loader;
import dave.json.Saveable;
import dave.json.Saver;

@Container
public class EmailConfiguration implements Saveable
{
	public final String smtp_address;
	public final int smtp_port;
	public final String username;
	public final String password;
	
	public EmailConfiguration(String smtp_address, int smtp_port, String username, String password)
	{
		this.smtp_address = smtp_address;
		this.smtp_port = smtp_port;
		this.username = username;
		this.password = password;
	}
	
	private EmailConfiguration(JsonObject json)
	{
		JsonObject smtp = json.getObject("smtp");
		JsonObject auth = json.getObject("credentials");
		
		this.smtp_address = smtp.getString("address");
		this.smtp_port = smtp.getInt("port");
		this.username = auth.getString("username");
		this.password = auth.getString("password");
	}
	
	@Saver
	@Override
	public JsonValue save()
	{
		JsonObject json = new JsonObject();
		JsonObject smtp = new JsonObject();
		JsonObject auth = new JsonObject();
		
		smtp.putString("address", smtp_address);
		smtp.putInt("port", smtp_port);
		
		auth.putString("username", username);
		auth.putString("password", password);
		
		json.put("smtp", smtp);
		json.put("credentials", auth);
		
		return json;
	}
	
	@Loader
	public static EmailConfiguration load(JsonValue json)
	{
		return new EmailConfiguration((JsonObject) json);
	}
}
