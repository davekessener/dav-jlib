package dave.util.screengrab.ut;

import static org.junit.Assert.assertTrue;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.imageio.ImageIO;

import org.junit.Test;

import dave.util.Utils;
import dave.util.screengrab.ScreenGrabUtils;
import dave.util.screengrab.ScreenGrabber;

public class ScreengrabTest
{
	@Test
	public void testScreengrab() throws IOException
	{
		List<BufferedImage> frames = new LinkedList<>();
		ScreenGrabber sg = new ScreenGrabber(ScreenGrabUtils.getWindowRectByPartialTitle("Minecraft"), 60, frames::add);
		
		sg.start();
		
		Utils.sleep(2000);
		
		sg.stop();
		
		assertTrue(frames.size() >= 100 && frames.size() <= 130);
		
		for(int i = 0 ; i < frames.size() ; ++i)
		{
			ImageIO.write(frames.get(i), "PNG", new File(String.format("frame_%03d.png", i)));
		}
	}
}
