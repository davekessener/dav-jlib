package dave.util.screengrab;

import java.awt.AWTException;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.image.BufferedImage;
import java.util.function.Consumer;

import dave.util.Actor;
import dave.util.SevereException;
import dave.util.Sync;
import dave.util.Utils;
import dave.util.math.IRect;

public class ScreenGrabber implements Actor
{
	private final Robot mRobot;
	private final Rectangle mPart;
	private final Thread mThread;
	private final Consumer<BufferedImage> mCallback;
	private final Sync mSync;
	private boolean mRunning;
	
	public ScreenGrabber(IRect r, double fps, Consumer<BufferedImage> cb)
	{
		try
		{
			mRobot = new Robot();
			mPart = r.toRectangle();
			mThread = new Thread(this::run);
			mCallback = cb;
			mSync = Sync.build((long) (1000 / fps));
			mRunning = false;
		}
		catch(AWTException e)
		{
			throw new SevereException(e);
		}
	}
	
	@Override
	public void start()
	{
		mRunning = true;
		mThread.start();
	}
	
	@Override
	public void stop()
	{
		mRunning = false;
		
		try
		{
			mThread.join(100);
		}
		catch(InterruptedException e)
		{
			mThread.interrupt();
			Utils.run(() -> mThread.join(100));
		}
	}
	
	private void run()
	{
		while(mRunning)
		{
			mCallback.accept(mRobot.createScreenCapture(mPart));
			
			mSync.sync();
		}
	}
}
