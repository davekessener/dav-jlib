package dave.util.screengrab;

import java.util.function.Predicate;
import java.util.regex.Pattern;

import com.sun.jna.platform.DesktopWindow;
import com.sun.jna.platform.WindowUtils;

import dave.util.math.IRect;

public final class ScreenGrabUtils
{
	public static IRect getWindowRectByTitleRegex(String regex)
	{
		Pattern ptrn = Pattern.compile(regex);
		
		return getWindowRect(w -> ptrn.matcher(w.getTitle()).find());
	}
	
	public static IRect getWindowRectByPartialTitle(String id)
	{
		return getWindowRect(w -> w.getTitle().contains(id));
	}
	
	public static IRect getWindowRect(Predicate<DesktopWindow> f)
	{
		DesktopWindow window = WindowUtils.getAllWindows(false).stream()
			.filter(f)
			.findFirst().get();
		
		return IRect.fromRectangle(window.getLocAndSize());
	}
	
	private ScreenGrabUtils( ) { }
}
